//
// Copyright 2020 Axual B.V. 2020
//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Axual.Kafka.Proxy.ValueHeader
{
    internal class ValueHeader<TValue> : ISerializable
    {
        internal ValueHeader(TValue value, AxualMessageId axualMessageId,
            AxualTime serializationTime, AxualCopyFlag axualCopyFlag)
        {
            Value = value;
            AxualMessageId = axualMessageId;
            AxualCopyFlag = axualCopyFlag;
            SerializationTime = serializationTime;
        }

        public ValueHeader(TValue value)
        {
            Value = value;
            AxualMessageId = new AxualMessageId();
            AxualCopyFlag = AxualCopyFlag.OriginalMessage;
        }

        internal TValue Value { get; }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            if (info == null)
                throw new ArgumentNullException(nameof(info));
            info.AddValue("AltName", "XXX");
            info.AddValue("AltID", 9999);
        }

        public static ValueHeader<TValue> EmptyWithValue(TValue value)
        {
            return new ValueHeader<TValue>(value, AxualMessageId.Empty, AxualTime.Zero, AxualCopyFlag.OriginalMessage);
        }

        public override string ToString()
        {
            return
                $"Message ID: {AxualMessageId}, Value: {Value}, Serialization Time: {SerializationTime}, Distribution Level Flag: {AxualCopyFlag}";
        }

        public override bool Equals(object obj)
        {
            //Check for null and compare run-time types.
            if (obj == null || GetType() != obj.GetType()) return false;

            var other = (ValueHeader<TValue>) obj;

            return Value.Equals(other.Value) &&
                   AxualMessageId.Equals(other.AxualMessageId) &&
                   SerializationTime.Equals(other.SerializationTime) &&
                   AxualCopyFlag.Equals(other.AxualCopyFlag);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = EqualityComparer<TValue>.Default.GetHashCode(Value);
                hashCode = (hashCode * 397) ^ AxualCopyFlag.GetHashCode();
                hashCode = (hashCode * 397) ^ AxualMessageId.GetHashCode();
                hashCode = (hashCode * 397) ^ SerializationTime.GetHashCode();
                return hashCode;
            }
        }

        #region axualHeaders

        public AxualCopyFlag AxualCopyFlag { get; }

        public AxualMessageId AxualMessageId { get; }

        /// <summary>
        ///     Bitwise flag used for the Axual distribution
        /// </summary>
        public AxualTime SerializationTime { get; }

        #endregion
    }
}