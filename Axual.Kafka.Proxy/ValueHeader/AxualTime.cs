//
// Copyright 2020 Axual B.V. 2020
//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

using System;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("Axual.Kafka.Tests")]

namespace Axual.Kafka.Proxy.ValueHeader
{
    /// <summary>
    ///     Axual Time represents the difference, measured in milliseconds, between the current time and midnight,
    ///     January 1, 1970 UTC.
    /// </summary>
    internal class AxualTime : IComparable
    {
        internal const int Size = sizeof(double);

        private static readonly DateTime Jan1St1970 = new DateTime
            (1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

        private readonly double _time;

        private AxualTime(double time)
        {
            _time = time;
        }

        public static AxualTime Now => new AxualTime(ConvertToUnixTimestamp(DateTime.Now).TotalMilliseconds);
        public static AxualTime Zero => new AxualTime(ConvertToUnixTimestamp(Jan1St1970).TotalMilliseconds);

        public int CompareTo(object obj)
        {
            if (obj == null)
                return 1;

            if (!(obj is AxualTime otherTime))
                throw new ArgumentException("object must be AxualTime type.");
            return _time.CompareTo(otherTime._time);
        }

        private static DateTime ConvertFromUnixTimestamp(double milliseconds)
        {
            return Jan1St1970.AddMilliseconds(milliseconds);
        }

        private static TimeSpan ConvertToUnixTimestamp(DateTime date)
        {
            return date.ToUniversalTime() - Jan1St1970;
        }

        public override string ToString()
        {
            return ConvertFromUnixTimestamp(_time).ToString("dd/MM/yyyy HH:mm:ss.fff");
        }

        public static bool operator <(AxualTime a, AxualTime b)
        {
            return a.CompareTo(b) < 0;
        }

        public static bool operator >(AxualTime a, AxualTime b)
        {
            return a.CompareTo(b) > 0;
        }

        public override bool Equals(object obj)
        {
            return CompareTo(obj) == 0;
        }

        public override int GetHashCode()
        {
            return _time.GetHashCode();
        }

        public byte[] Serialize()
        {
            return Utils.BitsUtils.BigEndian.GetValueBytesBigEndian(_time);
        }

        public static AxualTime Deserialize(byte[] data)
        {
            return new AxualTime(Utils.BitsUtils.BigEndian.ToDoubleFromBigEndian(data, 0));
        }
    }
}