//
// Copyright 2020 Axual B.V. 2020
//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("Axual.Kafka.Tests")]

namespace Axual.Kafka.Proxy.ValueHeader
{
    internal static class Constants
    {
        internal static class AxualMessage
        {
            /// <summary>
            ///     Magic bytes that should never change. Magic bytes = fixed hex 0x0BEB
            /// </summary>
            internal const ushort AxualMagicUInt16Value = 0x0BEB;

            private const string MessageIdHeader = "Axual-Message-Id";
            private const string ProducerIdHeader = "Axual-Producer-Id";
            private const string ProducerVersionHeader = "Axual-Producer-Version";
            private const string IntermediateIdHeader = "Axual-Intermediate-Id";
            private const string IntermediateVersionHeader = "Axual-Intermediate-Version";
            private const string SerializationTimeHeader = "Axual-Serialization-Time";
            private const string DeserializationTimeHeader = "Axual-Deserialization-Time";
            private const string CopyFlagsHeader = "Axual-Copy-Flags";
            private const string SystemHeader = "Axual-System";
            private const string InstanceHeader = "Axual-Instance";
            private const string ClusterHeader = "Axual-Cluster";
            private const string TenantHeader = "Axual-Tenant";
            private const string EnvironmentHeader = "Axual-Environment";

            internal static bool IsValueHeaderMagicByte(ushort messageMagicByte)
            {
                return messageMagicByte == AxualMagicUInt16Value;
            }

            private enum AxualHeaderKeys
            {
                MessageIdHeader,
                ProducerIdHeader,
                ProducerVersionHeader,
                IntermediateIdHeader,
                IntermediateVersionHeader,
                SerializationTimeHeader,
                DeserializationTimeHeader,
                CopyFlagsHeader,
                SystemHeader,
                InstanceHeader,
                ClusterHeader,
                TenantHeader,
                EnvironmentHeader
            }

            internal enum EnumConsumerHeadersKey
            {
                // The ID of the distributor / forwarder 
                [Description(IntermediateIdHeader)] IntermediateId = AxualHeaderKeys.IntermediateIdHeader,

                [Description(IntermediateVersionHeader)]
                IntermediateVersion = AxualHeaderKeys.IntermediateVersionHeader,

                [Description(DeserializationTimeHeader)]
                DeserializationTime = AxualHeaderKeys.DeserializationTimeHeader,
                [Description(SystemHeader)] System = AxualHeaderKeys.SystemHeader,
                [Description(InstanceHeader)] Instance = AxualHeaderKeys.InstanceHeader,
                [Description(ClusterHeader)] Cluster = AxualHeaderKeys.ClusterHeader,
                [Description(TenantHeader)] Tenant = AxualHeaderKeys.TenantHeader,
                [Description(EnvironmentHeader)] Environment = AxualHeaderKeys.EnvironmentHeader
            }

            internal enum EnumProducerHeadersKey
            {
                [Description(MessageIdHeader)] MessageId = AxualHeaderKeys.MessageIdHeader,

                // producer of the original message
                [Description(ProducerIdHeader)] ProducerId = AxualHeaderKeys.ProducerIdHeader,
                [Description(ProducerVersionHeader)] ProducerVersion = AxualHeaderKeys.ProducerVersionHeader,

                // The ID of the distributor / forwarder 
                [Description(IntermediateIdHeader)] IntermediateId = AxualHeaderKeys.IntermediateIdHeader,

                [Description(IntermediateVersionHeader)]
                IntermediateVersion = AxualHeaderKeys.IntermediateVersionHeader,
                [Description(SerializationTimeHeader)] SerializationTime = AxualHeaderKeys.SerializationTimeHeader,
                [Description(CopyFlagsHeader)] CopyFlags = AxualHeaderKeys.CopyFlagsHeader,
                [Description(SystemHeader)] System = AxualHeaderKeys.SystemHeader,
                [Description(InstanceHeader)] Instance = AxualHeaderKeys.InstanceHeader,
                [Description(ClusterHeader)] Cluster = AxualHeaderKeys.ClusterHeader,
                [Description(TenantHeader)] Tenant = AxualHeaderKeys.TenantHeader,
                [Description(EnvironmentHeader)] Environment = AxualHeaderKeys.EnvironmentHeader
            }

            /// <summary>
            ///     <para>Axual Client message header is defined as follows:</para>
            ///     <para>Offset Length Description</para>
            ///     0      2      Magic bytes
            ///     2      2      Version
            ///     4      4      Extended header length
            ///     n = 8
            ///     <para>The following additional fields are defined in V1:</para>
            ///     8      1      Message flag bits
            ///     n = 9
            ///     <para>The following additional fields are defined in V2:</para>
            ///     9      16     Message id
            ///     25     8      Serialization timestamp
            ///     n = 33
            ///     <para>The following fields are defined in all versions:</para>
            ///     n      ...    Body
            ///     <para>Where:</para>
            ///     Extended header length = size in bytes of the fields that follow
            ///     Body = Body of the message (key or value)
            /// </summary>
            internal static class Offsets
            {
                // Version 0 constants
                internal const int MagicOffset = 0;

                /// <summary>
                ///     Version = two-byte encoded version number
                /// </summary>
                internal const int VersionNumberOffset = 2;

                internal const int ExtendedHeaderSizeOffset = 4;

                // Version 1 Constants
                internal const int MessageFlagsOffset = 8;

                /// <summary>
                ///     Message Id = binary encoded GUID identifying the message
                ///     MsbOffset = 9, LsbOffset = 17;
                /// </summary>
                internal const int MessageIdOffset = 9;

                /// <summary>
                ///     Serialization timestamp = Time in millis on the system serializing the message
                /// </summary>
                internal const int SerializationTimeOffset = 25;
            }

            internal static class Versions
            {
                private const ushort Version0Number = 0;
                internal const ushort Version1Number = 1;
                internal const ushort Version2Number = 2;

                internal const int Version1ExtendedHeaderSize = 1;
                internal const int Version2ExtendedHeaderSize = 25;
                internal static readonly Version BaseVersion = new Version(Version0Number, 8);

                internal static readonly Dictionary<int, Version> AxualMessageVersions = new Dictionary<int, Version>
                {
                    {
                        Version0Number, BaseVersion
                    },
                    {
                        Version1Number,
                        new Version(
                            Version1Number,
                            BaseVersion.HeaderSize + Version1ExtendedHeaderSize,
                            Version1ExtendedHeaderSize)
                    },
                    {
                        Version2Number,
                        new Version(
                            Version2Number,
                            BaseVersion.HeaderSize + Version2ExtendedHeaderSize,
                            Version2ExtendedHeaderSize)
                    }
                };

                internal class Version
                {
                    internal Version(ushort versionNumber, int headerSize, int extendedHeaderSize = 0)
                    {
                        VersionNumber = versionNumber;
                        HeaderSize = headerSize;
                        ExtendedHeaderSize = extendedHeaderSize;
                    }

                    internal ushort VersionNumber { get; }
                    internal int HeaderSize { get; }
                    internal int ExtendedHeaderSize { get; }

                    public override string ToString()
                    {
                        return $"Version {VersionNumber}, Header Size: {HeaderSize}";
                    }
                }
            }
        }
    }
}