//
// Copyright 2020 Axual B.V. 2020
//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

using System;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("Axual.Kafka.Tests")]

namespace Axual.Kafka.Proxy.ValueHeader
{
    /// <summary>
    ///     Message ID is 16 Byte (128 bits) is Axual unique identifier for the Kafka messages
    /// </summary>
    internal class AxualMessageId
    {
        internal const int Size = 16;

        // Note that the order of bytes in the returned byte array is different
        // from the string representation of a Guid value.
        // The order of the beginning four-byte group and the next two two-byte groups is reversed,
        // whereas the order of the last two-byte group and the closing six-byte group is the same.
        // Use the function MessageId.ToByteArray() to have the correct order
        private readonly Guid _id;

        public AxualMessageId()
        {
            _id = Guid.NewGuid();
        }

        // For unit-tests  only
        internal AxualMessageId(Guid messageId)
        {
            _id = messageId;
        }

        public static AxualMessageId Empty => new AxualMessageId(Guid.Empty);

        public override string ToString()
        {
            return System.BitConverter.ToString(_id.ToByteArray()).ToLower().Replace("-", "");
        }

        public override bool Equals(object obj)
        {
            if (!(obj is AxualMessageId))
                return false;
            var messageId = (AxualMessageId) obj;
            return _id.Equals(messageId._id);
        }

        public override int GetHashCode()
        {
            return _id.GetHashCode();
        }

        public byte[] Serialize()
        {
            return _id.ToByteArray();
        }

        public static AxualMessageId Deserialize(byte[] data)
        {
            return new AxualMessageId(new Guid(data));
        }
    }
}