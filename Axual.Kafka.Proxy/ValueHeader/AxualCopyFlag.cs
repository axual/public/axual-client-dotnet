//
// Copyright 2020 Axual B.V. 2020
//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

using System;

namespace Axual.Kafka.Proxy.ValueHeader
{
    /// <summary>
    ///     CopyFlag is a 4 bytes - 32bits Axual flag indicating which distribution levels the message distributed.
    ///     Message distributed to level 'x', the 'x' position will be on - set.
    ///     CopyFlag with all zero (non of the position are set) indicating the message is the original one.
    /// </summary>
    internal class AxualCopyFlag
    {
        private readonly int _copyFlag;

        private AxualCopyFlag(int copyFlag)
        {
            _copyFlag = copyFlag;
        }

        public static AxualCopyFlag OriginalMessage => new AxualCopyFlag(0);

        public override string ToString()
        {
            return Convert.ToString(_copyFlag, 2);
        }

        /// <param name="distributionLevel">The distribution level the message sent</param>
        public bool IsDistributedToLevel(int distributionLevel)
        {
            // if out of band, more than distribution level 31 (or less than 1)
            if (distributionLevel < 1 || distributionLevel > 31) return false;

            distributionLevel--;
            return Utils.BitsUtils.IsBitSet(_copyFlag, distributionLevel);
        }

        internal AxualCopyFlag SetDistributedToLevel(int distributionLevel)
        {
            distributionLevel--;
            return new AxualCopyFlag(Utils.BitsUtils.SetBit(_copyFlag, distributionLevel));
        }

        public override bool Equals(object obj)
        {
            if (!(obj is AxualCopyFlag))
                return false;
            var other = (AxualCopyFlag) obj;
            return _copyFlag.Equals(other._copyFlag);
        }

        public override int GetHashCode()
        {
            return _copyFlag;
        }

        public byte[] Serialize()
        {
            return Utils.BitsUtils.BigEndian.GetValueBytesBigEndian(_copyFlag);
        }

        public static AxualCopyFlag Deserialize(byte[] data)
        {
            return data.Length == sizeof(byte)
                ? new AxualCopyFlag(data[0])
                : new AxualCopyFlag(Utils.BitsUtils.BigEndian.ToInt32FromBigEndian(data, 0));
        }
    }
}