//
// Copyright 2020 Axual B.V. 2020
//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

using System.IO;
using System.Runtime.Serialization;
using Axual.Kafka.Proxy.ValueHeader;
using Confluent.Kafka;
using Confluent.Kafka.SyncOverAsync;
using static Axual.Kafka.Proxy.ValueHeader.Constants.AxualMessage.EnumProducerHeadersKey;

namespace Axual.Kafka.Proxy.SerDes
{
    internal class ValueHeaderSerializer<T> : ISerializer<T>, IConfigurable
    {
        private readonly IAsyncSerializer<T> _innerAsyncSerializer;
        private readonly ISerializer<T> _innerSerializer;

        internal ValueHeaderSerializer(ISerializer<T> innerSerializer)
        {
            _innerSerializer = innerSerializer;
        }

        internal ValueHeaderSerializer(IAsyncSerializer<T> innerAsyncSerializer)
        {
            _innerAsyncSerializer = innerAsyncSerializer;
        }

        /// <summary>
        ///     Proxy <see cref="IConfigurable.Configure" />
        ///     <example>
        ///         <see cref="T:Axual.SchemaRegistry.Serdes.Avro.Serializers.SpecificAvroSerializer" />
        ///     </example>
        /// </summary>
        void IConfigurable.Configure(ClientConfig clientConfig)
        {
            if (_innerSerializer is IConfigurable configurableSerializer)
                configurableSerializer.Configure(clientConfig);

            if (_innerAsyncSerializer is IConfigurable configurableAsyncSerializer)
                configurableAsyncSerializer.Configure(clientConfig);
        }

        public byte[] Serialize(T data, SerializationContext context)
        {
            byte[] result;
            var innerSerializer = _innerSerializer;

            // serialize the value itself
            if (_innerAsyncSerializer != null)
                innerSerializer = _innerAsyncSerializer.AsSyncOverAsync();

            var tValueInBytes = innerSerializer.Serialize(data, context);

            switch (context.Component)
            {
                case MessageComponentType.Key:
                    result = tValueInBytes;
                    break;
                case MessageComponentType.Value:
                    // adding the value header

                    var messageVersion =
                        Constants.AxualMessage.Versions.AxualMessageVersions[
                            Constants.AxualMessage.Versions.Version2Number];
                    var messageSize = messageVersion.HeaderSize + tValueInBytes.Length;

                    using (var memoryStream = new MemoryStream(messageSize))
                    using (var binaryWriter = new Utils.BitsUtils.BinaryWriterEndian(memoryStream, true))
                    {
                        binaryWriter.Seek(Constants.AxualMessage.Offsets.MagicOffset, SeekOrigin.Begin);
                        binaryWriter.Write(Constants.AxualMessage.AxualMagicUInt16Value);

                        binaryWriter.Seek(Constants.AxualMessage.Offsets.VersionNumberOffset, SeekOrigin.Begin);
                        binaryWriter.Write(messageVersion.VersionNumber);

                        binaryWriter.Seek(Constants.AxualMessage.Offsets.ExtendedHeaderSizeOffset, SeekOrigin.Begin);
                        binaryWriter.Write(messageVersion.ExtendedHeaderSize);

                        byte[] copyFlagValue = null;
                        binaryWriter.Seek(Constants.AxualMessage.Offsets.MessageFlagsOffset, SeekOrigin.Begin);
                        context.Headers?.TryGetLastBytes(CopyFlags.GetDescription(), out copyFlagValue);
                        binaryWriter.Write(copyFlagValue ?? AxualCopyFlag.OriginalMessage.Serialize());

                        byte[] messageIdValue = null;
                        binaryWriter.Seek(Constants.AxualMessage.Offsets.MessageIdOffset, SeekOrigin.Begin);
                        context.Headers?.TryGetLastBytes(MessageId.GetDescription(), out messageIdValue);
                        binaryWriter.Write(messageIdValue ?? AxualMessageId.Empty.Serialize());

                        binaryWriter.Seek(Constants.AxualMessage.Offsets.SerializationTimeOffset, SeekOrigin.Begin);
                        binaryWriter.Write(AxualTime.Now.Serialize());

                        binaryWriter.Seek(messageVersion.HeaderSize, SeekOrigin.Begin);
                        binaryWriter.Write(tValueInBytes);

                        binaryWriter.Flush();

                        result = memoryStream.ToArray();
                    }

                    break;
                default:
                    throw new SerializationException($"Unexpected MessageComponentType type:{context.Component}");
            }

            return result;
        }
    }
}