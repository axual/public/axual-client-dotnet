//
// Copyright 2020 Axual B.V. 2020
//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

using System;
using System.IO;
using System.Runtime.Serialization;
using Axual.Kafka.Proxy.ValueHeader;
using Confluent.Kafka;
using Confluent.Kafka.SyncOverAsync;
using static Axual.Kafka.Proxy.ValueHeader.Constants.AxualMessage.EnumProducerHeadersKey;

namespace Axual.Kafka.Proxy.SerDes
{
    internal class ValueHeaderDeserializer<T> : IDeserializer<T>, IConfigurable
    {
        private readonly IAsyncDeserializer<T> _innerAsyncDeserializer;
        private readonly IDeserializer<T> _innerDeserializer;

        public ValueHeaderDeserializer(IDeserializer<T> innerDeserializer)
        {
            _innerDeserializer = innerDeserializer;
        }

        public ValueHeaderDeserializer(IAsyncDeserializer<T> innerAsyncDeserializer)
        {
            _innerAsyncDeserializer = innerAsyncDeserializer;
        }

        /// <summary>
        ///     Proxy <see cref="IConfigurable.Configure" />
        ///     <example>
        ///         <see cref="T:Axual.SchemaRegistry.Serdes.Avro.Deserializers.SpecificAvroDeserializer" />
        ///     </example>
        /// </summary>
        void IConfigurable.Configure(ClientConfig clientConfig)
        {
            if (_innerDeserializer is IConfigurable configurableDeserializer)
                configurableDeserializer.Configure(clientConfig);

            if (_innerAsyncDeserializer is IConfigurable configurableAsyncDeserializer)
                configurableAsyncDeserializer.Configure(clientConfig);
        }

        public T Deserialize(ReadOnlySpan<byte> data, bool isNull, SerializationContext context)
        {
            T result;

            if (isNull)
                throw new ArgumentNullException(nameof(data), "Null data encountered.");

            switch (context.Component)
            {
                case MessageComponentType.Value:
                {
                    var valueHeader = DeserializerValueHeader(data, isNull, context);
                    result = valueHeader.Value;
                    AddValueHeaderToKafkaHeadersIfMissing(valueHeader, context);
                    break;
                }
                default:
                    throw new SerializationException($"Unexpected MessageComponentType type: {context.Component}");
            }

            return result;
        }

        private static void AddValueHeaderToKafkaHeadersIfMissing(ValueHeader<T> valueHeader,
            SerializationContext serializationContext)
        {
            serializationContext.Headers.Add(SerializationTime.GetDescription(),
                valueHeader.SerializationTime.Serialize(), false);
            serializationContext.Headers.Add(CopyFlags.GetDescription(), valueHeader.AxualCopyFlag.Serialize(), false);
            serializationContext.Headers.Add(MessageId.GetDescription(), valueHeader.AxualMessageId.Serialize(), false);
        }

        private ValueHeader<T> DeserializerValueHeader(ReadOnlySpan<byte> data, bool isNull,
            SerializationContext context)
        {
            ValueHeader<T> valueHeader = null;
            var payload = data;

            var innerDeserializer = _innerDeserializer;
            if (_innerAsyncDeserializer != null)
                innerDeserializer = _innerAsyncDeserializer.AsSyncOverAsync();

            using (var stream = new MemoryStream(data.ToArray()))
            using (var binaryReaderBigEndian =
                new Utils.BitsUtils.BinaryReaderEndian(stream, true))
            {
                var serializationTime = AxualTime.Zero;
                var axualMessageId = AxualMessageId.Empty;
                var distributionLevelFlag = AxualCopyFlag.OriginalMessage;
                var currentHeaderSize = Constants.AxualMessage.Versions.BaseVersion.HeaderSize;

                if (data.Length >= sizeof(ushort))
                {
                    var messageMagicByte = binaryReaderBigEndian.ReadUInt16();
                    if (Constants.AxualMessage.IsValueHeaderMagicByte(messageMagicByte))
                    {
                        binaryReaderBigEndian.BaseStream.Seek(Constants.AxualMessage.Offsets.VersionNumberOffset,
                            SeekOrigin.Begin);
                        var version = binaryReaderBigEndian.ReadUInt16();

                        if (IsHaveExtendedHeaders(version))
                        {
                            SetHeaderValues(version, out var extendedHeaderSize, out axualMessageId,
                                out distributionLevelFlag,
                                out serializationTime,
                                binaryReaderBigEndian);

                            currentHeaderSize = Constants.AxualMessage.Versions.BaseVersion.HeaderSize +
                                                extendedHeaderSize;
                        }

                        if (data.Length < currentHeaderSize)
                            throw new InvalidDataException(
                                $"Encountered data of length {data.Length}." +
                                $" Which is even smaller than the expected header size: {currentHeaderSize}.");

                        // Deserialize the Payload (T object)
                        var payloadOffset = currentHeaderSize;
                        payload = data.Slice(payloadOffset);
                    }
                }

                var value = innerDeserializer.Deserialize(payload, isNull, context);
                valueHeader = new ValueHeader<T>(value, axualMessageId, serializationTime, distributionLevelFlag);
            }

            return valueHeader;
        }

        private static bool IsHaveExtendedHeaders(ushort version)
        {
            return version >= Constants.AxualMessage.Versions.Version1Number;
        }

        private static void SetHeaderValues(
            ushort version,
            out int extendedHeaderSize,
            out AxualMessageId axualMessageId,
            out AxualCopyFlag distributionLevelFlag,
            out AxualTime serializationTime,
            Utils.BitsUtils.BinaryReaderEndian binaryReaderBigEndian)
        {
            binaryReaderBigEndian.BaseStream.Seek(Constants.AxualMessage.Offsets.ExtendedHeaderSizeOffset,
                SeekOrigin.Begin);
            extendedHeaderSize = binaryReaderBigEndian.ReadInt32();

            binaryReaderBigEndian.BaseStream.Seek(Constants.AxualMessage.Offsets.MessageFlagsOffset,
                SeekOrigin.Begin);
            // CopyFlag size in value header message are byte
            distributionLevelFlag = AxualCopyFlag.Deserialize(new[] {binaryReaderBigEndian.ReadByte()});

            if (version >= Constants.AxualMessage.Versions.Version2Number)
            {
                binaryReaderBigEndian.BaseStream.Seek(Constants.AxualMessage.Offsets.MessageIdOffset,
                    SeekOrigin.Begin);
                axualMessageId = AxualMessageId.Deserialize(binaryReaderBigEndian.ReadBytes(AxualMessageId.Size));


                binaryReaderBigEndian.BaseStream.Seek(Constants.AxualMessage.Offsets.SerializationTimeOffset,
                    SeekOrigin.Begin);
                serializationTime = AxualTime.Deserialize(binaryReaderBigEndian.ReadBytes(AxualTime.Size));
            }
            else
            {
                axualMessageId = AxualMessageId.Empty;
                serializationTime = AxualTime.Zero;
            }
        }
    }
}