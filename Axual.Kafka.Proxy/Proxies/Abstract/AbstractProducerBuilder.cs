//
// Copyright 2020 Axual B.V. 2020
//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

using System;
using Confluent.Kafka;

namespace Axual.Kafka.Proxy.Proxies.Abstract
{
    public abstract class AbstractProducerBuilder<TKey, TValue> : IProducerBuilder<TKey, TValue>
    {
        protected IProducer<TKey, TValue> CurrentProducer;
        internal IProducerBuilder<TKey, TValue> NextProducerBuilder;

        protected AbstractProducerBuilder(ProducerConfig producerConfig,
            IProducerBuilder<TKey, TValue> nextProducerBuilder)
        {
            ProducerConfig = producerConfig;
            NextProducerBuilder = nextProducerBuilder;
        }

        /// <summary>The configured key serializer.</summary>
        protected ISerializer<TKey> KeySerializer { get; private set; }

        /// <summary>The configured value serializer.</summary>
        protected ISerializer<TValue> ValueSerializer { get; private set; }

        /// <summary> The configured async key serializer.</summary>
        protected IAsyncSerializer<TKey> AsyncKeySerializer { get; set; }

        /// <summary> The configured async value serializer.</summary>
        protected IAsyncSerializer<TValue> AsyncValueSerializer { get; set; }

        /// <summary> The configured log handler.</summary>
        protected Action<IProducer<TKey, TValue>, LogMessage> LogHandler { get; set; }

        /// <summary> The configured error handler.</summary>
        protected Action<IProducer<TKey, TValue>, Error> ErrorHandler { get; set; }

        /// <summary> The configured statistics handler. </summary>
        protected Action<IProducer<TKey, TValue>, string> StatisticsHandler { get; set; }

        public abstract bool Validate();

        IClient IClientBuilder.Build()
        {
            return Build();
        }

        public IClient Rebuild(ClientConfig clientConfig)
        {
            return Rebuild(new ProducerConfig(clientConfig));
        }

        public ProducerConfig ProducerConfig { get; protected set; }

        public abstract IProducer<TKey, TValue> Build();

        public abstract IProducer<TKey, TValue> Rebuild(ProducerConfig producerConfig);

        public IProducerBuilder<TKey, TValue> SetKeySerializer(ISerializer<TKey> keySerializer)
        {
            KeySerializer = keySerializer;

            return this;
        }

        public IProducerBuilder<TKey, TValue> SetKeySerializer(IAsyncSerializer<TKey> asyncKeySerializer)
        {
            AsyncKeySerializer = asyncKeySerializer;

            return this;
        }

        public IProducerBuilder<TKey, TValue> SetValueSerializer(ISerializer<TValue> valueSerializer)
        {
            ValueSerializer = valueSerializer;

            return this;
        }

        public IProducerBuilder<TKey, TValue> SetValueSerializer(IAsyncSerializer<TValue> asyncValueSerializer)
        {
            AsyncValueSerializer = asyncValueSerializer;

            return this;
        }

        public IProducerBuilder<TKey, TValue> SetLogHandler(Action<IProducer<TKey, TValue>, LogMessage> logHandler)
        {
            LogHandler = logHandler;

            return this;
        }

        public IProducerBuilder<TKey, TValue> SetErrorHandler(Action<IProducer<TKey, TValue>, Error> errorHandler)
        {
            ErrorHandler = errorHandler;

            return this;
        }

        public IProducerBuilder<TKey, TValue> SetStatisticsHandler(
            Action<IProducer<TKey, TValue>, string> statisticsHandler)
        {
            StatisticsHandler = statisticsHandler;

            return this;
        }

        protected virtual void SetNextBuilderSetters()
        {
            if (KeySerializer != null)
                NextProducerBuilder.SetKeySerializer(KeySerializer);
            if (ValueSerializer != null)
                NextProducerBuilder.SetValueSerializer(ValueSerializer);
            if (AsyncKeySerializer != null)
                NextProducerBuilder.SetKeySerializer(AsyncKeySerializer);
            if (AsyncValueSerializer != null)
                NextProducerBuilder.SetValueSerializer(AsyncValueSerializer);

            NextProducerBuilder.SetLogHandler(LogHandler);
            NextProducerBuilder.SetErrorHandler(ErrorHandler);
            NextProducerBuilder.SetStatisticsHandler(StatisticsHandler);
        }
    }
}