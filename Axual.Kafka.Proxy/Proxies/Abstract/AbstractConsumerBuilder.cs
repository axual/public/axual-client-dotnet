//
// Copyright 2020 Axual B.V. 2020
//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

using System;
using System.Collections.Generic;
using System.Linq;
using Confluent.Kafka;

namespace Axual.Kafka.Proxy.Proxies.Abstract
{
    /// <summary>
    ///     Class which wrapping the .NET Kafka ConsumerBuilder and add additional functionality
    /// </summary>
    /// <typeparam name="TKey"> The key type of the current Consumer </typeparam>
    /// <typeparam name="TValue">The value type of the current Consumer</typeparam>
    public abstract class AbstractConsumerBuilder<TKey, TValue> : IConsumerBuilder<TKey, TValue>
    {
        protected IConsumer<TKey, TValue> CurrentConsumer;
        internal IConsumerBuilder<TKey, TValue> NextConsumerBuilder;

        protected AbstractConsumerBuilder(ConsumerConfig consumerConfig,
            IConsumerBuilder<TKey, TValue> nextConsumerBuilder)
        {
            ConsumerConfig = consumerConfig;
            NextConsumerBuilder = nextConsumerBuilder;
        }

        /// <summary>The configured key deserializer.</summary>
        protected IDeserializer<TKey> KeyDeserializer { get; set; }

        /// <summary>The configured value deserializer.</summary>
        protected IDeserializer<TValue> ValueDeserializer { get; set; }

        /// <summary>The configured async key deserializer.</summary>
        protected IAsyncDeserializer<TKey> AsyncKeyDeserializer { get; set; }

        /// <summary>The configured async value deserializer.</summary>
        protected IAsyncDeserializer<TValue> AsyncValueDeserializer { get; set; }

        /// <summary>The configured log handler.</summary>
        protected Action<IConsumer<TKey, TValue>, LogMessage> LogHandler { get; set; }

        /// <summary>
        ///     The configured error handler.
        /// </summary>
        protected Action<IConsumer<TKey, TValue>, Error> ErrorHandler { get; set; }

        /// <summary>
        ///     The configured statistics handler.
        /// </summary>
        protected Action<IConsumer<TKey, TValue>, string> StatisticsHandler { get; set; }

        /// <summary>
        ///     The configured partitions assigned handler.
        /// </summary>
        protected Func<IConsumer<TKey, TValue>, List<TopicPartition>, IEnumerable<TopicPartitionOffset>>
            PartitionsAssignedHandler { get; set; }

        /// <summary>
        ///     The configured partitions revoked handler.
        /// </summary>
        protected Func<IConsumer<TKey, TValue>, List<TopicPartitionOffset>, IEnumerable<TopicPartitionOffset>>
            PartitionsRevokedHandler { get; set; }

        /// <summary>
        ///     The configured offsets committed handler.
        /// </summary>
        protected Action<IConsumer<TKey, TValue>, CommittedOffsets> OffsetsCommittedHandler { get; set; }

        public abstract bool Validate();

        IClient IClientBuilder.Build()
        {
            return Build();
        }

        public IClient Rebuild(ClientConfig clientConfig)
        {
            return Rebuild(new ConsumerConfig(clientConfig));
        }

        public ConsumerConfig ConsumerConfig { get; set; }

        public abstract IConsumer<TKey, TValue> Build();

        public abstract IConsumer<TKey, TValue> Rebuild(ConsumerConfig consumerConfig);

        public IConsumerBuilder<TKey, TValue> SetKeyDeserializer(IDeserializer<TKey> keyDeserializer)
        {
            KeyDeserializer = keyDeserializer;

            return this;
        }

        public IConsumerBuilder<TKey, TValue> SetKeyDeserializer(IAsyncDeserializer<TKey> asyncKeyDeserializer)
        {
            AsyncKeyDeserializer = asyncKeyDeserializer;

            return this;
        }

        public IConsumerBuilder<TKey, TValue> SetValueDeserializer(IDeserializer<TValue> valueDeserializer)
        {
            ValueDeserializer = valueDeserializer;

            return this;
        }

        public IConsumerBuilder<TKey, TValue> SetValueDeserializer(IAsyncDeserializer<TValue> asyncValueDeserializer)
        {
            AsyncValueDeserializer = asyncValueDeserializer;

            return this;
        }

        public IConsumerBuilder<TKey, TValue> SetLogHandler(Action<IConsumer<TKey, TValue>, LogMessage> logHandler)
        {
            LogHandler = logHandler;

            return this;
        }

        public IConsumerBuilder<TKey, TValue> SetErrorHandler(Action<IConsumer<TKey, TValue>, Error> errorHandler)
        {
            ErrorHandler = errorHandler;

            return this;
        }

        public IConsumerBuilder<TKey, TValue> SetStatisticsHandler(
            Action<IConsumer<TKey, TValue>, string> statisticsHandler)
        {
            StatisticsHandler = statisticsHandler;

            return this;
        }

        public IConsumerBuilder<TKey, TValue> SetPartitionsAssignedHandler(
            Func<IConsumer<TKey, TValue>, List<TopicPartition>, IEnumerable<TopicPartitionOffset>>
                partitionsAssignedHandler)
        {
            PartitionsAssignedHandler = partitionsAssignedHandler;

            return this;
        }

        public IConsumerBuilder<TKey, TValue> SetPartitionsAssignedHandler(
            Action<IConsumer<TKey, TValue>, List<TopicPartition>> partitionAssignmentHandler)
        {
            PartitionsAssignedHandler = (consumer, partitions) =>
            {
                partitionAssignmentHandler(consumer, partitions);
                return partitions.Select(tp => new TopicPartitionOffset(tp, Offset.Unset)).ToList();
            };

            return this;
        }

        public IConsumerBuilder<TKey, TValue> SetPartitionsRevokedHandler(
            Func<IConsumer<TKey, TValue>, List<TopicPartitionOffset>, IEnumerable<TopicPartitionOffset>>
                partitionsRevokedHandler)
        {
            PartitionsRevokedHandler = partitionsRevokedHandler;

            return this;
        }

        public IConsumerBuilder<TKey, TValue> SetPartitionsRevokedHandler(
            Action<IConsumer<TKey, TValue>, List<TopicPartitionOffset>> partitionsRevokedHandler)
        {
            PartitionsRevokedHandler = (consumer, partitions) =>
            {
                partitionsRevokedHandler(consumer, partitions);
                return new List<TopicPartitionOffset>();
            };

            return this;
        }

        public IConsumerBuilder<TKey, TValue> SetOffsetsCommittedHandler(
            Action<IConsumer<TKey, TValue>, CommittedOffsets> offsetsCommittedHandler)
        {
            OffsetsCommittedHandler = offsetsCommittedHandler;

            return this;
        }

        protected virtual void SetNextBuilderSetters()
        {
            if (KeyDeserializer != null)
                NextConsumerBuilder.SetKeyDeserializer(KeyDeserializer);
            if (AsyncKeyDeserializer != null)
                NextConsumerBuilder.SetKeyDeserializer(AsyncKeyDeserializer);
            if (ValueDeserializer != null)
                NextConsumerBuilder.SetValueDeserializer(ValueDeserializer);
            if (AsyncValueDeserializer != null)
                NextConsumerBuilder.SetValueDeserializer(AsyncValueDeserializer);

            NextConsumerBuilder.SetErrorHandler(ErrorHandler);
            NextConsumerBuilder.SetLogHandler(LogHandler);
            NextConsumerBuilder.SetStatisticsHandler(StatisticsHandler);
            NextConsumerBuilder.SetOffsetsCommittedHandler(OffsetsCommittedHandler);
            NextConsumerBuilder.SetPartitionsAssignedHandler(PartitionsAssignedHandler);
            NextConsumerBuilder.SetPartitionsRevokedHandler(PartitionsRevokedHandler);
        }
    }
}