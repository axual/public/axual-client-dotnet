//
// Copyright 2020 Axual B.V. 2020
//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

using System;
using System.Collections.Generic;
using System.Threading;
using Confluent.Kafka;

namespace Axual.Kafka.Proxy.Proxies.Abstract
{
    public abstract class AbstractConsumer<TKey, TValue> : IConsumer<TKey, TValue>
    {
        private readonly IConsumer<TKey, TValue> _nextConsumer;

        protected AbstractConsumer(IConsumer<TKey, TValue> nextConsumer)
        {
            _nextConsumer = nextConsumer;
        }

        public virtual void Dispose()
        {
            _nextConsumer.Dispose();
        }

        public virtual int AddBrokers(string brokers)
        {
            return _nextConsumer.AddBrokers(brokers);
        }

        public virtual Handle Handle => _nextConsumer.Handle;
        public virtual string Name => _nextConsumer.Name;

        public virtual ConsumeResult<TKey, TValue> Consume(int millisecondsTimeout)
        {
            var consumeResult = _nextConsumer.Consume(millisecondsTimeout);
            var layeredConsumerResult = AddLayerResponsibility(consumeResult);
            return layeredConsumerResult;
        }

        public virtual ConsumeResult<TKey, TValue> Consume(
            CancellationToken cancellationToken = new CancellationToken())
        {
            var consumeResult = _nextConsumer.Consume(cancellationToken);
            var layeredConsumerResult = AddLayerResponsibility(consumeResult);
            return layeredConsumerResult;
        }

        public virtual ConsumeResult<TKey, TValue> Consume(TimeSpan timeout)
        {
            var consumeResult = _nextConsumer.Consume(timeout);
            var layeredConsumerResult = AddLayerResponsibility(consumeResult);
            return layeredConsumerResult;
        }

        public virtual void Subscribe(IEnumerable<string> topics)
        {
            _nextConsumer.Subscribe(topics);
        }

        public virtual void Subscribe(string topic)
        {
            _nextConsumer.Subscribe(topic);
        }

        public virtual void Unsubscribe()
        {
            _nextConsumer.Unsubscribe();
        }

        public virtual void Assign(TopicPartition partition)
        {
            _nextConsumer.Assign(partition);
        }

        public virtual void Assign(TopicPartitionOffset partition)
        {
            _nextConsumer.Assign(partition);
        }

        public virtual void Assign(IEnumerable<TopicPartitionOffset> partitions)
        {
            _nextConsumer.Assign(partitions);
        }

        public virtual void Assign(IEnumerable<TopicPartition> partitions)
        {
            _nextConsumer.Assign(partitions);
        }

        public virtual void IncrementalAssign(IEnumerable<TopicPartitionOffset> partitions)
        {
            _nextConsumer.IncrementalAssign(partitions);
        }

        public virtual void IncrementalAssign(IEnumerable<TopicPartition> partitions)
        {
            _nextConsumer.IncrementalAssign(partitions);
        }

        public virtual void IncrementalUnassign(IEnumerable<TopicPartition> partitions)
        {
            _nextConsumer.IncrementalUnassign(partitions);
        }

        public virtual void Unassign()
        {
            _nextConsumer.Unassign();
        }

        public virtual void StoreOffset(ConsumeResult<TKey, TValue> result)
        {
            _nextConsumer.StoreOffset(new TopicPartitionOffset(result.TopicPartition, result.Offset + 1));
        }

        public virtual void StoreOffset(TopicPartitionOffset offset)
        {
            _nextConsumer.StoreOffset(offset);
        }

        public virtual List<TopicPartitionOffset> Commit()
        {
            return _nextConsumer.Commit();
        }

        public virtual void Commit(IEnumerable<TopicPartitionOffset> offsets)
        {
            _nextConsumer.Commit(offsets);
        }

        public virtual void Commit(ConsumeResult<TKey, TValue> result)
        {
            if (result.Message == null)
                throw new InvalidOperationException(
                    "Attempt was made to commit offset corresponding to an empty consume result");

            _nextConsumer.Commit(new TopicPartitionOffset[1]
            {
                new TopicPartitionOffset(result.TopicPartition, result.Offset + 1)
            });
        }

        public virtual void Seek(TopicPartitionOffset tpo)
        {
            _nextConsumer.Seek(tpo);
        }

        public virtual void Pause(IEnumerable<TopicPartition> partitions)
        {
            _nextConsumer.Pause(partitions);
        }

        public virtual void Resume(IEnumerable<TopicPartition> partitions)
        {
            _nextConsumer.Resume(partitions);
        }

        public virtual List<TopicPartitionOffset> Committed(TimeSpan timeout)
        {
            return _nextConsumer.Committed(timeout);
        }

        public virtual List<TopicPartitionOffset> Committed(IEnumerable<TopicPartition> partitions, TimeSpan timeout)
        {
            return _nextConsumer.Committed(partitions, timeout);
        }

        public virtual Offset Position(TopicPartition partition)
        {
            return _nextConsumer.Position(partition);
        }

        public virtual List<TopicPartitionOffset> OffsetsForTimes(
            IEnumerable<TopicPartitionTimestamp> timestampsToSearch,
            TimeSpan timeout)
        {
            return _nextConsumer.OffsetsForTimes(timestampsToSearch, timeout);
        }

        public virtual WatermarkOffsets GetWatermarkOffsets(TopicPartition topicPartition)
        {
            return _nextConsumer.GetWatermarkOffsets(topicPartition);
        }

        public virtual WatermarkOffsets QueryWatermarkOffsets(TopicPartition topicPartition, TimeSpan timeout)
        {
            return _nextConsumer.QueryWatermarkOffsets(topicPartition, timeout);
        }

        public virtual void Close()
        {
            _nextConsumer.Close();
        }

        public string MemberId => _nextConsumer.MemberId;
        public List<TopicPartition> Assignment => _nextConsumer.Assignment;
        public virtual List<string> Subscription => _nextConsumer.Subscription;
        public IConsumerGroupMetadata ConsumerGroupMetadata => _nextConsumer.ConsumerGroupMetadata;

        protected virtual ConsumeResult<TKey, TValue> AddLayerResponsibility(ConsumeResult<TKey, TValue> consumeResult)
        {
            return consumeResult;
        }
    }
}