//
// Copyright 2020 Axual B.V. 2020
//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Confluent.Kafka;

namespace Axual.Kafka.Proxy.Proxies.Abstract
{
    public abstract class AbstractProducer<TKey, TValue> : IProducer<TKey, TValue>
    {
        private readonly IProducer<TKey, TValue> _nextProducer;

        protected AbstractProducer(IProducer<TKey, TValue> nextProducer)
        {
            _nextProducer = nextProducer;
        }

        public virtual void Dispose()
        {
            _nextProducer.Dispose();
        }

        public virtual int AddBrokers(string brokers)
        {
            return _nextProducer.AddBrokers(brokers);
        }

        public virtual Handle Handle => _nextProducer.Handle;

        public virtual string Name => _nextProducer.Name;

        public virtual Task<DeliveryResult<TKey, TValue>> ProduceAsync(string topic, Message<TKey, TValue> message,
            CancellationToken cancellationToken = default)
        {
            return Task.Run(() =>
            {
                var layeredMessage = AddLayerResponsibility(message);
                return _nextProducer.ProduceAsync(topic, layeredMessage, cancellationToken)
                    .ContinueWith(task => task.Result, cancellationToken);
            }, cancellationToken);
        }

        public virtual Task<DeliveryResult<TKey, TValue>> ProduceAsync(TopicPartition topicPartition,
            Message<TKey, TValue> message, CancellationToken cancellationToken = default)
        {
            return Task.Run(() =>
            {
                var layeredMessage = AddLayerResponsibility(message);
                return _nextProducer.ProduceAsync(topicPartition, layeredMessage, cancellationToken)
                    .ContinueWith(task => task.Result, cancellationToken);
            }, cancellationToken);
        }

        public virtual void Produce(string topic, Message<TKey, TValue> message,
            Action<DeliveryReport<TKey, TValue>> deliveryHandler = null)
        {
            var layeredMessage = AddLayerResponsibility(message);

            if (deliveryHandler == null)
                _nextProducer.Produce(topic, layeredMessage);
            else
                _nextProducer.Produce(topic, layeredMessage, deliveryHandler);
        }

        public virtual void Produce(TopicPartition topicPartition, Message<TKey, TValue> message,
            Action<DeliveryReport<TKey, TValue>> deliveryHandler = null)
        {
            var layeredMessage = AddLayerResponsibility(message);

            if (deliveryHandler == null)
                _nextProducer.Produce(topicPartition, layeredMessage);
            else
                _nextProducer.Produce(topicPartition, layeredMessage, deliveryHandler);
        }

        public virtual int Poll(TimeSpan timeout)
        {
            return _nextProducer.Poll(timeout);
        }

        public virtual int Flush(TimeSpan timeout)
        {
            return _nextProducer.Flush(timeout);
        }

        public virtual void Flush(CancellationToken cancellationToken = new CancellationToken())
        {
            _nextProducer.Flush(cancellationToken);
        }

        public virtual void InitTransactions(TimeSpan timeout)
        {
            _nextProducer.InitTransactions(timeout);
        }

        public virtual void BeginTransaction()
        {
            _nextProducer.BeginTransaction();
        }

        public virtual void CommitTransaction(TimeSpan timeout)
        {
            _nextProducer.CommitTransaction(timeout);
        }

        public virtual void CommitTransaction()
        {
            _nextProducer.CommitTransaction();
        }

        public virtual void AbortTransaction(TimeSpan timeout)
        {
            _nextProducer.AbortTransaction(timeout);
        }

        public virtual void AbortTransaction()
        {
            _nextProducer.AbortTransaction();
        }

        public virtual void SendOffsetsToTransaction(IEnumerable<TopicPartitionOffset> offsets,
            IConsumerGroupMetadata groupMetadata, TimeSpan timeout)
        {
            _nextProducer.SendOffsetsToTransaction(offsets, groupMetadata, timeout);
        }

        protected virtual Message<TKey, TValue> AddLayerResponsibility(Message<TKey, TValue> message)
        {
            return message;
        }
    }
}