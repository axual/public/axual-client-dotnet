//
// Copyright 2020 Axual B.V. 2020
//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

using System;
using System.Threading;
using System.Threading.Tasks;
using Axual.Kafka.Proxy.Proxies.Abstract;
using Axual.Kafka.Proxy.Proxies.Resolving.Resolvers;
using Confluent.Kafka;

namespace Axual.Kafka.Proxy.Proxies.Resolving
{
    internal class ResolvingProducer<TKey, TValue> : AbstractProducer<TKey, TValue>
    {
        private readonly IResolver _topicResolver;

        internal ResolvingProducer(IProducerBuilder<TKey, TValue> nextProducerBuilder,
            ProducerConfig producerConfig, IResolver topicResolver)
            : base(nextProducerBuilder.Rebuild(producerConfig))
        {
            _topicResolver = topicResolver;
        }

        protected override Message<TKey, TValue> AddLayerResponsibility(Message<TKey, TValue> message)
        {
            // This layer doesn't have any responsibility 
            return message;
        }

        public override Task<DeliveryResult<TKey, TValue>> ProduceAsync(string topic,
            Message<TKey, TValue> message, CancellationToken cancellationToken = default)
        {
            var resolvedTopic = ResolveTopic(topic);
            return base.ProduceAsync(resolvedTopic, message, cancellationToken);
        }

        public override Task<DeliveryResult<TKey, TValue>> ProduceAsync(TopicPartition topicPartition,
            Message<TKey, TValue> message, CancellationToken cancellationToken = default)
        {
            var resolvedTopicPartition = ResolveTopic(topicPartition);
            return base.ProduceAsync(resolvedTopicPartition, message, cancellationToken);
        }

        public override void Produce(string topic, Message<TKey, TValue> message,
            Action<DeliveryReport<TKey, TValue>> deliveryHandler = null)
        {
            var resolvedTopic = ResolveTopic(topic);
            var resolvedDeliveryHandler = ResolveTopic(deliveryHandler);

            base.Produce(resolvedTopic, message, resolvedDeliveryHandler);
        }

        public override void Produce(TopicPartition topicPartition, Message<TKey, TValue> message,
            Action<DeliveryReport<TKey, TValue>> deliveryHandler = null)
        {
            var resolvedTopicPartition = ResolveTopic(topicPartition);
            var resolvedDeliveryHandler = ResolveTopic(deliveryHandler);

            base.Produce(resolvedTopicPartition, message, resolvedDeliveryHandler);
        }

        private TopicPartition ResolveTopic(TopicPartition topicPartition)
        {
            return new TopicPartition(ResolveTopic(topicPartition.Topic), topicPartition.Partition);
        }

        private string ResolveTopic(string unResolveTopic)
        {
            return _topicResolver.Resolve(unResolveTopic);
        }

        private Action<DeliveryReport<TKey, TValue>> ResolveTopic(Action<DeliveryReport<TKey, TValue>> myActionT)
        {
            return myActionT == null
                ? null
                : new Action<DeliveryReport<TKey, TValue>>(deliveryReport =>
                {
                    myActionT(ResolveTopic(deliveryReport));
                });
        }

        private DeliveryReport<TKey, TValue> ResolveTopic(DeliveryReport<TKey, TValue> resolvedDeliveryReport)
        {
            resolvedDeliveryReport.Topic = UnResolveTopic(resolvedDeliveryReport.Topic);
            return resolvedDeliveryReport;
        }

        private string UnResolveTopic(string resolvedTopic)
        {
            return _topicResolver.UnResolve(resolvedTopic);
        }
    }
}