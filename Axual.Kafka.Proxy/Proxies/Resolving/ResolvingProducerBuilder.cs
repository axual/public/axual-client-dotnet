//
// Copyright 2020 Axual B.V. 2020
//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

using Axual.Kafka.Proxy.Exceptions;
using Axual.Kafka.Proxy.Proxies.Abstract;
using Axual.Kafka.Proxy.Proxies.Resolving.Resolvers;
using Confluent.Kafka;
using static Axual.Kafka.Proxy.Proxies.Constants.ConfigurationKeys.Resolver;

namespace Axual.Kafka.Proxy.Proxies.Resolving
{
    internal class ResolvingProducerBuilder<TKey, TValue> : AbstractProducerBuilder<TKey, TValue>
    {
        private readonly IResolverFactory _patternResolverFactory;

        public ResolvingProducerBuilder(ProducerConfig producerConfig,
            IProducerBuilder<TKey, TValue> nextProducerBuilder) : base(producerConfig, nextProducerBuilder)
        {
            _patternResolverFactory = new PatternResolverFactory();
        }

        public override bool Validate()
        {
            var resolvingProducerConfig = new ResolvingProducerConfig(ProducerConfig);

            if (string.IsNullOrEmpty(resolvingProducerConfig.TopicResolver))
                throw new RequireConfigurationException(TopicResolver);

            if (string.IsNullOrEmpty(resolvingProducerConfig.TopicPattern))
                throw new RequireConfigurationException(TopicPattern);

            if (!string.IsNullOrEmpty(resolvingProducerConfig.TransactionalId))
            {
                if (string.IsNullOrEmpty(resolvingProducerConfig.TransactionalIdPattern))
                    throw new RequireConfigurationException(TransactionalIdPattern);

                if (string.IsNullOrEmpty(resolvingProducerConfig.TransactionalIdResolver))
                    throw new RequireConfigurationException(TransactionalIdResolver);
            }

            if (string.IsNullOrEmpty(resolvingProducerConfig.Environment))
                throw new RequireConfigurationException(Constants.ConfigurationKeys.Axual.Environment);

            if (string.IsNullOrEmpty(resolvingProducerConfig.Tenant))
                throw new RequireConfigurationException(Constants.ConfigurationKeys.Axual.Tenant);

            if (string.IsNullOrEmpty(resolvingProducerConfig.Instance))
                throw new RequireConfigurationException(Constants.ConfigurationKeys.Axual.Instance);

            return true;
        }

        public override IProducer<TKey, TValue> Build()
        {
            var resolvingProducerConfig = new ResolvingProducerConfig(ProducerConfig);

            Validate();
            SetNextBuilderSetters();
            var topicResolver = _patternResolverFactory.CreateResolver(
                resolvingProducerConfig.TopicResolver, resolvingProducerConfig);

            if (!ProducerConfig.TransactionalId.IsNullOrEmpty())
            {
                var transactionalIdResolver = _patternResolverFactory.CreateResolver(
                    resolvingProducerConfig.TransactionalIdResolver, resolvingProducerConfig);
                resolvingProducerConfig.TransactionalId =
                    transactionalIdResolver.Resolve(resolvingProducerConfig.TransactionalId);
            }

            CurrentProducer =
                new ResolvingProducer<TKey, TValue>(NextProducerBuilder, resolvingProducerConfig, topicResolver);
            return CurrentProducer;
        }

        public override IProducer<TKey, TValue> Rebuild(ProducerConfig producerConfig)
        {
            ProducerConfig = producerConfig;
            return Build();
        }
    }
}