//
// Copyright 2020 Axual B.V. 2020
//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

using System;
using System.Collections.Generic;
using System.Linq;
using Axual.Kafka.Proxy.Proxies.Abstract;
using Axual.Kafka.Proxy.Proxies.Resolving.Resolvers;
using Confluent.Kafka;

namespace Axual.Kafka.Proxy.Proxies.Resolving
{
    internal class ResolvingConsumer<TKey, TValue> : AbstractConsumer<TKey, TValue>
    {
        private readonly IResolver _topicResolver;

        internal ResolvingConsumer(IConsumerBuilder<TKey, TValue> nextConsumerBuilder,
            ConsumerConfig consumerConfig, IResolver topicResolver)
            : base(nextConsumerBuilder.Rebuild(consumerConfig))
        {
            _topicResolver = topicResolver;
        }

        public override List<string> Subscription => UnResolve(base.Subscription)?.ToList();

        protected override ConsumeResult<TKey, TValue> AddLayerResponsibility(
            ConsumeResult<TKey, TValue> consumeResult)
        {
            return consumeResult != null ? UnResolve(consumeResult) : null;
        }

        public override void Subscribe(IEnumerable<string> topics)
        {
            var resolvedTopics = Resolve(topics);
            base.Subscribe(resolvedTopics);
        }

        public override void Subscribe(string topic)
        {
            var resolvedTopic = Resolve(topic);
            base.Subscribe(resolvedTopic);
        }

        public override void Assign(TopicPartition partition)
        {
            var resolvedTopicPartition = Resolve(partition);
            base.Assign(resolvedTopicPartition);
        }

        public override void Assign(TopicPartitionOffset partition)
        {
            var resolvedTopicPartitionOffset = Resolve(partition);
            base.Assign(resolvedTopicPartitionOffset);
        }

        public override void Assign(IEnumerable<TopicPartitionOffset> partitions)
        {
            var resolvedTopicPartitionOffsets = Resolve(partitions);
            base.Assign(resolvedTopicPartitionOffsets);
        }

        public override void Assign(IEnumerable<TopicPartition> partitions)
        {
            var resolvedTopicPartitions = Resolve(partitions);
            base.Assign(resolvedTopicPartitions);
        }
        
        public override void IncrementalAssign(IEnumerable<TopicPartitionOffset> partitions)
        {
            var resolvedTopicPartitionOffsets = Resolve(partitions);
            base.IncrementalAssign(resolvedTopicPartitionOffsets);
        }

        public override void IncrementalAssign(IEnumerable<TopicPartition> partitions)
        {
            var resolvedTopicPartitions = Resolve(partitions);
            base.IncrementalAssign(resolvedTopicPartitions);
        }

        public override void IncrementalUnassign(IEnumerable<TopicPartition> partitions)
        {
            var resolvedTopicPartitions = Resolve(partitions);
            base.IncrementalUnassign(resolvedTopicPartitions);
        }

        public override void StoreOffset(ConsumeResult<TKey, TValue> result)
        {
            var resolvedConsumeResult = Resolve(result);
            base.StoreOffset(resolvedConsumeResult);
        }

        public override void StoreOffset(TopicPartitionOffset offset)
        {
            var resolvedTopicPartitionOffset = Resolve(offset);
            base.StoreOffset(resolvedTopicPartitionOffset);
        }

        public override List<TopicPartitionOffset> Commit()
        {
            var topicPartitionOffsets = base.Commit();
            var resolvedTopicPartitionOffsets = UnResolve(topicPartitionOffsets)?.ToList();
            return resolvedTopicPartitionOffsets;
        }

        public override void Commit(IEnumerable<TopicPartitionOffset> offsets)
        {
            var resolvedTopicPartitionOffsets = Resolve(offsets);
            base.Commit(resolvedTopicPartitionOffsets);
        }

        public override void Commit(ConsumeResult<TKey, TValue> result)
        {
            var resolvedConsumeResult = Resolve(result);
            base.Commit(resolvedConsumeResult);
        }

        public override void Seek(TopicPartitionOffset tpo)
        {
            var resolvedTopicPartitionOffset = Resolve(tpo);
            base.Seek(resolvedTopicPartitionOffset);
        }

        public override void Pause(IEnumerable<TopicPartition> partitions)
        {
            var resolvedTopicPartitions = Resolve(partitions);
            base.Pause(resolvedTopicPartitions);
        }

        public override void Resume(IEnumerable<TopicPartition> partitions)
        {
            var resolvedTopicPartitions = Resolve(partitions);
            base.Resume(resolvedTopicPartitions);
        }

        public override List<TopicPartitionOffset> Committed(TimeSpan timeout)
        {
            var partitionOffsets = base.Committed(timeout);
            var resolvedPartitionOffsets = UnResolve(partitionOffsets)?.ToList();
            return resolvedPartitionOffsets;
        }

        public override List<TopicPartitionOffset> Committed(IEnumerable<TopicPartition> partitions, TimeSpan timeout)
        {
            var resolvedTopicPartitions = Resolve(partitions);
            var resolvedTopicPartitionOffsets = base.Committed(resolvedTopicPartitions, timeout);
            return UnResolve(resolvedTopicPartitionOffsets)?.ToList();
        }

        public override Offset Position(TopicPartition partition)
        {
            var resolveTopicPartition = Resolve(partition);
            var offset = base.Position(resolveTopicPartition);
            return offset;
        }

        public override List<TopicPartitionOffset> OffsetsForTimes(
            IEnumerable<TopicPartitionTimestamp> timestampsToSearch,
            TimeSpan timeout)
        {
            var resolvedTimestampsToSearches = Resolve(timestampsToSearch);
            var topicPartitionOffsets = base.OffsetsForTimes(resolvedTimestampsToSearches, timeout);
            return topicPartitionOffsets?.ToList();
        }

        public override WatermarkOffsets GetWatermarkOffsets(TopicPartition topicPartition)
        {
            var resolvedTopicPartition = Resolve(topicPartition);
            var watermarkOffsets = base.GetWatermarkOffsets(resolvedTopicPartition);
            return watermarkOffsets;
        }

        public override WatermarkOffsets QueryWatermarkOffsets(TopicPartition topicPartition, TimeSpan timeout)
        {
            var resolvedTopicPartition = Resolve(topicPartition);
            var queryWatermarkOffsets = base.QueryWatermarkOffsets(resolvedTopicPartition, timeout);
            return queryWatermarkOffsets;
        }

        private TopicPartitionOffset Resolve(TopicPartitionOffset urtpo)
        {
            return new TopicPartitionOffset(Resolve(urtpo.TopicPartition), urtpo.Offset);
        }

        private TopicPartition Resolve(TopicPartition urtp)
        {
            return new TopicPartition(_topicResolver.Resolve(urtp.Topic), urtp.Partition);
        }

        private TopicPartitionTimestamp Resolve(TopicPartitionTimestamp timestampsToSearch)
        {
            return new TopicPartitionTimestamp
                (Resolve(timestampsToSearch.TopicPartition), timestampsToSearch.Timestamp);
        }

        private ConsumeResult<TKey, TValue> Resolve(ConsumeResult<TKey, TValue> urcr)
        {
            urcr.Topic = Resolve(urcr.Topic);
            return urcr;
        }

        private string Resolve(string unresolvedTopic)
        {
            return _topicResolver.Resolve(unresolvedTopic);
        }

        private IEnumerable<string> Resolve(IEnumerable<string> unResolveTopics)
        {
            return unResolveTopics?.Select(_topicResolver.Resolve);
        }

        private IEnumerable<TopicPartition> Resolve(IEnumerable<TopicPartition> urtp)
        {
            return urtp?.Select(Resolve);
        }

        private IEnumerable<TopicPartitionOffset> Resolve(IEnumerable<TopicPartitionOffset> urtpo)
        {
            return urtpo?.Select(Resolve);
        }

        private IEnumerable<TopicPartitionTimestamp> Resolve(IEnumerable<TopicPartitionTimestamp> timestampsToSearch)
        {
            return timestampsToSearch?.Select(Resolve);
        }

        private ConsumeResult<TKey, TValue> UnResolve(ConsumeResult<TKey, TValue> urcr)
        {
            urcr.Topic = UnResolve(urcr.Topic);
            return urcr;
        }

        private IEnumerable<string> UnResolve(List<string> resolveTopics)
        {
            return resolveTopics?.Select(_topicResolver.UnResolve);
        }

        private string UnResolve(string resolveTopic)
        {
            return _topicResolver.UnResolve(resolveTopic);
        }

        private TopicPartition UnResolve(TopicPartition rtp)
        {
            return new TopicPartition(UnResolve(rtp.Topic), rtp.Partition);
        }

        private TopicPartitionOffset UnResolve(TopicPartitionOffset rtpo)
        {
            return new TopicPartitionOffset(UnResolve(rtpo.TopicPartition), rtpo.Offset);
        }

        private IEnumerable<TopicPartitionOffset> UnResolve(List<TopicPartitionOffset> rtpos)
        {
            return rtpos?.Select(UnResolve);
        }
    }
}