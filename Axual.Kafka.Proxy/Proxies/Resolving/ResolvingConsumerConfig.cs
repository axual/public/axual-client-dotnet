//
// Copyright 2020 Axual B.V. 2020
//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

using Confluent.Kafka;
using static Axual.Kafka.Proxy.Proxies.Constants.ConfigurationKeys;

namespace Axual.Kafka.Proxy.Proxies.Resolving
{
    public class ResolvingConsumerConfig : ConsumerConfig
    {
        public ResolvingConsumerConfig(ClientConfig consumerConfig) : base(consumerConfig)
        {
        }

        [ResolvingKeyAttribute(Constants.ConfigurationKeys.Axual.ApplicationId)]
        public string ApplicationId
        {
            get => Get(ClientConfigMapper[Constants.ConfigurationKeys.Axual.ApplicationId]);
            set => Set(ClientConfigMapper[Constants.ConfigurationKeys.Axual.ApplicationId], value);
        }

        [ResolvingKeyAttribute(Constants.ConfigurationKeys.Axual.Tenant)]
        public string Tenant
        {
            get => Get(ClientConfigMapper[Constants.ConfigurationKeys.Axual.Tenant]);
            set => Set(ClientConfigMapper[Constants.ConfigurationKeys.Axual.Tenant], value);
        }

        [ResolvingKeyAttribute(Constants.ConfigurationKeys.Axual.Environment)]
        public string Environment
        {
            get => Get(ClientConfigMapper[Constants.ConfigurationKeys.Axual.Environment]);
            set => Set(ClientConfigMapper[Constants.ConfigurationKeys.Axual.Environment], value);
        }


        [ResolvingKeyAttribute(Constants.ConfigurationKeys.Axual.Instance)]
        public string Instance
        {
            get => Get(ClientConfigMapper[Constants.ConfigurationKeys.Axual.Instance]);
            set => Set(ClientConfigMapper[Constants.ConfigurationKeys.Axual.Instance], value);
        }


        [ResolvingKeyAttribute("Group")]
        public new string GroupId
        {
            get => base.GroupId;
            set => base.GroupId = value;
        }


        [ResolvingKeyAttribute(Resolver.GroupIdPattern)]
        internal string GroupIdPattern
        {
            get => Get(ClientConfigMapper[Resolver.GroupIdPattern]);
            set => Set(ClientConfigMapper[Resolver.GroupIdPattern], value);
        }


        [ResolvingKeyAttribute(Resolver.TopicPattern)]
        internal string TopicPattern
        {
            get => Get(ClientConfigMapper[Resolver.TopicPattern]);
            set => Set(ClientConfigMapper[Resolver.TopicPattern], value);
        }


        [ResolvingKeyAttribute(Resolver.GroupIdResolver)]
        internal string GroupIdResolver
        {
            get => Utils.RemoveNameSpace(Get(ClientConfigMapper[Resolver.GroupIdResolver]));
            set => Set(ClientConfigMapper[Resolver.GroupIdResolver], Utils.RemoveNameSpace(value));
        }

        internal string TopicResolver
        {
            get => Utils.RemoveNameSpace(Get(ClientConfigMapper[Resolver.TopicResolver]));
            set => Set(ClientConfigMapper[Resolver.TopicResolver], Utils.RemoveNameSpace(value));
        }
    }
}