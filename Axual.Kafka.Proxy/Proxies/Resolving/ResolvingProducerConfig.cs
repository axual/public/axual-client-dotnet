//
// Copyright 2020 Axual B.V. 2020
//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

using Confluent.Kafka;
using static Axual.Kafka.Proxy.Proxies.Constants.ConfigurationKeys;

namespace Axual.Kafka.Proxy.Proxies.Resolving
{
    public class ResolvingProducerConfig : ProducerConfig
    {
        public ResolvingProducerConfig(ClientConfig producerConfig) : base(producerConfig)
        {
        }


        [ResolvingKeyAttribute(Constants.ConfigurationKeys.Axual.ApplicationId)]
        public string ApplicationId
        {
            get => Get(ClientConfigMapper[Constants.ConfigurationKeys.Axual.ApplicationId]);
            set => Set(ClientConfigMapper[Constants.ConfigurationKeys.Axual.ApplicationId], value);
        }


        [ResolvingKeyAttribute(Constants.ConfigurationKeys.Axual.Tenant)]
        public string Tenant
        {
            get => Get(ClientConfigMapper[Constants.ConfigurationKeys.Axual.Tenant]);
            set => Set(ClientConfigMapper[Constants.ConfigurationKeys.Axual.Tenant], value);
        }


        [ResolvingKeyAttribute(Constants.ConfigurationKeys.Axual.Environment)]
        public string Environment
        {
            get => Get(ClientConfigMapper[Constants.ConfigurationKeys.Axual.Environment]);
            set => Set(ClientConfigMapper[Constants.ConfigurationKeys.Axual.Environment], value);
        }


        [ResolvingKeyAttribute(Constants.ConfigurationKeys.Axual.Instance)]
        public string Instance
        {
            get => Get(ClientConfigMapper[Constants.ConfigurationKeys.Axual.Instance]);
            set => Set(ClientConfigMapper[Constants.ConfigurationKeys.Axual.Instance], value);
        }


        [ResolvingKeyAttribute(Resolver.TopicPattern)]
        public string TopicPattern
        {
            get => Get(ClientConfigMapper[Resolver.TopicPattern]);
            set => Set(ClientConfigMapper[Resolver.TopicPattern], value);
        }


        [ResolvingKeyAttribute(Resolver.TopicResolver)]
        internal string TopicResolver
        {
            get => Utils.RemoveNameSpace(Get(ClientConfigMapper[Resolver.TopicResolver]));
            set => Set(ClientConfigMapper[Resolver.TopicResolver], Utils.RemoveNameSpace(value));
        }

        [ResolvingKeyAttribute(Constants.ConfigurationKeys.Kafka.TransactionalIdKey)]
        internal new string TransactionalId
        {
            get => base.TransactionalId;
            set => base.TransactionalId = value;
        }

        [ResolvingKeyAttribute(Resolver.TransactionalIdPattern)]
        internal string TransactionalIdPattern
        {
            get => Utils.RemoveNameSpace(Get(ClientConfigMapper[Resolver.TransactionalIdPattern]));
            set => Set(ClientConfigMapper[Resolver.TransactionalIdPattern], Utils.RemoveNameSpace(value));
        }

        [ResolvingKeyAttribute(Resolver.TransactionalIdResolver)]
        internal string TransactionalIdResolver
        {
            get => Utils.RemoveNameSpace(Get(ClientConfigMapper[Resolver.TransactionalIdResolver]));
            set => Set(ClientConfigMapper[Resolver.TransactionalIdResolver], Utils.RemoveNameSpace(value));
        }
    }
}
