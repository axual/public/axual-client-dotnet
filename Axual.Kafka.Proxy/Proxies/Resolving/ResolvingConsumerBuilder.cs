//
// Copyright 2020 Axual B.V. 2020
//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

using Axual.Kafka.Proxy.Exceptions;
using Axual.Kafka.Proxy.Proxies.Abstract;
using Axual.Kafka.Proxy.Proxies.Resolving.Resolvers;
using Confluent.Kafka;
using static Axual.Kafka.Proxy.Proxies.Constants.ConfigurationKeys;

namespace Axual.Kafka.Proxy.Proxies.Resolving
{
    internal class ResolvingConsumerBuilder<TKey, TValue> : AbstractConsumerBuilder<TKey, TValue>
    {
        private readonly IResolverFactory _patternResolverFactory;

        public ResolvingConsumerBuilder(ConsumerConfig consumerConfig,
            IConsumerBuilder<TKey, TValue> nextConsumerBuilder)
            : base(consumerConfig, nextConsumerBuilder)
        {
            _patternResolverFactory = new PatternResolverFactory();
        }

        public override bool Validate()
        {
            var resolvingConsumerConfig = new ResolvingConsumerConfig(ConsumerConfig);

            if (string.IsNullOrEmpty(resolvingConsumerConfig.GroupIdResolver))
                throw new RequireConfigurationException(Resolver.GroupIdResolver);

            if (string.IsNullOrEmpty(resolvingConsumerConfig.GroupIdPattern))
                throw new RequireConfigurationException(Resolver.GroupIdPattern);

            if (string.IsNullOrEmpty(resolvingConsumerConfig.TopicResolver))
                throw new RequireConfigurationException(Resolver.TopicResolver);

            if (string.IsNullOrEmpty(resolvingConsumerConfig.TopicPattern))
                throw new RequireConfigurationException(Resolver.TopicPattern);

            if (string.IsNullOrEmpty(resolvingConsumerConfig.Environment))
                throw new RequireConfigurationException(Constants.ConfigurationKeys.Axual.Environment);

            if (string.IsNullOrEmpty(resolvingConsumerConfig.Tenant))
                throw new RequireConfigurationException(Constants.ConfigurationKeys.Axual.Tenant);

            if (string.IsNullOrEmpty(resolvingConsumerConfig.Instance))
                throw new RequireConfigurationException(Constants.ConfigurationKeys.Axual.Instance);

            return true;
        }

        public override IConsumer<TKey, TValue> Build()
        {
            var resolvingConsumerConfig = new ResolvingConsumerConfig(ConsumerConfig);

            Validate();
            SetNextBuilderSetters();

            var topicResolver = _patternResolverFactory.CreateResolver(
                resolvingConsumerConfig.TopicResolver, resolvingConsumerConfig);

            var groupIdResolver = _patternResolverFactory.CreateResolver(
                resolvingConsumerConfig.GroupIdResolver, resolvingConsumerConfig);
            ConsumerConfig.GroupId = groupIdResolver.Resolve(resolvingConsumerConfig.ApplicationId);

            CurrentConsumer =
                new ResolvingConsumer<TKey, TValue>(NextConsumerBuilder, resolvingConsumerConfig, topicResolver);

            return CurrentConsumer;
        }

        public override IConsumer<TKey, TValue> Rebuild(ConsumerConfig consumerConfig)
        {
            ConsumerConfig = consumerConfig;
            return Build();
        }
    }
}