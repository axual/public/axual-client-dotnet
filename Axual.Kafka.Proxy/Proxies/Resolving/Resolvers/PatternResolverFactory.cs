//
// Copyright 2020 Axual B.V. 2020
//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

using System;
using System.Collections.Generic;
using System.IO;
using Confluent.Kafka;

namespace Axual.Kafka.Proxy.Proxies.Resolving.Resolvers
{
    internal class PatternResolverFactory : IResolverFactory
    {
        // the following class have to be public and extends IResolver, otherwise the Activator.CreateInstance
        // will failed.
        private readonly Dictionary<string, Type> _resolvers = new Dictionary<string, Type>
        {
            {"GroupPatternResolver", typeof(GroupPatternResolver)},
            {"LegacyGroupResolver", typeof(LegacyGroupResolver)},
            {"TopicPatternResolver", typeof(TopicPatternResolver)},
            {"TransactionalIdPatternResolver", typeof(TransactionalIdPatternResolver)}
        };

        public IResolver CreateResolver(string className, ClientConfig clientConfig)
        {
            IResolver resolver;

            if (!_resolvers.ContainsKey(className))
                throw new FileLoadException($"Couldn't find Resolver Class '{className}'");

            try
            {
                var classType = _resolvers[className];
                resolver = (IResolver) Activator.CreateInstance(classType, clientConfig);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Unable to start resolver '{className}'.", ex);
                throw;
            }

            return resolver;
        }
    }
}