//
// Copyright 2020 Axual B.V. 2020
//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

using static Axual.Kafka.Proxy.Proxies.Constants.ConfigurationKeys.Resolver;

namespace Axual.Kafka.Proxy.Proxies.Resolving.Resolvers
{
    public class TopicPatternResolver : AbstractPatternResolver
    {
        private readonly object _lockObj = new object();
        
        public TopicPatternResolver(ResolvingConsumerConfig resolvingConfig)
            : base(resolvingConfig, TopicResolverKey, resolvingConfig.TopicPattern)
        {
        }

        public TopicPatternResolver(ResolvingProducerConfig resolvingConfig)
            : base(resolvingConfig, TopicResolverKey, resolvingConfig.TopicPattern)
        {
        }

        public override string Resolve(string unResolvedString)
        {
            lock (_lockObj)
            {
                ResolverContext[TopicResolverKey] = unResolvedString;
                return base.Resolve(unResolvedString);
            }
        }
    }
}