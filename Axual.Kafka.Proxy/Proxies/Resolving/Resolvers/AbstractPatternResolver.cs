//
// Copyright 2020 Axual B.V. 2020
//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using Axual.Kafka.Proxy.Exceptions;
using Axual.Kafka.Proxy.Exceptions.Resolving;
using Confluent.Kafka;
using static Axual.Kafka.Proxy.Proxies.Constants.ConfigurationKeys.Kafka;
using static Axual.Kafka.Proxy.Proxies.Constants.ConfigurationKeys.Resolver;

namespace Axual.Kafka.Proxy.Proxies.Resolving.Resolvers
{
    public abstract class AbstractPatternResolver : IResolver
    {
        private const string LegalNamePattern = @"([-.\w]+)";
        private const string LegalNameWithoutHyphenPattern = @"([.\w]+)";

        private const char PatternSeparator = '-';
        private const char OpenBracket = '{';
        private const char CloseBracket = '}';
        private static readonly string PlaceholderRegexPattern = $@"\{OpenBracket}{LegalNamePattern}\{CloseBracket}";

        private readonly string _pattern;
        private readonly List<string> _patternKeysByOrder;
        private readonly Regex _resolvedPatternRegex;
        private readonly string _resolveKey;

        protected AbstractPatternResolver(ClientConfig resolverContext, string resolveKey, string pattern)
        {
            if (!IsValidPattern(pattern)) 
                throw new InvalidPatternException(pattern);
            
            _resolveKey = resolveKey;
            _pattern = pattern;
            _patternKeysByOrder = new Regex(LegalNameWithoutHyphenPattern).Matches(pattern).Cast<Match>()
                .Select(m => m.Value)
                .ToList();
            _resolvedPatternRegex = BuildResolvedPatternRegex(_patternKeysByOrder);
            ResolverContext = GetPatternKeysFromKafkaConfig(resolverContext);

            // In case the resolverKey is not from Kafka Configuration (resolverContext) add an empty key
            if (!ResolverContext.ContainsKey(resolveKey))
                ResolverContext.Add(resolveKey, null);

            VerifyAllPatternKeysExist(ResolverContext, _patternKeysByOrder);
        }

        protected IDictionary<string, string> ResolverContext { get; }

        /// <summary>
        ///     Getting the user friendly Stream name base on the pattern
        /// </summary>
        /// <param name="unResolvedString">User friendly stream name</param>
        /// <returns>The Stream name in Kafka</returns>
        public virtual string Resolve(string unResolvedString)
        {
            string result;
            try
            {
                // replace each placeholder with the value from the dictionary
                // e.g. the _pattern = {tenant}-{environment}-{topic} with
                // the _placeholderRegexPattern '\{([-.\w]+)\}'
                // will replace each value between the '{' and '}' with the dictionary value.
                result = new Regex(PlaceholderRegexPattern, RegexOptions.Compiled)
                    .Replace(_pattern,
                        match => ResolverContext[match.Groups[1].Value]);
            }
            catch (Exception innerException)
            {
                throw new ResolvingException(unResolvedString, _pattern, innerException);
            }

            return result;
        }

        public IEnumerable<string> Resolve(IEnumerable<string> unResolvedStrings)
        {
            return unResolvedStrings.Select(Resolve);
        }

        /// <summary>
        ///     Getting the Kafka Stream name according the pattern.
        /// </summary>
        /// <param name="resolvedString">The Stream name in Kafka</param>
        /// <returns>User friendly Stream name</returns>
        /// <exception cref="UnResolvingException"></exception>
        public virtual string UnResolve(string resolvedString)
        {
            var resolvedValuesByOrder = _resolvedPatternRegex.Split(resolvedString)
                .Where(s=> !string.IsNullOrEmpty(s)).ToList();

            if (_patternKeysByOrder.Count != resolvedValuesByOrder.Count)
                throw new UnResolvingException(resolvedString, _pattern);

            return _patternKeysByOrder.Zip(resolvedValuesByOrder, (key, value) =>
                    new {key, value})
                .ToDictionary(record =>
                        record.key, record => record.value,
                    StringComparer.OrdinalIgnoreCase)[_resolveKey];
        }

        public IEnumerable<string> UnResolve(IEnumerable<string> resolvedStrings)
        {
            return resolvedStrings.Select(UnResolve);
        }

        private static void VerifyAllPatternKeysExist(IDictionary<string, string> resolverContext,
            IEnumerable<string> patternKeysByOrder)
        {
            var firstMissingConfig =
                patternKeysByOrder.Except(resolverContext.Keys, StringComparer.OrdinalIgnoreCase).FirstOrDefault();
            if (firstMissingConfig != null)
                throw new RequireConfigurationException(firstMissingConfig);
        }

        private static Regex BuildResolvedPatternRegex(IReadOnlyCollection<string> patternKeysByOrder)
        {
            var stringBuilder = new StringBuilder();
            var lastPlaceholder = patternKeysByOrder.Last();

            foreach (var placeholder in patternKeysByOrder)
            {
                switch (placeholder)
                {
                    case TransactionalIdKey:
                    case GroupIdResolverKey:
                    case TopicResolverKey:
                        stringBuilder.Append(LegalNamePattern);
                        break;
                    default:
                        stringBuilder.Append(LegalNameWithoutHyphenPattern);
                        break;
                }

                if (placeholder != lastPlaceholder) stringBuilder.Append(PatternSeparator);
            }

            return new Regex(stringBuilder.ToString(), RegexOptions.Compiled);
        }

        private static bool IsValidPattern(string pattern)
        {
            var count = 0;

            if (pattern == null || pattern[0] != OpenBracket)
                return false;

            // check the if the open and close brackets are correct
            foreach (var currentChar in pattern)
                switch (currentChar)
                {
                    case OpenBracket:
                        count++;
                        break;
                    case CloseBracket when count == 0:
                        return false;
                    case CloseBracket:
                        count--;
                        break;
                }

            return count == 0;
        }


        /// <param name="clientConfig"></param>
        /// <returns>
        ///     Dictionary with the ResolvingKeyAttribute value of the pattern instead of Kafka/Axual key
        /// </returns>
        private static Dictionary<string, string> GetPatternKeysFromKafkaConfig(
            ClientConfig clientConfig)
        {
            return clientConfig.GetType().GetRuntimeProperties()
                .Where(p => p.GetCustomAttribute<ResolvingKeyAttribute>() != null)
                .Select(p => new
                {
                    key = p.GetCustomAttribute<ResolvingKeyAttribute>()?.Key,
                    value = p.GetValue(clientConfig)?.ToString()
                })
                .ToDictionary(
                    p => p.key,
                    p => p.value,
                    StringComparer.OrdinalIgnoreCase);
        }
    }
}
