//
// Copyright 2020 Axual B.V. 2020
//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

using System;
using Confluent.Kafka;

namespace Axual.Kafka.Proxy.Proxies
{
    public interface IProducerBuilder<TKey, TValue> : IClientBuilder
    {
        ProducerConfig ProducerConfig { get; }

        /// <summary>  Rebuild a new IProducer implementation instance base on the producerConfig parameter. </summary>
        IProducer<TKey, TValue> Rebuild(ProducerConfig producerConfig);

        /// <summary>
        ///     Build a new IProducer implementation instance.
        /// </summary>
        new IProducer<TKey, TValue> Build();

        /// <summary>
        ///     The serializer to use to serialize keys.
        /// </summary>
        IProducerBuilder<TKey, TValue> SetKeySerializer(ISerializer<TKey> keySerializer);

        /// <summary>
        ///     The async serializer to use to serialize keys.
        /// </summary>
        /// <remarks> This function is temporarily translated to synchronize.</remarks>
        IProducerBuilder<TKey, TValue> SetKeySerializer(IAsyncSerializer<TKey> asyncKeySerializer);
        
        /// <summary>
        ///     The serializer to use to serialize values.
        /// </summary>
        IProducerBuilder<TKey, TValue> SetValueSerializer(ISerializer<TValue> valueSerializer);

        /// <summary>
        ///     The async serializer to use to serialize values.
        /// </summary>
        /// <remarks> This function is temporarily translated to synchronize.</remarks>
        IProducerBuilder<TKey, TValue> SetValueSerializer(IAsyncSerializer<TValue> asyncValueSerializer);

        /// <summary>
        ///     Set the handler to call when there is information available
        ///     to be logged. If not specified, a default callback that writes
        ///     to stderr will be used.
        /// </summary>
        /// <remarks>
        ///     By default not many log messages are generated.
        ///     For more verbose logging, specify one or more debug contexts
        ///     using the 'debug' configuration property.
        ///     Warning: Log handlers are called spontaneously from internal
        ///     librdkafka threads and the application must not call any
        ///     Confluent.Kafka APIs from within a log handler or perform any
        ///     prolonged operations.
        /// </remarks>
        IProducerBuilder<TKey, TValue> SetLogHandler(Action<IProducer<TKey, TValue>, LogMessage> logHandler);

        /// <summary>
        ///     Set the handler to call on error events e.g. connection failures or all
        ///     brokers down. Note that the client will try to automatically recover from
        ///     errors that are not marked as fatal. Non-fatal errors should be interpreted
        ///     as informational rather than catastrophic.
        /// </summary>
        /// <remarks>
        ///     Executes on the poll thread (by default, a background thread managed by
        ///     the producer).
        /// </remarks>
        IProducerBuilder<TKey, TValue> SetErrorHandler(Action<IProducer<TKey, TValue>, Error> errorHandler);

        /// <summary>
        ///     Set the handler to call on statistics events. Statistics are provided as
        ///     a JSON formatted string as defined here:
        ///     https://github.com/edenhill/librdkafka/blob/master/STATISTICS.md
        /// </summary>
        /// <remarks>
        ///     You can enable statistics and set the statistics interval
        ///     using the statistics.interval.ms configuration parameter
        ///     (disabled by default).
        ///     Executes on the poll thread (by default, a background thread managed by
        ///     the producer).
        /// </remarks>
        IProducerBuilder<TKey, TValue> SetStatisticsHandler(Action<IProducer<TKey, TValue>, string> statisticsHandler);
    }
}