//
// Copyright 2020 Axual B.V. 2020
//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

using Axual.Kafka.Proxy.Proxies.Abstract;
using Axual.Kafka.Proxy.ValueHeader;
using Confluent.Kafka;
using static Axual.Kafka.Proxy.ValueHeader.Constants.AxualMessage;

namespace Axual.Kafka.Proxy.Proxies.Lineage
{
    internal class LineageProducer<TKey, TValue> : AbstractProducer<TKey, TValue>
    {
        private readonly LineageProducerConfig _lineageProducerConfig;

        internal LineageProducer(IProducerBuilder<TKey, TValue> nextProducerBuilder, ProducerConfig producerConfig)
            : base(nextProducerBuilder.Rebuild(producerConfig))
        {
            _lineageProducerConfig = new LineageProducerConfig(producerConfig);
        }

        protected override Message<TKey, TValue> AddLayerResponsibility(Message<TKey, TValue> message)
        {
            if (message == null)
                return null;

            var messageHeaders =
                message.Headers == null ? new Confluent.Kafka.Headers() : message.Headers.Clone();

            messageHeaders.Add(new Header(EnumProducerHeadersKey.MessageId.GetDescription(),
                new AxualMessageId().Serialize()));

            messageHeaders.Add(EnumProducerHeadersKey.ProducerId.GetDescription(),
                _lineageProducerConfig.ApplicationId);

            messageHeaders.Add(EnumProducerHeadersKey.ProducerVersion.GetDescription(),
                _lineageProducerConfig.ApplicationVersion);
            messageHeaders.Add(EnumProducerHeadersKey.IntermediateId.GetDescription(),
                _lineageProducerConfig.ApplicationId);
            messageHeaders.Add(EnumProducerHeadersKey.IntermediateVersion.GetDescription(),
                _lineageProducerConfig.ApplicationVersion);

            messageHeaders.Add(new Header(EnumProducerHeadersKey.SerializationTime.GetDescription(),
                AxualTime.Now.Serialize()));
            messageHeaders.Add(new Header(EnumProducerHeadersKey.CopyFlags.GetDescription(),
                AxualCopyFlag.OriginalMessage.Serialize()));

            messageHeaders.Add(EnumProducerHeadersKey.System.GetDescription(),
                _lineageProducerConfig.System);
            messageHeaders.Add(EnumProducerHeadersKey.Instance.GetDescription(),
                _lineageProducerConfig.Instance);
            messageHeaders.Add(EnumProducerHeadersKey.Tenant.GetDescription(),
                _lineageProducerConfig.Tenant);
            messageHeaders.Add(EnumProducerHeadersKey.Environment.GetDescription(),
                _lineageProducerConfig.Environment);
            messageHeaders.Add(EnumProducerHeadersKey.Cluster.GetDescription(),
                _lineageProducerConfig.Cluster);

            message.Headers = messageHeaders;
            return message;
        }
    }
}