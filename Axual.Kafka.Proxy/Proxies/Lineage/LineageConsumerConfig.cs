//
// Copyright 2020 Axual B.V. 2020
//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

using Confluent.Kafka;
using static Axual.Kafka.Proxy.Proxies.Constants.ConfigurationKeys;

namespace Axual.Kafka.Proxy.Proxies.Lineage
{
    internal class LineageConsumerConfig : ConsumerConfig
    {
        internal LineageConsumerConfig(ClientConfig consumerConfig) : base(consumerConfig)
        {
        }


        internal string ApplicationId
        {
            get => Get(ClientConfigMapper[Constants.ConfigurationKeys.Axual.ApplicationId]);
            set => Set(ClientConfigMapper[Constants.ConfigurationKeys.Axual.ApplicationId], value);
        }


        internal string ApplicationVersion
        {
            get => this.Get(ClientConfigMapper[Constants.ConfigurationKeys.Axual.ApplicationVersion], "unknown");
            set => Set(ClientConfigMapper[Constants.ConfigurationKeys.Axual.ApplicationVersion], value);
        }


        internal string Tenant
        {
            get => Get(ClientConfigMapper[Constants.ConfigurationKeys.Axual.Tenant]);
            set => Set(ClientConfigMapper[Constants.ConfigurationKeys.Axual.Tenant], value);
        }


        internal string Environment
        {
            get => Get(ClientConfigMapper[Constants.ConfigurationKeys.Axual.Environment]);
            set => Set(ClientConfigMapper[Constants.ConfigurationKeys.Axual.Environment], value);
        }


        internal string Instance
        {
            get => Get(ClientConfigMapper[Constants.ConfigurationKeys.Axual.Instance]);
            set => Set(ClientConfigMapper[Constants.ConfigurationKeys.Axual.Instance], value);
        }


        internal string System
        {
            get => Get(ClientConfigMapper[Constants.ConfigurationKeys.Axual.System]);
            set => Set(ClientConfigMapper[Constants.ConfigurationKeys.Axual.System], value);
        }


        internal string Cluster
        {
            get => Get(ClientConfigMapper[Constants.ConfigurationKeys.Axual.Cluster]);
            set => Set(ClientConfigMapper[Constants.ConfigurationKeys.Axual.Cluster], value);
        }
    }
}