//
// Copyright 2020 Axual B.V. 2020
//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

using Axual.Kafka.Proxy.Proxies.Abstract;
using Axual.Kafka.Proxy.ValueHeader;
using Confluent.Kafka;
using static Axual.Kafka.Proxy.ValueHeader.Constants.AxualMessage;

namespace Axual.Kafka.Proxy.Proxies.Lineage
{
    internal class LineageConsumer<TKey, TValue> : AbstractConsumer<TKey, TValue>
    {
        private readonly LineageConsumerConfig _lineageConsumerConfig;

        internal LineageConsumer(IConsumerBuilder<TKey, TValue> nextConsumerBuilder, ConsumerConfig consumerConfig)
            : base(nextConsumerBuilder.Rebuild(consumerConfig))
        {
            _lineageConsumerConfig = new LineageConsumerConfig(consumerConfig);
        }

        protected override ConsumeResult<TKey, TValue> AddLayerResponsibility(ConsumeResult<TKey, TValue> consumeResult)
        {
            if (consumeResult == null)
                return null;
            
            var messageHeaders = consumeResult.Message.Headers == null
                ? new Confluent.Kafka.Headers()
                : consumeResult.Message.Headers.Clone();

            // Reset the Deserializer time
            messageHeaders.Add(new Header(EnumConsumerHeadersKey.DeserializationTime.GetDescription(),
                AxualTime.Now.Serialize()), true);

            // Set the functional lineage context
            messageHeaders.Add(EnumConsumerHeadersKey.Environment.GetDescription(),
                _lineageConsumerConfig.Environment);
            messageHeaders.Add(EnumConsumerHeadersKey.Tenant.GetDescription(),
                _lineageConsumerConfig.Tenant);

            // Set the technical lineage context
            messageHeaders.Add(EnumConsumerHeadersKey.IntermediateId.GetDescription(),
                _lineageConsumerConfig.ApplicationId, true);
            messageHeaders.Add(EnumConsumerHeadersKey.IntermediateVersion.GetDescription(),
                _lineageConsumerConfig.ApplicationVersion, true);
            messageHeaders.Add(EnumConsumerHeadersKey.System.GetDescription(),
                _lineageConsumerConfig.System);
            messageHeaders.Add(EnumConsumerHeadersKey.Instance.GetDescription(),
                _lineageConsumerConfig.Instance);
            messageHeaders.Add(EnumConsumerHeadersKey.Cluster.GetDescription(),
                _lineageConsumerConfig.Cluster);

            consumeResult.Message.Headers = messageHeaders;
            return consumeResult;
        }
    }
}