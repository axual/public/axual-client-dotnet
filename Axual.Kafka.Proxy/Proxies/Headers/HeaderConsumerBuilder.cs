//
// Copyright 2020 Axual B.V. 2020
//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

using Axual.Kafka.Proxy.Proxies.Abstract;
using Axual.Kafka.Proxy.SerDes;
using Confluent.Kafka;

namespace Axual.Kafka.Proxy.Proxies.Headers
{
    internal class HeaderConsumerBuilder<TKey, TValue> : AbstractConsumerBuilder<TKey, TValue>
    {
        public HeaderConsumerBuilder(ConsumerConfig consumerConfig, IConsumerBuilder<TKey, TValue> nextConsumerBuilder)
            : base(consumerConfig, nextConsumerBuilder)
        {
        }

        public override bool Validate()
        {
            return true;
        }

        public override IConsumer<TKey, TValue> Build()
        {
            Validate();

            if (new HeaderConsumerConfig(ConsumerConfig).EnableValueHeaders)
            {
                if (ValueDeserializer != null)
                    SetValueDeserializer(new ValueHeaderDeserializer<TValue>(ValueDeserializer));

                if (AsyncValueDeserializer != null)
                {
                    SetValueDeserializer(new ValueHeaderDeserializer<TValue>(AsyncValueDeserializer));
                    AsyncValueDeserializer = null;
                }
            }

            SetNextBuilderSetters();
            CurrentConsumer = new HeaderConsumer<TKey, TValue>(NextConsumerBuilder, ConsumerConfig);
            return CurrentConsumer;
        }

        public override IConsumer<TKey, TValue> Rebuild(ConsumerConfig consumerConfig)
        {
            ConsumerConfig = consumerConfig;
            return Build();
        }
    }
}