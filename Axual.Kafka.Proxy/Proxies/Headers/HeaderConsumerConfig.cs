//
// Copyright 2020 Axual B.V. 2020
//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

using Confluent.Kafka;
using static Axual.Kafka.Proxy.Proxies.Constants.ConfigurationKeys;

namespace Axual.Kafka.Proxy.Proxies.Headers
{
    internal class HeaderConsumerConfig : ConsumerConfig
    {
        public HeaderConsumerConfig(ClientConfig consumerConfig) : base(consumerConfig)
        {
        }

        /// <summary>
        ///     Add a wrapper of axual flags around the message value.
        ///     default: false
        /// </summary>

        public bool EnableValueHeaders
        {
            get => GetBool(ClientConfigMapper[Constants.ConfigurationKeys.Headers.EnableValueHeaders])
                .GetValueOrDefault(false);
            set => SetObject(ClientConfigMapper[Constants.ConfigurationKeys.Headers.EnableValueHeaders], value);
        }
    }
}