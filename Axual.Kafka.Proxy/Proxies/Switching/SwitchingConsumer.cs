//
// Copyright 2020 Axual B.V. 2020
//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Axual.Kafka.Proxy.DiscoveryFetcher;
using Axual.Kafka.Proxy.Exceptions.Switching;
using Axual.Kafka.Proxy.Proxies.Switching.Proxy;
using Confluent.Kafka;

namespace Axual.Kafka.Proxy.Proxies.Switching
{
    internal class SwitchingConsumer<TKey, TValue> : IConsumer<TKey, TValue>
    {
        private readonly ISwitcher _clusterSwitcher;
        private readonly IConsumerBuilder<TKey, TValue> _nextConsumerBuilder;

        private readonly ReaderWriterLockSlim _readerWriterLockSlimSwitching;
        private readonly AutoResetEvent _waitToInit = new AutoResetEvent(false);
        private Utils.NewDiscoveryConfigEventArgs _discoveryConfigEvent;
        private InitializeException _initializeException;
        private bool _isInitClientWithDiscoveryConfiguration;
        private bool _needToSwitch;
        private IConsumer<TKey, TValue> _nextConsumer;
        private ConsumerConfig _consumerConfig;
        private SwitchException _switchException;

        internal SwitchingConsumer(IConsumerBuilder<TKey, TValue> nextConsumerBuilder,
            ConsumerConfig switchingConsumerConfig, ISwitcher clusterSwitcher)
        {
            _isInitClientWithDiscoveryConfiguration = false;
            _needToSwitch = false;
            Subscription = new List<string>();
            Assignment = new List<TopicPartition>();

            _nextConsumerBuilder = nextConsumerBuilder;
            _consumerConfig = switchingConsumerConfig;
            _clusterSwitcher = clusterSwitcher;

            _readerWriterLockSlimSwitching = new ReaderWriterLockSlim();

            try
            {
                _clusterSwitcher.OnFirstDiscoveryResult += InitInnerConsumer;
                _clusterSwitcher.OnSwitchNeeded += NeedToSwitch;
                _clusterSwitcher.StartListening();
            }
            catch (Exception ex)
            {
                _switchException = new ConsumerSwitchException(ex);
                throw;
            }
        }

        public int AddBrokers(string brokers)
        {
            return RunActionWithReadLockSwitching(() => _nextConsumer.AddBrokers(brokers));
        }

        public ConsumeResult<TKey, TValue> Consume(int millisecondsTimeout)
        {
            PerformSwitchIfGetNewConfiguration();
            return RunActionWithReadLockSwitching(() => _nextConsumer.Consume(millisecondsTimeout));
        }

        public ConsumeResult<TKey, TValue> Consume(CancellationToken cancellationToken = new CancellationToken())
        {
            ConsumeResult<TKey, TValue> consumeResult = null;

            while (consumeResult == null)
            {
                cancellationToken.ThrowIfCancellationRequested();
                consumeResult = this.Consume(TimeSpan.FromSeconds(10));
            }
            return consumeResult;
        }

        public ConsumeResult<TKey, TValue> Consume(TimeSpan timeout)
        {
            PerformSwitchIfGetNewConfiguration();
            return RunActionWithReadLockSwitching(() => _nextConsumer.Consume(timeout));
        }

        public List<string> Subscription { get; private set; }

        public void Subscribe(IEnumerable<string> topics)
        {
            RunActionWithReadLockSwitching(
                () =>
                {
                    _nextConsumer.Subscribe(topics);
                    Subscription.AddRange(topics);
                });
        }

        public void Subscribe(string topic)
        {
            RunActionWithReadLockSwitching(() =>
            {
                if (!_nextConsumer.Assignment.IsNullOrEmpty())
                    throw new InvalidOperationException(
                        "Subscribe to topic when manual partition assignment is active");
                _nextConsumer.Subscribe(topic);
                Subscription.Add(topic);
            });
        }

        public List<TopicPartition> Assignment { get; }

        public void Assign(TopicPartition partition)
        {
            RunActionWithReadLockSwitching(() =>
            {
                if (!_nextConsumer.Subscription.IsNullOrEmpty())
                    throw new InvalidOperationException(
                        "Manual partition assignment when a subscription is active");

                if (partition == null) InnerUnassign();

                _nextConsumer.Assign(partition);
                Assignment.Add(partition);
            });
        }

        public void Assign(TopicPartitionOffset partition)
        {
            RunActionWithReadLockSwitching(() =>
            {
                if (_nextConsumer.Subscription.Any())
                    throw new InvalidOperationException(
                        "Manual partition assignment when a subscription is active");

                if (partition == null)
                    InnerUnassign();

                _nextConsumer.Assign(partition);
            });
        }

        public void Assign(IEnumerable<TopicPartitionOffset> partitions)
        {
            RunActionWithReadLockSwitching(() =>
            {
                if (_nextConsumer.Subscription.Any())
                    throw new InvalidOperationException(
                        "Manual partition assignment when a subscription is active");

                if (partitions.IsNullOrEmpty())
                    InnerUnassign();

                _nextConsumer.Assign(partitions);
            });
        }

        public void Assign(IEnumerable<TopicPartition> partitions)
        {
            RunActionWithReadLockSwitching(() =>
            {
                if (_nextConsumer.Subscription.Any())
                    throw new InvalidOperationException(
                        "Manual partition assignment when a subscription is active");

                if (partitions == null || !partitions.Any())
                {
                    InnerUnassign();
                    return;
                }

                _nextConsumer.Assign(partitions);
                Assignment.AddRange(partitions);
            });
        }

        public void IncrementalAssign(IEnumerable<TopicPartitionOffset> partitions)
        {
            RunActionWithReadLockSwitching(() => _nextConsumer.IncrementalAssign(partitions));
        }

        public void IncrementalAssign(IEnumerable<TopicPartition> partitions)
        {
            RunActionWithReadLockSwitching(() => _nextConsumer.IncrementalAssign(partitions));
        }

        public void IncrementalUnassign(IEnumerable<TopicPartition> partitions)
        {
            RunActionWithReadLockSwitching(() => _nextConsumer.IncrementalUnassign(partitions));
        }

        public void StoreOffset(ConsumeResult<TKey, TValue> result)
        {
            RunActionWithReadLockSwitching(() => _nextConsumer.StoreOffset(result));
        }

        public void StoreOffset(TopicPartitionOffset offset)
        {
            RunActionWithReadLockSwitching(() => _nextConsumer.StoreOffset(offset));
        }

        public List<TopicPartitionOffset> Commit()
        {
            return RunActionWithReadLockSwitching(_nextConsumer.Commit);
        }

        public void Commit(IEnumerable<TopicPartitionOffset> offsets)
        {
            RunActionWithReadLockSwitching(() => _nextConsumer.Commit(offsets));
        }

        public void Commit(ConsumeResult<TKey, TValue> result)
        {
            RunActionWithReadLockSwitching(() => _nextConsumer.Commit(result));
        }

        public void Seek(TopicPartitionOffset tpo)
        {
            RunActionWithReadLockSwitching(() => _nextConsumer.Seek(tpo));
        }

        public void Pause(IEnumerable<TopicPartition> partitions)
        {
            RunActionWithReadLockSwitching(() => _nextConsumer.Pause(partitions));
        }

        public void Resume(IEnumerable<TopicPartition> partitions)
        {
            RunActionWithReadLockSwitching(() => _nextConsumer.Resume(partitions));
        }

        public List<TopicPartitionOffset> Committed(TimeSpan timeout)
        {
            return RunActionWithReadLockSwitching(() => _nextConsumer.Committed(timeout));
        }

        public List<TopicPartitionOffset> Committed(IEnumerable<TopicPartition> partitions, TimeSpan timeout)
        {
            return RunActionWithReadLockSwitching(() => _nextConsumer.Committed(partitions, timeout));
        }

        public Offset Position(TopicPartition partition)
        {
            return RunActionWithReadLockSwitching(() => _nextConsumer.Position(partition));
        }

        public List<TopicPartitionOffset> OffsetsForTimes(IEnumerable<TopicPartitionTimestamp> timestampsToSearch,
            TimeSpan timeout)
        {
            return RunActionWithReadLockSwitching(() => _nextConsumer.OffsetsForTimes(timestampsToSearch, timeout));
        }

        public WatermarkOffsets GetWatermarkOffsets(TopicPartition topicPartition)
        {
            return RunActionWithReadLockSwitching(() => _nextConsumer.GetWatermarkOffsets(topicPartition));
        }

        public WatermarkOffsets QueryWatermarkOffsets(TopicPartition topicPartition, TimeSpan timeout)
        {
            return RunActionWithReadLockSwitching(() => _nextConsumer.QueryWatermarkOffsets(topicPartition, timeout));
        }

        public string MemberId => RunActionWithReadLockSwitching(() => _nextConsumer.MemberId);
        public IConsumerGroupMetadata ConsumerGroupMetadata => _nextConsumer.ConsumerGroupMetadata;
        public Handle Handle => RunActionWithReadLockSwitching(() => _nextConsumer.Handle);
        public string Name => RunActionWithReadLockSwitching(() => _nextConsumer.Name);

        public void Unsubscribe()
        {
            RunActionWithReadLockSwitching(InnerUnsubscribe);
        }

        public void Unsubscribe(bool force)
        {
            WaitToInitToComplete();

            if (force)
                InnerUnsubscribe();
            else
                Unsubscribe();
        }

        private void InnerUnsubscribe()
        {
            _nextConsumer.Unsubscribe();
            Subscription.Clear();
        }

        public void Unassign(bool force)
        {
            WaitToInitToComplete();

            if (force)
                InnerUnassign();
            else
                Unassign();
        }
        
        public void Unassign()
        {
            RunActionWithReadLockSwitching(InnerUnassign);
        }

        private void InnerUnassign()
        {
            _nextConsumer.Unassign();
            Assignment.Clear();
        }

        public void Close(bool force)
        {
            WaitToInitToComplete();

            if (force)
                InnerClose();
            else
                Close();
        }
        
        public void Close()
        {
            RunActionWithReadLockSwitching(InnerClose);
        }

        private void InnerClose()
        {
            _nextConsumer.Close();
        }
        
        public void Dispose(bool force)
        {
            WaitToInitToComplete();

            if (force)
                InnerDispose();
            else
                Dispose();
        }
        
        public void Dispose()
        {
            RunActionWithReadLockSwitching(InnerDispose);
        }

        private void InnerDispose()
        {
            _nextConsumer.Dispose();
            _clusterSwitcher.Dispose();
        }

        private void InitInnerConsumer(object sender, DiscoveryResult firstDiscoveryResult)
        {
            try
            {
                _clusterSwitcher.OnFirstDiscoveryResult -= InitInnerConsumer;
                _consumerConfig = new ConsumerConfig(_consumerConfig.UpdateConfiguration(firstDiscoveryResult));
                _nextConsumer = _nextConsumerBuilder.Rebuild(_consumerConfig);
                _isInitClientWithDiscoveryConfiguration = true;
            }
            catch (Exception ex)
            {
                _initializeException = new ConsumerInitializeException(ex);
                Console.WriteLine(ex.Message);
            }
            finally
            {
                _waitToInit.Set();
            }
        }

        private void NeedToSwitch(object sender, Utils.NewDiscoveryConfigEventArgs e)
        {
            _needToSwitch = true;
            _discoveryConfigEvent = e;
        }


        /// <summary>
        /// Rebuild the nextProducer to connect to the new Kafka Cluster
        /// </summary>
        /// <exception cref="SwitchException">When error occured while switching producer, see InnerException for more details</exception>
        private void PerformSwitchIfGetNewConfiguration()
        {
            if (!_needToSwitch)
                return;
            
            // Only one Thread enter here
            _readerWriterLockSlimSwitching.EnterWriteLock();

            // If other thread already switched
            if (!_needToSwitch)
            {
                _readerWriterLockSlimSwitching.ExitWriteLock();
                return;
            }

            try
            {
                var temp = new List<string>(Subscription);
                // Get new Consumer according the new configuration from the Discovery API
                _nextConsumer = (IConsumer<TKey, TValue>) _clusterSwitcher.Switch(this, _discoveryConfigEvent);
                Subscription = temp;

                _needToSwitch = false;
            }
            catch (Exception innerException)
            {
                _switchException = new ConsumerSwitchException(innerException);
                throw;
            }
            finally
            {
                _readerWriterLockSlimSwitching.ExitWriteLock();
            }
        }

        private void RunActionWithReadLockSwitching(Action action)
        {
            WaitToInitToComplete();

            _readerWriterLockSlimSwitching.EnterReadLock();
            try
            {
                // is switch failed
                if (_switchException != null) throw _switchException;

                action.Invoke();
            }
            finally
            {
                _readerWriterLockSlimSwitching.ExitReadLock();
            }
        }

        private TResult RunActionWithReadLockSwitching<TResult>(Func<TResult> func)
        {
            WaitToInitToComplete();

            _readerWriterLockSlimSwitching.EnterReadLock();
            try
            {
                // is switch failed
                if (_switchException != null) throw _switchException;

                return func();
            }
            finally
            {
                _readerWriterLockSlimSwitching.ExitReadLock();
            }
        }

        private void WaitToInitToComplete()
        {
            if (_isInitClientWithDiscoveryConfiguration)
                return;

            // wait for OnFirstDiscoveryResult function to end
            _waitToInit.WaitOne();
            if (_initializeException != null) throw _initializeException;
        }
    }
}