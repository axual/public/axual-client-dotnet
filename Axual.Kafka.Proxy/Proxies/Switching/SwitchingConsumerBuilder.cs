//
// Copyright 2020 Axual B.V. 2020
//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

using Axual.Kafka.Proxy.Exceptions;
using Axual.Kafka.Proxy.Proxies.Abstract;
using Axual.Kafka.Proxy.Proxies.Switching.Proxy;
using Confluent.Kafka;

namespace Axual.Kafka.Proxy.Proxies.Switching
{
    internal class SwitchingConsumerBuilder<TKey, TValue> : AbstractConsumerBuilder<TKey, TValue>
    {
        private ISwitcher _switcher;

        internal SwitchingConsumerBuilder(ConsumerConfig consumerConfig,
            IConsumerBuilder<TKey, TValue> nextConsumerBuilder, ISwitcher switcher = null)
            : base(consumerConfig, nextConsumerBuilder)
        {
            _switcher = switcher ?? new ConsumerSwitcher<TKey, TValue>(nextConsumerBuilder, consumerConfig);
        }

        public override bool Validate()
        {
            var switchingConsumerConfig = new SwitchingConsumerConfig(ConsumerConfig);

            if (switchingConsumerConfig.ApplicationId.IsNullOrEmpty())
                throw new RequireConfigurationException(Constants.ConfigurationKeys.Axual.ApplicationId);

            if (switchingConsumerConfig.EndPoint == null)
                throw new RequireConfigurationException(Constants.ConfigurationKeys.Axual.EndPoint);

            return true;
        }

        public override IConsumer<TKey, TValue> Build()
        {
            Validate();
            SetNextBuilderSetters();
            CurrentConsumer = new SwitchingConsumer<TKey, TValue>(NextConsumerBuilder, ConsumerConfig, _switcher);
            return CurrentConsumer;
        }

        public override IConsumer<TKey, TValue> Rebuild(ConsumerConfig consumerConfig)
        {
            ConsumerConfig = consumerConfig;
            _switcher = _switcher.Configure(consumerConfig, NextConsumerBuilder);
            return Build();
        }
    }
}