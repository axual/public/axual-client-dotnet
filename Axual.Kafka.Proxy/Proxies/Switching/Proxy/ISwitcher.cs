using System;
using Axual.Kafka.Proxy.DiscoveryFetcher;
using Confluent.Kafka;

namespace Axual.Kafka.Proxy.Proxies.Switching.Proxy
{
    internal interface ISwitcher : IDisposable
    {

        /// <summary>
        /// Start checking for cluster configuration from the Discovery API. <br />
        /// For first result raising 'OnFirstDiscoveryResult' event <br />
        /// In case of cluster switch raising 'OnSwitchNeeded' event <br />
        /// </summary>
        /// <remarks>Make sure you register to `OnSwitchNeeded` and `OnFirstFetch` event before invoking the method.</remarks>
        void StartListening();

        void StopListening();
        IClient Switch(IClient client, Utils.NewDiscoveryConfigEventArgs newDiscoveryConfigEventArgs);
        ISwitcher Configure(ClientConfig clientConfig, IClientBuilder clientBuilder);

        /// <summary>
        /// When the last and previous DiscoveryResult.Cluster difference
        /// </summary>
        event EventHandler<Utils.NewDiscoveryConfigEventArgs> OnSwitchNeeded;

        /// <summary>
        /// When the first DiscoveryResult from the server is available
        /// </summary>
        event EventHandler<DiscoveryResult> OnFirstDiscoveryResult;
    }
}