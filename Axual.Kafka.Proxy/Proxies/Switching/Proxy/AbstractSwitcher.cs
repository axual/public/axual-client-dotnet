//
// Copyright 2020 Axual B.V. 2020
//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

using System;
using System.Linq;
using System.Threading.Tasks;
using Axual.Kafka.Proxy.DiscoveryFetcher;
using Axual.Kafka.Proxy.Exceptions;
using Axual.Kafka.Proxy.Exceptions.Switching;
using Axual.Kafka.Proxy.HttpClient;
using Confluent.Kafka;
using static Axual.Kafka.Proxy.Proxies.Constants.ConfigurationKeys;

namespace Axual.Kafka.Proxy.Proxies.Switching.Proxy
{
    internal abstract class AbstractSwitcher<TKey, TValue> : ISwitcher
    {
        /// <summary>
        ///     Consumer/Producer 'ApplicationId'
        /// </summary>
        private readonly string _clientApplicationId;

        private readonly object _lockDiscoveryResult = new object();
        private readonly object _lockSwitching = new object();

        /// <summary>
        ///     The last Discovery Result received from the Discovery API
        /// </summary>
        private DiscoveryResult _lastDiscoveryResult;

        /// <summary>
        ///     Producer/Consumer Configuration
        /// </summary>
        protected ClientConfig ClientConfig;

        private readonly Func<IFetcher> _fetchFactory;
        private readonly ClientRegistry _clientRegistry;

        protected AbstractSwitcher(ClientConfig clientConfig,
            IFetcherFactory fetcherFactory, IDiscoveryRestService discoveryRestService, ClientRegistry clientRegistry = null)
        {
            ClientConfig = clientConfig;
            _clientApplicationId =
                clientConfig.Get(ClientConfigMapper[Constants.ConfigurationKeys.Axual.ApplicationId]);

            if (_clientApplicationId == null)
                throw new RequireConfigurationException(Constants.ConfigurationKeys.Axual.ApplicationId);

            // if 'fetcherFactory' null,  using FetcherFactory as default implementation for IFetcherFactory
            fetcherFactory ??= new FetcherFactory();
            // if 'discoveryRestService' null,  using DiscoveryRestService as default implementation for IDiscoveryRestService
            discoveryRestService ??= new DiscoveryRestService(new HttpClientFactory(),
                ClientConfig.ToDictionary(pair => pair.Key, pair => pair.Value));

            _fetchFactory = ()=> fetcherFactory.CreateFetcher(discoveryRestService);
            _clientRegistry= clientRegistry ?? ClientRegistry.Registry;
        }
        
        public IClient Switch(IClient client, Utils.NewDiscoveryConfigEventArgs newDiscoveryConfigEventArgs)
        {
            lock (_lockSwitching)
            {
                return client switch
                {
                    SwitchingConsumer<TKey, TValue> switchingConsumer =>
                        Switch(switchingConsumer, newDiscoveryConfigEventArgs),
                    SwitchingProducer<TKey, TValue> switchingProducer =>
                        Switch(switchingProducer, newDiscoveryConfigEventArgs),
                    _ => throw new InvalidOperationException($"Switch not support for type {client.GetType().FullName}")
                };
            }
        }

        public ISwitcher Configure(ClientConfig clientConfig, IClientBuilder clientBuilder)
        {
            return clientBuilder switch
            {
                IConsumerBuilder<TKey, TValue> consumerBuilder =>
                    Configure(clientConfig, consumerBuilder),
                IProducerBuilder<TKey, TValue> producerBuilder =>
                    Configure(clientConfig, producerBuilder),
                _ => null
            };
        }

        public event EventHandler<Utils.NewDiscoveryConfigEventArgs> OnSwitchNeeded;
        public event EventHandler<DiscoveryResult> OnFirstDiscoveryResult;


        public void StartListening()
        {
            if (_isDisposed)
                return;

            _clientRegistry.RegisterClientApplication(_clientApplicationId, _fetchFactory, OnNewResultFromDiscoveryApi);
        }

        public void StopListening()
        {
            _clientRegistry.UnregisterClientRegistration(_clientApplicationId, OnNewResultFromDiscoveryApi);
        }

        protected virtual IConsumer<TKey, TValue> Switch(SwitchingConsumer<TKey, TValue> consumer,
            Utils.NewDiscoveryConfigEventArgs newDiscoveryConfigEventArgs)
        {
            throw new NotImplementedException();
        }

        protected virtual IProducer<TKey, TValue> Switch(SwitchingProducer<TKey, TValue> producer,
            Utils.NewDiscoveryConfigEventArgs newDiscoveryConfigEventArgs)
        {
            throw new NotImplementedException();
        }

        protected virtual ISwitcher Configure(ClientConfig clientConfig, IConsumerBuilder<TKey, TValue> clientBuilder)
        {
            throw new NotImplementedException();
        }

        protected virtual ISwitcher Configure(ClientConfig clientConfig, IProducerBuilder<TKey, TValue> clientBuilder)
        {
            throw new NotImplementedException();
        }

        private void OnNewResultFromDiscoveryApi(object sender, DiscoveryResult newDiscoveryResult)
        {
            lock (_lockDiscoveryResult)
            {
                var oldConfiguration = _lastDiscoveryResult;
                _lastDiscoveryResult = newDiscoveryResult;

                if (newDiscoveryResult.Cluster.IsNullOrEmpty())
                    throw new NoClusterAvailableException("No Cluster Available.");

                // is First Fetch
                if (oldConfiguration == null)
                    Task.Run(() => OnFirstDiscoveryResult?.Invoke(this, newDiscoveryResult))
                        .ConfigureAwait(false);
                else if (!oldConfiguration.Cluster.Equals(newDiscoveryResult.Cluster))
                    Task.Run(() => OnSwitchNeeded?.Invoke(this,
                            new Utils.NewDiscoveryConfigEventArgs(oldConfiguration, newDiscoveryResult)))
                        .ConfigureAwait(false);
            }
        }

        protected virtual TimeSpan GetSwitchTimeout(DiscoveryResult newDiscoveryResult)
        {
            var alreadyWaited = MathUtils.Max(DateTime.UtcNow - newDiscoveryResult.TimeStamp, TimeSpan.Zero);
            var distributionDuration =
                TimeSpan.FromMilliseconds(
                    newDiscoveryResult.DistributorTimeout * newDiscoveryResult.DistributorDistance);
            return distributionDuration - alreadyWaited;
        }

        // Implement IDisposable.
        // Do not make this method virtual.
        public void Dispose()
        {
            Dispose(disposing: true);
            // This object will be cleaned up by the Dispose method.
            // Therefore, you should call GC.SuppressFinalize to
            // take this object off the finalization queue
            // and prevent finalization code for this object
            // from executing a second time.
            GC.SuppressFinalize(this);
        }

        // Dispose(bool disposing) executes in two distinct scenarios.
        // If disposing equals true, the method has been called directly
        // or indirectly by a user's code. Managed and unmanaged resources
        // can be disposed.
        // If disposing equals false, the method has been called by the
        // runtime from inside the finalizer and you should not reference
        // other objects. Only unmanaged resources can be disposed.
        protected virtual void Dispose(bool disposing)
        {
            if (disposing && !_isDisposed)
            {
                StopListening();
            }

            _isDisposed = true;
        }

        private bool _isDisposed;
    }
}