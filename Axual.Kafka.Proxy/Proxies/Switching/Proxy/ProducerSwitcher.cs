//
// Copyright 2020 Axual B.V. 2020
//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

using System;
using System.Threading.Tasks;
using Axual.Kafka.Proxy.DiscoveryFetcher;
using Confluent.Kafka;

namespace Axual.Kafka.Proxy.Proxies.Switching.Proxy
{
    internal class ProducerSwitcher<TKey, TValue> : AbstractSwitcher<TKey, TValue>
    {
        private IProducerBuilder<TKey, TValue> _nextProducerBuilder;

        public ProducerSwitcher(IProducerBuilder<TKey, TValue> nextProducerBuilder, ClientConfig producerConfig,
            IFetcherFactory fetcherFactory = null, IDiscoveryRestService discoveryRestService = null) :
            base(producerConfig, fetcherFactory, discoveryRestService)
        {
            _nextProducerBuilder = nextProducerBuilder;
        }

        protected override ISwitcher Configure(ClientConfig clientConfig, IProducerBuilder<TKey, TValue> clientBuilder)
        {
            ClientConfig = clientConfig;
            _nextProducerBuilder = clientBuilder;
            return this;
        }

        protected override IProducer<TKey, TValue> Switch(SwitchingProducer<TKey, TValue> producer,
            Utils.NewDiscoveryConfigEventArgs newDiscoveryConfigEventArgs)
        {
            var clientConfig = new SwitchingProducerConfig(ClientConfig);
            var isInitTransactions = producer.IsInitTransactions();
            TimeSpan? initTransactionsTimeout = null;
            
            if (isInitTransactions)
            {
                initTransactionsTimeout = producer.InitTransactionsTimeout;
            }

            // STEP 1: Close the old proxy object gracefully.
            var waitTime = GetSwitchTimeout(newDiscoveryConfigEventArgs.NewConfiguration);

            if (isInitTransactions)
            {
                try
                {
                    // is producer called once InitTransactions, aborting the transaction on the old 
                    producer.AbortTransaction(initTransactionsTimeout.Value, true);
                }
                catch (Exception exception)
                {
                    Console.WriteLine(exception);
                    // Continue in case of failure, we can't catch only for InvalidTxnState Error code (as the cluster could be down)
                }
            }

            try
            {
                producer.Dispose(true);
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
                // Continue even if didn't manage to dispose object
            }

            // STEP 2: Wait for Distributor to finish its job server-side (only if necessary).
            Task.Delay(waitTime).Wait();

            // STEP 3:
            // Initialize the new proxy and return it. 
            // And register again to the topics
            clientConfig.UpdateConfiguration(newDiscoveryConfigEventArgs.NewConfiguration);
            var newProducer = _nextProducerBuilder.Rebuild(clientConfig);

            if (isInitTransactions)
            {
                // Call InitTransactions on the new cluster.
                newProducer.InitTransactions(initTransactionsTimeout.Value);
            }
            
            ClientConfig = clientConfig;
            return newProducer;
        }

        protected override TimeSpan GetSwitchTimeout(DiscoveryResult newDiscoveryResult)
        {
            var timeout = TimeSpan.Zero;

            if (ClientConfig.MaxInFlight == 1)
                timeout = base.GetSwitchTimeout(newDiscoveryResult);

            return timeout;
        }
    }
}
