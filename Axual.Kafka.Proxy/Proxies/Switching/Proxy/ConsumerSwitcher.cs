//
// Copyright 2020 Axual B.V. 2020
//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Axual.Kafka.Proxy.DiscoveryFetcher;
using Confluent.Kafka;

namespace Axual.Kafka.Proxy.Proxies.Switching.Proxy
{
    internal class ConsumerSwitcher<TKey, TValue> : AbstractSwitcher<TKey, TValue>
    {
        private IConsumerBuilder<TKey, TValue> _nextConsumerBuilder;

        public ConsumerSwitcher(IConsumerBuilder<TKey, TValue> nextConsumerBuilder, ClientConfig consumerConfig,
            IFetcherFactory fetcherFactory = null, IDiscoveryRestService discoveryRestService = null) :
            base(consumerConfig, fetcherFactory, discoveryRestService)
        {
            _nextConsumerBuilder = nextConsumerBuilder;
        }

        protected override ISwitcher Configure(ClientConfig clientConfig, IConsumerBuilder<TKey, TValue> clientBuilder)
        {
            ClientConfig = clientConfig;
            _nextConsumerBuilder = clientBuilder;
            return this;
        }

        protected override IConsumer<TKey, TValue> Switch(SwitchingConsumer<TKey, TValue> consumer,
            Utils.NewDiscoveryConfigEventArgs newDiscoveryConfigEventArgs)
        {
            var isAssignOrSubscribed = consumer.Subscription.IsNullOrEmpty() || consumer.Assignment.IsNullOrEmpty();
            var switchingConsumerConfig = new SwitchingConsumerConfig(ClientConfig);

            // STEP 1:
            // Calculate the waitTime + close the old proxy object gracefully.
            var waitTime = GetSwitchTimeout(isAssignOrSubscribed, newDiscoveryConfigEventArgs.OldConfiguration,
                newDiscoveryConfigEventArgs.NewConfiguration);

            var originalAssignment = consumer.Assignment == null
                ? new List<TopicPartition>()
                : new List<TopicPartition>(consumer.Assignment);
            var originalSubscription = consumer.Subscription == null
                ? new List<string>()
                : new List<string>(consumer.Subscription);

            consumer.Unsubscribe(true);
            consumer.Unassign(true);
            consumer.Close(true);
            consumer.Dispose(true);

            // STEP 2:
            // Wait  for Distributor to finish its job server-side (only if necessary).
            Task.Delay(waitTime).Wait();

            // STEP 3:
            // Initialize the new proxy and return it. 
            // And register again to the topics
            switchingConsumerConfig.UpdateConfiguration(newDiscoveryConfigEventArgs.NewConfiguration);
            // LOG.info("Creating new backing {} with Discovery API result: {}", config.getProxyType(), newResult);

            var newConsumer = _nextConsumerBuilder.Rebuild(switchingConsumerConfig);

            UpdateAssignments(originalAssignment, newConsumer);
            UpdateSubscription(originalSubscription, newConsumer);

            // LOG.info("Created new backing {}", config.getProxyType());
            // LOG.info("Consumer switch finished");
            ClientConfig = switchingConsumerConfig;
            return newConsumer;
        }

        private static void UpdateAssignments(IReadOnlyCollection<TopicPartition> originalAssignment,
            IConsumer<TKey, TValue> newConsumer)
        {
            if (originalAssignment != null)
                // LOG.info("Consumer switched, applying assignments");
                newConsumer.Assign(originalAssignment);
        }

        private static void UpdateSubscription(IReadOnlyCollection<string> originalSubscription,
            IConsumer<TKey, TValue> newConsumer)
        {
            if (!originalSubscription.IsNullOrEmpty())
                // LOG.info("Consumer switched, applying subscriptions");
                newConsumer.Subscribe(originalSubscription);
        }

        protected TimeSpan GetSwitchTimeout(bool isAssignOrSubscribed, DiscoveryResult oldDiscoveryResult,
            DiscoveryResult newDiscoveryResult)
        {
            // Consumers are tricky to switch, since multiple instances of the same consuming application
            // must be switched over at the same time to prevent parallel consumption from two clusters
            // at the same time. This means we have to wait at least TTL time before actually switching
            // to allow fellow consumer instances to also detect the switch and stop consumption on the
            // old cluster.
            TimeSpan getDistributorTime;

            // We can switch directly if
            // - there is no subscription or assignment
            // -  we're not switching to a different cluster, we can just reinitialize the proxied object without
            // additional waiting.
            if (!isAssignOrSubscribed || oldDiscoveryResult.Cluster == newDiscoveryResult.Cluster)
            {
                getDistributorTime = TimeSpan.Zero;
            }
            else
            {
                // we need to wait enough time to ensure all "just produced" messages on the old cluster are distributed
                // to the new cluster. (distance * DistributorTimeout time).
                var switchTimeout = base.GetSwitchTimeout(newDiscoveryResult);
                var newTtl = TimeSpan.FromMilliseconds(newDiscoveryResult.Ttl);
                // At the same time we need to wait at least TTL time (see above).
                // So we wait the maximum of switch timeout and TTL.
                getDistributorTime = MathUtils.Max(switchTimeout, newTtl);
            }

            return getDistributorTime;
        }
    }
}