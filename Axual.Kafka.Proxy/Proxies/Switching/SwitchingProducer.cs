//
// Copyright 2020 Axual B.V. 2020
//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Axual.Kafka.Proxy.DiscoveryFetcher;
using Axual.Kafka.Proxy.Exceptions.Switching;
using Axual.Kafka.Proxy.Proxies.Switching.Proxy;
using Confluent.Kafka;

namespace Axual.Kafka.Proxy.Proxies.Switching
{
    internal class SwitchingProducer<TKey, TValue> : IProducer<TKey, TValue>
    {
        /// <summary>
        /// Indicate the InitTransactions timeout with the value the client passed.
        /// if null the client didn't call InitTransactions.
        /// </summary>
        internal TimeSpan? InitTransactionsTimeout;

        private readonly ISwitcher _clusterSwitcher;
        private readonly IProducerBuilder<TKey, TValue> _nextProducerBuilder;

        private readonly ReaderWriterLockSlim _readerWriterLockSlimSwitching;
        private readonly AutoResetEvent _waitToInit = new AutoResetEvent(false);
        private Utils.NewDiscoveryConfigEventArgs _discoveryConfigEvent;
        private InitializeException _initializeException;
        private bool _isInitClientWithDiscoveryConfiguration;
        private bool _needToSwitch;
        private IProducer<TKey, TValue> _nextProducer;
        private ProducerConfig _producerConfig;
        private SwitchException _switchException;

        internal SwitchingProducer(IProducerBuilder<TKey, TValue> nextProducerBuilder,
            ProducerConfig switchingProducerConfig, ISwitcher clusterSwitcher)
        {
            _isInitClientWithDiscoveryConfiguration = false;
            _needToSwitch = false;

            _nextProducerBuilder = nextProducerBuilder;
            _producerConfig = switchingProducerConfig;
            _clusterSwitcher = clusterSwitcher;

            _readerWriterLockSlimSwitching = new ReaderWriterLockSlim();

            try
            {
                _clusterSwitcher.OnFirstDiscoveryResult += InitInnerProducer;
                _clusterSwitcher.OnSwitchNeeded += NeedToSwitch;
                _clusterSwitcher.StartListening();
            }
            catch (Exception ex)
            {
                _switchException = new ProducerSwitchException(ex);
                throw;
            }
        }

        public int AddBrokers(string brokers)
        {
            return RunActionWithReadLock(() => _nextProducer.AddBrokers(brokers));
        }

        public Handle Handle => RunActionWithReadLock(() => _nextProducer.Handle);
        public string Name => RunActionWithReadLock(() => _nextProducer.Name);

        /// <exception cref="TransactionSwitchedException">if the producer switches over to another cluster during a transaction.</exception>
        public Task<DeliveryResult<TKey, TValue>> ProduceAsync(string topic, Message<TKey, TValue> message,
            CancellationToken cancellationToken = default)
        {
            return Task.Run(() =>
            {
                var isSwitchPerformed = PerformSwitchIfGetNewConfiguration();
                if (isSwitchPerformed && IsInitTransactions())
                    throw new TransactionSwitchedException();

                return RunActionWithReadLock(() => _nextProducer.ProduceAsync(topic, message, cancellationToken));
            }, cancellationToken);
        }

        /// <exception cref="TransactionSwitchedException">if the producer switches over to another cluster during a transaction.</exception>
        public Task<DeliveryResult<TKey, TValue>> ProduceAsync(TopicPartition topicPartition,
            Message<TKey, TValue> message, CancellationToken cancellationToken = default)
        {
            return Task.Run(() =>
            {
                var isSwitchPerformed = PerformSwitchIfGetNewConfiguration();
                return RunActionWithReadLock(() =>
                        _nextProducer.ProduceAsync(topicPartition, message, cancellationToken))
                    .ContinueWith(task =>
                    {
                        if (isSwitchPerformed && IsInitTransactions())
                            throw new TransactionSwitchedException();
                        return task;
                    }, cancellationToken).Unwrap();
            }, cancellationToken);
        }

        /// <exception cref="TransactionSwitchedException">if the producer switches over to another cluster during a transaction.</exception>
        public void Produce(string topic, Message<TKey, TValue> message,
            Action<DeliveryReport<TKey, TValue>> deliveryHandler = null)
        {
            var isSwitchPerformed = PerformSwitchIfGetNewConfiguration();
            if (isSwitchPerformed && IsInitTransactions())
                throw new TransactionSwitchedException();

            RunActionWithReadLock(() => _nextProducer.Produce(topic, message, deliveryHandler));
        }

        /// <exception cref="TransactionSwitchedException">if the producer switches over to another cluster during a transaction.</exception>
        public void Produce(TopicPartition topicPartition, Message<TKey, TValue> message,
            Action<DeliveryReport<TKey, TValue>> deliveryHandler = null)
        {
            var isSwitchPerformed = PerformSwitchIfGetNewConfiguration();
            if (isSwitchPerformed && IsInitTransactions())
                throw new TransactionSwitchedException();

            RunActionWithReadLock(() => _nextProducer.Produce(topicPartition, message, deliveryHandler));
        }

        public int Poll(TimeSpan timeout)
        {
            return RunActionWithReadLock(() => _nextProducer.Poll(timeout));
        }

        public int Flush(TimeSpan timeout)
        {
            return RunActionWithReadLock(() => _nextProducer.Flush(timeout));
        }

        public void Flush(CancellationToken cancellationToken = default)
        {
            RunActionWithReadLock(() => _nextProducer.Flush(cancellationToken));
        }

        public void InitTransactions(TimeSpan timeout)
        {
            RunActionWithReadLock(() => _nextProducer.InitTransactions(timeout));
            InitTransactionsTimeout = timeout;
        }

        public void BeginTransaction()
        {
            RunActionWithReadLock(() => _nextProducer.BeginTransaction());
        }

        public void CommitTransaction(TimeSpan timeout)
        {
            RunActionWithReadLock(() => _nextProducer.CommitTransaction(timeout));
        }

        public void CommitTransaction()
        {
            RunActionWithReadLock(() => _nextProducer.CommitTransaction());
        }

        public void AbortTransaction(TimeSpan timeout, bool force)
        {
            WaitToInitToComplete();

            if (force)
                InnerAbortTransaction(timeout);
            else
                AbortTransaction(timeout);
        }

        public void AbortTransaction(TimeSpan timeout)
        {
            RunActionWithReadLock(() => InnerAbortTransaction(timeout));
        }

        public void AbortTransaction()
        {
            RunActionWithReadLock(() => _nextProducer.AbortTransaction());
        }

        private void InnerAbortTransaction(TimeSpan timeout)
        {
            _nextProducer.AbortTransaction(timeout);
        }

        /// <exception cref="TransactionSwitchedException">if the producer switches over to another cluster during a transaction.</exception>
        public void SendOffsetsToTransaction(IEnumerable<TopicPartitionOffset> offsets,
            IConsumerGroupMetadata groupMetadata, TimeSpan timeout)
        {
            var isSwitchPerformed = PerformSwitchIfGetNewConfiguration();
            if (isSwitchPerformed && IsInitTransactions())
                throw new TransactionSwitchedException();

            RunActionWithReadLock(() => _nextProducer.SendOffsetsToTransaction(offsets, groupMetadata, timeout));
        }

        public void Dispose(bool force)
        {
            WaitToInitToComplete();

            if (force)
                InnerDispose();
            else
                Dispose();
        }

        public void Dispose()
        {
            RunActionWithReadLock(InnerDispose);
        }

        private void InnerDispose()
        {
            _nextProducer.Dispose();
            _clusterSwitcher.Dispose();
        }

        private void InitInnerProducer(object sender, DiscoveryResult firstDiscoveryResult)
        {
            try
            {
                _clusterSwitcher.OnFirstDiscoveryResult -= InitInnerProducer;
                _producerConfig = new ProducerConfig(_producerConfig.UpdateConfiguration(firstDiscoveryResult));
                _nextProducer = _nextProducerBuilder.Rebuild(_producerConfig);
                _isInitClientWithDiscoveryConfiguration = true;
            }
            catch (Exception ex)
            {
                _initializeException = new ProducerInitializeException(ex);
                Console.WriteLine(ex.Message);
            }
            finally
            {
                _waitToInit.Set();
            }
        }

        private void NeedToSwitch(object sender, Utils.NewDiscoveryConfigEventArgs e)
        {
            _needToSwitch = true;
            _discoveryConfigEvent = e;
        }

        /// <summary>
        /// Rebuild the nextProducer to connect to the new Kafka Cluster
        /// </summary>
        /// <exception cref="SwitchException">When error occured while switching producer, see InnerException for more details</exception>
        /// <returns> true - if switched otherwise return false</returns>
        private bool PerformSwitchIfGetNewConfiguration()
        {
            if (!_needToSwitch)
                return false;

            // Only one Thread enter here
            _readerWriterLockSlimSwitching.EnterWriteLock();

            // If other thread already switched
            if (!_needToSwitch)
            {
                _readerWriterLockSlimSwitching.ExitWriteLock();
                return false;
            }

            try
            {
                // Get new Producer according the new configuration from the Discovery API
                _nextProducer = (IProducer<TKey, TValue>)_clusterSwitcher.Switch(this, _discoveryConfigEvent);
                _needToSwitch = false;
            }
            catch (Exception innerException)
            {
                _switchException = new ProducerSwitchException(innerException);
                throw;
            }
            finally
            {
                _readerWriterLockSlimSwitching.ExitWriteLock();
            }

            return true;
        }

        private void RunActionWithReadLock(Action action)
        {
            WaitToInitToComplete();

            _readerWriterLockSlimSwitching.EnterReadLock();
            try
            {
                action.Invoke();
            }
            finally
            {
                _readerWriterLockSlimSwitching.ExitReadLock();
            }
        }

        private TResult RunActionWithReadLock<TResult>(Func<TResult> func)
        {
            WaitToInitToComplete();

            _readerWriterLockSlimSwitching.EnterReadLock();
            try
            {
                // is switch failed
                if (_switchException != null) throw _switchException;

                return func();
            }
            finally
            {
                _readerWriterLockSlimSwitching.ExitReadLock();
            }
        }

        private void WaitToInitToComplete()
        {
            if (_isInitClientWithDiscoveryConfiguration)
                return;

            // wait for OnFirstDiscoveryResult function to end
            _waitToInit.WaitOne();
            if (_initializeException != null) throw _initializeException;
        }

        internal bool IsInitTransactions()
        {
            return InitTransactionsTimeout.HasValue;
        }
    }
}
