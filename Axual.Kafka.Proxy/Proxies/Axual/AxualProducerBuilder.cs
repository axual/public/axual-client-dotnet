//
// Copyright 2020 Axual B.V. 2020
//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

using Axual.Kafka.Proxy.Exceptions;
using Axual.Kafka.Proxy.Proxies.Abstract;
using Axual.Kafka.Proxy.Proxies.Base;
using Axual.Kafka.Proxy.Proxies.Headers;
using Axual.Kafka.Proxy.Proxies.Lineage;
using Axual.Kafka.Proxy.Proxies.Resolving;
using Axual.Kafka.Proxy.Proxies.Switching;
using Confluent.Kafka;

namespace Axual.Kafka.Proxy.Proxies.Axual
{
    public class AxualProducerBuilder<TKey, TValue> : AbstractProducerBuilder<TKey, TValue>
    {
        public AxualProducerBuilder(ProducerConfig producerConfig)
            : base(producerConfig,
                new SwitchingProducerBuilder<TKey, TValue>(producerConfig,
                    new ResolvingProducerBuilder<TKey, TValue>(producerConfig,
                        new LineageProducerBuilder<TKey, TValue>(producerConfig,
                            new HeaderProducerBuilder<TKey, TValue>(producerConfig,
                                new BaseProducerBuilder<TKey, TValue>(producerConfig,
                                    new AdapterProducerBuilder<TKey, TValue>(producerConfig)))))))
        {
        }

        public override bool Validate()
        {
            var axualProducerConfig = new AxualProducerConfig(ProducerConfig);

            if (axualProducerConfig.ApplicationId.IsNullOrEmpty())
                throw new RequireConfigurationException(Constants.ConfigurationKeys.Axual.ApplicationId);

            return true;
        }

        public override IProducer<TKey, TValue> Build()
        {
            Validate();
            SetNextBuilderSetters();
            CurrentProducer = new AxualProducer<TKey, TValue>(NextProducerBuilder, ProducerConfig);
            return CurrentProducer;
        }

        public override IProducer<TKey, TValue> Rebuild(ProducerConfig producerConfig)
        {
            ProducerConfig = producerConfig;
            return Build();
        }
    }
}