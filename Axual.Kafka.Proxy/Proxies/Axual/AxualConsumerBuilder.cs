//
// Copyright 2020 Axual B.V. 2020
//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

using Axual.Kafka.Proxy.Exceptions;
using Axual.Kafka.Proxy.Proxies.Abstract;
using Axual.Kafka.Proxy.Proxies.Base;
using Axual.Kafka.Proxy.Proxies.Headers;
using Axual.Kafka.Proxy.Proxies.Lineage;
using Axual.Kafka.Proxy.Proxies.Resolving;
using Axual.Kafka.Proxy.Proxies.Switching;
using Confluent.Kafka;

namespace Axual.Kafka.Proxy.Proxies.Axual
{
    public class AxualConsumerBuilder<TKey, TValue> : AbstractConsumerBuilder<TKey, TValue>
    {
        public AxualConsumerBuilder(ConsumerConfig consumerConfig)
            : base(consumerConfig,
                new SwitchingConsumerBuilder<TKey, TValue>(consumerConfig,
                    new ResolvingConsumerBuilder<TKey, TValue>(consumerConfig,
                        new LineageConsumerBuilder<TKey, TValue>(consumerConfig,
                            new HeaderConsumerBuilder<TKey, TValue>(consumerConfig,
                                new BaseConsumerBuilder<TKey, TValue>(consumerConfig,
                                    new AdapterConsumerBuilder<TKey, TValue>(consumerConfig)))))))
        {
        }

        public override bool Validate()
        {
            var axualConsumerConfig = new AxualConsumerConfig(ConsumerConfig);

            if (axualConsumerConfig.ApplicationId.IsNullOrEmpty())
                throw new RequireConfigurationException(Constants.ConfigurationKeys.Axual.ApplicationId);

            return true;
        }

        public override IConsumer<TKey, TValue> Build()
        {
            Validate();

            ConsumerConfig.GroupId = new AxualConsumerConfig(ConsumerConfig).ApplicationId;
            SetNextBuilderSetters();
            CurrentConsumer = new AxualConsumer<TKey, TValue>(NextConsumerBuilder, ConsumerConfig);
            return CurrentConsumer;
        }

        public override IConsumer<TKey, TValue> Rebuild(ConsumerConfig consumerConfig)
        {
            ConsumerConfig = consumerConfig;
            return Build();
        }
    }
}