//
// Copyright 2020 Axual B.V. 2020
//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

using System;
using System.IO;
using Axual.Kafka.Proxy.Proxies.Resolving;
using Confluent.Kafka;
using static Axual.Kafka.Proxy.Proxies.Constants.ConfigurationKeys;

namespace Axual.Kafka.Proxy.Proxies.Axual
{
    public class AxualConsumerConfig : ConsumerConfig
    {
        public AxualConsumerConfig() : base(new ConsumerConfig())
        {
        }

        public AxualConsumerConfig(ConsumerConfig consumerConfig) : base(consumerConfig)
        {
        }
        
        public Uri EndPoint
        {
            get => this.GetUri(ClientConfigMapper[Constants.ConfigurationKeys.Axual.EndPoint]);
            set => this.SetUri(ClientConfigMapper[Constants.ConfigurationKeys.Axual.EndPoint], value);
        }

        /// <summary>
        ///     Unique ID of the Application, as specified in the Self Service.
        /// </summary>
        [ResolvingKey(Constants.ConfigurationKeys.Axual.ApplicationId)]
        public string ApplicationId
        {
            get => Get(ClientConfigMapper[Constants.ConfigurationKeys.Axual.ApplicationId]);
            set => Set(ClientConfigMapper[Constants.ConfigurationKeys.Axual.ApplicationId], value);
        }
        
        public string ApplicationVersion
        {
            get => Get(ClientConfigMapper[Constants.ConfigurationKeys.Axual.ApplicationVersion]);
            set => Set(ClientConfigMapper[Constants.ConfigurationKeys.Axual.ApplicationVersion], value);
        }
        
        public string Tenant
        {
            get => Get(ClientConfigMapper[Constants.ConfigurationKeys.Axual.Tenant]);
            set => Set(ClientConfigMapper[Constants.ConfigurationKeys.Axual.Tenant], value);
        }
        
        public string Environment
        {
            get => Get(ClientConfigMapper[Constants.ConfigurationKeys.Axual.Environment]);
            set => Set(ClientConfigMapper[Constants.ConfigurationKeys.Axual.Environment], value);
        }
        
        public string SchemaRegistryUrl
        {
            get => Get(Constants.ConfigurationKeys.Kafka.SchemaRegistryUrl);
            set => Set(Constants.ConfigurationKeys.Kafka.SchemaRegistryUrl, value);
        }
        
        /// <summary>
        ///     PEM string / File / directory path to CA certificate(s) for verifying the broker's key.
        ///     Defaults: On Windows the system's CA certificates are automatically looked up in the Windows Root certificate store.
        ///     On Mac OSX it is recommended to install openssl using Homebrew, to provide CA certificates.
        ///     On Linux install the distribution's ca-certificates package.
        ///     If OpenSSL is statically linked or `ssl.ca.location` is set to `probe` a list of standard paths will be probed and the first one found will be used as the default CA certificate location path.
        ///     If OpenSSL is dynamically linked the OpenSSL library's default path will be used (see `OPENSSLDIR` in `openssl version -a`).
        /// 
        ///     default: ''
        ///     importance: low
        /// </summary>
        public new string SslCaLocation
        {
            get => this.Get("ssl.ca.location");
            set
            {
                // if not a file
                if (!File.Exists(value))
                {
                    var fileCaName = Path.GetTempPath() + Guid.NewGuid() + ".cer";
                    File.WriteAllText(fileCaName,value);
                    value = fileCaName;
                }
                SetObject("ssl.ca.location", (object) value);
            }
        }
    }
}