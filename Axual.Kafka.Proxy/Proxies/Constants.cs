//
// Copyright 2020 Axual B.V. 2020
//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

[assembly: InternalsVisibleTo("Axual.Kafka.Tests")]

namespace Axual.Kafka.Proxy.Proxies
{
    internal static class Constants
    {
        internal const string JavaResolverPath = "io.axual.common.resolver.";
        internal const string AxualConfigPrefix = "axual.dotnet";

        internal static class ConfigurationKeys
        {
            public enum AxualConfiguration
            {
                Kafka,
                Header,
                Resolver,
                Switching,
                Lineage,
                Axual
            }

            internal static readonly Dictionary<string, string> ClientConfigMapper =
                new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase)
                {
                    {
                        Headers.EnableValueHeaders,
                        $"{AxualConfigPrefix}.client.{AxualConfiguration.Header}.{Headers.EnableValueHeaders}"
                    },
                    // Topic is not part of client configuration
                    {
                        Resolver.TopicPattern,
                        $"{AxualConfigPrefix}.client.{AxualConfiguration.Resolver}.{Resolver.TopicPattern}"
                    },
                    {
                        Resolver.TopicResolver,
                        $"{AxualConfigPrefix}.client.{AxualConfiguration.Resolver}.{Resolver.TopicResolver}"
                    },
                    {
                        Resolver.GroupIdPattern,
                        $"{AxualConfigPrefix}.consumer.{AxualConfiguration.Resolver}.{Resolver.GroupIdPattern}"
                    },
                    {
                        Resolver.GroupIdResolver,
                        $"{AxualConfigPrefix}.consumer.{AxualConfiguration.Resolver}.{Resolver.GroupIdResolver}"
                    },
                    {
                        Resolver.TransactionalIdPattern,
                        $"{AxualConfigPrefix}.producer.{AxualConfiguration.Resolver}.{Resolver.TransactionalIdPattern}"
                    },
                    {
                        Resolver.TransactionalIdResolver,
                        $"{AxualConfigPrefix}.producer.{AxualConfiguration.Resolver}.{Resolver.TransactionalIdResolver}"
                    },
                    {
                        Switching.DistributorDistance,
                        $"{AxualConfigPrefix}.client.{AxualConfiguration.Switching}.{Switching.DistributorDistance}"
                    },
                    {
                        Switching.DistributorTimeout,
                        $"{AxualConfigPrefix}.client.{AxualConfiguration.Switching}.{Switching.DistributorTimeout}"
                    },
                    {Switching.Ttl, $"{AxualConfigPrefix}.consumer.{AxualConfiguration.Switching}.{Switching.Ttl}"},
                    {
                        Switching.AutoOffsetReset,
                        $"{AxualConfigPrefix}.consumer.{AxualConfiguration.Switching}.{Switching.AutoOffsetReset}"
                    },
                    {
                        Axual.ApplicationId,
                        $"{AxualConfigPrefix}.client.{AxualConfiguration.Axual}.{Axual.ApplicationId}"
                    },
                    {
                        Axual.ApplicationVersion,
                        $"{AxualConfigPrefix}.client.{AxualConfiguration.Axual}.{Axual.ApplicationVersion}"
                    },
                    {Axual.System, $"{AxualConfigPrefix}.client.{AxualConfiguration.Axual}.{Axual.System}"},
                    {Axual.Environment, $"{AxualConfigPrefix}.client.{AxualConfiguration.Axual}.{Axual.Environment}"},
                    {Axual.Instance, $"{AxualConfigPrefix}.client.{AxualConfiguration.Axual}.{Axual.Instance}"},
                    {Axual.Tenant, $"{AxualConfigPrefix}.client.{AxualConfiguration.Axual}.{Axual.Tenant}"},
                    {Axual.Cluster, $"{AxualConfigPrefix}.client.{AxualConfiguration.Axual}.{Axual.Cluster}"},
                    {Axual.EndPoint, $"{AxualConfigPrefix}.client.{AxualConfiguration.Axual}.{Axual.EndPoint}"},
                    {Kafka.SchemaRegistryUrl, Kafka.SchemaRegistryUrl},
                    {Kafka.BootstrapServers, Kafka.BootstrapServers},
                    {Kafka.GroupId, Kafka.GroupId},
                    {Kafka.TransactionalIdKey, Kafka.TransactionalIdKey}
                };

            internal static class Resolver
            {
                internal const string GroupIdResolverKey = "group";
                internal const string GroupIdPattern = "group.id.pattern";
                internal const string GroupIdResolver = "group.id.resolver";

                internal const string TopicResolverKey = "topic";
                internal const string TopicPattern = "topic.pattern";
                internal const string TopicResolver = "topic.resolver";
                
                internal const string TransactionalIdPattern = "transactional.id.pattern";
                internal const string TransactionalIdResolver = "transactional.id.resolver";
            }

            public static class Kafka
            {
                public const string SchemaRegistryUrl = "schema.registry.url";
                public const string BootstrapServers = "bootstrap.servers";
                public const string GroupId = "group.id";
                public const string TransactionalIdKey = "transactional.id";

                // SSL file - PKCS#12 format
                public const string SslCaLocation = "ssl.ca.location";
                public const string SslKeystoreLocation = "ssl.keystore.location";
                public const string SslKeystorePassword = "ssl.keystore.password";

                // SSL file - PEM format
                public const string SslCertificateLocation = "ssl.certificate.location";
                public const string SslKeyLocation = "ssl.key.location";
                public const string SslKeyPassword = "ssl.key.password";

                // SSL in memory - PEM format
                public const string SslCertificatePem = "ssl.certificate.pem";
                public const string SslKeyPem = "ssl.key.pem";
            }

            public static class Headers
            {
                public const string EnableValueHeaders = "enable.value.headers";
            }

            public static class Switching
            {
                public const string Ttl = "ttl";
                public const string DistributorTimeout = "distributor.timeout";
                public const string DistributorDistance = "distributor.distance";
                public const string AutoOffsetReset = "auto.offset.reset";
            }

            internal static class Axual
            {
                internal const string ApplicationId = "app.id";
                internal const string ApplicationVersion = "applicationVersion";
                internal const string Tenant = "tenant";
                internal const string Instance = "instance";
                internal const string Environment = "environment";
                internal const string System = "system";
                internal const string Cluster = "cluster";
                internal const string EndPoint = "endpoint";
            }
        }
    }
}
