//
// Copyright 2020 Axual B.V. 2020
//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

using Axual.Kafka.Proxy.Proxies.Abstract;
using Axual.Kafka.Proxy.SerDes;
using Confluent.Kafka;
using Confluent.Kafka.SyncOverAsync;

namespace Axual.Kafka.Proxy.Proxies.Base
{
    /// <summary>
    ///     Wrapper for <see cref="Confluent.Kafka.ConsumerBuilder&lt;TKey, TValue&gt;" /> (Kafka Object), which Implements
    ///     IConsumerBuilder(Axual Object)
    /// </summary>
    internal class AdapterConsumerBuilder<TKey, TValue> : AbstractConsumerBuilder<TKey, TValue>
    {
        private ConsumerBuilder<TKey, TValue> _consumerBuilder;

        protected internal AdapterConsumerBuilder(ConsumerConfig consumerConfig) : base(consumerConfig, null)
        {
            _consumerBuilder = new ConsumerBuilder<TKey, TValue>(ConsumerConfig.RemoveAxualConfiguration());
        }

        public override bool Validate()
        {
            return true;
        }

        public override IConsumer<TKey, TValue> Build()
        {
            Validate();
            SetNextBuilderSetters();
            CurrentConsumer = _consumerBuilder.Build();
            return CurrentConsumer;
        }

        public override IConsumer<TKey, TValue> Rebuild(ConsumerConfig consumerConfig)
        {
            ConsumerConfig = consumerConfig;
            _consumerBuilder = new ConsumerBuilder<TKey, TValue>(consumerConfig.RemoveAxualConfiguration());
            return Build();
        }

        protected override void SetNextBuilderSetters()
        {
            SetDeserializersAndReconfigureDeserializers();

            _consumerBuilder.SetErrorHandler(ErrorHandler);
            _consumerBuilder.SetLogHandler(LogHandler);
            _consumerBuilder.SetPartitionsAssignedHandler(PartitionsAssignedHandler);
            _consumerBuilder.SetPartitionsRevokedHandler(PartitionsRevokedHandler);
            _consumerBuilder.SetStatisticsHandler(StatisticsHandler);
            _consumerBuilder.SetOffsetsCommittedHandler(OffsetsCommittedHandler);
        }

        private void SetDeserializersAndReconfigureDeserializers()
        {
            if (KeyDeserializer != null)
                _consumerBuilder.SetKeyDeserializer(KeyDeserializer);
            if (KeyDeserializer is IConfigurable configurableKeySerializer)
                configurableKeySerializer.Configure(ConsumerConfig);

            if (AsyncKeyDeserializer is IConfigurable configurableAsyncKeySerializer)
                configurableAsyncKeySerializer.Configure(ConsumerConfig);
            if (AsyncKeyDeserializer != null)
                _consumerBuilder.SetKeyDeserializer(AsyncKeyDeserializer.AsSyncOverAsync());

            if (ValueDeserializer is IConfigurable configurableValueSerializer)
                configurableValueSerializer.Configure(ConsumerConfig);
            if (ValueDeserializer != null)
                _consumerBuilder.SetValueDeserializer(ValueDeserializer);

            if (AsyncValueDeserializer is IConfigurable configurableAsyncValueSerializer)
                configurableAsyncValueSerializer.Configure(ConsumerConfig);
            if (AsyncValueDeserializer != null)
                _consumerBuilder.SetValueDeserializer(AsyncValueDeserializer.AsSyncOverAsync());
        }
    }
}