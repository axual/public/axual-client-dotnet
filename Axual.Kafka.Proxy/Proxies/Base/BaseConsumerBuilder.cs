//
// Copyright 2020 Axual B.V. 2020
//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

using Axual.Kafka.Proxy.Exceptions;
using Axual.Kafka.Proxy.Proxies.Abstract;
using Confluent.Kafka;

namespace Axual.Kafka.Proxy.Proxies.Base
{
    internal class BaseConsumerBuilder<TKey, TValue> : AbstractConsumerBuilder<TKey, TValue>
    {
        internal BaseConsumerBuilder(ConsumerConfig consumerConfig, IConsumerBuilder<TKey, TValue> nextConsumerBuilder)
            : base(new BaseConsumerConfig(consumerConfig), nextConsumerBuilder)
        {
        }

        public override bool Validate()
        {
            if (ConsumerConfig.BootstrapServers.IsNullOrEmpty())
                throw new RequireConfigurationException(Constants.ConfigurationKeys.Kafka.BootstrapServers);

            if (ConsumerConfig.GroupId.IsNullOrEmpty())
                throw new RequireConfigurationException(Constants.ConfigurationKeys.Kafka.GroupId);

            if (KeyDeserializer == null && AsyncKeyDeserializer == null)
                throw new RequireConfigurationException("Key Deserializer / Async Key Deserializer");

            if (ValueDeserializer == null && AsyncValueDeserializer == null)
                throw new RequireConfigurationException("Value Deserializer / Async Value Deserializer");

            return true;
        }

        public override IConsumer<TKey, TValue> Build()
        {
            Validate();
            SetNextBuilderSetters();
            CurrentConsumer = new BaseConsumer<TKey, TValue>(NextConsumerBuilder, ConsumerConfig);
            return CurrentConsumer;
        }

        public override IConsumer<TKey, TValue> Rebuild(ConsumerConfig consumerConfig)
        {
            ConsumerConfig = consumerConfig;
            return Build();
        }
    }
}