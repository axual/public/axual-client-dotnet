//
// Copyright 2020 Axual B.V. 2020
//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

using Axual.Kafka.Proxy.Proxies.Abstract;
using Axual.Kafka.Proxy.SerDes;
using Confluent.Kafka;
using Confluent.Kafka.SyncOverAsync;

namespace Axual.Kafka.Proxy.Proxies.Base
{
    /// <summary>
    ///     Wrapper for <see cref="Confluent.Kafka.ProducerBuilder&lt;TKey, TValue&gt;" /> (Kafka Object), which Implements
    ///     IProducerBuilder(Axual Object)
    /// </summary>
    internal class AdapterProducerBuilder<TKey, TValue> : AbstractProducerBuilder<TKey, TValue>
    {
        private ProducerBuilder<TKey, TValue> _producerBuilder;

        internal AdapterProducerBuilder(ProducerConfig producerConfig) : base(producerConfig, null)
        {
            _producerBuilder = new ProducerBuilder<TKey, TValue>(ProducerConfig.RemoveAxualConfiguration());
        }

        public override bool Validate()
        {
            return true;
        }

        public override IProducer<TKey, TValue> Build()
        {
            Validate();
            SetNextBuilderSetters();
            CurrentProducer = _producerBuilder.Build();
            return CurrentProducer;
        }

        public override IProducer<TKey, TValue> Rebuild(ProducerConfig producerConfig)
        {
            ProducerConfig = producerConfig;
            _producerBuilder = new ProducerBuilder<TKey, TValue>(producerConfig.RemoveAxualConfiguration());
            return Build();
        }

        protected override void SetNextBuilderSetters()
        {
            SetSerializersAndReconfigureSerializers();

            _producerBuilder.SetLogHandler(LogHandler);
            _producerBuilder.SetErrorHandler(ErrorHandler);
            _producerBuilder.SetStatisticsHandler(StatisticsHandler);
        }

        private void SetSerializersAndReconfigureSerializers()
        {
            if (KeySerializer is IConfigurable configurableKeySerializer)
                configurableKeySerializer.Configure(ProducerConfig);
            if (KeySerializer != null)
                _producerBuilder.SetKeySerializer(KeySerializer);

            if (AsyncKeySerializer is IConfigurable configurableAsyncKeySerializer)
                configurableAsyncKeySerializer.Configure(ProducerConfig);
            if (AsyncKeySerializer != null)
                _producerBuilder.SetKeySerializer(AsyncKeySerializer.AsSyncOverAsync());

            if (ValueSerializer is IConfigurable configurableValueSerializer)
                configurableValueSerializer.Configure(ProducerConfig);
            if (ValueSerializer != null)
                _producerBuilder.SetValueSerializer(ValueSerializer);

            if (AsyncValueSerializer is IConfigurable configurableAsyncValueSerializer)
                configurableAsyncValueSerializer.Configure(ProducerConfig);
            if (AsyncValueSerializer != null)
                _producerBuilder.SetValueSerializer(AsyncValueSerializer.AsSyncOverAsync());
        }
    }
}