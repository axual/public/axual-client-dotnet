//
// Copyright 2020 Axual B.V. 2020
//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

using System;
using System.Collections.Generic;
using Confluent.Kafka;

namespace Axual.Kafka.Proxy.Proxies
{
    public interface IConsumerBuilder<TKey, TValue> : IClientBuilder
    {
        ConsumerConfig ConsumerConfig { get; }

        /// <summary>  Build a new IConsumer implementation instance. </summary>
        new IConsumer<TKey, TValue> Build();

        /// <summary>  Rebuild a new IConsumer implementation instance base on the consumerConfig parameter. </summary>
        IConsumer<TKey, TValue> Rebuild(ConsumerConfig consumerConfig);
        
        /// <summary> Set the deserializer to use to deserialize values. </summary>
        IConsumerBuilder<TKey, TValue> SetValueDeserializer(IDeserializer<TValue> valueDeserializer);
        
        /// <summary> Set the deserializer to use to deserialize values. </summary>
        /// <remarks> This function is temporarily translated to synchronize.</remarks>
        IConsumerBuilder<TKey, TValue> SetValueDeserializer(IAsyncDeserializer<TValue> asyncValueDeserializer);

        /// <summary> Set the deserializer to use to deserialize keys. </summary>
        IConsumerBuilder<TKey, TValue> SetKeyDeserializer(IDeserializer<TKey> keyDeserializer);
        /// <summary> Set the deserializer to use to deserialize keys. </summary>
        /// <remarks> This function is temporarily translated to synchronize.</remarks>
        IConsumerBuilder<TKey, TValue> SetKeyDeserializer(IAsyncDeserializer<TKey> asyncKeyDeserializer);

        /// <summary>
        ///     Set the handler to call when there is information available
        ///     to be logged. If not specified, a default callback that writes
        ///     to stderr will be used.
        /// </summary>
        /// <remarks>
        ///     By default not many log messages are generated.
        ///     For more verbose logging, specify one or more debug contexts
        ///     using the 'debug' configuration property.
        ///     Warning: Log handlers are called spontaneously from internal
        ///     librdkafka threads and the application must not call any
        ///     Confluent.Kafka APIs from within a log handler or perform any
        ///     prolonged operations.
        /// </remarks>
        IConsumerBuilder<TKey, TValue> SetLogHandler(Action<IConsumer<TKey, TValue>, LogMessage> logHandler);

        /// <summary>
        ///     Set the handler to call on error events e.g. connection failures or all
        ///     brokers down. Note that the client will try to automatically recover from
        ///     errors that are not marked as fatal. Non-fatal errors should be interpreted
        ///     as informational rather than catastrophic.
        /// </summary>
        /// <remarks>
        ///     Executes as a side-effect of the Consume method (on the same thread).
        /// </remarks>
        IConsumerBuilder<TKey, TValue> SetErrorHandler(Action<IConsumer<TKey, TValue>, Error> errorHandler);

        /// <summary>
        ///     Set the handler to call on statistics events. Statistics
        ///     are provided as a JSON formatted string as defined here:
        ///     https://github.com/edenhill/librdkafka/blob/master/STATISTICS.md
        /// </summary>
        /// <remarks>
        ///     You can enable statistics and set the statistics interval
        ///     using the statistics.interval.ms configuration parameter
        ///     (disabled by default).
        ///     Executes as a side-effect of the Consume method (on the same thread).
        /// </remarks>
        IConsumerBuilder<TKey, TValue> SetStatisticsHandler(
            Action<IConsumer<TKey, TValue>, string> statisticsHandler);

        /// <summary>
        ///     This handler is called when a new consumer group partition assignment has been received
        ///     by this consumer.
        ///     Note: corresponding to every call to this handler there will be a corresponding call to
        ///     the partitions revoked handler (if one has been set using SetPartitionsRevokedHandler).
        ///     The actual partitions to consume from and start offsets are specified by the return value
        ///     of the handler. This set of partitions is not required to match the assignment provided
        ///     by the consumer group, but typically will. Partition offsets may be a specific offset, or
        ///     special value (Beginning, End or Unset). If Unset, consumption will resume from the
        ///     last committed offset for each partition, or if there is no committed offset, in accordance
        ///     with the `auto.offset.reset` configuration property.
        /// </summary>
        /// <remarks>
        ///     May execute as a side-effect of the Consumer.Consume call (on the same thread).
        ///     Assign/Unassign must not be called in the handler.
        /// </remarks>
        IConsumerBuilder<TKey, TValue> SetPartitionsAssignedHandler(
            Func<IConsumer<TKey, TValue>, List<TopicPartition>, IEnumerable<TopicPartitionOffset>>
                partitionsAssignedHandler);

        /// <summary>
        ///     This handler is called when a new consumer group partition assignment has been received
        ///     by this consumer.
        ///     Note: corresponding to every call to this handler there will be a corresponding call to
        ///     the partitions revoked handler (if one has been set using SetPartitionsRevokedHandler").
        ///     Consumption will resume from the last committed offset for each partition, or if there is
        ///     no committed offset, in accordance with the `auto.offset.reset` configuration property.
        /// </summary>
        /// <remarks>
        ///     May execute as a side-effect of the Consumer.Consume call (on the same thread).
        ///     Assign/Unassign must not be called in the handler.
        /// </remarks>
        IConsumerBuilder<TKey, TValue> SetPartitionsAssignedHandler(
            Action<IConsumer<TKey, TValue>, List<TopicPartition>> partitionAssignmentHandler);

        /// <summary>
        ///     This handler is called immediately prior to a group partition assignment being
        ///     revoked. The second parameter provides the set of partitions the consumer is
        ///     currently assigned to, and the current position of the consumer on each of these
        ///     partitions.
        /// </summary>
        /// <remarks>
        ///     May execute as a side-effect of the Consumer.Consume call (on the same thread).
        ///     Assign/Unassign must not be called in the handler.
        /// </remarks>
        IConsumerBuilder<TKey, TValue> SetPartitionsRevokedHandler(
            Func<IConsumer<TKey, TValue>, List<TopicPartitionOffset>, IEnumerable<TopicPartitionOffset>>
                partitionsRevokedHandler);

        /// <summary>
        ///     This handler is called immediately prior to a group partition assignment being
        ///     revoked. The second parameter provides the set of partitions the consumer is
        ///     currently assigned to, and the current position of the consumer on each of these
        ///     partitions.
        ///     The return value of the handler specifies the partitions/offsets the consumer
        ///     should be assigned to following completion of this method (typically empty).
        /// </summary>
        /// <remarks>
        ///     May execute as a side-effect of the Consumer.Consume call (on the same thread).
        ///     Assign/Unassign must not be called in the handler.
        /// </remarks>
        IConsumerBuilder<TKey, TValue> SetPartitionsRevokedHandler(
            Action<IConsumer<TKey, TValue>, List<TopicPartitionOffset>> partitionsRevokedHandler);

        /// <summary>
        ///     A handler that is called to report the result of (automatic) offset
        ///     commits. It is not called as a result of the use of the Commit method.
        /// </summary>
        /// <remarks>
        ///     Executes as a side-effect of the Consumer.Consume call (on the same thread).
        /// </remarks>
        IConsumerBuilder<TKey, TValue> SetOffsetsCommittedHandler(
            Action<IConsumer<TKey, TValue>, CommittedOffsets> offsetsCommittedHandler);
    }
}