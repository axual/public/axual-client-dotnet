//
// Copyright 2020 Axual B.V. 2020
//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Text.RegularExpressions;
using Axual.Kafka.Proxy.DiscoveryFetcher;
using Axual.Kafka.Proxy.Proxies;
using BitConverter;
using Confluent.Kafka;

[assembly: InternalsVisibleTo("Axual.SchemaRegistry.Serdes.Avro")]

namespace Axual.Kafka.Proxy
{
    internal static class Utils
    {
        internal static string RemoveNameSpace(string fullFileName)
        {
            return fullFileName?.Replace(Constants.JavaResolverPath, string.Empty);
        }

        internal static ClientConfig UpdateConfiguration(this Config config, DiscoveryResult discoveryResult)
        {
            using var enumerator = discoveryResult.GetEnumerator();

            while (enumerator.MoveNext())
            {
                var key = enumerator.Current.Key;
                var value = enumerator.Current.Value;

                if (Constants.ConfigurationKeys.ClientConfigMapper.ContainsKey(key))
                {
                    var axualKey = Constants.ConfigurationKeys.ClientConfigMapper[key];
                    config.Set(axualKey, value);
                }
            }

            return new ClientConfig(config.ToDictionary(pair => pair.Key, pair => pair.Value));
        }

        internal static class BitsUtils
        {
            internal static bool IsBitSet(int dataInt, int position)
            {
                return ((dataInt >> position) & 1) == 1;
            }

            internal static int ClearBit(int dataInt, int position)
            {
                return dataInt & ~(1 << position);
            }

            internal static int SetBit(int dataInt, int position)
            {
                return dataInt | (1 << position);
            }

            internal static class BigEndian
            {
                private static readonly EndianBitConverter EndianBitConverter = EndianBitConverter.BigEndian;

                internal static byte[] GetValueBytesBigEndian(byte value)
                {
                    return EndianBitConverter.GetBytes(value);
                }

                internal static byte[] GetValueBytesBigEndian(ushort value)
                {
                    return EndianBitConverter.GetBytes(value);
                }

                internal static byte[] GetValueBytesBigEndian(int value)
                {
                    return EndianBitConverter.GetBytes(value);
                }

                internal static byte[] GetValueBytesBigEndian(long value)
                {
                    return EndianBitConverter.GetBytes(value);
                }

                internal static byte[] GetValueBytesBigEndian(double value)
                {
                    return EndianBitConverter.GetBytes(value);
                }

                internal static int ToInt32FromBigEndian(byte[] value, int startIndex)
                {
                    return EndianBitConverter.ToInt32(value, startIndex);
                }

                internal static long ToInt64FromBigEndian(byte[] value, int startIndex)
                {
                    return EndianBitConverter.ToInt64(value, startIndex);
                }

                internal static double ToDoubleFromBigEndian(byte[] value, int startIndex)
                {
                    return EndianBitConverter.ToDouble(value, startIndex);
                }

                internal static ushort ToUInt16FromBigEndian(byte[] value, int startIndex)
                {
                    return EndianBitConverter.ToUInt16(value, startIndex);
                }
            }

            internal class BinaryReaderEndian : BinaryReader
            {
                private readonly EndianBitConverter _endianBitConverter;

                internal BinaryReaderEndian(Stream stream, bool isReadInBigEndianFormat) : base(stream)
                {
                    _endianBitConverter =
                        isReadInBigEndianFormat ? EndianBitConverter.BigEndian : EndianBitConverter.LittleEndian;
                }

                public override int Read()
                {
                    var data = base.ReadBytes(4);
                    return _endianBitConverter.ToInt32(data, 0);
                }

                public override int ReadInt32()
                {
                    var data = base.ReadBytes(4);
                    return _endianBitConverter.ToInt32(data, 0);
                }

                public override short ReadInt16()
                {
                    var data = base.ReadBytes(2);
                    return _endianBitConverter.ToInt16(data, 0);
                }

                public override ushort ReadUInt16()
                {
                    var data = base.ReadBytes(2);
                    return _endianBitConverter.ToUInt16(data, 0);
                }

                public override long ReadInt64()
                {
                    var data = base.ReadBytes(8);
                    return _endianBitConverter.ToInt64(data, 0);
                }

                public override byte[] ReadBytes(int count)
                {
                    return base.ReadBytes(count);
                }
            }

            internal class BinaryWriterEndian : BinaryWriter
            {
                private readonly EndianBitConverter _endianBitConverter;

                internal BinaryWriterEndian(Stream stream, bool isWriteInBigEndianFormat) : base(stream)
                {
                    _endianBitConverter =
                        isWriteInBigEndianFormat ? EndianBitConverter.BigEndian : EndianBitConverter.LittleEndian;
                }

                public override void Write(int value)
                {
                    Write(_endianBitConverter.GetBytes(value));
                }

                public override void Write(ushort value)
                {
                    Write(_endianBitConverter.GetBytes(value));
                }

                public override void Write(short value)
                {
                    Write(_endianBitConverter.GetBytes(value));
                }

                public override void Write(long value)
                {
                    Write(_endianBitConverter.GetBytes(value));
                }

                internal void Write(IHeader header)
                {
                    Write(header.GetValueBytes());
                }
            }
        }

        internal class NewDiscoveryConfigEventArgs : EventArgs
        {
            internal NewDiscoveryConfigEventArgs(DiscoveryResult oldConfiguration, DiscoveryResult newConfiguration)
            {
                OldConfiguration = oldConfiguration;
                NewConfiguration = newConfiguration;
            }

            internal DiscoveryResult NewConfiguration { get; }
            internal DiscoveryResult OldConfiguration { get; }
        }
    }

    internal static class SslUtils
    {
        internal static bool IsSecure(string endpoint)
        {
            return endpoint.StartsWith("https", StringComparison.CurrentCultureIgnoreCase);
        }

        internal static List<string> GetSubjectAlternativeNames(X509Certificate2 cert)
        {
            var lst = new List<string>();
            var sanRex = new Regex(@"(DNS|DNS Name|IP Address)[:=](.*)",
                RegexOptions.Compiled | RegexOptions.CultureInvariant);

            var subjectAlternativeName = cert.Extensions.Cast<X509Extension>()
                .Where(n => n.Oid.Value == "2.5.29.17") //n.Oid.FriendlyName=="Subject Alternative Name")
                .Select(n => new AsnEncodedData(n.Oid, n.RawData))
                .Select(n =>
                    n.Format(false).Split(new[] {Environment.NewLine, ","}, StringSplitOptions.RemoveEmptyEntries))
                .FirstOrDefault();

            if (subjectAlternativeName != null)
                foreach (var s in subjectAlternativeName)
                {
                    var match = sanRex.Match(s.Trim());
                    if (match.Success) lst.Add(match.Groups[2].Value);
                }

            return lst;
        }

        internal static bool VerifyHostNameWithSans(string hostname, IEnumerable<string> subjectAltNames)
        {
            return subjectAltNames.Any(san => IsHostNameMatch(hostname, san));
        }

        /// <summary>
        ///     Verify if the hostname
        /// </summary>
        /// <param name="expectedHostname">Hostname name from server</param>
        /// <param name="providedHostname">CN or SAN name</param>
        /// <returns></returns>
        internal static bool IsHostNameMatch(string expectedHostname, string providedHostname)
        {
            if (expectedHostname.Equals(providedHostname)) return true;

            // wild card
            if (providedHostname.StartsWith("*."))
            {
                // in case the expectedHostname doesn't have '.' return the same string 
                var expectedHostnameWithoutDomain = expectedHostname.Substring(expectedHostname.IndexOf('.') + 1);
                var providedHostnameWithoutWildCard = providedHostname.Substring(2);
                if (expectedHostnameWithoutDomain.Equals(providedHostnameWithoutWildCard)) return true;
            }

            return false;
        }
    }

    internal static class ClientConfigExtensions
    {
        internal static ClientConfig Clone(this ClientConfig clientConfig)
        {
            return new ClientConfig(clientConfig.ToDictionary(pair => pair.Key, pair => pair.Value));
        }

        internal static ClientConfig RemoveAxualConfiguration(
            this ClientConfig clientConfig)
        {
            return new ClientConfig(clientConfig.Where(pair =>
                    !pair.Key.StartsWith(Constants.AxualConfigPrefix) &&
                    pair.Key != Constants.ConfigurationKeys.Kafka.SchemaRegistryUrl)
                .ToDictionary(pair => pair.Key, pair => pair.Value));
        }

        internal static string Get(this Config config, string key, string defaultValue)
        {
            return config.Get(key) ?? defaultValue;
        }

        internal static Uri GetUri(this ClientConfig clientConfig, string configUriKey)
        {
            var uriString = clientConfig.Get(configUriKey);
            return uriString.IsNullOrEmpty() ? null : new Uri(uriString);
        }

        internal static void SetUri(this ClientConfig clientConfig, string key, Uri value)
        {
            if (value != null && !UriExtensions.UriSchemeHttp.Equals(value.Scheme, StringComparison.OrdinalIgnoreCase)
                              && !UriExtensions.UriSchemeHttps.Equals(value.Scheme, StringComparison.OrdinalIgnoreCase))
            {
                throw new UriFormatException(value + " is not a valid URL");
            }

            clientConfig.Set(key, value == null ? null : value.ToString());
        }

        internal static Dictionary<string, string> ToDictionary(this ClientConfig clientConfig)
        {
            return clientConfig.ToDictionary(pair => pair.Key, pair => pair.Value);
        }
    }

    internal static class UriExtensions
    {
        internal const string UriSchemeHttps = "https";
        internal const string UriSchemeHttp = "http";

        internal static Uri AddRelativeUri(this Uri uri, string relativeUri)
        {
            return new Uri(uri, relativeUri);
        }

        /// <summary>
        ///     Adds the specified parameter to the Query String.
        ///     If key exists - override the previous value
        /// </summary>
        /// <param name="uri"></param>
        /// <param name="key">Name of the parameter to add.</param>
        /// <param name="value">Value for the parameter to add.</param>
        /// <returns>Url with added parameter.</returns>
        internal static Uri AddParameter(this Uri uri, string key, string value)
        {
            var uriBuilder = new UriBuilder(uri);
            var query = ParseQueryString(uriBuilder.Query);
            query[key] = value;
            uriBuilder.Query = string.Join("&", query.Select(kvp => $"{kvp.Key}={kvp.Value}"));
            return uriBuilder.Uri;
        }

        /// <summary>
        ///     Remove the specified parameter from the Query String.
        /// </summary>
        /// <param name="uri"></param>
        /// <param name="key">Name of the parameter to remove.</param>
        /// <returns>Url without the parameter.</returns>
        internal static Uri RemoveParameter(this Uri uri, string key)
        {
            var uriBuilder = new UriBuilder(uri);

            var query = ParseQueryString(uriBuilder.Query);
            query.Remove(key);
            uriBuilder.Query = string.Join("&", query.Select(kvp => $"{kvp.Key}={kvp.Value}"));

            return uriBuilder.Uri;
        }

        # region .NET Standard 1.3 Support

        private static Dictionary<string, string> ParseQueryString(string query)
        {
            var result = new Dictionary<string, string>();

            if (query == null) throw new ArgumentNullException(nameof(query));

            if (query.Length == 0 || query.Length > 0 && query[0] != '?') return result;

            var queryLength = query.Length;
            var queryIndex = 1; //start index, after '?'

            while (queryIndex <= query.Length)
            {
                int valueIndex = -1, valueEndIndex = -1;
                for (var i = queryIndex; i < query.Length; i++)
                    // if missing parameter name , e.g. '?=someValue'
                    if (valueIndex == -1 && query[i] == '=')
                    {
                        valueIndex = i + 1;
                    }
                    // if end of value ,e.g. '?param=paramValue&
                    else if (query[i] == '&')
                    {
                        valueEndIndex = i;
                        break;
                    }

                string key;
                // if missing parameter name , e.g. '?=someValue'
                if (valueIndex == -1)
                {
                    key = null;
                    valueIndex = queryIndex;
                }
                else
                {
                    key = WebUtility.UrlDecode(query.Substring(queryIndex, valueIndex - queryIndex - 1));
                }

                // if the last parameter
                if (valueEndIndex < 0) valueEndIndex = query.Length;

                if (key != null)
                {
                    var value = WebUtility.UrlDecode(query.Substring(valueIndex, valueEndIndex - valueIndex));
                    result.Add(key, value);
                }

                queryIndex = valueEndIndex + 1;
            }

            return result;
        }

        #endregion
    }

    internal static class MathUtils
    {
        internal static T Max<T>(T first, T second)
        {
            return Comparer<T>.Default.Compare(first, second) > 0 ? first : second;
        }
    }

    internal static class Enum<T> where T : struct, IConvertible
    {
        internal static IEnumerable<string> GetDescriptions()
        {
            return typeof(T).GetRuntimeFields()
                .Where(i => i.GetCustomAttribute<DescriptionAttribute>() != null)
                .Select(info => info.GetCustomAttribute<DescriptionAttribute>().Description);
        }
    }

    internal static class EnumExtensions
    {
        internal static string GetDescription<T>(this T e)
        {
            return e.GetType()
                .GetRuntimeField(e.ToString())
                .GetCustomAttribute<DescriptionAttribute>()?
                .Description ?? string.Empty;
        }
    }

    /// <summary>
    ///     Detect if we are running as part of a nUnit unit test.
    ///     This is DIRTY and should only be used if absolutely necessary
    ///     as its usually a sign of bad design.
    /// </summary>
    internal static class LinqExtensions
    {
        internal static bool IsNullOrEmpty<TSource>(this IEnumerable<TSource> source)
        {
            return source == null || !source.Any();
        }
    }

    internal static class StringExtensions
    {
        internal static bool IsNullOrEmpty(this string source)
        {
            return string.IsNullOrEmpty(source);
        }
    }

    internal static class KafkaHeaderExtensions
    {
        internal static Headers Clone(this Headers originalHeaders)
        {
            var clone = new Headers();

            if (originalHeaders != null)
                foreach (var srcHeader in originalHeaders)
                    clone.Add(new Header(srcHeader.Key, srcHeader.GetValueBytes()));

            return clone;
        }

        /// <summary>
        ///     Add the new Header to headers with UTF-8 string encoding
        /// </summary>
        /// <param name="headers"></param>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <param name="overwrite">should override header value if exists</param>
        /// <returns></returns>
        internal static void Add(this Headers headers, string key, string value, bool overwrite = false)
        {
            if (value == null) return;

            var encodedString = Encoding.UTF8.GetBytes(value);
            headers.Add(key, encodedString, overwrite);
        }

        /// <summary>
        ///     Add the new Header to headers
        /// </summary>
        /// <param name="headers"></param>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <param name="overwrite">should override header value if exists</param>
        /// <returns></returns>
        internal static void Add(this Headers headers, string key, byte[] value, bool overwrite)
        {
            headers.Add(new Header(key, value), overwrite);
        }

        /// <summary>
        ///     Add the new Header to headers
        /// </summary>
        /// <param name="headers"></param>
        /// <param name="header"></param>
        /// <param name="overwrite">should override header value if exists</param>
        /// <returns></returns>
        internal static void Add(this Headers headers, Header header, bool overwrite)
        {
            if (header.GetValueBytes() == null) return;

            if (overwrite && headers.Exists(header.Key)) headers.Remove(header.Key);

            headers.Add(header);
        }

        private static bool Exists(this Headers headers, string key)
        {
            return headers.Any(h => h.Key == key);
        }

        internal static Headers UpdateOrAdd(this Headers headers, string key, byte[] value)
        {
            headers.Remove(key);
            headers.Add(new Header(key, value));
            return headers;
        }
    }
}