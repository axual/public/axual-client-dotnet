//
// Copyright 2020 Axual B.V. 2020
//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

using System;
using System.Collections;
using System.Collections.Generic;
using Axual.Kafka.Proxy.Proxies;
using static Axual.Kafka.Proxy.Proxies.Constants.ConfigurationKeys;

namespace Axual.Kafka.Proxy.DiscoveryFetcher
{
    internal class DiscoveryResult : IEnumerable<KeyValuePair<string, string>>
    {
        internal static readonly long DefaultTtl = 10_000;
        private readonly IDictionary<string, string> _dictionary;
        internal readonly DateTime TimeStamp;

        /// <summary>
        ///     Create discovery result object
        /// </summary>
        /// <param name="timeStamp">The timestamp where the configuration was received </param>
        /// <param name="configuration">
        ///     Dictionary represent the JSON response from discovery. if null set values to the default
        ///     once
        /// </param>
        internal DiscoveryResult(DateTime timeStamp, IDictionary<string, string> configuration = null)
        {
            TimeStamp = timeStamp;
            _dictionary = configuration ?? new Dictionary<string, string>();
        }

        /// <summary>
        ///     Create discovery result object with TimeStamp of current UTC time
        /// </summary>
        /// <param name="configuration">
        ///     Dictionary represent the JSON response from discovery. if null set values to the default
        ///     once
        /// </param>
        internal DiscoveryResult(IDictionary<string, string> configuration = null)
        {
            TimeStamp = DateTime.UtcNow;
            _dictionary = configuration ?? new Dictionary<string, string>();
        }

        /// <summary>
        ///     Time in milliseconds that this data is valid
        /// </summary>
        internal long Ttl =>
            Get(Switching.Ttl, DefaultTtl);

        internal string Cluster =>
            Get<string>(Constants.ConfigurationKeys.Axual.Cluster);

        internal string Instance =>
            Get<string>(Constants.ConfigurationKeys.Axual.Instance);

        internal string System =>
            Get<string>(Constants.ConfigurationKeys.Axual.System);

        internal string BootstrapServers =>
            Get<string>(Constants.ConfigurationKeys.Kafka.BootstrapServers);

        internal string SchemaRegistryUrl =>
            Get<string>(Constants.ConfigurationKeys.Kafka.SchemaRegistryUrl);

        /// <summary>
        ///     Time in milliseconds it takes for a message to be distributed from one cluster to another without any hops in
        ///     between
        ///     Default 60_000 (ms)
        /// </summary>
        internal long DistributorTimeout =>
            Get(Switching.DistributorTimeout, 60_000);

        /// <summary>
        ///     The distance of the previous cluster selected (lastCluster parameter) and the newly selected cluster
        ///     Default 1
        /// </summary>
        internal long DistributorDistance =>
            Get(Switching.DistributorDistance, 1);

        /// <summary>
        ///     Enable Header proxy or not
        ///     Default 1
        /// </summary>
        internal bool EnableValueHeaders =>
            Get(Headers.EnableValueHeaders, false);

        internal string GroupIdResolver
        {
            get
            {
                var groupResolver = Get<string>(Resolver.GroupIdResolver);
                return groupResolver?.Replace("io.axual.common.resolve.", string.Empty);
            }
        }

        internal string GroupIdPattern => Get<string>(Resolver.TopicResolver);

        internal string TopicResolver
        {
            get
            {
                var topicResolver = Get<string>(Resolver.TopicResolver);
                return topicResolver?.Replace("io.axual.common.resolve.", string.Empty);
            }
        }

        internal string TopicPattern => Get<string>("topic.pattern");

        public IEnumerator<KeyValuePair<string, string>> GetEnumerator()
        {
            return _dictionary.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable) _dictionary).GetEnumerator();
        }

        internal T Get<T>(string key, T defaultValue = default)
        {
            return _dictionary.ContainsKey(key)
                ? (T) Convert.ChangeType(_dictionary[key], typeof(T))
                : defaultValue;
        }

        internal IEnumerable<string> GetKeys()
        {
            return _dictionary.Keys;
        }
    }
}