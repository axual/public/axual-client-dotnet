//
// Copyright 2020 Axual B.V. 2020
//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

using System.Threading;
using System.Threading.Tasks;

namespace Axual.Kafka.Proxy.DiscoveryFetcher
{
    internal interface IDiscoveryRestService
    {
        /// <summary>
        ///     Gets the latest discovery result
        /// </summary>
        /// <param name="lastCluster">
        ///     The previous cluster value in the Discovery result. Used internally by the Discovery API,
        ///     so that unnecessary switching is avoided as much as possible.
        ///     set to <c>null</c>, if the last active cluster is unknown.
        /// </param>
        /// <param name="cancellationToken"></param>
        Task<DiscoveryResult> GetLatestDiscoveryResult(string lastCluster, CancellationToken cancellationToken);
    }
}