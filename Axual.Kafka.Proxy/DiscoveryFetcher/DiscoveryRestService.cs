//
// Copyright 2020 Axual B.V. 2020
//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using Axual.Kafka.Proxy.Exceptions;
using Axual.Kafka.Proxy.HttpClient;
using Axual.Kafka.Proxy.Proxies;
using Newtonsoft.Json;
using static Axual.Kafka.Proxy.Proxies.Constants.ConfigurationKeys;

namespace Axual.Kafka.Proxy.DiscoveryFetcher
{
    internal class DiscoveryRestService : IDiscoveryRestService
    {
        private static readonly string _versionResourceName = "Axual.Kafka.Proxy.Resources.axual_client_version";
        private static readonly string _applicationIdParameter = "applicationId";
        private readonly string _applicationId;
        private readonly System.Net.Http.HttpClient _httpClient;
        private readonly String _versionHeader;


        internal DiscoveryRestService(IHttpClientFactory httpClientFactory, IDictionary<string, string> config)
        {
            // Determine version of client

#if NETSTANDARD2_0_OR_GREATER || NETCOREAPP2_0_OR_GREATER || NETCOREAPP3_0_OR_GREATER || NET
            Assembly assembly = Assembly.GetExecutingAssembly();
#else
            Assembly assembly = typeof(DiscoveryRestService).GetTypeInfo().Assembly;
#endif

            if (assembly.GetManifestResourceNames().Contains(_versionResourceName))
            {
                using (Stream stream = assembly.GetManifestResourceStream(_versionResourceName))
                using (StreamReader reader = new StreamReader(stream))
                {
                    var version = reader.ReadToEnd();
                    _versionHeader = ".NET Axual Client " + version.Trim();
                }
            }
            else
            {
                _versionHeader = ".NET Axual Client <version unknown>";
            }

            _applicationId = config[ClientConfigMapper[Constants.ConfigurationKeys.Axual.ApplicationId]];
            var discoveryEndPoint = config[ClientConfigMapper[Constants.ConfigurationKeys.Axual.EndPoint]];

            if (discoveryEndPoint.IsNullOrEmpty())
                throw new RequireConfigurationException(Constants.ConfigurationKeys.Axual.EndPoint);
            discoveryEndPoint = RedirectToV2(discoveryEndPoint);

            var httpConfig = new HttpClientConfig(config)
            {
                EndPoint = discoveryEndPoint,
                ApplicationId = _applicationId,
                HttpRequestHeader = DiscoveryClientHeaders(config)
            };

            _httpClient = httpClientFactory.CreateClient(httpConfig);
            _httpClient.BaseAddress = new UriBuilder(discoveryEndPoint).Uri;
        }

        private static string RedirectToV2(string discoveryEndPoint)
        {
            // if not set to v2
            if (!discoveryEndPoint.EndsWith("/v2", StringComparison.CurrentCultureIgnoreCase)
                && !discoveryEndPoint.EndsWith("/v2/", StringComparison.CurrentCultureIgnoreCase))
                // Redirect to V2
                discoveryEndPoint += discoveryEndPoint.EndsWith("/") ? "v2" : "/v2";

            return discoveryEndPoint;
        }

        async Task<DiscoveryResult> IDiscoveryRestService.GetLatestDiscoveryResult(string lastCluster,
            CancellationToken cancellationToken)
        {
            var discoveryEndpoint = BuildQuery(lastCluster);
            var httpResponseMessage =
                await _httpClient.GetAsync(discoveryEndpoint, cancellationToken).ConfigureAwait(false);
            var lastFetchTimestamp = DateTime.UtcNow;

            if (httpResponseMessage.IsSuccessStatusCode)
            {
#if NET
                var readAsStringAsync = await httpResponseMessage.Content.ReadAsStringAsync(CancellationToken.None)
                    .ConfigureAwait(false);
#else
                var readAsStringAsync = await httpResponseMessage.Content.ReadAsStringAsync()
                    .ConfigureAwait(false);
#endif
                cancellationToken.ThrowIfCancellationRequested();
                var jsonResponse = JsonConvert.DeserializeObject<Dictionary<string, string>>(readAsStringAsync);
                cancellationToken.ThrowIfCancellationRequested();

                return new DiscoveryResult(lastFetchTimestamp, jsonResponse);
            }

            switch (httpResponseMessage.StatusCode)
            {
                // When Discovery API returns an error or empty cluster configuration.
                case HttpStatusCode.NoContent:
                    // LOG.info("Empty response from Discovery API, no active clusters found")
                    // return empty result
                    return new DiscoveryResult(lastFetchTimestamp);
                case HttpStatusCode.NotFound:
                    throw new HttpRequestException($"Couldn't reach Discovery API, URL: '{discoveryEndpoint}'");
                default:
                    throw new HttpRequestException(
                        $"Unexpected response code from Discovery API: '{httpResponseMessage.StatusCode}', URL: {discoveryEndpoint}");
            }
        }

        private IDictionary<string, string> DiscoveryClientHeaders(IDictionary<string, string> config)
        {
            var headers = new Dictionary<string, string>
            {
                { "X-Application-Id", config[ClientConfigMapper[Constants.ConfigurationKeys.Axual.ApplicationId]] },
                { "X-Client-Library-Version", _versionHeader }
            };

            config.TryGetValue(ClientConfigMapper[Constants.ConfigurationKeys.Axual.ApplicationVersion],
                out var applicationVersion);
            if (!applicationVersion.IsNullOrEmpty())
                headers.Add("X-Application-Version", applicationVersion);

            return headers;
        }

        private Uri BuildQuery(string lastCluster)
        {
            return lastCluster == null
                ? _httpClient.BaseAddress
                    .AddParameter(_applicationIdParameter, _applicationId)
                : _httpClient.BaseAddress
                    .AddParameter(_applicationIdParameter, _applicationId)
                    // For the Discovery API to calculate the 'distributor.distance' field
                    .AddParameter("lastCluster", lastCluster);
        }
    }
}