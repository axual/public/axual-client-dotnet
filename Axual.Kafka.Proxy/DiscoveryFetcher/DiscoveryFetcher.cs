//
// Copyright 2020 Axual B.V. 2020
//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

using System;
using System.Threading;
using System.Threading.Tasks;

namespace Axual.Kafka.Proxy.DiscoveryFetcher
{
    internal class DiscoveryFetcher : IFetcher
    {
        public event EventHandler<DiscoveryResult> OnNewFetchResult;
        public event EventHandler<Exception> OnError;

        private static readonly TimeSpan BackOffPeriod = TimeSpan.FromMilliseconds(10);
        private readonly IDiscoveryRestService _discoveryRestService;
        private readonly CancellationTokenSource _fetchDiscoveryResultAsyncTaskCancellationTokenSource;
        private Task _fetchDiscoveryResultAsyncTask;

        public bool IsRunning { get; private set; }

        public DiscoveryResult CurrentDiscoveryResult { get; private set; }

        internal DiscoveryFetcher(IDiscoveryRestService discoveryRestService)
        {
            CurrentDiscoveryResult = null;
            IsRunning = false;
            _fetchDiscoveryResultAsyncTaskCancellationTokenSource = new CancellationTokenSource();
            _discoveryRestService = discoveryRestService;
        }

        public async Task StartAsync(CancellationToken cancellationToken)
        {
            if (IsRunning)
                throw new InvalidOperationException("DiscoveryFetcher already running");

            IsRunning = true;

            try
            {
                _fetchDiscoveryResultAsyncTask = FetchDiscoveryResultAsync(CancellationTokenSource
                    .CreateLinkedTokenSource(_fetchDiscoveryResultAsyncTaskCancellationTokenSource.Token,
                        cancellationToken).Token);
                await _fetchDiscoveryResultAsyncTask;
            }
            catch (Exception ex)
            {
                IsRunning = false;
                if (!(ex is TaskCanceledException))
                    Console.WriteLine(ex);
                throw;
            }
        }

        public async Task StopAsync(CancellationToken cancellationToken)
        {
            // Stop called without start
            if (_fetchDiscoveryResultAsyncTask == null)
            {
                return;
            }

            try
            {
                // Signal cancellation to the executing method
                _fetchDiscoveryResultAsyncTaskCancellationTokenSource?.Cancel();
            }
            finally
            {
                // Wait until the task completes or the stop token triggers
                await Task.WhenAny(_fetchDiscoveryResultAsyncTask, Task.Delay(Timeout.Infinite,
                    cancellationToken));
            }
        }

        private async Task FetchDiscoveryResultAsync(CancellationToken cancellationToken)
        {
            while (!cancellationToken.IsCancellationRequested)
            {
                TimeSpan waitTimeInMs;
                try
                {
                    var cluster = IsFirstFetch() ? null : CurrentDiscoveryResult.Cluster;
                    CurrentDiscoveryResult = await _discoveryRestService
                        .GetLatestDiscoveryResult(cluster, cancellationToken)
                        .ConfigureAwait(false);

                    await Task.Run(() => OnNewFetchResult?.Invoke(this, CurrentDiscoveryResult),
                            CancellationToken.None)
                        .ConfigureAwait(false);
                    waitTimeInMs = TimeSpan.FromMilliseconds(CurrentDiscoveryResult.Ttl);
                }
                catch (Exception exception1)
                {
                    await Task.Run(() =>
                    {
                        var errorMessage = exception1 is AggregateException aggregateException
                            ? aggregateException.Flatten().ToString()
                            : exception1.ToString();

                        OnError?.Invoke(this, new Exception(
                            $"Exception: Failed to fetch information from Discovery API. " +
                            $"{Environment.NewLine}" + errorMessage));
                    }, CancellationToken.None).ConfigureAwait(false);

                    waitTimeInMs = BackOffPeriod;
                }

                await Task.Delay(waitTimeInMs, cancellationToken)
                    .ConfigureAwait(false);
            }

            IsRunning = false;
        }

        private bool IsFirstFetch()
        {
            return CurrentDiscoveryResult == null;
        }
    }
}