using System;
using System.Collections.Concurrent;
using System.Threading;

namespace Axual.Kafka.Proxy.DiscoveryFetcher
{
    /// <summary>
    /// Class <c>ClientRegistry</c> handles the registration of clients.
    /// Each application id gets its own registration, which is reused on subsequent calls.
    /// This ensures that all instances of the client will be notified of changes at the same time.
    ///
    /// The registry entries are cleaned up when disposed
    /// </summary>
    public class ClientRegistry : IDisposable
    {
        // Shared registry, available for everyone
        public static readonly ClientRegistry Registry = new ClientRegistry();

        private sealed class Registration
        {
            public IFetcher Fetcher;
            public CancellationTokenSource CancellationTokenSource;
            public int NumberOfRegistrations;
        }


        private readonly ConcurrentDictionary<string, Registration> _registrations;
        private readonly Func<CancellationTokenSource> _tokenSourceFactory;

        /// <summary>
        /// Constructs a client new registry instance
        /// </summary>
        /// <param name="tokenSourceFactory"> a factory to create cancellation token sources, by default it will use the CancellationTokenSource constructor</param>
        internal ClientRegistry(Func<CancellationTokenSource> tokenSourceFactory = null)
        {
            _tokenSourceFactory = tokenSourceFactory ?? (() => new CancellationTokenSource());
            _registrations = new ConcurrentDictionary<string, Registration>();
        }

        /// <summary>
        /// Register a new client instance for the provided application id.
        /// If a registration does not exists, a fetcher will be created and started.
        /// The provided event handler will be registered to the fetcher.
        /// If a registration already exists the provided event handler will be registered on the existing fetcher
        /// </summary>
        /// <param name="applicationId">The application id used to identify the producer/consumer</param>
        /// <param name="fetcherFactory">This factory will create <c>IFetcher</c> if needed</param>
        /// <param name="onNewResultFromDiscoveryApi">Event handler for handling the new Discovery Results</param>
        /// <returns>The number of known registrations for the application id</returns>
        internal int RegisterClientApplication(string applicationId, Func<IFetcher> fetcherFactory,
            EventHandler<DiscoveryResult> onNewResultFromDiscoveryApi)
        {
            lock (_registrations)
            {
                var registration = _registrations.AddOrUpdate(applicationId, id => new Registration()
                    {
                        NumberOfRegistrations = 1, Fetcher = fetcherFactory.Invoke(), CancellationTokenSource = null
                    },
                    (id, reg) =>
                    {
                        reg.NumberOfRegistrations++;
                        return reg;
                    });

                registration.Fetcher.OnNewFetchResult += onNewResultFromDiscoveryApi;
                // Send discovery result if one is already available, else the client will have to wait for TTL expiry
                var discoveryResult = registration.Fetcher.CurrentDiscoveryResult;
                if (discoveryResult != null)
                {
                    onNewResultFromDiscoveryApi.Invoke(this, discoveryResult);
                }

                // If the fetcher is running, return immediately
                if (registration.Fetcher.IsRunning)
                    return registration.NumberOfRegistrations;

                // Create cancellation token source and start fetching
                var tokenSource = _tokenSourceFactory.Invoke();
                registration.CancellationTokenSource = tokenSource;
                registration.Fetcher.StartAsync(tokenSource.Token);

                return registration.NumberOfRegistrations;
            }
        }

        /// <summary>
        /// Removes the client instance registration, and remove the callback registration.
        ///
        /// If the final instance of a client is removed, the corresponding fetcher is stopped and the entry removed
        /// </summary>
        /// <param name="applicationId">The application id used to identify the producer/consumer</param>
        /// <param name="onNewResultFromDiscoveryApi"></param>
        /// <returns>The number of known registrations for the application id, or -1 if the application id was not found</returns>
        internal int UnregisterClientRegistration(string applicationId,
            EventHandler<DiscoveryResult> onNewResultFromDiscoveryApi)
        {
            lock (_registrations)
            {
                // get registration, return -1 if not found
                if (!_registrations.TryGetValue(applicationId, out var registration))
                    return -1;

                // Remove callback registration
                registration.Fetcher.OnNewFetchResult -= onNewResultFromDiscoveryApi;

                // Decrease registration number
                registration.NumberOfRegistrations--;

                // Return is there are registrations left
                if (registration.NumberOfRegistrations > 0)
                    return registration.NumberOfRegistrations;

                // Stop fetcher if running
                if (registration.Fetcher.IsRunning)
                {
                    registration.CancellationTokenSource?.Cancel();
                }

                if (!_registrations.TryRemove(applicationId, out _))
                    Console.Error.WriteLine($"Could not remove registration for appid {applicationId}");

                return registration.NumberOfRegistrations;
            }
        }

        /// <summary>
        /// Cleans up the registrations by stopping the fetchers and removing the entries from the registration map.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        // Allows for future extensions to clean up properly
        protected virtual void Dispose(bool disposing)
        {
            if (!disposing) return;

            lock (_registrations)
            {
                foreach (var registration in _registrations.Values)
                {
                    if (registration.Fetcher.IsRunning)
                    {
                        registration.CancellationTokenSource?.Cancel();
                    }
                }

                _registrations.Clear();
            }
        }
    }
}