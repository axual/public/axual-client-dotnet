//
// Copyright 2020 Axual B.V. 2020
//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Security;
using System.Runtime.CompilerServices;
using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;
using Confluent.Kafka;

[assembly: InternalsVisibleTo("DynamicProxyGenAssembly2")]

namespace Axual.Kafka.Proxy.HttpClient
{
    public class HttpClientFactory : IHttpClientFactory
    {
        private class ClientHandler : HttpClientHandler
        {
            private readonly X509Certificate2Collection _caCertificates = new X509Certificate2Collection();

            public X509Certificate2Collection CaCertificates => _caCertificates;

            public bool ValidateHostnames { get; set; } = true;
            public bool VerifyCertificate { get; set; } = true;

            public ClientHandler()
            {
                this.ServerCertificateCustomValidationCallback = ValidateCertificate;
            }

            private bool VerifyIssuer(X509Certificate2 serverCertificate, X509Chain x509Chain)
            {
                if (_caCertificates.Count > 0)
                {
                    // The self-signed certificate doesn't need to be registered as trusted on the system (e.g. in the LM\Root store).
                    x509Chain.ChainPolicy.VerificationFlags = X509VerificationFlags.AllowUnknownCertificateAuthority;
                    x509Chain.ChainPolicy.ExtraStore.AddRange(_caCertificates);

                    var isValid = x509Chain.Build(serverCertificate);
                    if (!isValid) return false;

                    foreach (var chainElement in x509Chain.ChainElements)
                    {
                        foreach (var caCertificate in _caCertificates)
                        {
                            if (caCertificate.RawData.SequenceEqual(chainElement.Certificate.RawData))
                                return true;
                        }
                    }
                }

                return false;
            }

            private bool
                ValidateCertificate(HttpRequestMessage request, X509Certificate2 serverCertificate,
                    X509Chain x509Chain,
                    SslPolicyErrors sslPolicyErrors)
            {
                // No errors found, no need for more validations
                if (sslPolicyErrors == SslPolicyErrors.None) return true;

                // Verify if any certificate in the issuer chain matches, stop validation is no match
                if (VerifyCertificate && !VerifyIssuer(serverCertificate, x509Chain)) return false;

                // Final check for hostname, if set
                if (ValidateHostnames)
                {
                    // first check the DNS name in the certificate
                    var expectedHostName = request.RequestUri.DnsSafeHost;

                    // Check if the hostname match one of the SANs in the subject alternative name
                    // or if the hostname the same as server certificate - Common Name
                    var serverSans = SslUtils.GetSubjectAlternativeNames(serverCertificate);
                    var serverCn = serverCertificate.GetNameInfo(X509NameType.SimpleName, false);
                    if (SslUtils.VerifyHostNameWithSans(expectedHostName, serverSans)
                        || SslUtils.IsHostNameMatch(expectedHostName, serverCn))
                        return true;

                    // Server Certificate rejected due to invalid subject name
                    return false;
                }

                // Accept certificate without checking hostnames
                return true;
            }
        }

        public System.Net.Http.HttpClient CreateClient(HttpClientConfig httpClientConfig)
        {
            var httpClientHandler = new ClientHandler();
            httpClientHandler.ValidateHostnames = httpClientConfig.SslEndpointIdentificationAlgorithm ==
                                                  SslEndpointIdentificationAlgorithm.Https;
            httpClientHandler.VerifyCertificate = httpClientConfig.EnableSslCertificateVerification ?? true;

            httpClientHandler.SslProtocols = SslProtocols.Tls12;
            httpClientHandler.ClientCertificateOptions = ClientCertificateOption.Manual;

            SetCertificateAuthority(httpClientConfig, httpClientHandler.CaCertificates);
            var clientCertificates = CreateClientCertificates(httpClientConfig);
            httpClientHandler.ClientCertificates.AddRange(clientCertificates);

            var httpClient = new System.Net.Http.HttpClient(httpClientHandler);
            httpClient = SetHttpHeaders(httpClient, httpClientConfig.HttpRequestHeader);

            httpClient.Timeout = httpClientConfig.Timeout ?? httpClient.Timeout;

            return httpClient;
        }


        private static void SetCertificateAuthority(HttpClientConfig config, X509Certificate2Collection caCertificates)
        {
            var sslCaStore = config.SslCaCertificateStores;
            var sslCaLocation = config.SslCaLocation;
            var sslKeyPassword = config.SslKeyPassword;

            if (!string.IsNullOrEmpty(sslCaStore))
            {
                var storeNames = sslCaStore.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                foreach (var storeName in storeNames)
                {
                    X509Store store = new X509Store(storeName, StoreLocation.CurrentUser);
                    store.Open(OpenFlags.ReadOnly);
                    caCertificates.AddRange(store.Certificates);
#if NET
                    store.Close();
#endif
                }
            }

            if (!string.IsNullOrEmpty(sslCaLocation))
                caCertificates.Add(sslKeyPassword.IsNullOrEmpty()
                    ? new X509Certificate2(sslCaLocation)
                    : new X509Certificate2(sslCaLocation, sslKeyPassword));
        }

        private static X509CertificateCollection CreateClientCertificates(HttpClientConfig config)
        {
            X509CertificateCollection certificateCollection = new X509Certificate2Collection();

            certificateCollection.AddRange(GetClientCertificateFromPkcs(config));
#if NET
            certificateCollection.AddRange(GetClientCertificateFromPemLocation(config));
            certificateCollection.AddRange(GetClientCertificateFromPemString(config));
#endif
            return certificateCollection;
        }

        private static X509CertificateCollection GetClientCertificateFromPkcs(HttpClientConfig config)
        {
            X509CertificateCollection certificateCollection = new X509Certificate2Collection();

            // Path to client's keystore (PKCS#12) used for authentication.
            var sslKeystoreLocation = config.SslKeystoreLocation;
            var sslKeystorePassword = config.SslKeystorePassword;

            if (!string.IsNullOrEmpty(sslKeystoreLocation))
                certificateCollection.Add(sslKeystorePassword.IsNullOrEmpty()
                    ? new X509Certificate2(fileName: sslKeystoreLocation)
                    : new X509Certificate2(fileName: sslKeystoreLocation, password: sslKeystorePassword));

            return certificateCollection;
        }

#if NET
        private static X509CertificateCollection GetClientCertificateFromPemLocation(HttpClientConfig config)
        {
            X509CertificateCollection certificateCollection = new X509Certificate2Collection();
            // Path to client's public key (PEM) used for authentication.
            var sslCertificateLocation = config.SslCertificateLocation;
            // Path to client's private key (PEM) used for authentication.
            var sslKeyLocation = config.SslKeyLocation;
            // Private key passphrase (for use with SslKeyPem)
            var sslKeyPassword = config.SslKeyPassword;

            if (string.IsNullOrEmpty(sslCertificateLocation) || string.IsNullOrEmpty(sslKeyLocation))
                return certificateCollection;
            
            X509Certificate2 asRead = sslKeyPassword.IsNullOrEmpty()
                ? X509Certificate2.CreateFromPemFile(sslCertificateLocation, sslKeyLocation)
                : X509Certificate2.CreateFromEncryptedPemFile(sslCertificateLocation, sslKeyPassword,
                    sslKeyLocation);

            // Create new certificate with PKCS12 to use. Needed for Windows deployments
            // See https://github.com/dotnet/runtime/issues/45680
            certificateCollection.Add(new X509Certificate2(asRead.Export(X509ContentType.Pkcs12)));

            return certificateCollection;
        }
#endif

#if NET
        private static X509CertificateCollection GetClientCertificateFromPemString(HttpClientConfig config)
        {
            X509CertificateCollection certificateCollection = new X509Certificate2Collection();
            // Client's public key string (PEM format) used for authentication 
            var sslCertificatePem = config.SslCertificatePem;
            // Client's private key string (PEM format) used for authentication.
            var sslKeyPem = config.SslKeyPem;
            // Private key passphrase (for use with SslKeyPem)
            var sslKeyPassword = config.SslKeyPassword;

            if (string.IsNullOrEmpty(sslCertificatePem) || string.IsNullOrEmpty(sslKeyPem))
                return certificateCollection;
            
            X509Certificate2 asRead = sslKeyPassword.IsNullOrEmpty()
                ? X509Certificate2.CreateFromPem(sslCertificatePem, sslKeyPem)
                : X509Certificate2.CreateFromEncryptedPem(sslCertificatePem, sslKeyPem, sslKeyPassword);

            // Create new certificate with PKCS12 to use. Needed for Windows deployments
            // See https://github.com/dotnet/runtime/issues/45680
            certificateCollection.Add(new X509Certificate2(asRead.Export(X509ContentType.Pkcs12)));

            return certificateCollection;
        }
#endif

        private static System.Net.Http.HttpClient SetHttpHeaders(System.Net.Http.HttpClient httpClient,
            IEnumerable<KeyValuePair<string, string>> httpHeaders)
        {
            httpClient.DefaultRequestHeaders.CacheControl = new CacheControlHeaderValue { NoCache = true };

            foreach (var header in httpHeaders ?? Enumerable.Empty<KeyValuePair<string, string>>())
                httpClient.DefaultRequestHeaders.Add(header.Key, header.Value);

            return httpClient;
        }
    }
}