using System;
using System.Collections.Generic;
using System.Linq;
using Axual.Kafka.Proxy.Proxies;
using Confluent.Kafka;
using static Axual.Kafka.Proxy.Proxies.Constants.ConfigurationKeys;

namespace Axual.Kafka.Proxy.HttpClient
{
    public class HttpClientConfig : ClientConfig
    {
        internal HttpClientConfig()
        {
        }
        
        public HttpClientConfig(ClientConfig clientConfig) : base(clientConfig)
        {
        }
        internal HttpClientConfig(IDictionary<string,string> config) : base(config)
        {
        }

        internal bool IsAnyEndPointSecured => !EndPoint.IsNullOrEmpty() && EndPoint.Split(',').Any(SslUtils.IsSecure);

        internal string ApplicationId
        {
            get => Get(ClientConfigMapper[Constants.ConfigurationKeys.Axual.ApplicationId]);
            set => Set(ClientConfigMapper[Constants.ConfigurationKeys.Axual.ApplicationId],value);
        }

        /// <summary>
        /// Comma seperated list of endpoints which will be used by the HttpClient 
        /// </summary>
        internal string EndPoint
        {
            get => Get(ClientConfigMapper[Constants.ConfigurationKeys.Axual.EndPoint]);
            set => Set(ClientConfigMapper[Constants.ConfigurationKeys.Axual.EndPoint],value);
        }

        internal IDictionary<string,string> HttpRequestHeader { get; set; }
        
        /// <summary>
        /// Gets or sets the timespan to wait before the request times out.
        /// </summary>
        internal TimeSpan? Timeout { get; set; }
    }
}