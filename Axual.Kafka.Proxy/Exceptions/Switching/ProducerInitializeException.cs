using System;
using System.Runtime.Serialization;

namespace Axual.Kafka.Proxy.Exceptions.Switching
{
    [Serializable]
    public class ProducerInitializeException : InitializeException
    {
        internal ProducerInitializeException(Exception innerException) :
            base(string.Format(InitializeExceptionErrorMessage, "Producer"), innerException)
        {
        }

#if !NETSTANDARD1_3
        protected ProducerInitializeException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
#endif
    }
}