using System;
using System.Runtime.Serialization;

namespace Axual.Kafka.Proxy.Exceptions.Switching
{
    [Serializable]
    public class ConsumerSwitchException : SwitchException
    {
        internal ConsumerSwitchException(Exception innerException) : 
            base(string.Format(SwitchExceptionMessageFormat, "Consumer"), innerException)
        {
        }
        
#if !NETSTANDARD1_3
        protected ConsumerSwitchException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
#endif
    }
}