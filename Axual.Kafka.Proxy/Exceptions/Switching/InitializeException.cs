using System;
using System.Runtime.Serialization;

namespace Axual.Kafka.Proxy.Exceptions.Switching
{
    [Serializable]
    public abstract class InitializeException : Exception
    {
        protected const string InitializeExceptionErrorMessage = "Error occurred while initialize to client: {0}.";

        protected InitializeException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

#if !NETSTANDARD1_3
        protected InitializeException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
#endif
    }
}