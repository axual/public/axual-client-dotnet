using System;
using System.Runtime.Serialization;

namespace Axual.Kafka.Proxy.Exceptions.Switching
{
    [Serializable]
    public class ConsumerInitializeException : InitializeException
    {
        internal ConsumerInitializeException(Exception innerException) : 
            base(string.Format(InitializeExceptionErrorMessage, "Consumer"), innerException)
        {
        }
        
#if !NETSTANDARD1_3
        protected ConsumerInitializeException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
#endif
    }
}