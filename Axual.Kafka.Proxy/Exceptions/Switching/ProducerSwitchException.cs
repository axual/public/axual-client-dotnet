using System;
using System.Runtime.Serialization;

namespace Axual.Kafka.Proxy.Exceptions.Switching
{
    [Serializable]
    public class ProducerSwitchException : SwitchException
    {
        internal ProducerSwitchException(Exception innerException) :
            base(string.Format(SwitchExceptionMessageFormat, "Producer"), innerException)
        {
        }

#if !NETSTANDARD1_3
        protected ProducerSwitchException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
#endif
    }
}