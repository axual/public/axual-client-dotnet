//
// Copyright 2020 Axual B.V. 2020
//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

using System;
using System.Runtime.Serialization;

namespace Axual.Kafka.Proxy.Exceptions.Resolving
{
    /// <summary>
    /// Thrown if the GroupId, Stream or TransactionalId cannot be resolved.
    /// </summary>
    [Serializable]
    public class ResolvingException : Exception
    {
        internal string Pattern { get; private set; }
        internal string UnResolvedString { get; private set; }
        internal ResolvingException(string unResolvedString, string pattern)
            : base($"Unable to resolve '{unResolvedString}' according to pattern '{pattern}'.")
        {
            UnResolvedString = unResolvedString;
            Pattern = pattern;
        }
        
        internal ResolvingException(string unResolvedString, string pattern, Exception innerException)
            : base($"Unable to resolve '{unResolvedString}' according to pattern '{pattern}'.", innerException)
        {
            UnResolvedString = unResolvedString;
            Pattern = pattern;
        }

#if !NETSTANDARD1_3
        protected ResolvingException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            Pattern = info.GetString("pattern");
            UnResolvedString = info.GetString("unResolvedString");
        }
        
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            base.GetObjectData(info, context);
            info.AddValue("pattern", this.Pattern);
            info.AddValue("unResolvedString", this.UnResolvedString);
        }
#endif
    }
}