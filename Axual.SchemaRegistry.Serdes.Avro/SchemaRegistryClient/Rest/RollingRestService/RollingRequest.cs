//
// Copyright 2020 Axual B.V. 2020
//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Axual.SchemaRegistry.Serdes.Avro.SchemaRegistryClient.Rest.DataContracts;
using Confluent.SchemaRegistry;
using Newtonsoft.Json.Linq;

namespace Axual.SchemaRegistry.Serdes.Avro.SchemaRegistryClient.Rest.RollingRestService
{
    internal class RollingRequest : IRollingRequest
    {
        /// <summary>
        ///     The index of the last client successfully used (or random if none worked).
        /// </summary>
        private int _lastEndPointUsedIndex;
        private readonly object _lastClientUsedLock = new object();
        private readonly HttpClient _httpClient;
        private readonly List<Uri> _schemaRegistrySlavesList;
        
        internal RollingRequest(HttpClient httpClient, IEnumerable<Uri> endPoints)
        {
            _httpClient = httpClient;
            _schemaRegistrySlavesList = endPoints.ToList();
            _lastEndPointUsedIndex = -1;
        }

        public async Task<HttpResponseMessage> ExecuteRequestAsync(Func<Uri, HttpRequestMessage> createRequest)
        {
            var exceptions = new List<Exception>();
            // Start with the last client that was used by this method, which only gets set on 
            // success, so it's probably going to work.
            var startClientIndex = GetLastEndPointUsedIndex();

            for (var loopIndex = startClientIndex;
                loopIndex < _schemaRegistrySlavesList.Count + startClientIndex;
                ++loopIndex)
            {
                var urlIndex = loopIndex % _schemaRegistrySlavesList.Count;
                var schemaRegistrySlaveHostName = _schemaRegistrySlavesList[urlIndex];
                var httpRequestMessage = createRequest(schemaRegistrySlaveHostName);

                var response = await _httpClient
                    .SendAsync(httpRequestMessage)
                    .ConfigureAwait(continueOnCapturedContext: false);

                switch (response.StatusCode)
                {
                    case HttpStatusCode.OK:
                    case HttpStatusCode.NoContent:
                        SetLastEndPointUsedIndex(urlIndex);
                        return response;
                    // consider an unauthorized response from any server to be conclusive.
                    case HttpStatusCode.Unauthorized:
                        throw new HttpRequestException("Unauthorized");
                    default:
                    {
                        var errorMessage = new ErrorMessage(-1, string.Empty);
                        try
                        {
                            errorMessage = JObject.Parse(await response.Content.ReadAsStringAsync()
                                .ConfigureAwait(continueOnCapturedContext: false)).ToObject<ErrorMessage>();
                        }
                        catch
                        {
                            // Ignore, Failed to parse
                        }

                        if ((int)response.StatusCode >= 400 && (int)response.StatusCode < 500)
                        {
                            // Stop iterating over the host and throw SchemaRegistryException exception
                            throw new SchemaRegistryException(errorMessage?.Message,
                                response.StatusCode, errorMessage?.ErrorCode ?? -1);
                        }

                        exceptions.Add(new HttpRequestException(
                            $"[{_schemaRegistrySlavesList[urlIndex]}] {response.StatusCode} {errorMessage?.ErrorCode ?? -1} {errorMessage?.Message}"));
                        break;
                    }
                }
            }

            throw new AggregateException(exceptions);
        }

        private void SetLastEndPointUsedIndex(int urlIndex)
        {
            lock (_lastClientUsedLock)
            {
                _lastEndPointUsedIndex = urlIndex;
            }
        }
        
        private int GetLastEndPointUsedIndex()
        {
            int index;
            lock (_lastClientUsedLock)
            {
                // If first call to SR set the start index to 0
                index = _lastEndPointUsedIndex == -1 ? 0 : _lastEndPointUsedIndex;
            }

            return index;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                _httpClient?.Dispose();
            }
        }
    }
}