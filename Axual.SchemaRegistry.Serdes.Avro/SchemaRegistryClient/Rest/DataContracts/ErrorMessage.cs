//
// Copyright 2020 Axual B.V. 2020
//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

using System.Runtime.Serialization;

namespace Axual.SchemaRegistry.Serdes.Avro.SchemaRegistryClient.Rest.DataContracts
{
    /// <summary>
    ///     Generic JSON error message.
    /// </summary>
    /// <remarks>
    ///     This source code is originated from  <see cref="Confluent.SchemaRegistry.ErrorMessage"/>
    /// </remarks>
    [DataContract]
    internal class ErrorMessage
    {
        [DataMember(Name = "error_code")] public int ErrorCode { get; set; }

        [DataMember(Name = "message")] public string Message { get; set; }

        public ErrorMessage(int errorCode, string message)
        {
            ErrorCode = errorCode;
            Message = message;
        }

        public override string ToString()
            => $"{{error_code={ErrorCode}, message={Message}}}";
    }
}