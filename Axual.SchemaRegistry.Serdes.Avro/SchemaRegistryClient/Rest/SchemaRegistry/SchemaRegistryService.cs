﻿//
// Copyright 2020 Axual B.V. 2020
//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Axual.Kafka.Proxy;
using Axual.SchemaRegistry.Serdes.Avro.SchemaRegistryClient.Rest.DataContracts;
using Axual.SchemaRegistry.Serdes.Avro.SchemaRegistryClient.Rest.RollingRestService;
using Confluent.SchemaRegistry;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Axual.SchemaRegistry.Serdes.Avro.SchemaRegistryClient.Rest.SchemaRegistry
{
    /// <remarks>
    ///     This source code is originated from  <see cref="Confluent.SchemaRegistry.RestService"/>
    /// </remarks>
    internal class SchemaRegistryService : ISchemaRegistryService
    {
        private readonly IRollingRequest _rollingRequest;
        internal const string SchemaRegistryV1Json = "application/vnd.schemaregistry.v1+json";
        internal const string SchemaRegistryDefaultJson = "application/vnd.schemaregistry+json";
        internal const string Json = "application/json";

        /// <summary>
        ///     Initializes a new instance of the RestService class.
        /// </summary>
        internal SchemaRegistryService(IRollingRequest rollingRequest)
        {
            _rollingRequest = rollingRequest;
        }
        
        public async Task<Schema> GetSchemaAsync(int id, string format = null)
            => SanitizeSchema(await RequestAsync<Schema>($"schemas/ids/{id}{(format != null ? "?format=" + format : "")}",
                        HttpMethod.Get)
                    .ConfigureAwait(continueOnCapturedContext: false));
        
        // Checks whether a schema has been registered under a given subject.
        public async Task<RegisteredSchema> LookupSchemaAsync(string subject, Schema schema, bool ignoreDeletedSchemas)
            => await RequestAsync<RegisteredSchema>(
                    $"subjects/{WebUtility.UrlEncode(subject)}?deleted={!ignoreDeletedSchemas}", HttpMethod.Post,
                    new SchemaString(schema.SchemaString))
                .ConfigureAwait(continueOnCapturedContext: false);

        /// <remarks>
        ///     Used for end points that return a json object { ... }
        /// </remarks>
        private async Task<T> RequestAsync<T>(string path, HttpMethod method, params object[] jsonBody)
        {
            var response = await _rollingRequest.ExecuteRequestAsync((host) => CreateRequest(host, path, method, jsonBody))
                .ConfigureAwait(continueOnCapturedContext: false);
            var responseJson =
                await response.Content.ReadAsStringAsync().ConfigureAwait(continueOnCapturedContext: false);
            var t = JObject.Parse(responseJson).ToObject<T>();
            return t;
        }

        private static HttpRequestMessage CreateRequest(Uri hostUri, string path, HttpMethod method, params object[] jsonBody)
        { 
            var httpRequestMessage = new HttpRequestMessage
            {
                RequestUri = hostUri.AddRelativeUri(path),
                Method = method
            };

            if (jsonBody.Length == 0) 
                return httpRequestMessage;

            httpRequestMessage.Content = new StringContent(
                string.Join("\n", jsonBody.Select(JsonConvert.SerializeObject)),
                Encoding.UTF8,
                SchemaRegistryV1Json);
            
            httpRequestMessage.Content.Headers.ContentType.CharSet = string.Empty;

            return httpRequestMessage;
        }
        
        private static Schema SanitizeSchema(Schema schema)
        {
            // The JSON response from Schema Registry does not include
            // a references list if there are no references, which means
            // schema.References will be null here in that case. It's
            // semantically better if this is an empty list however, so
            // expose that.
            schema.References ??= new List<SchemaReference>();

            // The JSON response from Schema Registry does not include
            // schemaType in the avro case (only).
            
            // PATH: not possible access Confluent.SchemaRegistry.Schema.SchemaType_String
            // If schema.SchemaType is not set (throws InvalidOperationException) set the type to AVRO
            try
            {
                var isSchemaTypeSet = schema.SchemaType;
            }
            catch (InvalidOperationException)
            {
                schema.SchemaType = SchemaType.Avro;
            }

            return schema;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                _rollingRequest.Dispose();
            }
        }
    }
}
