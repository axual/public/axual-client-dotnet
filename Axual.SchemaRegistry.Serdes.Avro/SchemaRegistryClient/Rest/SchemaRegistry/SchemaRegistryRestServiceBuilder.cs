//
// Copyright 2020 Axual B.V. 2020
//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

using System;
using System.Linq;
using Axual.Kafka.Proxy;
using Axual.Kafka.Proxy.Exceptions;
using Axual.Kafka.Proxy.HttpClient;
using Axual.SchemaRegistry.Serdes.Avro.SchemaRegistryClient.Rest.RollingRestService;
using Confluent.SchemaRegistry;

namespace Axual.SchemaRegistry.Serdes.Avro.SchemaRegistryClient.Rest.SchemaRegistry
{
    internal class SchemaRegistryRestServiceBuilder : ISchemaRegistryRestServiceBuilder
    {
        private IHttpClientFactory _httpClientFactory;
        private AxualSchemaRegistryConfig _axualSchemaRegistryConfig;

        public bool Verify()
        {
            if (_httpClientFactory == null)
            {
                throw new RequireConfigurationException(nameof(HttpClientFactory));
            }
            
            if (_axualSchemaRegistryConfig == null)
            {
                throw new RequireConfigurationException(nameof(SchemaRegistryConfig));
            }
            
            if (_axualSchemaRegistryConfig.Url.IsNullOrEmpty())
            {
                throw new RequireConfigurationException(SchemaRegistryConfig.PropertyNames.SchemaRegistryUrl);
            }

            foreach (var srUrl in _axualSchemaRegistryConfig.Url.Split(','))
            {
                if (!Uri.IsWellFormedUriString(srUrl, UriKind.Absolute))
                {
                    throw new ArgumentException($"The Url '{srUrl}' is not valid Uri.");
                }
            }

            return true;
        }

        public ISchemaRegistryRestServiceBuilder SetHttpClient(IHttpClientFactory httpClientFactory)
        {
            _httpClientFactory = httpClientFactory;
            return this;
        }

        public ISchemaRegistryRestServiceBuilder SetSchemaRegistryConfig(AxualSchemaRegistryConfig schemaRegistryConfig)
        {
            _axualSchemaRegistryConfig = schemaRegistryConfig;
            return this;
        }
        
        public ISchemaRegistryService Build()
        {
            Verify();

            var httpClientConfig = new HttpClientConfig(_axualSchemaRegistryConfig.ToClientConfig())
            {
                EndPoint = _axualSchemaRegistryConfig.Url,
                Timeout = TimeSpan.FromMilliseconds(_axualSchemaRegistryConfig.RequestTimeoutMs)
            };
            var schemaRegistryUrls = _axualSchemaRegistryConfig.Url
                .Split(',')
                .Select(url => new Uri(url));
            var httpClient = _httpClientFactory.CreateClient(httpClientConfig);
            var rollingRestService = new RollingRequest(httpClient, schemaRegistryUrls);
            
            return new SchemaRegistryService(rollingRestService);
        }
    }
}