//
// Copyright 2020 Axual B.V. 2020
//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

using System.Runtime.CompilerServices;
using Axual.Kafka.Proxy.Proxies;
using Confluent.Kafka;
using Confluent.SchemaRegistry;

[assembly: InternalsVisibleTo("Axual.Kafka.Tests")]

namespace Axual.SchemaRegistry.Serdes.Avro.SchemaRegistryClient
{
    /// <summary>
    ///     <see cref="Serdes.Avro.SchemaRegistryClient.AxualSchemaRegistryClient" /> configuration properties.
    /// </summary>
    internal class AxualSchemaRegistryConfig : SchemaRegistryConfig
    {
        /// <summary>
        ///     Configuration property names specific to the schema registry client.
        /// </summary>
        /// <remarks>
        ///     This properties class is extenstion of <see cref="Confluent.SchemaRegistry.SchemaRegistryConfig.PropertyNames" /> configuration properties.
        /// </remarks>
        internal static class AxualPropertyNames
        {
            /// <summary>
            ///     PEM path to CA certificate(s) for verifying the Schema Registry's key. System CA certs will be used if not specified.
            /// 
            ///     Defaults: On Windows the system's CA certificates are automatically looked up in the Windows Root certificate store.
            ///     On Mac OSX it is recommended to install openssl using Homebrew, to provide CA certificates.
            ///     On Linux install the distribution's ca-certificates package. If OpenSSL is statically linked or `ssl.ca.location` is set to `probe` a list of standard paths will be probed and the first one found will be used as the default CA certificate location path. If OpenSSL is dynamically linked the OpenSSL library's default path will be used (see `OPENSSLDIR` in `openssl version -a`).
            /// </summary>
            public const string SslCertificatePem = "schema.registry.ssl.certificate.pem";

            /// <summary>
            ///     Path to client's public key (PEM) used for authentication.
            /// 
            ///     default: ''
            ///     importance: low
            /// </summary>
            public const string SslCertificateLocation = "ssl.certificate.location";

            /// <summary>
            ///     Client's private key string (PEM format) used for authentication.
            /// 
            ///     default: ''
            ///     importance: low
            /// </summary>
            public const string SslKeyPem = "schema.registry.ssl.key.pem";

            /// <summary>
            ///     Path to client's private key (PEM) used for authentication.
            /// 
            ///     default: ''
            ///     importance: low
            /// </summary>
            public const string SslKeyLocation = "schema.registry.ssl.key.location";

            /// <summary>
            ///     Private key passphrase (for use with `ssl.key.location` and `set_ssl_cert()`)
            /// 
            ///     default: ''
            ///     importance: low
            /// </summary>
            public const string SslKeyPassword = "schema.registry.ssl.key.location.password";

            /// <summary>
            ///     Endpoint identification algorithm to validate server hostname using server certificate. https - Server (server) hostname verification as specified in RFC2818. none - No endpoint verification. OpenSSL &gt;= 1.0.2 required.
            /// 
            ///     default: none
            ///     importance: low
            /// </summary>
            public const string SslEndpointIdentificationAlgorithm = "ssl.endpoint.identification.algorithm";

            /// <summary>
            ///     Comma-separated list of Windows Certificate stores to load CA certificates from. Certificates will be loaded in the same order as stores are specified. If no certificates can be loaded from any of the specified stores an error is logged and the OpenSSL library's default CA location is used instead. Store names are typically one or more of: MY, Root, Trust, CA.
            /// 
            ///     default: Root
            ///     importance: low
            /// </summary>
            public const string SslCaCertificateStores = "ssl.ca.certificate.stores";
        }

        /// <summary>
        ///     The default timeout value for Schema Registry REST API calls.
        /// </summary>
        private const int SchemaRegistryRequestTimeoutMs = 30000;

        /// <summary>
        ///     The default maximum capacity of the local schema cache.
        /// </summary>
        private const int SchemaRegistryMaxCachedSchemas = 1000;

        /// <summary>
        ///     The default key subject name strategy.
        /// </summary>
        private const SubjectNameStrategy SchemaRegistryKeySubjectNameStrategy = SubjectNameStrategy.Topic;

        /// <summary>
        ///     The default value subject name strategy.
        /// </summary>
        private const SubjectNameStrategy SchemaRegistryValueSubjectNameStrategy = SubjectNameStrategy.Topic;


        /// <summary>
        ///     Enable/Disable SSL server certificate verification. Only use in contained test/dev environments.
        /// 
        ///     default: true
        ///     importance: low
        /// </summary>
        public new bool? EnableSslCertificateVerification
        {
            get => base.EnableSslCertificateVerification ?? true;
            set => base.EnableSslCertificateVerification = value;
        }


        /// <summary>
        ///     Endpoint identification algorithm to validate server hostname using server certificate. https - Server (server) hostname verification as specified in RFC2818. none - No endpoint verification. OpenSSL &gt;= 1.0.2 required.
        /// 
        ///     default: none
        ///     importance: low
        /// </summary>
        public SslEndpointIdentificationAlgorithm SslEndpointIdentificationAlgorithm
        {
            get => Get(AxualPropertyNames.SslEndpointIdentificationAlgorithm) == "https"
                ? SslEndpointIdentificationAlgorithm.Https
                : SslEndpointIdentificationAlgorithm.None;
            set => Set(AxualPropertyNames.SslEndpointIdentificationAlgorithm,
                value == SslEndpointIdentificationAlgorithm.Https ? "https" : string.Empty);
        }

        /// <summary>
        ///     Comma-separated list of Windows Certificate stores to load CA certificates from. Certificates will be loaded in the same order as stores are specified. If no certificates can be loaded from any of the specified stores an error is logged and the OpenSSL library's default CA location is used instead. Store names are typically one or more of: MY, Root, Trust, CA.
        /// 
        ///     default: Root
        ///     importance: low
        /// </summary>
        public string SslCaCertificateStores
        {
            get => Get(AxualPropertyNames.SslCaCertificateStores);
            set => Set(AxualPropertyNames.SslCaCertificateStores, value);
        }
        
        /// <summary>
        ///     Path to client's public key (PEM) used for authentication.
        /// 
        ///     default: ''
        ///     importance: low
        /// </summary>
        public string SslCertificatePem
        {
            get => Get(AxualPropertyNames.SslCertificatePem);
            set => Set(AxualPropertyNames.SslCertificatePem, value);
        }

        /// <summary>
        ///     Path to client's public key (PEM) used for authentication.
        /// 
        ///     default: ''
        ///     importance: low
        /// </summary>
        public string SslCertificateLocation
        {
            get => Get(AxualPropertyNames.SslCertificateLocation);
            set => Set(AxualPropertyNames.SslCertificateLocation, value);
        }

        /// <summary>
        ///     Path to client's private key (PEM) used for authentication.
        /// 
        ///     default: ''
        ///     importance: low
        /// </summary>
        public string SslKeyLocation
        {
            get => Get(AxualPropertyNames.SslKeyLocation);
            set => Set(AxualPropertyNames.SslKeyLocation, value);
        }

        /// <summary>
        ///     Private key passphrase (for use with `ssl.key.location` and `set_ssl_cert()`)
        /// 
        ///     default: ''
        ///     importance: low
        /// </summary>
        public string SslKeyPassword
        {
            get => Get(AxualPropertyNames.SslKeyPassword);
            set => Set(AxualPropertyNames.SslKeyPassword, value);
        }

        /// <summary>
        ///     Client's private key string (PEM format) used for authentication.
        /// 
        ///     default: ''
        ///     importance: low
        /// </summary>
        public string SslKeyPem
        {
            get => Get(AxualPropertyNames.SslKeyPem);
            set => Set(AxualPropertyNames.SslKeyPem, value);
        }

        /// <summary>
        ///     Specifies the timeout for requests to Confluent Schema Registry.
        /// 
        ///     default: 30000
        /// </summary>
        public new int RequestTimeoutMs
        {
            get => base.RequestTimeoutMs ?? SchemaRegistryRequestTimeoutMs;
            set => base.RequestTimeoutMs = value;
        }


        /// <summary>
        ///     Specifies the maximum number of schemas CachedSchemaRegistryClient
        ///     should cache locally.
        /// 
        ///     default: 1000
        /// </summary>
        public new int MaxCachedSchemas
        {
            get => base.MaxCachedSchemas ?? SchemaRegistryMaxCachedSchemas;
            set => base.MaxCachedSchemas = value;
        }

        /// <summary>
        ///     Key subject name strategy.
        /// 
        ///     default: SubjectNameStrategy.Topic
        /// </summary>
        public new SubjectNameStrategy? KeySubjectNameStrategy
        {
            get => base.KeySubjectNameStrategy ?? SchemaRegistryKeySubjectNameStrategy;
            set => base.KeySubjectNameStrategy = value;
        }

        /// <summary>
        ///     Value subject name strategy.
        /// 
        ///     default: SubjectNameStrategy.Topic
        /// </summary>
        public new SubjectNameStrategy? ValueSubjectNameStrategy
        {
            get => base.ValueSubjectNameStrategy ?? SchemaRegistryValueSubjectNameStrategy;
            set => base.ValueSubjectNameStrategy = value;
        }
    }

    internal static class Extension
    {
        internal static AxualSchemaRegistryConfig ToAxualSchemaRegistryConfig(this ClientConfig clientConfig)
        {
            var config = new AxualSchemaRegistryConfig
            {
                Url = clientConfig.Get(Constants.ConfigurationKeys.Kafka.SchemaRegistryUrl),
                EnableSslCertificateVerification = clientConfig.EnableSslCertificateVerification,
                SslEndpointIdentificationAlgorithm = clientConfig.SslEndpointIdentificationAlgorithm ??
                                                     SslEndpointIdentificationAlgorithm.None,

                SslCaLocation = clientConfig.SslCaLocation ?? string.Empty,
                SslCaCertificateStores = clientConfig.SslCaCertificateStores,

                SslCertificateLocation = clientConfig.SslCertificateLocation ?? string.Empty,
                SslCertificatePem = clientConfig.SslCertificatePem ?? string.Empty,

                SslKeyLocation = clientConfig.SslKeyLocation ?? string.Empty,
                SslKeyPem = clientConfig.SslKeyPem ?? string.Empty,
                SslKeyPassword = clientConfig.SslKeyPassword ?? string.Empty,

                SslKeystoreLocation = clientConfig.SslKeystoreLocation ?? string.Empty,
                SslKeystorePassword = clientConfig.SslKeystorePassword ?? string.Empty
            };
            return config;
        }

        internal static ClientConfig ToClientConfig(this AxualSchemaRegistryConfig axualSchemaRegistryConfig)
        {
            var config = new ClientConfig
            {
                EnableSslCertificateVerification = axualSchemaRegistryConfig.EnableSslCertificateVerification,
                SslEndpointIdentificationAlgorithm = axualSchemaRegistryConfig.SslEndpointIdentificationAlgorithm,

                SslCaLocation = axualSchemaRegistryConfig.SslCaLocation ?? string.Empty,
                SslCaCertificateStores = axualSchemaRegistryConfig.SslCaCertificateStores,

                SslCertificateLocation = axualSchemaRegistryConfig.SslCertificateLocation ?? string.Empty,
                SslCertificatePem = axualSchemaRegistryConfig.SslCertificatePem ?? string.Empty,

                SslKeyLocation = axualSchemaRegistryConfig.SslKeyLocation ?? string.Empty,
                SslKeyPem = axualSchemaRegistryConfig.SslKeyPem ?? string.Empty,
                SslKeyPassword = axualSchemaRegistryConfig.SslKeyPassword ?? string.Empty,

                SslKeystoreLocation = axualSchemaRegistryConfig.SslKeystoreLocation ?? string.Empty,
                SslKeystorePassword = axualSchemaRegistryConfig.SslKeystorePassword ?? string.Empty
            };
            return config;
        }
    }
}