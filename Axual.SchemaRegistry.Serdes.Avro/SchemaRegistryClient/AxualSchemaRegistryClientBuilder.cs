//
// Copyright 2020 Axual B.V. 2020
//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

using System.Runtime.CompilerServices;
using Axual.Kafka.Proxy.Exceptions;
using Axual.Kafka.Proxy.HttpClient;
using Axual.SchemaRegistry.Serdes.Avro.SchemaRegistryClient.Rest.SchemaRegistry;
using Confluent.SchemaRegistry;

[assembly: InternalsVisibleTo("Axual.Kafka.Tests")]

namespace Axual.SchemaRegistry.Serdes.Avro.SchemaRegistryClient
{
    internal class AxualSchemaRegistryClientBuilder : ISchemaRegistryClientBuilder
    {
        private ISchemaRegistryRestServiceBuilder _schemaRegistryServiceBuilder;
        private AxualSchemaRegistryConfig _axualSchemaRegistryConfig;

        public void Verify()
        {
            if (_axualSchemaRegistryConfig == null)
            {
                throw new RequireConfigurationException(nameof(AxualSchemaRegistryConfig));
            }
        }

        public ISchemaRegistryClientBuilder SetAxualSchemaRegistryConfig(
            AxualSchemaRegistryConfig axualSchemaRegistryConfig)
        {
            _axualSchemaRegistryConfig = axualSchemaRegistryConfig;
            return this;
        }

        public ISchemaRegistryClientBuilder SetRestService(ISchemaRegistryRestServiceBuilder schemaRegistryService)
        {
            _schemaRegistryServiceBuilder = schemaRegistryService;
            return this;
        }

        public ISchemaRegistryClient Build()
        {
            Verify();
            
            // use default HttpClient and set set SchemaRegistryConfig in case builder is not set
            _schemaRegistryServiceBuilder ??= new SchemaRegistryRestServiceBuilder()
                .SetHttpClient(new HttpClientFactory())
                .SetSchemaRegistryConfig(_axualSchemaRegistryConfig);
            var schemaRegistryService = _schemaRegistryServiceBuilder.Build();

            return new AxualSchemaRegistryClient(schemaRegistryService, _axualSchemaRegistryConfig);
        }
    }
}