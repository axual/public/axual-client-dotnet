﻿//
// Copyright 2020 Axual B.V. 2020
//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using Axual.SchemaRegistry.Serdes.Avro.SchemaRegistryClient.Rest.SchemaRegistry;
using Confluent.Kafka;
using Confluent.SchemaRegistry;

[assembly: InternalsVisibleTo("DynamicProxyGenAssembly2")]

namespace Axual.SchemaRegistry.Serdes.Avro.SchemaRegistryClient
{
    /// <summary>
    ///     A caching Schema Registry client.
    /// <br/>
    ///     The following method calls cache results:<br/>
    ///     - <see cref="AxualSchemaRegistryClient.GetSchemaIdAsync(string, Schema)" /><br/>
    ///     - <see cref="AxualSchemaRegistryClient.GetSchemaIdAsync(string, string)" /><br/>
    ///     - <see cref="AxualSchemaRegistryClient.GetSchemaAsync(int, string)" /><br/>
    ///     - <see cref="AxualSchemaRegistryClient.GetRegisteredSchemaAsync(string, int)" /><br/>
    ///<br/>
    ///     The following method are not supported:<br/>
    ///     - <see cref="AxualSchemaRegistryClient.LookupSchemaAsync(string, Schema, bool)" /><br/>
    ///     - <see cref="AxualSchemaRegistryClient.GetLatestSchemaAsync(string)" /><br/>
    ///     - <see cref="AxualSchemaRegistryClient.GetAllSubjectsAsync" /><br/>
    ///     - <see cref="AxualSchemaRegistryClient.GetSubjectVersionsAsync(string)" /><br/>
    ///     - <see cref="AxualSchemaRegistryClient.RegisterSchemaAsync(string, string)" /><br/>
    ///     - <see cref="AxualSchemaRegistryClient.IsCompatibleAsync(string, Schema)" /><br/>
    ///     - <see cref="AxualSchemaRegistryClient.IsCompatibleAsync(string, string)" /><br/>
    /// </summary>
    /// <remarks>
    ///     This source code is originated from  <see cref="Confluent.SchemaRegistry.CachedSchemaRegistryClient "/>
    ///     Remove support for unnecessary functionalities
    /// </remarks>
    internal class AxualSchemaRegistryClient : ISchemaRegistryClient
    {
        private readonly ISchemaRegistryService _schemaRegistryService;

        /// <summary>
        /// Cache dictionary to map SchemaId to <see cref="Confluent.SchemaRegistry.Schema"/> 
        /// </summary>
        private readonly Dictionary<int, Schema> _schemaById = new Dictionary<int, Schema>();

        /// <summary>
        /// Dictionary cached for Schema  
        /// </summary>
        private readonly Dictionary<string /*subject*/, Dictionary<string, int>> _idBySchemaBySubject;

        private readonly SubjectNameStrategyDelegate _keySubjectNameStrategy;
        private readonly SubjectNameStrategyDelegate _valueSubjectNameStrategy;

        private readonly SemaphoreSlim _cacheMutex = new SemaphoreSlim(1);

        /// <inheritdoc />
        public int MaxCachedSchemas { get; }

        /// <summary>
        ///     Initialize a new instance of the <see cref="AxualSchemaRegistryClient"/> class.
        /// </summary>
        internal AxualSchemaRegistryClient(ISchemaRegistryService schemaRegistryClient,
            AxualSchemaRegistryConfig axualSchemaRegistryConfig)
        {
            _idBySchemaBySubject = new Dictionary<string, Dictionary<string, int>>();
            _keySubjectNameStrategy = GetKeySubjectNameStrategy(axualSchemaRegistryConfig);
            _valueSubjectNameStrategy = GetValueSubjectNameStrategy(axualSchemaRegistryConfig);
            MaxCachedSchemas = axualSchemaRegistryConfig.MaxCachedSchemas;

            _schemaRegistryService = schemaRegistryClient;
        }

        /// <remarks>
        ///     This is to make sure memory doesn't explode in the case of incorrect usage.
        /// 
        ///     It's behavior is pretty extreme - remove everything and start again if the 
        ///     cache gets full. However, in practical situations this is not expected.
        /// </remarks>
        private void CleanCacheIfFull()
        {
            if (_schemaById.Count < MaxCachedSchemas) 
                return;
            
            _schemaById.Clear();
            _idBySchemaBySubject.Clear();
        }

        /// <inheritdoc/>
        public Task<int> GetSchemaIdAsync(string subject, string avroSchema)
            => GetSchemaIdAsync(subject, new Schema(avroSchema, new List<SchemaReference>(), SchemaType.Avro));


        /// <inheritdoc/>
        public async Task<int> GetSchemaIdAsync(string subject, Schema schema)
        {
            await _cacheMutex.WaitAsync().ConfigureAwait(continueOnCapturedContext: false);
            try
            {
                if (!_idBySchemaBySubject.TryGetValue(subject, out var idBySchema))
                {
                    idBySchema = new Dictionary<string, int>();
                    _idBySchemaBySubject.Add(subject, idBySchema);
                }

                // contains very few elements and the schema string passed in is always the same
                // instance.

                if (!idBySchema.TryGetValue(schema.SchemaString, out var schemaId))
                {
                    CleanCacheIfFull();

                    // throws SchemaRegistryException if schema is not known.
                    var registeredSchema = await _schemaRegistryService.LookupSchemaAsync(subject, schema, true)
                        .ConfigureAwait(continueOnCapturedContext: false);
                    idBySchema[schema.SchemaString] = registeredSchema.Id;
                    _schemaById[registeredSchema.Id] = registeredSchema.Schema;
                    schemaId = registeredSchema.Id;
                }

                return schemaId;
            }
            finally
            {
                _cacheMutex.Release();
            }
        }

        /// <inheritdoc/>
        public async Task<Schema> GetSchemaAsync(int id, string format = null)
        {
            await _cacheMutex.WaitAsync().ConfigureAwait(continueOnCapturedContext: false);
            try
            {
                if (_schemaById.TryGetValue(id, out var schema) &&
                    CheckSchemaMatchesFormat(format, schema.SchemaString))
                    return schema;

                CleanCacheIfFull();
                schema = await _schemaRegistryService.GetSchemaAsync(id, format)
                    .ConfigureAwait(continueOnCapturedContext: false);
                _schemaById[id] = schema;
                return schema;
            }
            finally
            {
                _cacheMutex.Release();
            }
        }

        /// <summary>
        /// Not Supported
        /// </summary>
        [Obsolete(
            "Superseded by GetRegisteredSchemaAsync(string subject, int version). This method will be removed in a future release.")]
        public Task<string> GetSchemaAsync(string subject, int version)
        {
            throw new NotSupportedException();
        }

        /// <summary>
        ///     Check if the given schema string matches a given format name.
        /// </summary>
        private static bool CheckSchemaMatchesFormat(string format, string schemaString)
        {
            // if a format isn't specified, then assume text is desired.
            if (format == null)
            {
                try
                {
                    Convert.FromBase64String(schemaString);
                }
                catch (Exception)
                {
                    return true; // Base64 conversion failed, infer the schemaString format is text.
                }

                return false; // Base64 conversion succeeded, so infer the schamaString format is base64.
            }

            if (format != "serialized")
            {
                throw new ArgumentException($"Invalid schema format was specified: {format}.");
            }

            try
            {
                Convert.FromBase64String(schemaString);
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }

        [Obsolete(
            "SubjectNameStrategy should now be specified via serializer configuration. This method will be removed in a future release.")]
        private static SubjectNameStrategyDelegate GetKeySubjectNameStrategy(
            IEnumerable<KeyValuePair<string, string>> config)
        {
            var keySubjectNameStrategyString = config.FirstOrDefault(prop =>
                                                   prop.Key.ToLower() == SchemaRegistryConfig.PropertyNames
                                                       .SchemaRegistryKeySubjectNameStrategy).Value ??
                                               "";
            var keySubjectNameStrategy = SubjectNameStrategy.Topic;

            if (keySubjectNameStrategyString != "" &&
                !Enum.TryParse(keySubjectNameStrategyString, out keySubjectNameStrategy))
            {
                throw new ArgumentException($"Unknown KeySubjectNameStrategy: {keySubjectNameStrategyString}");
            }

            return keySubjectNameStrategy.ToDelegate();
        }

        [Obsolete(
            "SubjectNameStrategy should now be specified via serializer configuration. This method will be removed in a future release.")]
        private static SubjectNameStrategyDelegate GetValueSubjectNameStrategy(
            IEnumerable<KeyValuePair<string, string>> config)
        {
            var valueSubjectNameStrategyString = config.FirstOrDefault(prop =>
                    prop.Key.ToLower() == SchemaRegistryConfig.PropertyNames.SchemaRegistryValueSubjectNameStrategy)
                .Value ?? "";
            var valueSubjectNameStrategy = SubjectNameStrategy.Topic;

            if (valueSubjectNameStrategyString != "" &&
                !Enum.TryParse(valueSubjectNameStrategyString, out valueSubjectNameStrategy))
            {
                throw new ArgumentException($"Unknown ValueSubjectNameStrategy: {valueSubjectNameStrategyString}");
            }

            return valueSubjectNameStrategy.ToDelegate();
        }

        /// <inheritdoc />
        [Obsolete(
            "SubjectNameStrategy should now be specified via serializer configuration. This method will be removed in a future release.")]
        public string ConstructKeySubjectName(string topic, string recordType = null)
            => _keySubjectNameStrategy(new SerializationContext(MessageComponentType.Key, topic), recordType);


        /// <inheritdoc />
        [Obsolete(
            "SubjectNameStrategy should now be specified via serializer configuration. This method will be removed in a future release.")]
        public string ConstructValueSubjectName(string topic, string recordType = null)
            => _valueSubjectNameStrategy(new SerializationContext(MessageComponentType.Value, topic), recordType);

        #region NotSupported

        /// <summary>
        /// Not Supported
        /// </summary>
        public Task<RegisteredSchema> LookupSchemaAsync(string subject, Schema schema, bool ignoreDeletedSchemas)
        {
            throw new NotSupportedException();
        }

        /// <summary>
        /// Not Supported
        /// </summary>
        public Task<int> RegisterSchemaAsync(string subject, Schema schema)
        {
            throw new NotSupportedException();
        }

        /// <summary>
        /// Not Supported
        /// </summary>
        public Task<int> RegisterSchemaAsync(string subject, string avroSchema)
            => throw new NotSupportedException();

        /// <summary>
        /// Not Supported
        /// </summary>
        public Task<RegisteredSchema> GetRegisteredSchemaAsync(string subject,
            int version)
        {
            throw new NotSupportedException();
        }

        /// <summary>
        /// Not Supported
        /// </summary>
        public Task<RegisteredSchema> GetLatestSchemaAsync(string subject)
        {
            throw new NotSupportedException();
        }

        /// <summary>
        /// Not Supported
        /// </summary>
        public Task<List<string>> GetAllSubjectsAsync()
        {
            throw new NotSupportedException();
        }

        /// <summary>
        /// Not Supported
        /// </summary>
        public Task<List<int>> GetSubjectVersionsAsync(string subject)
        {
            throw new NotSupportedException();
        }

        /// <summary>
        /// Not Supported
        /// </summary>
        public Task<bool> IsCompatibleAsync(string subject, Schema schema)
        {
            throw new NotSupportedException();
        }

        /// <summary>
        /// Not Supported
        /// </summary>
        public Task<bool> IsCompatibleAsync(string subject, string avroSchema)
        {
            throw new NotSupportedException();
        }

        #endregion

        /// <summary>
        ///     Releases unmanaged resources owned by this CachedSchemaRegistryClient instance.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        ///     Releases the unmanaged resources used by this object
        ///     and optionally disposes the managed resources.
        /// </summary>
        /// <param name="disposing">
        ///     true to release both managed and unmanaged resources;
        ///     false to release only unmanaged resources.
        /// </param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                _schemaRegistryService.Dispose();
            }
        }
    }
}
