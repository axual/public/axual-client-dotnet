//
// Copyright 2020 Axual B.V. 2020
//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

using System;
using System.Threading.Tasks;
using Axual.Kafka.Proxy.SerDes;
using Axual.SchemaRegistry.Serdes.Avro.SchemaRegistryClient;
using Confluent.Kafka;
using Confluent.SchemaRegistry.Serdes;

namespace Axual.SchemaRegistry.Serdes.Avro.Deserializers
{
    public abstract class AbstractAvroDeserializer<T> : IAsyncDeserializer<T>, IConfigurable
    {
        private IAsyncDeserializer<T> _asyncDeserializer;

        public Task<T> DeserializeAsync(ReadOnlyMemory<byte> data, bool isNull, SerializationContext context)
        {
            if (_asyncDeserializer == null)
                throw new InvalidOperationException("AvroDeserializer is not Configured");

            return _asyncDeserializer.DeserializeAsync(data, isNull, context);
        }

        public void Configure(ClientConfig clientConfig)
        {
            var axualSchemaRegistryConfig = clientConfig.ToAxualSchemaRegistryConfig();
            var schemaRegistryClient = new AxualSchemaRegistryClientBuilder()
                .SetAxualSchemaRegistryConfig(axualSchemaRegistryConfig)
                .Build();

            _asyncDeserializer = new AvroDeserializer<T>(schemaRegistryClient);
        }
    }
}