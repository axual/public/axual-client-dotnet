//
// Copyright 2020 Axual B.V. 2020
//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

using System.Collections.Generic;
using System.Text.Json;
using Axual.Kafka.Proxy.DiscoveryFetcher;
using Axual.Kafka.Proxy.Proxies;

namespace Axual.Kafka.Tests
{
    internal static class TestConstants
    {
        internal static readonly string DiscoveryApiResponse =
            "{" +
            "    \"tenant\":\"axual\"," +
            "    \"environment\":\"dev\"," +
            "    \"instance\":\"local\"," +
            "    \"system\":\"axual-cloud\"," +
            "    \"cluster\":\"clusterA\"," +
            "    \"bootstrap.servers\":\"192.168.99.100:9096\"," +
            "    \"schema.registry.url\":\"http://platform.local:25000\"," +
            "    \"rest.proxy.url\":\"http://platform.local:32000\"," +
            "    \"distributor.timeout\":\"5000\"," +
            "    \"distributor.distance\":\"1\"," +
            "    \"ttl\":\"600000\"," +
            "    \"system\":\"systemA\"," +
            "    \"enable.value.headers\":\"true\"," +
            "    \"group.id.resolver\":\"io.axual.common.resolver.GroupPatternResolver\"," +
            "    \"group.id.pattern\":\"{tenant}-{environment}-{group}\"," +
            "    \"topic.resolver\":\"io.axual.common.resolver.TopicPatternResolver\"," +
            "    \"topic.pattern\":\"{tenant}-{environment}-{topic}\"" +
            "}";

        internal static readonly string DiscoveryApiResponseClusterB =
            "{" +
            "    \"tenant\":\"axual\"," +
            "    \"environment\":\"dev\"," +
            "    \"instance\":\"local\"," +
            "    \"system\":\"axual-cloud\"," +
            "    \"cluster\":\"clusterB\"," +
            "    \"bootstrap.servers\":\"192.168.99.100:9096\"," +
            "    \"schema.registry.url\":\"http://platform.local:25000\"," +
            "    \"rest.proxy.url\":\"http://platform.local:32000\"," +
            "    \"distributor.timeout\":\"1000\"," +
            "    \"distributor.distance\":\"1\"," +
            "    \"ttl\":\"600000\"," +
            "    \"enable.value.headers\":\"true\"," +
            "    \"group.id.resolver\":\"io.axual.common.resolver.GroupPatternResolver\"," +
            "    \"group.id.pattern\":\"{tenant}-{environment}-{group}\"," +
            "    \"topic.resolver\":\"io.axual.common.resolver.TopicPatternResolver\"," +
            "    \"topic.pattern\":\"{tenant}-{environment}-{topic}\"" +
            "}";

        internal static readonly Dictionary<string, string> DiscoveryApiConfiguration =
            new(
                new List<KeyValuePair<string, string>>
                {
                    new(Constants.ConfigurationKeys.Axual.Tenant, "axual"),
                    new(Constants.ConfigurationKeys.Axual.Environment, "dev"),
                    new(Constants.ConfigurationKeys.Axual.Instance, "local"),
                    new(Constants.ConfigurationKeys.Axual.System, "axual-cloud"),
                    new(Constants.ConfigurationKeys.Axual.Cluster, "clusterA"),
                    new(Constants.ConfigurationKeys.Kafka.BootstrapServers,
                        "192.168.99.100:9096"),
                    new("schema.registry.url", "http://platform.local:25000"),
                    new("rest.proxy.url", "http://platform.local:32000"),
                    new("distributor.timeout", "5000"),
                    new("distributor.distance", "1"),
                    new("ttl", "600000"),
                    new("enable.value.headers", "true"),
                    new("group.id.resolver",
                        "io.axual.common.resolver.GroupPatternResolver"),
                    new("group.id.pattern", "{tenant}-{environment}-{group}"),
                    new("topic.resolver", "io.axual.common.resolver.TopicPatternResolver"),
                    new("topic.pattern", "{tenant}-{environment}-{topic}")
                });


        private static readonly JsonSerializerOptions JsonSerializerOptions = new()
        {
            PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
            WriteIndented = true
        };

        internal static string CreateDiscoveryResult(Dictionary<string, string> config)
        {
            var dr = new DiscoveryResult(config);
            return JsonSerializer.Serialize(dr, JsonSerializerOptions);
        }
    }
}