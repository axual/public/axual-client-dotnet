using System;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using Axual.Kafka.Proxy;
using NUnit.Framework;

namespace Axual.Kafka.Tests
{
    [TestFixture]
    internal class UtilsTests
    {
        [TestCase("localhost", "localhost")]
        [TestCase("platform.local", "platform.local")]
        [TestCase("axual-local-discovery-api", "axual-local-discovery-api")]
        [TestCase("local.com", "local.com")]
        [TestCase("main.local.com", "main.local.com")]
        public void IsHostNameMatchSan_CheckDifferentSANs_True(string hostname, string subjectAltNames)
        {
            Assert.IsTrue(SslUtils.IsHostNameMatch(hostname, subjectAltNames));
        }

        [TestCase("main.local.com", "*.local.com")]
        [TestCase("platform.local.com", "*.local.com")]
        public void IsHostNameMatchSan_CheckSANsWithWildCard_True(string hostname, string subjectAltNames)
        {
            Assert.IsTrue(SslUtils.IsHostNameMatch(hostname, subjectAltNames));
        }

        [TestCase("localhost", "platform.local")]
        [TestCase("local", "platform.local")]
        public void IsHostNameMatchSan_CheckDifferentSANs_False(string hostname, string subjectAltNames)
        {
            Assert.IsFalse(SslUtils.IsHostNameMatch(hostname, subjectAltNames));
        }

        [TestCase("localhost", "*.test.com")] // only support 1 wild card
        [TestCase("main.local.com", "*.test.com")] // sub domain not correct
        [TestCase("main.local.test.com", "*.local.*.com")] // only support 1 wild card
        [TestCase("main.local.test.com", "*.*.test.com")] // only support 1 wild card
        public void IsHostNameMatchSan_CheckDifferentSANsWithWildCard_False(string hostname, string subjectAltNames)
        {
            Assert.IsFalse(SslUtils.IsHostNameMatch(hostname, subjectAltNames));
        }

        [TestCase("DNS_company.local.com.cer", "company.local.com")]
        [TestCase("DNS_*.local.com.cer", "*.local.com")]
        [TestCase("IP_Address_127.0.0.1.cer", "127.0.0.1")]
        public void GetSubjectAlternativeNames_ParseSubjectAlternativeNames(string certName, string expectedSans)
        {
            var certFullPath = UtilsHelper.GetFileFromResourcesPath(certName);
            var list = SslUtils.GetSubjectAlternativeNames(new X509Certificate2(certFullPath));

            CollectionAssert.AreEquivalent(list, new List<string> {expectedSans});
        }

        [Test]
        public void Utils_UriAddParameter_ParameterAdded()
        {
            var uri = new Uri("https://localhost");
            uri = uri.AddParameter("key", "value");

            Assert.AreEqual("https://localhost/?key=value", uri.ToString());
        }

        [Test]
        public void Utils_UriRemoveParameter_ParameterRemoved()
        {
            var uri = new Uri("https://localhost");
            uri = uri.AddParameter("key", "value");
            uri = uri.RemoveParameter("key");

            Assert.AreEqual("https://localhost/", uri.ToString());
        }

        [Test]
        public void Utils_UriRemoveParameterWhenHavingMultiParameters_OnlyOneParameterBeenRemoved()
        {
            var uri = new Uri("https://localhost");
            uri = uri.AddParameter("key1", "value1");
            uri = uri.AddParameter("key2", "value2");
            uri = uri.RemoveParameter("key1");

            Assert.AreEqual("https://localhost/?key2=value2", uri.ToString());
        }
    }
}