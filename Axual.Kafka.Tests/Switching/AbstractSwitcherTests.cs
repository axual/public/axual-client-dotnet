using System;
using System.Collections.Generic;
using System.Threading;
using Axual.Kafka.Proxy.DiscoveryFetcher;
using Axual.Kafka.Proxy.Proxies;
using Axual.Kafka.Proxy.Proxies.Switching.Proxy;
using Confluent.Kafka;
using NSubstitute;
using NUnit.Framework;

namespace Axual.Kafka.Tests.Switching
{
    [TestFixture]
    internal abstract class AbstractSwitcherTests
    {
        protected ISwitcher Switcher;
        protected IFetcherFactory MockFetcherFactory;
        protected IDiscoveryRestService MockDiscoveryRestService;
        protected ClientConfig ClientConfig;
        protected IFetcher Fetcher;

        [SetUp]
        public virtual void SetUp()
        {
            Fetcher = Substitute.For<IFetcher>();
            MockFetcherFactory = Substitute.For<IFetcherFactory>();
            MockDiscoveryRestService = Substitute.For<IDiscoveryRestService>();

            ClientConfig = new ClientConfig(new Dictionary<string, string>
            {
                {
                    Constants.ConfigurationKeys.ClientConfigMapper[Constants.ConfigurationKeys.Axual.ApplicationId],
                    Guid.NewGuid().ToString()
                }
            });

            MockFetcherFactory.CreateFetcher(MockDiscoveryRestService).Returns(Fetcher, null);
        }

        [Test]
        public abstract void AbstractSwitcherTests_MissingApplicationId_ThrowRequire();

        [Test]
        public abstract void AbstractSwitcherTests_StartListeningWithSameApplicationId_DoesntCallFetcherStartAgain();

        [Test]
        public void AbstractSwitcherTests_StartListening_StartingFetching()
        {
            Fetcher.Received(1).StartAsync(Arg.Any<CancellationToken>());
            Fetcher.Received(1).OnNewFetchResult += Arg.Any<EventHandler<DiscoveryResult>>();
        }

        [Test]
        public void AbstractSwitcherTests_StopListeningAfterStart_NoException()
        {
            Switcher.StartListening();
            Switcher.StopListening();
        }
    }
}