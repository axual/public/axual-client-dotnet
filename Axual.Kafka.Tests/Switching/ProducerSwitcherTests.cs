using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using Axual.Kafka.Proxy;
using Axual.Kafka.Proxy.DiscoveryFetcher;
using Axual.Kafka.Proxy.Exceptions;
using Axual.Kafka.Proxy.Proxies;
using Axual.Kafka.Proxy.Proxies.Switching;
using Axual.Kafka.Proxy.Proxies.Switching.Proxy;
using Confluent.Kafka;
using NSubstitute;
using NUnit.Framework;
using static Axual.Kafka.Proxy.Proxies.Constants.ConfigurationKeys;

namespace Axual.Kafka.Tests.Switching
{
    [TestFixture]
    internal class ProducerSwitcherTests : AbstractSwitcherTests
    {
        private IProducer<int, int> _switchingProducer;
        private Confluent.Kafka.ProducerConfig _producerConfig;
        private IProducer<int, int> _mockNextProducer;
        private IProducer<int, int> _mockNextProducerOfNewCluster;
        private IProducerBuilder<int,int> _mockNextProducerBuilder;
        private readonly DiscoveryResult _discoveryResult = new DiscoveryResult();

        [SetUp]
        public override void SetUp()
        {
            base.SetUp();

            _mockNextProducer = Substitute.For<IProducer<int, int>>();
            _mockNextProducer.Name.Returns("old Cluster Producer");
            _mockNextProducerOfNewCluster = Substitute.For<IProducer<int, int>>();
            _mockNextProducerOfNewCluster.Name.Returns("new Cluster Producer");
            _mockNextProducerBuilder = Substitute.For<IProducerBuilder<int, int>>();
            //_mockNextProducerBuilder.Build().Returns(_mockNextProducer);
            _mockNextProducerBuilder.Rebuild(Arg.Any<Confluent.Kafka.ProducerConfig>()).Returns(_mockNextProducer, _mockNextProducerOfNewCluster);

            _producerConfig = new Confluent.Kafka.ProducerConfig(ClientConfig);
            Switcher = new ProducerSwitcher<int, int>(_mockNextProducerBuilder, _producerConfig, MockFetcherFactory,
                MockDiscoveryRestService);
            _switchingProducer = new SwitchingProducer<int, int>(_mockNextProducerBuilder, _producerConfig, Switcher);

            // trigger event of first fetch to init nextConsumer in SwitchingConsumer
            Fetcher.OnNewFetchResult += Raise.Event<EventHandler<DiscoveryResult>>(this,
                new DiscoveryResult(new Dictionary<string, string>
                {
                    {Constants.ConfigurationKeys.Axual.Cluster, "ClusterName"}
                }));
        }

        [Test]
        public void ProducerSwitcher_ProducerSwitch_CallDispose()
        {
            Switcher.Switch(_switchingProducer,
                new Utils.NewDiscoveryConfigEventArgs(_discoveryResult, new DiscoveryResult()));

            _mockNextProducer.Received(1).Dispose();
        }

        [TestCase(1000, 1, 0, 1)]
        [TestCase(1000, 2, 0, 1)]
        [TestCase(1000, 1, 2000, 1)]
        [TestCase(1000, 1, 0, 2)]
        [TestCase(1000, 2, 0, 2)]
        [TestCase(1000, 1, 2000, 2)]
        public void ProducerSwitcher_NewCluster_WaitCalculateCorrectTotalDistributionTimeout(int distributionTimeout,
            int distributorDistance, int ttl, int maxInFlight)
        {
            var producerConfig = new Confluent.Kafka.ProducerConfig(ClientConfig)
            {
                MaxInFlight = maxInFlight
            };

            Switcher = new ProducerSwitcher<int, int>(_mockNextProducerBuilder, producerConfig, MockFetcherFactory,
                MockDiscoveryRestService);

            var stopwatch = new Stopwatch();
            var time = DateTime.UtcNow; // Remove the "alreadyWaited" element in the AbstractSwitcher.GetSwitchTimeout
            var oldDiscoveryResult = new DiscoveryResult(time, new Dictionary<string, string>
            {
                {Constants.ConfigurationKeys.Axual.Cluster, "Cluster A"}
            });
            var newDiscoveryResult = new DiscoveryResult(time, new Dictionary<string, string>
            {
                {Constants.ConfigurationKeys.Axual.Cluster, "Cluster B"}, // difference cluster to trigger the wait time
                {Constants.ConfigurationKeys.Switching.DistributorDistance, distributorDistance.ToString()},
                {Constants.ConfigurationKeys.Switching.Ttl, ttl.ToString()},
                {Constants.ConfigurationKeys.Switching.DistributorTimeout, distributionTimeout.ToString()}
            });

            stopwatch.Start();
            Switcher.Switch(_switchingProducer,
                new Utils.NewDiscoveryConfigEventArgs(oldDiscoveryResult, newDiscoveryResult));
            stopwatch.Stop();

            var duration = stopwatch.Elapsed;
            var buffer = TimeSpan.FromSeconds(0.3);
            var atLeastWaitTime =
                maxInFlight == 1
                    ?
                    // NOTE : TTL doesn't have a factor
                    TimeSpan.FromMilliseconds(distributionTimeout * distributorDistance)
                    : TimeSpan.Zero;
            var atMostWaitTime = atLeastWaitTime;

            // Note: Not best practice should validate only the GetSwitchTimeout
            Assert.IsTrue(duration >= atLeastWaitTime - buffer,
                $"Switching waited {duration:c}, expected to wait at least {atLeastWaitTime:c}.");
            Assert.IsTrue(duration <= atMostWaitTime + buffer,
                $"Switching waited {stopwatch:c}, expected to wait at most {atLeastWaitTime:c}.");
        }

        [Test]
        public void ProducerSwitcher_Configure_NoException()
        {
            Switcher.Configure(new ClientConfig(), _mockNextProducerBuilder);
        }

        [Test]
        public override void AbstractSwitcherTests_MissingApplicationId_ThrowRequire()
        {
            Assert.Throws<RequireConfigurationException>(() =>
            {
                var consumerSwitcher = new ProducerSwitcher<int, int>(_mockNextProducerBuilder,
                    new ClientConfig(), MockFetcherFactory, MockDiscoveryRestService);
            });
        }

        [Test]
        public override void AbstractSwitcherTests_StartListeningWithSameApplicationId_DoesntCallFetcherStartAgain()
        {
            var applicationIdKey = ClientConfigMapper[Constants.ConfigurationKeys.Axual.ApplicationId];
            var clientConfig = new ClientConfig(new Dictionary<string, string>
            {
                {applicationIdKey, Guid.NewGuid().ToString()}
            });

            var mockFetch = Substitute.For<IFetcher>();
            var mockFetcherFactory = Substitute.For<IFetcherFactory>();
            mockFetcherFactory.CreateFetcher(Arg.Any<IDiscoveryRestService>()).Returns(mockFetch);

            var producerSwitcher = new ProducerSwitcher<int, int>(Substitute.For<IProducerBuilder<int, int>>(),
                clientConfig,
                mockFetcherFactory, 
                MockDiscoveryRestService);
            var secondProducerSwitcherSameId = new ProducerSwitcher<int, int>(
                Substitute.For<IProducerBuilder<int, int>>(),
                clientConfig, 
                mockFetcherFactory, 
                MockDiscoveryRestService);
            
            mockFetch.StartAsync(Arg.Any<CancellationToken>())
                .Returns(info => Task.Delay(1))
                .AndDoes(info => mockFetch.IsRunning.Returns(true));

            producerSwitcher.StartListening();
            secondProducerSwitcherSameId.StartListening();

            // Assert 
            // Create one fetcher both client have the same applicationIdKey
            mockFetcherFactory.Received(1).CreateFetcher(Arg.Any<IDiscoveryRestService>());
            // should call once to 'ConsumerSwitcher.StartListening', 2nd consumer switcher using the same
            // fetcher as the first consumer
            mockFetch.Received(1).StartAsync(Arg.Any<CancellationToken>());
        }
        
        [Test]
        public void ProducerSwitcherTests_SwitchWithInitTransactions_AbortTransactionsAndInitTransactionsOnNewCluster()
        {
            var timeout = TimeSpan.FromSeconds(1);
            _switchingProducer.InitTransactions(timeout);
            
            // Switching
            Switcher.Switch(_switchingProducer,
                new Utils.NewDiscoveryConfigEventArgs(new DiscoveryResult(), new DiscoveryResult()));
            
            // Abort Transaction on old cluster
            _mockNextProducer.Received(1).AbortTransaction(Arg.Any<TimeSpan>());
            // InitTransactions on the new cluster
            _mockNextProducerOfNewCluster.Received(1).InitTransactions(timeout);
        }
        
        [Test]
        public void ProducerSwitcherTests_SwitchNoCallToInitTransactions_NotAbortTransactionsAndInitTransactions()
        {
            // Switching
            Switcher.Switch(_switchingProducer,
                new Utils.NewDiscoveryConfigEventArgs(new DiscoveryResult(), new DiscoveryResult()));
            
            // Abort Transaction on old cluster
            _mockNextProducer.Received(0).AbortTransaction(Arg.Any<TimeSpan>());
            // InitTransactions on the new cluster
            _mockNextProducerOfNewCluster.Received(0).InitTransactions(Arg.Any<TimeSpan>());
        }
        
        [Test]
        public void ProducerSwitcherTests_AbortTransactionThrowException_Switched()
        {
            // Arrange
            _mockNextProducer.WhenForAnyArgs(producer => producer.AbortTransaction(TimeSpan.Zero)).Throw(new Exception());
            _switchingProducer.InitTransactions(TimeSpan.Zero);
            
            // Switching
            var nextProducer = Switcher.Switch(_switchingProducer,
                new Utils.NewDiscoveryConfigEventArgs(new DiscoveryResult(), new DiscoveryResult()));
            
            // Call Abort Transaction on old cluster
            _mockNextProducer.Received(1).AbortTransaction(Arg.Any<TimeSpan>());
            // Get NextProducer instance even with the exception
            Assert.IsTrue(nextProducer == _mockNextProducerOfNewCluster);
        }
    }
}