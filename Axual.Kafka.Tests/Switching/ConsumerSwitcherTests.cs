using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Axual.Kafka.Proxy;
using Axual.Kafka.Proxy.DiscoveryFetcher;
using Axual.Kafka.Proxy.Exceptions;
using Axual.Kafka.Proxy.Proxies;
using Axual.Kafka.Proxy.Proxies.Switching;
using Axual.Kafka.Proxy.Proxies.Switching.Proxy;
using Confluent.Kafka;
using NSubstitute;
using NUnit.Framework;
using static Axual.Kafka.Proxy.Proxies.Constants.ConfigurationKeys;

namespace Axual.Kafka.Tests.Switching
{
    [TestFixture]
    internal class ConsumerSwitcherTests : AbstractSwitcherTests
    {
        private SwitchingConsumer<int, int> _consumer;
        private Confluent.Kafka.ConsumerConfig _consumerConfig;
        private IConsumer<int, int> _mockNextConsumer;
        private IConsumerBuilder<int, int> _mockNextConsumerBuilder;
        private readonly DiscoveryResult _discoveryResult = new DiscoveryResult();

        [SetUp]
        public override void SetUp()
        {
            base.SetUp();
            _mockNextConsumer = Substitute.For<IConsumer<int, int>>();
            _mockNextConsumerBuilder = Substitute.For<IConsumerBuilder<int, int>>();
            _mockNextConsumerBuilder.Build().Returns(_mockNextConsumer);
            _mockNextConsumerBuilder.Rebuild(Arg.Any<Confluent.Kafka.ConsumerConfig>()).Returns(_mockNextConsumer);

            _consumerConfig = new Confluent.Kafka.ConsumerConfig(ClientConfig);
            Switcher = new ConsumerSwitcher<int, int>(_mockNextConsumerBuilder, _consumerConfig, MockFetcherFactory,
                MockDiscoveryRestService);
            _consumer = new SwitchingConsumer<int, int>(_mockNextConsumerBuilder, _consumerConfig, Switcher);

            // trigger event of first fetch to init nextConsumer in SwitchingConsumer
            Fetcher.OnNewFetchResult += Raise.Event<EventHandler<DiscoveryResult>>(this,
                new DiscoveryResult(new Dictionary<string, string>
                {
                    { Constants.ConfigurationKeys.Axual.Cluster, "ClusterName" }
                }));
        }

        [Test]
        public void ConsumerSwitcher_SubscribedToTopic_NewConsumerSubscribedAsWell()
        {
            _mockNextConsumer.Subscription.Returns(new List<string> { "test" });

            _consumer.Subscribe("test");
            Switcher.Switch(_consumer,
                new Utils.NewDiscoveryConfigEventArgs(_discoveryResult, new DiscoveryResult()));

            _mockNextConsumer.Received(1)
                .Subscribe(Arg.Is<IEnumerable<string>>(topics => topics.Any(topic => topic == "test")));
        }

        [Test]
        public void ConsumerSwitcher_AssignmentToTopic_NewConsumerAssignmentAsWell()
        {
            var topicPartition = new TopicPartition("test", new Partition());
            _mockNextConsumer.Assignment.Returns(new List<TopicPartition> { topicPartition });

            _consumer.Assign(topicPartition);
            Switcher.Switch(_consumer,
                new Utils.NewDiscoveryConfigEventArgs(_discoveryResult, new DiscoveryResult()));

            _mockNextConsumer.Received(1).Assign(Arg.Is<IEnumerable<TopicPartition>>(topicPartitions =>
                topicPartitions.Any(tp => tp.Topic == "test")));
        }

        [Test]
        public void ConsumerSwitcher_ConsumerClosed_ConsumerCallsUnsubscribeUnassignCloseDispose()
        {
            Switcher.Switch(_consumer,
                new Utils.NewDiscoveryConfigEventArgs(_discoveryResult, _discoveryResult));

            _mockNextConsumer.Received(1).Unsubscribe();
            _mockNextConsumer.Received(1).Unassign();
            _mockNextConsumer.Received(1).Close();
            _mockNextConsumer.Received(1).Dispose();
        }

        [TestCase(1000, 1, 0)]
        [TestCase(1000, 2, 0)]
        [TestCase(1000, 1, 2000)]
        public void ConsumerSwitcher_NewCluster_WaitCalculateCorrectTotalDistributionTimeout(int distributionTimeout,
            int distributorDistance, int ttl)
        {
            var stopwatch = new Stopwatch();
            var time = DateTime.UtcNow; // Remove the "alreadyWaited" element in the AbstractSwitcher.GetSwitchTimeout
            var oldDiscoveryResult = new DiscoveryResult(time, new Dictionary<string, string>
            {
                { Constants.ConfigurationKeys.Axual.Cluster, "Cluster A" },
            });
            var newDiscoveryResult = new DiscoveryResult(time, new Dictionary<string, string>
            {
                {
                    Constants.ConfigurationKeys.Axual.Cluster, "Cluster B"
                }, // difference cluster to trigger the wait time
                { Constants.ConfigurationKeys.Switching.DistributorDistance, distributorDistance.ToString() },
                { Constants.ConfigurationKeys.Switching.Ttl, ttl.ToString() },
                { Constants.ConfigurationKeys.Switching.DistributorTimeout, distributionTimeout.ToString() }
            });

            _mockNextConsumer.Subscription.Returns(new List<string> { "test" });

            stopwatch.Start();
            Switcher.Switch(_consumer,
                new Utils.NewDiscoveryConfigEventArgs(oldDiscoveryResult, newDiscoveryResult));
            stopwatch.Stop();

            var duration = stopwatch.Elapsed;
            var buffer = TimeSpan.FromSeconds(0.3);
            var atLeastWaitTime = TimeSpan.FromMilliseconds(Math.Max(distributionTimeout * distributorDistance, ttl));
            var atMostWaitTime = atLeastWaitTime;

            // Note: Not best practice should validate only the GetSwitchTimeout
            Assert.IsTrue(duration >= atLeastWaitTime - buffer,
                $"Switching waited {duration:c}, expected to wait at least {atLeastWaitTime:c}.");
            Assert.IsTrue(duration <= atMostWaitTime + buffer,
                $"Switching waited {duration:c}, expected to wait at most {atLeastWaitTime:c}.");
        }

        [Test]
        public void ConsumerSwitcher_NoException()
        {
            ISwitcher switcher = Switcher.Configure(new ClientConfig(), _mockNextConsumerBuilder);
        }

        public override void AbstractSwitcherTests_MissingApplicationId_ThrowRequire()
        {
            Assert.Throws<RequireConfigurationException>(() =>
            {
                var consumerSwitcher = new ConsumerSwitcher<int, int>(_mockNextConsumerBuilder,
                    new ClientConfig(), MockFetcherFactory, MockDiscoveryRestService);
            });
        }

        [Test]
        public override void AbstractSwitcherTests_StartListeningWithSameApplicationId_DoesntCallFetcherStartAgain()
        {
            // Arrange
            var applicationIdKey = ClientConfigMapper[Constants.ConfigurationKeys.Axual.ApplicationId];
            var clientConfig = new ClientConfig(new Dictionary<string, string>
            {
                { applicationIdKey, Guid.NewGuid().ToString() }
            });

            var mockFetch = Substitute.For<IFetcher>();
            var mockFetcherFactory = Substitute.For<IFetcherFactory>();
            mockFetcherFactory.CreateFetcher(Arg.Any<IDiscoveryRestService>())
                .Returns(mockFetch);

            var consumerSwitcher = new ConsumerSwitcher<int, int>(
                Substitute.For<IConsumerBuilder<int, int>>(), 
                clientConfig,
                mockFetcherFactory, 
                MockDiscoveryRestService);
            var secondConsumerSwitcherSameId = new ConsumerSwitcher<int, int>(
                Substitute.For<IConsumerBuilder<int, int>>(), 
                clientConfig, 
                mockFetcherFactory,
                MockDiscoveryRestService);

            mockFetch.StartAsync(Arg.Any<CancellationToken>())
                .Returns(info => Task.Delay(1))
                .AndDoes(info => mockFetch.IsRunning.Returns(true));

            consumerSwitcher.StartListening();
            secondConsumerSwitcherSameId.StartListening();

            // Assert 
            
            // Create one fetcher both client have the same applicationIdKey
            mockFetcherFactory.Received(1).CreateFetcher(Arg.Any<IDiscoveryRestService>());
            // should call once to 'ConsumerSwitcher.StartListening', 2nd consumer switcher using the same
            // fetcher as the first consumer
            mockFetch.Received(1).StartAsync(Arg.Any<CancellationToken>());
        }
    }
}