//
// Copyright 2020 Axual B.V. 2020
//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

using Axual.Kafka.Proxy.Exceptions.Resolving;
using Axual.Kafka.Proxy.Proxies;
using Axual.Kafka.Proxy.Proxies.Resolving;
using Axual.Kafka.Proxy.Proxies.Resolving.Resolvers;
using Confluent.Kafka;
using NUnit.Framework;

namespace Axual.Kafka.Tests.Resolving
{
    internal class GroupIdPatternResolverTests : AbstractPatternResolverTests
    {
        [TestCase(Tenant, Environment, "test")]
        [TestCase(Tenant, Environment, "TeST")]
        [TestCase(Tenant, Environment, "test")]
        [TestCase(Tenant, Environment, "test-hyphen")]
        [TestCase(Tenant, Environment, "test_underscore")]
        [TestCase(Tenant, Environment, "test.dot")]
        [TestCase(Tenant, Environment, "test-hyphen.dot-underscore")]
        [TestCase(Tenant, "Environment.dot", GroupId)]
        [TestCase(Tenant, "Environment_underscore", GroupId)]
        [TestCase("Tenant.dot", Environment, GroupId)]
        [TestCase("Tenant_underscore", Environment, GroupId)]
        public void PatternResolver_Resolve_ResolvedCorrect(string tenant, string environment, string groupId)
        {
            var config = new ResolvingConsumerConfig(new ClientConfig())
            {
                GroupIdPattern = $"{{{Constants.ConfigurationKeys.Axual.Tenant}}}-" +
                                 $"{{{Constants.ConfigurationKeys.Axual.Environment}}}-" +
                                 $"{{{Constants.ConfigurationKeys.Resolver.GroupIdResolverKey}}}",
                Tenant = tenant,
                Environment = environment,
                GroupId = groupId
            };

            Resolver = new GroupPatternResolver(config);

            var expectedResolvedValue = $"{tenant}-{environment}-{groupId}";
            PatternResolver_ResolveAndUnResolve_ResolvedUnResolveCorrect(groupId, expectedResolvedValue);
        }

        [Test]
        public void GroupPatternResolver_ProducerInitWithIncorrectFormatPattern_ThrowInvalidPatternException()
        {
            // Arrange
            var resolvingConsumerConfig = new ResolvingConsumerConfig(new ClientConfig())
            {
                GroupIdPattern = "WRONG PATTERN"
            };

            // Act + Assert
            Assert.Throws<InvalidPatternException>(
                () => new GroupPatternResolver(resolvingConsumerConfig));
        }
    }
}