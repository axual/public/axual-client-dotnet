//
// Copyright 2020 Axual B.V. 2020
//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

using Axual.Kafka.Proxy.Exceptions.Resolving;
using Axual.Kafka.Proxy.Proxies;
using Axual.Kafka.Proxy.Proxies.Resolving;
using Axual.Kafka.Proxy.Proxies.Resolving.Resolvers;
using Confluent.Kafka;
using NUnit.Framework;

namespace Axual.Kafka.Tests.Resolving
{
    [TestFixture]
    internal class TransactionalIdPatternResolverTests : AbstractPatternResolverTests
    {
        [TestCase(Tenant, Environment, ApplicationId, TransactionalId)]
        [TestCase(Tenant, Environment, ApplicationId, "TeST")]
        [TestCase(Tenant, Environment, ApplicationId, "test")]
        [TestCase(Tenant, Environment, ApplicationId, "test-hyphen")]
        [TestCase(Tenant, Environment, ApplicationId, "test_underscore")]
        [TestCase(Tenant, Environment, ApplicationId, "test.dot")]
        [TestCase(Tenant, Environment, ApplicationId, "test-hyphen.dot-underscore")]
        [TestCase(Tenant, Environment, "app.1", TransactionalId)]
        [TestCase(Tenant, "Environment.dot", ApplicationId, TransactionalId)]
        [TestCase(Tenant, "Environment_underscore", ApplicationId, TransactionalId)]
        [TestCase("Tenant.dot", Environment, ApplicationId, TransactionalId)]
        [TestCase("Tenant_underscore", Environment, ApplicationId, TransactionalId)]
        public void TransactionalIdPatternResolver_ResolveDefaultPattern_ResolvedTopicCorrect(string tenant,
            string environment, string applicationId, string transactionalId)
        {
            var producerConfig = new ResolvingProducerConfig(new ClientConfig())
            {
                TopicPattern = $"{{{Constants.ConfigurationKeys.Axual.Tenant}}}-" +
                               $"{{{Constants.ConfigurationKeys.Axual.Environment}}}-" +
                               $"{{{Constants.ConfigurationKeys.Resolver.TopicResolverKey}}}",
                Tenant = tenant,
                Environment = environment,
                ApplicationId = applicationId,
                TransactionalId = transactionalId,
                TransactionalIdPattern = $"{{{Constants.ConfigurationKeys.Axual.Tenant}}}-" +
                                         $"{{{Constants.ConfigurationKeys.Axual.Environment}}}-" +
                                         $"{{{Constants.ConfigurationKeys.Axual.ApplicationId}}}-" +
                                         $"{{{Constants.ConfigurationKeys.Kafka.TransactionalIdKey}}}"
            };

            // check with ProducerConfig
            Resolver = new TransactionalIdPatternResolver(producerConfig);
            PatternResolver_ResolveAndUnResolve_ResolvedUnResolveCorrect(transactionalId,
                $"{tenant}-{environment}-{applicationId}-{transactionalId}");
        }

        [Test]
        public void TransactionalIDPatternResolver_ProducerInitWithIncorrectFormatPattern_ThrowInvalidPatternException()
        {
            // Arrange
            var resolvingProducerConfig = new ResolvingProducerConfig(new ClientConfig())
            {
                TopicPattern = "WRONG PATTERN"
            };

            // Act + Assert
            Assert.Throws<InvalidPatternException>(
                () => new TopicPatternResolver(resolvingProducerConfig));
        }
    }
}
