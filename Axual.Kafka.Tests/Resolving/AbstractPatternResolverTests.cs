using Axual.Kafka.Proxy.Proxies;
using Axual.Kafka.Proxy.Proxies.Resolving.Resolvers;
using NUnit.Framework;

namespace Axual.Kafka.Tests.Resolving
{
    [TestFixture]
    internal abstract class AbstractPatternResolverTests
    {
        protected const string Tenant = "axual";
        protected const string Environment = "dev";
        protected const string ApplicationId = "app_id";
        protected const string Instance = "platform";
        protected const string GroupId = "resolver.unit.tests";
        protected const string TransactionalId = "transactionA";

        protected IResolver Resolver;

        protected void PatternResolver_ResolveAndUnResolve_ResolvedUnResolveCorrect(string value,
            string expectedResolvedValue)
        {
            // Act
            var resolvedValue = Resolver.Resolve(value);
            var unResolvedValue = Resolver.UnResolve(expectedResolvedValue);

            // Assert
            Assert.AreEqual(expectedResolvedValue, resolvedValue);
            Assert.AreEqual(value, unResolvedValue);
        }

        [Test]
        public void Constants_Resolver_ConstantsValueDidNotChange()
        {
            Assert.AreEqual("axual.dotnet", Constants.AxualConfigPrefix);
        }
    }
}