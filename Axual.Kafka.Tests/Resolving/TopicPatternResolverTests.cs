//
// Copyright 2020 Axual B.V. 2020
//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

using System.Threading.Tasks;
using Axual.Kafka.Proxy.Exceptions.Resolving;
using Axual.Kafka.Proxy.Proxies;
using Axual.Kafka.Proxy.Proxies.Resolving;
using Axual.Kafka.Proxy.Proxies.Resolving.Resolvers;
using Confluent.Kafka;
using NUnit.Framework;

namespace Axual.Kafka.Tests.Resolving
{
    [TestFixture]
    internal class TopicPatternResolverTests : AbstractPatternResolverTests
    {
        [TestCase(Tenant, Environment, "test")]
        [TestCase(Tenant, Environment, "TeST")]
        [TestCase(Tenant, Environment, "test")]
        [TestCase(Tenant, Environment, "test-hyphen")]
        [TestCase(Tenant, Environment, "test_underscore")]
        [TestCase(Tenant, Environment, "test.dot")]
        [TestCase(Tenant, Environment, "test-hyphen.dot-underscore")]
        [TestCase(Tenant, "Environment.dot", "test")]
        [TestCase(Tenant, "Environment_underscore", "test")]
        [TestCase("Tenant.dot", Environment, "test")]
        [TestCase("Tenant_underscore", Environment, "test")]
        public void PatternResolver_Resolve_ResolvedCorrect(string tenant, string environment, string topicName)
        {
            var consumerConfig = new ResolvingConsumerConfig(new ClientConfig())
            {
                TopicPattern = $"{{{Constants.ConfigurationKeys.Axual.Tenant}}}-" +
                               $"{{{Constants.ConfigurationKeys.Axual.Environment}}}-" +
                               $"{{{Constants.ConfigurationKeys.Resolver.TopicResolverKey}}}",
                Tenant = tenant,
                Environment = environment
            };

            var producerConfig = new ResolvingProducerConfig(new ClientConfig())
            {
                TopicPattern = $"{{{Constants.ConfigurationKeys.Axual.Tenant}}}-" +
                               $"{{{Constants.ConfigurationKeys.Axual.Environment}}}-" +
                               $"{{{Constants.ConfigurationKeys.Resolver.TopicResolverKey}}}",
                Tenant = tenant,
                Environment = environment
            };

            consumerConfig.Set("topic", topicName);
            producerConfig.Set("topic", topicName);

            // check with ConsumerConfig
            Resolver = new TopicPatternResolver(consumerConfig);
            PatternResolver_ResolveAndUnResolve_ResolvedUnResolveCorrect(topicName,
                $"{tenant}-{environment}-{topicName}");

            // check with ProducerConfig
            Resolver = new TopicPatternResolver(producerConfig);
            PatternResolver_ResolveAndUnResolve_ResolvedUnResolveCorrect(topicName,
                $"{tenant}-{environment}-{topicName}");
        }

        [Test]
        public void TopicPatternResolver_ProducerInitWithIncorrectFormatPattern_ThrowInvalidPatternException()
        {
            // Arrange
            var resolvingConsumerConfig = new ResolvingConsumerConfig(new ClientConfig());
            var resolvingProducerConfig = new ResolvingProducerConfig(new ClientConfig());
            resolvingConsumerConfig.TopicPattern = "WRONG PATTERN";
            resolvingProducerConfig.TopicPattern = "WRONG PATTERN";

            // Act + Assert
            Assert.Throws<InvalidPatternException>(
                () => new TopicPatternResolver(resolvingConsumerConfig));
            Assert.Throws<InvalidPatternException>(
                () => new TopicPatternResolver(resolvingProducerConfig));
        }
        
        [Test]
        public void TopicPatternResolver_ResolveSameTopic_ThreadSafe()
        {
            var producerConfig = new ResolvingProducerConfig(new ClientConfig())
            {
                TopicPattern = $"{{{Constants.ConfigurationKeys.Axual.Tenant}}}-" +
                               $"{{{Constants.ConfigurationKeys.Axual.Environment}}}-" +
                               $"{{{Constants.ConfigurationKeys.Resolver.TopicResolverKey}}}",
                Tenant = Tenant,
                Environment = Environment
            };
            
            Resolver = new TopicPatternResolver(producerConfig);

            const string topicName = "topic";
            var resolvedValue1 = string.Empty;
            var resolvedValue2 = string.Empty;
            var expectedResolvedValue = $"{Tenant}-{Environment}-{topicName}";
            
            // Act
            Parallel.Invoke(
                () => { resolvedValue1 = Resolver.Resolve(topicName); },
                () => { resolvedValue2 = Resolver.Resolve(topicName); });
            
            // Assert
            Assert.AreEqual(expectedResolvedValue, resolvedValue1);
            Assert.AreEqual(expectedResolvedValue, resolvedValue2);
        }
        
        [Test]
        public void TopicPatternResolver_ResolveDifferentTopic_ThreadSafe()
        {
            var producerConfig = new ResolvingProducerConfig(new ClientConfig())
            {
                TopicPattern = $"{{{Constants.ConfigurationKeys.Axual.Tenant}}}-" +
                               $"{{{Constants.ConfigurationKeys.Axual.Environment}}}-" +
                               $"{{{Constants.ConfigurationKeys.Resolver.TopicResolverKey}}}",
                Tenant = Tenant,
                Environment = Environment
            };
            
            Resolver = new TopicPatternResolver(producerConfig);

            const string topicName1 = "topic1";
            const string topicName2 = "topic2";
            var resolvedValue1 = string.Empty;
            var resolvedValue2 = string.Empty;
            var expectedResolvedValue1 = $"{Tenant}-{Environment}-{topicName1}";
            var expectedResolvedValue2 = $"{Tenant}-{Environment}-{topicName2}";
            
            // Act
            Parallel.Invoke(
                () => { resolvedValue1 = Resolver.Resolve(topicName1); },
                () => { resolvedValue2 = Resolver.Resolve(topicName2); });
            
            // Assert
            Assert.AreEqual(expectedResolvedValue1, resolvedValue1);
            Assert.AreEqual(expectedResolvedValue2, resolvedValue2);
        }
    }
}