using System;
using System.IO;
using Axual.Kafka.Proxy.Proxies.Axual;
using NUnit.Framework;

namespace Axual.Kafka.Tests.ConsumerConfig
{
    [TestFixture]
    public class AxualConsumerConfigTests
    {
        [Test]
        public virtual void AxualConsumerConfig_EndPointSetAndGet_GetCorrectValue()
        {
            var axualConsumerConfig = new AxualConsumerConfig(new Confluent.Kafka.ConsumerConfig())
                {EndPoint = new Uri("https://localhost/")};
            Assert.AreEqual(new Uri("https://localhost/"), axualConsumerConfig.EndPoint);
        }

        [Test]
        public virtual void AxualConsumerConfig_SchemaRegistryUrlSetAndGet_GetCorrectValue()
        {
            var axualConsumerConfig = new AxualConsumerConfig(new Confluent.Kafka.ConsumerConfig())
                {SchemaRegistryUrl = "https://schema.registry.url/"};
            Assert.AreEqual("https://schema.registry.url/", axualConsumerConfig.SchemaRegistryUrl);
        }

        [Test]
        public virtual void AxualConsumerConfig_ApplicationIdSetAndGet_GetCorrectValue()
        {
            var guid = new Guid();
            var axualConsumerConfig = new AxualConsumerConfig(new Confluent.Kafka.ConsumerConfig())
                {ApplicationId = guid.ToString()};
            Assert.AreEqual(guid.ToString(), axualConsumerConfig.ApplicationId);
        }

        [Test]
        public virtual void AxualConsumerConfig_ApplicationVersionSetAndGet_GetCorrectValue()
        {
            var axualConsumerConfig = new AxualConsumerConfig(new Confluent.Kafka.ConsumerConfig())
                {ApplicationVersion = "test 1.0"};
            Assert.AreEqual("test 1.0", axualConsumerConfig.ApplicationVersion);
        }

        [Test]
        public virtual void AxualConsumerConfig_TenantSetAndGet_GetCorrectValue()
        {
            var axualConsumerConfig = new AxualConsumerConfig(new Confluent.Kafka.ConsumerConfig())
                {Tenant = "tenantName"};
            Assert.AreEqual("tenantName", axualConsumerConfig.Tenant);
        }

        [Test]
        public virtual void AxualConsumerConfig_EnvironmentSetAndGet_GetCorrectValue()
        {
            var axualConsumerConfig = new AxualConsumerConfig(new Confluent.Kafka.ConsumerConfig())
                {Environment = "environmentName"};
            Assert.AreEqual("environmentName", axualConsumerConfig.Environment);
        }
        
        [Test]
        public virtual void AxualConsumerConfig_PemFormatCaLocation_TempFileCreatedInTempFolder()
        {
            var pemFormat = File.ReadAllText(UtilsHelper.GetFileFromResourcesPath("certificate.cer"));
            var axualProducerConfig = new AxualConsumerConfig(new Confluent.Kafka.ConsumerConfig())
                {SslCaLocation = pemFormat};
            Assert.IsTrue(axualProducerConfig.SslCaLocation.StartsWith(Path.GetTempPath()));
        }
    }
}