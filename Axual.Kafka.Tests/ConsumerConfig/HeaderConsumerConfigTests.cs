using Axual.Kafka.Proxy.Proxies.Headers;
using Confluent.Kafka;
using NUnit.Framework;

namespace Axual.Kafka.Tests.ConsumerConfig
{
    [TestFixture]
    internal class HeaderConsumerConfigTests
    {
        [Test]
        public void HeaderConsumerConfig_EnableValueHeaderSetAndGet_GetCorrectValue()
        {
            var headerConsumerConfig = new HeaderConsumerConfig(new ClientConfig()){EnableValueHeaders = true};
            Assert.AreEqual(true, headerConsumerConfig.EnableValueHeaders);

            headerConsumerConfig.EnableValueHeaders = false;
            Assert.AreEqual(false, headerConsumerConfig.EnableValueHeaders);
        }
    }
}