//
// Copyright 2020 Axual B.V. 2020
//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

using System;
using System.Collections.Generic;
using Axual.Kafka.Proxy.Proxies;
using Confluent.Kafka;
using NSubstitute;
using NUnit.Framework;

namespace Axual.Kafka.Tests.Consumer_Builders
{
    [TestFixture]
    internal abstract class ConsumerBuildersTests<TKey, TValue>
    {
        protected Confluent.Kafka.ConsumerConfig ConsumerConfig;
        protected IConsumer<TKey, TValue> MockNextConsumer;
        protected IConsumerBuilder<TKey, TValue> ConsumerBuilder;
        protected IConsumerBuilder<TKey, TValue> MockNextConsumerBuilder;
        
        [SetUp]
        public virtual void SetUp()
        {
            ConsumerConfig = new Confluent.Kafka.ConsumerConfig();
            MockNextConsumer = Substitute.For<IConsumer<TKey, TValue>>();
            MockNextConsumerBuilder = Substitute.For<IConsumerBuilder<TKey, TValue>>();
            MockNextConsumerBuilder.Build().Returns(MockNextConsumer);
            MockNextConsumerBuilder.Rebuild(Arg.Any<Confluent.Kafka.ConsumerConfig>()).Returns(MockNextConsumer);
        }

        [Test]
        public virtual void ConsumerBuilders_Build_NoException()
        {
            ConsumerBuilder.Build();

            Assert.Pass();
        }

        [Test]
        public virtual void ConsumerBuilders_Rebuild_RebuildConsumerBuilder()
        {
            ConsumerBuilder.Rebuild(ConsumerConfig);

            Assert.Pass();
        }

        [Test]
        public virtual void ConsumerBuilders_RebuildAfterConfigurationChange_ConfigurationChanged()
        {
            var newConfig = new Confluent.Kafka.ConsumerConfig(ConsumerConfig)
            {
                ClientId = "TEST"
            };
            var newConsumer = ConsumerBuilder.Rebuild(newConfig);

            Assert.AreEqual("TEST", ConsumerBuilder.ConsumerConfig.ClientId);
        }

        [Test]
        public virtual void ConsumerBuilders_SetErrorHandler_NoException()
        {
            ConsumerBuilder.SetErrorHandler(ErrorHandler)
                .Build();

            Assert.Pass();
        }

        [Test]
        public virtual void ConsumerBuilders_SetKeyDeserializer_NoException()
        {
            ConsumerBuilder.SetLogHandler(LogHandler)
                .Build();

            Assert.Pass();
        }

        [Test]
        public virtual void ConsumerBuilders_SetOffsetsCommittedHandler_NoException()
        {
            ConsumerBuilder.SetOffsetsCommittedHandler(OffsetsCommittedHandler)
                .Build();

            Assert.Pass();
        }

        [Test]
        public virtual void ConsumerBuilders_SetPartitionsAssignedHandler_NoException()
        {
            ConsumerBuilder.SetPartitionsAssignedHandler(PartitionsAssignedHandler)
                .Build();

            Assert.Pass();
        }

        [Test]
        public virtual void ConsumerBuilders_SetPartitionsRevokedHandler_NoException()
        {
            ConsumerBuilder.SetPartitionsRevokedHandler(PartitionsRevokedHandler)
                .Build();

            Assert.Pass();
        }

        [Test]
        public virtual void ConsumerBuilders_SetStatisticsHandler_NoException()
        {
            ConsumerBuilder.SetStatisticsHandler(StatisticsHandler)
                .Build();

            Assert.Pass();
        }

        [Test]
        public virtual void ConsumerBuilders_Validate_NoException()
        {
            ConsumerBuilder.Validate();
        }

        private void PartitionsAssignedHandler(IConsumer<TKey, TValue> consumer,
            List<TopicPartition> arg2)
        {
            throw new NotImplementedException();
        }

        private IEnumerable<TopicPartitionOffset> PartitionsRevokedHandler(IConsumer<TKey, TValue> arg1,
            List<TopicPartitionOffset> arg2)
        {
            throw new NotImplementedException();
        }

        private void OffsetsCommittedHandler(IConsumer<TKey, TValue> consumer, CommittedOffsets arg2)
        {
            throw new NotImplementedException();
        }

        private void ErrorHandler(IConsumer<TKey, TValue> consumer, Error arg2)
        {
            throw new NotImplementedException();
        }

        private void LogHandler(IConsumer<TKey, TValue> consumer, LogMessage arg2)
        {
            throw new NotImplementedException();
        }

        private void StatisticsHandler(IConsumer<TKey, TValue> consumer, string arg2)
        {
            throw new NotImplementedException();
        }
    }
}