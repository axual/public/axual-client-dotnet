//
// Copyright 2020 Axual B.V. 2020
//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

using System;
using Axual.Kafka.Proxy.Exceptions;
using Axual.Kafka.Proxy.Proxies;
using Axual.Kafka.Proxy.Proxies.Switching;
using Axual.Kafka.Proxy.Proxies.Switching.Proxy;
using Confluent.Kafka;
using NSubstitute;
using NUnit.Framework;

namespace Axual.Kafka.Tests.Consumer_Builders
{
    [TestFixture]
    internal class SwitchingConsumerBuilderTests : ConsumerBuildersTests<string, string>
    {
        private const string ApplicationId = "SwitchingConsumerBuildersTests";
        private static readonly Uri EndPoint = new UriBuilder("http", "localhost").Uri;
        private ISwitcher _mockSwitcher;

        [SetUp]
        public override void SetUp()
        {
            base.SetUp();

            ConsumerConfig = new SwitchingConsumerConfig(ConsumerConfig)
            {
                GroupId = "SwitchingConsumerBuilderTests",
                ApplicationId = ApplicationId,
                EndPoint = EndPoint
            };

            _mockSwitcher = Substitute.For<ISwitcher>();

            ConsumerBuilder =
                new SwitchingConsumerBuilder<string, string>(ConsumerConfig, MockNextConsumerBuilder, _mockSwitcher)
                    .SetKeyDeserializer(Deserializers.Utf8)
                    .SetValueDeserializer(Deserializers.Utf8);
        }

        [Test]
        public void SwitchingConsumerBuilder_ValidateMissingApplicationId_ThrowRequireConfigurationException()
        {
            ConsumerConfig = new SwitchingConsumerConfig(new Confluent.Kafka.ConsumerConfig())
            {
                //ApplicationId = base.ApplicationId,
                EndPoint = EndPoint
            };
            ConsumerBuilder =
                new SwitchingConsumerBuilder<string, string>(ConsumerConfig, MockNextConsumerBuilder, _mockSwitcher);

            var exception = Assert.Throws<RequireConfigurationException>(() => ConsumerBuilder.Validate());
            Assert.That(exception.Message,
                Is.EqualTo(string.Format(
                    RequireConfigurationException.RequireConfigurationExceptionMessageFormat,
                    Constants.ConfigurationKeys.Axual.ApplicationId)));
        }

        [Test]
        public void SwitchingConsumerBuilder_ValidateMissingEndPoint_ThrowRequireConfigurationException()
        {
            ConsumerConfig = new SwitchingConsumerConfig(new Confluent.Kafka.ConsumerConfig())
            {
                ApplicationId = ApplicationId
                //EndPoint = base.EndPoint,
            };
            ConsumerBuilder =
                new SwitchingConsumerBuilder<string, string>(ConsumerConfig, MockNextConsumerBuilder, _mockSwitcher);

            var exception = Assert.Throws<RequireConfigurationException>(() => ConsumerBuilder.Validate());
            Assert.That(exception.Message,
                Is.EqualTo(string.Format(
                    RequireConfigurationException.RequireConfigurationExceptionMessageFormat,
                    Constants.ConfigurationKeys.Axual.EndPoint)));
        }
    }
}