//
// Copyright 2020 Axual B.V. 2020
//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

using Axual.Kafka.Proxy.Proxies.Headers;
using Axual.Kafka.Proxy.SerDes;
using Axual.Kafka.Tests.SerDes;
using Confluent.Kafka;
using NSubstitute;
using NUnit.Framework;

namespace Axual.Kafka.Tests.Consumer_Builders
{
    [TestFixture]
    internal class HeaderConsumerBuilderTests : ConsumerBuildersTests<string, string>
    {
        [SetUp]
        public override void SetUp()
        {
            base.SetUp();
            ConsumerBuilder = new HeaderConsumerBuilder<string, string>(ConsumerConfig, MockNextConsumerBuilder)
                .SetKeyDeserializer(Deserializers.Utf8)
                .SetValueDeserializer(Deserializers.Utf8);
        }

        [Test]
        public void HeaderConsumerBuilder_IsEnableValueHeadersIsSet_SetValueHeaderSerializer()
        {
            var headerConsumerConfig = new HeaderConsumerConfig(ConsumerConfig) {EnableValueHeaders = true};
            ConsumerBuilder = new HeaderConsumerBuilder<string, string>(headerConsumerConfig, MockNextConsumerBuilder)
                .SetKeyDeserializer(new DummySerDes.DummyDeserializer<string>())
                .SetValueDeserializer(new DummySerDes.DummyDeserializer<string>());

            ConsumerBuilder.Build();

            // Assert wrapped DummyDeserializer with ValueHeaderDeserializer
            MockNextConsumerBuilder.Received(1).SetValueDeserializer(
                Arg.Is<IDeserializer<string>>(configurableSerializer =>
                    configurableSerializer.GetType() == typeof(ValueHeaderDeserializer<string>)));
        }

        [Test]
        public void HeaderConsumerBuilder_IsEnableValueHeadersIsNotSet_NotSetValueHeaderSerializer()
        {
            var headerConsumerConfig = new HeaderConsumerConfig(ConsumerConfig) {EnableValueHeaders = false};
            ConsumerBuilder = new HeaderConsumerBuilder<string, string>(headerConsumerConfig, MockNextConsumerBuilder)
                .SetKeyDeserializer(new DummySerDes.DummyDeserializer<string>())
                .SetValueDeserializer(new DummySerDes.DummyDeserializer<string>());

            ConsumerBuilder.Build();

            // Assert kept DummyDeserializer as ValueDeserializer
            MockNextConsumerBuilder.Received(1).SetValueDeserializer(
                Arg.Is<IDeserializer<string>>(configurableSerializer =>
                    configurableSerializer.GetType() == typeof(DummySerDes.DummyDeserializer<string>)));
        }

        [Test]
        public void HeaderConsumerBuilder_IsEnableValueHeadersIsSetWithAsyncValueDeserializer_SetAsValueDeserializer()
        {
            var headerConsumerConfig = new HeaderConsumerConfig(ConsumerConfig) {EnableValueHeaders = true};
            ConsumerBuilder = new HeaderConsumerBuilder<string, string>(headerConsumerConfig, MockNextConsumerBuilder)
                .SetKeyDeserializer(new DummySerDes.DummyAsyncDeserializer<string>())
                .SetValueDeserializer(new DummySerDes.DummyAsyncDeserializer<string>());

            ConsumerBuilder.Build();

            // Assert wrapped DummyAsyncDeserializer with ValueHeaderDeserializer
            MockNextConsumerBuilder.Received(1).SetValueDeserializer(
                Arg.Is<IDeserializer<string>>(configurableSerializer =>
                    configurableSerializer.GetType() == typeof(ValueHeaderDeserializer<string>)));
        }

        [Test]
        public void
            HeaderConsumerBuilder_IsEnableValueHeadersIsNotSetWithAsyncValueDeserializer_NotSetValueHeaderSerializer()
        {
            var headerConsumerConfig = new HeaderConsumerConfig(ConsumerConfig) {EnableValueHeaders = false};
            ConsumerBuilder = new HeaderConsumerBuilder<string, string>(headerConsumerConfig, MockNextConsumerBuilder)
                .SetKeyDeserializer(new DummySerDes.DummyAsyncDeserializer<string>())
                .SetValueDeserializer(new DummySerDes.DummyAsyncDeserializer<string>());

            ConsumerBuilder.Build();
            
            // Assert kept DummyAsyncDeserializer as ValueDeserializer
            MockNextConsumerBuilder.Received(1).SetValueDeserializer(
                Arg.Is<IAsyncDeserializer<string>>(configurableSerializer =>
                    configurableSerializer.GetType() == typeof(DummySerDes.DummyAsyncDeserializer<string>)));
        }
    }
}