using Axual.Kafka.Proxy.Proxies.Base;
using Axual.Kafka.Tests.SerDes;
using Confluent.Kafka;
using NSubstitute;
using NUnit.Framework;

namespace Axual.Kafka.Tests.Consumer_Builders
{
    internal class AdapterConsumerBuilderTests : ConsumerBuildersTests<string, string>
    {
        [SetUp]
        public override void SetUp()
        {
            base.SetUp();
            ConsumerConfig = new Confluent.Kafka.ConsumerConfig(ConsumerConfig)
            {
                GroupId = "AdapterConsumerBuilderTests"
            };

            ConsumerBuilder = new AdapterConsumerBuilder<string, string>(ConsumerConfig)
            {
                NextConsumerBuilder = MockNextConsumerBuilder
            };
        }

        [Test]
        public void AdapterConsumerBuilder_BuildKeyValueDeserializer_NoException()
        {
            var keyDeserializer = Substitute.For<IDeserializer<string>>();
            var valueDeserializer = Substitute.For<IDeserializer<string>>();
            
            var test = new AdapterConsumerBuilder<string, string>(ConsumerConfig)
                .SetKeyDeserializer(keyDeserializer)
                .SetValueDeserializer(valueDeserializer)
                .Build();
        }
        
        [Test]
        public void AdapterConsumerBuilder_BuildKeyValueAsyncDeserializer_NoException()
        {
            var keyDeserializer = Substitute.For<IAsyncDeserializer<string>>();
            var valueDeserializer = Substitute.For<IAsyncDeserializer<string>>();
            
            new AdapterConsumerBuilder<string, string>(ConsumerConfig)
                .SetKeyDeserializer(keyDeserializer)
                .SetValueDeserializer(valueDeserializer)
                .Build();
        }
        
        [Test]
        public void AdapterConsumerBuilder_BuildAsyncKeyValueIConfigurableDeserializer_NoException()
        {
            var keyDeserializer = Substitute.For<DummySerDes.IConfigurableDeserializer<string>>();
            var valueDeserializer = Substitute.For<DummySerDes.IConfigurableDeserializer<string>>();

            new AdapterConsumerBuilder<string, string>(ConsumerConfig)
                .SetKeyDeserializer(keyDeserializer)
                .SetValueDeserializer(valueDeserializer)
                .Build();
            
            // Assert
            keyDeserializer.Received(1).Configure(ConsumerConfig);
            valueDeserializer.Received(1).Configure(ConsumerConfig);
        }
        
        [Test]
        public void AdapterConsumerBuilder_BuildAsyncKeyValueIConfigurableAsyncDeserializer_NoException()
        {
            var keyDeserializer = Substitute.For<DummySerDes.IConfigurableAsyncDeserializer<string>>();
            var valueDeserializer = Substitute.For<DummySerDes.IConfigurableAsyncDeserializer<string>>();

            new AdapterConsumerBuilder<string, string>(ConsumerConfig)
                .SetKeyDeserializer(keyDeserializer)
                .SetValueDeserializer(valueDeserializer)
                .Build();
            
            // Assert
            keyDeserializer.Received(1).Configure(ConsumerConfig);
            valueDeserializer.Received(1).Configure(ConsumerConfig);
        }
    }
}