//
// Copyright 2020 Axual B.V. 2020
//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

using Axual.Kafka.Proxy.Exceptions;
using Axual.Kafka.Proxy.Proxies;
using Axual.Kafka.Proxy.Proxies.Resolving;
using Confluent.Kafka;
using NUnit.Framework;

namespace Axual.Kafka.Tests.Consumer_Builders
{
    [TestFixture]
    internal class ResolvingConsumerBuilderTests : ConsumerBuildersTests<string, string>
    {
        [SetUp]
        public override void SetUp()
        {
            base.SetUp();

            ConsumerConfig = new ResolvingConsumerConfig(ConsumerConfig)
            {
                GroupId = "ResolvingConsumerBuilderTests",
                ApplicationId = ApplicationId,
                Environment = Environment,
                Tenant = Tenant,
                Instance = Instance,
                BootstrapServers = "BootstrapServers",
                GroupIdResolver = $"{Constants.JavaResolverPath}GroupPatternResolver",
                GroupIdPattern = $"{{{Constants.ConfigurationKeys.Axual.Tenant}}}-" +
                                 $"{{{Constants.ConfigurationKeys.Axual.Environment}}}-" +
                                 $"{{{Constants.ConfigurationKeys.Resolver.GroupIdResolverKey}}}",
                TopicResolver = $"{Constants.JavaResolverPath}TopicPatternResolver",
                TopicPattern = $"{{{Constants.ConfigurationKeys.Axual.Tenant}}}-" +
                               $"{{{Constants.ConfigurationKeys.Axual.Environment}}}-" +
                               $"{{{Constants.ConfigurationKeys.Resolver.TopicResolverKey}}}"
            };

            ConsumerBuilder = new ResolvingConsumerBuilder<string, string>(ConsumerConfig, MockNextConsumerBuilder)
                .SetKeyDeserializer(Deserializers.Utf8)
                .SetValueDeserializer(Deserializers.Utf8);
        }

        private const string ApplicationId = "ResolvingConsumerBuildersTests";
        private const string Environment = "dev";
        private const string Tenant = "local";
        private const string Instance = "axual";

        [Test]
        public void ResolvingConsumerBuilder_ValidateMissingEnvironment_ThrowRequireConfigurationException()
        {
            ConsumerConfig = new ResolvingConsumerConfig(ConsumerConfig)
            {
                Environment = null
            };
            ConsumerBuilder = new ResolvingConsumerBuilder<string, string>(ConsumerConfig, MockNextConsumerBuilder);

            var exception = Assert.Throws<RequireConfigurationException>(() => ConsumerBuilder.Validate());
            Assert.That(exception.Message,
                Is.EqualTo(string.Format(
                    RequireConfigurationException.RequireConfigurationExceptionMessageFormat,
                    Constants.ConfigurationKeys.Axual.Environment)));
        }

        [Test]
        public void ResolvingConsumerBuilder_ValidateMissingInstance_ThrowRequireConfigurationException()
        {
            ConsumerConfig = new ResolvingConsumerConfig(ConsumerConfig)
            {
                Instance = null
            };
            ConsumerBuilder = new ResolvingConsumerBuilder<string, string>(ConsumerConfig, MockNextConsumerBuilder);

            var exception = Assert.Throws<RequireConfigurationException>(() => ConsumerBuilder.Validate());
            Assert.That(exception.Message,
                Is.EqualTo(string.Format(
                    RequireConfigurationException.RequireConfigurationExceptionMessageFormat,
                    Constants.ConfigurationKeys.Axual.Instance)));
        }

        [Test]
        public void ResolvingConsumerBuilder_ValidateMissingTenant_ThrowRequireConfigurationException()
        {
            ConsumerConfig = new ResolvingConsumerConfig(ConsumerConfig)
            {
                Tenant = null
            };
            ConsumerBuilder = new ResolvingConsumerBuilder<string, string>(ConsumerConfig, MockNextConsumerBuilder);

            var exception = Assert.Throws<RequireConfigurationException>(() => ConsumerBuilder.Validate());
            Assert.That(exception.Message,
                Is.EqualTo(string.Format(
                    RequireConfigurationException.RequireConfigurationExceptionMessageFormat,
                    Constants.ConfigurationKeys.Axual.Tenant)));
        }
        
        [Test]
        public void ResolvingConsumerBuilder_ValidateMissingTopicPattern_ThrowRequireConfigurationException()
        {
            ConsumerConfig = new ResolvingConsumerConfig(ConsumerConfig)
            {
                TopicPattern = null
            };
            ConsumerBuilder = new ResolvingConsumerBuilder<string, string>(ConsumerConfig, MockNextConsumerBuilder);

            var exception = Assert.Throws<RequireConfigurationException>(() => ConsumerBuilder.Validate());
            Assert.That(exception.Message,
                Is.EqualTo(string.Format(
                    RequireConfigurationException.RequireConfigurationExceptionMessageFormat,
                    Constants.ConfigurationKeys.Resolver.TopicPattern)));
        }
        
        [Test]
        public void ResolvingConsumerBuilder_ValidateMissingTopicResolver_ThrowRequireConfigurationException()
        {
            ConsumerConfig = new ResolvingConsumerConfig(ConsumerConfig)
            {
                TopicResolver = null
            };
            ConsumerBuilder = new ResolvingConsumerBuilder<string, string>(ConsumerConfig, MockNextConsumerBuilder);

            var exception = Assert.Throws<RequireConfigurationException>(() => ConsumerBuilder.Validate());
            Assert.That(exception.Message,
                Is.EqualTo(string.Format(
                    RequireConfigurationException.RequireConfigurationExceptionMessageFormat,
                    Constants.ConfigurationKeys.Resolver.TopicResolver)));
        }
    }
}