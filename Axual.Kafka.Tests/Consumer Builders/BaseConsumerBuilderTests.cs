//
// Copyright 2020 Axual B.V. 2020
//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

using Axual.Kafka.Proxy.Exceptions;
using Axual.Kafka.Proxy.Proxies;
using Axual.Kafka.Proxy.Proxies.Base;
using Confluent.Kafka;
using NUnit.Framework;

namespace Axual.Kafka.Tests.Consumer_Builders
{
    [TestFixture]
    internal class BaseConsumerBuilderTests : ConsumerBuildersTests<string, string>
    {
        [SetUp]
        public override void SetUp()
        {
            base.SetUp();
            ConsumerConfig = new BaseConsumerConfig(ConsumerConfig)
            {
                GroupId = "GroupID",
                BootstrapServers = "BootstrapServers"
            };
            
            ConsumerBuilder = new BaseConsumerBuilder<string, string>(ConsumerConfig, MockNextConsumerBuilder)
                .SetKeyDeserializer(Deserializers.Utf8)
                .SetValueDeserializer(Deserializers.Utf8);
        }

        [Test]
        public void BaseConsumerBuilder_PartitionAssignmentStrategyNotDefined_ChangeToRange()
        {
            Assert.AreEqual(PartitionAssignmentStrategy.Range,
                ConsumerBuilder.ConsumerConfig.PartitionAssignmentStrategy);
        }

        [Test]
        public void BaseConsumerBuilder_ValidateMissingBootstrapServers_ThrowRequireConfigurationException()
        {
            var bootstrapServers = ConsumerBuilder.ConsumerConfig.GroupId;
            ConsumerBuilder.ConsumerConfig.BootstrapServers = null;

            var exception = Assert.Throws<RequireConfigurationException>(() => ConsumerBuilder.Validate());

            Assert.That(exception.Message,
                Is.EqualTo(string.Format(
                    RequireConfigurationException.RequireConfigurationExceptionMessageFormat,
                    Constants.ConfigurationKeys.Kafka.BootstrapServers)));
            
            ConsumerBuilder.ConsumerConfig.BootstrapServers = bootstrapServers;
        }

        [Test]
        public void BaseConsumerBuilder_ValidateMissingGroupId_ThrowRequireConfigurationException()
        {
            var groupId = ConsumerBuilder.ConsumerConfig.GroupId;
            ConsumerBuilder.ConsumerConfig.GroupId = null;

            var exception = Assert.Throws<RequireConfigurationException>(() => ConsumerBuilder.Validate());

            Assert.That(exception.Message,
                Is.EqualTo(string.Format(
                    RequireConfigurationException.RequireConfigurationExceptionMessageFormat,
                    Constants.ConfigurationKeys.Kafka.GroupId)));

            ConsumerBuilder.ConsumerConfig.GroupId = groupId;
        }

        [Test]
        public void BaseConsumerBuilder_ValidateMissingValueDeserializer_ThrowRequireConfigurationException()
        {
           var consumerBuilder = new BaseConsumerBuilder<string, string>(ConsumerConfig, MockNextConsumerBuilder)
                .SetKeyDeserializer(Deserializers.Utf8);

            var exception = Assert.Throws<RequireConfigurationException>(() => consumerBuilder.Validate());
                
            Assert.That(exception.Message,
                Is.EqualTo(string.Format(
                    RequireConfigurationException.RequireConfigurationExceptionMessageFormat,
                    "Value Deserializer / Async Value Deserializer")));
        }
        
        [Test]
        public void BaseConsumerBuilder_ValidateMissingKeyDeserializer_ThrowRequireConfigurationException()
        {
            var consumerBuilder = new BaseConsumerBuilder<string, string>(ConsumerConfig, MockNextConsumerBuilder)
                .SetValueDeserializer(Deserializers.Utf8);

            var exception = Assert.Throws<RequireConfigurationException>(() => consumerBuilder.Validate());
                
            Assert.That(exception.Message,
                Is.EqualTo(string.Format(
                    RequireConfigurationException.RequireConfigurationExceptionMessageFormat,
                    "Key Deserializer / Async Key Deserializer")));
        }
    }
}