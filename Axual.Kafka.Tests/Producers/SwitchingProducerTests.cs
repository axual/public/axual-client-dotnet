//
// Copyright 2020 Axual B.V. 2020
//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Axual.Kafka.Proxy;
using Axual.Kafka.Proxy.DiscoveryFetcher;
using Axual.Kafka.Proxy.Exceptions.Switching;
using Axual.Kafka.Proxy.Proxies;
using Axual.Kafka.Proxy.Proxies.Switching;
using Axual.Kafka.Proxy.Proxies.Switching.Proxy;
using Confluent.Kafka;
using NSubstitute;
using NUnit.Framework;

namespace Axual.Kafka.Tests.Producers
{
    internal class SwitchingProducerTests : ProducerTests<int, long>
    {
        [SetUp]
        public override void SetUp()
        {
            base.SetUp();
            _mockSwitcher = Substitute.For<ISwitcher>();
            _innerNextProducer = Substitute.For<IProducer<int, long>>();

            Producer = new SwitchingProducer<int, long>(MockInnerProducerBuilder, ProducerConfig, _mockSwitcher);
            _mockSwitcher.OnFirstDiscoveryResult += Raise.Event<EventHandler<DiscoveryResult>>(
                this, new DiscoveryResult());
        }

        [OneTimeSetUp]
        public override void OneTimeSetUp()
        {
            base.OneTimeSetUp();

            ProducerConfig = new SwitchingProducerConfig(new Confluent.Kafka.ProducerConfig())
            {
                ApplicationId = "SwitchingProducerTests",
                EndPoint = new UriBuilder("http", "localhost").Uri
            };
        }

        [OneTimeTearDown]
        public override void OneTimeTearDown()
        {
            base.OneTimeTearDown();
        }

        private ISwitcher _mockSwitcher;
        private IProducer<int, long> _innerNextProducer;

        [Test]
        public void switchingProducer_InitTransactions_IsInitTransactionsReturnTrue()
        {
            var switchingProducer = Producer as SwitchingProducer<int, long>;

            switchingProducer.InitTransactions(TimeSpan.Zero);

            Assert.IsTrue(switchingProducer.IsInitTransactions());
        }

        [Test]
        public void switchingProducer_DidNotInitTransactions_IsInitTransactionsReturnFalse()
        {
            var switchingProducer = Producer as SwitchingProducer<int, long>;
            Assert.IsFalse(switchingProducer.IsInitTransactions());
        }

        #region ThrowTransactionSwitchedException

        #region Produce

        [Test, Timeout(2000)]
        public void SwitchingProducer_Produce_DidNotCallSwitch()
        {
            Producer.Produce(Topic, Message);

            // Assert switch occured
            _mockSwitcher.DidNotReceive().Switch(
                Arg.Any<IProducer<int, long>>(),
                Arg.Any<Utils.NewDiscoveryConfigEventArgs>());
        }

        [Test, Timeout(2000)]
        public void SwitchingProducer_InitTransactionsAndProduce_DidNotCallSwitch()
        {
            Producer.InitTransactions(TimeSpan.Zero);
            Producer.Produce(Topic, Message);

            // Assert switch occured
            _mockSwitcher.DidNotReceive().Switch(
                Arg.Any<IProducer<int, long>>(),
                Arg.Any<Utils.NewDiscoveryConfigEventArgs>());
        }

        [Test, Timeout(2000)]
        public void SwitchingProducer_ProduceAfterSwitch_CalledSwitch()
        {
            _mockSwitcher.Switch(Arg.Any<IProducer<int, long>>(), Arg.Any<Utils.NewDiscoveryConfigEventArgs>())
                .Returns(_innerNextProducer);

            _mockSwitcher.OnSwitchNeeded +=
                Raise.Event<EventHandler<Utils.NewDiscoveryConfigEventArgs>>(
                    this,
                    new Utils.NewDiscoveryConfigEventArgs(
                        null,
                        new DiscoveryResult()));

            // Should not throw TransactionSwitchedException as didn't InitTransaction on previous cluster
            Producer.Produce(Topic, Message);

            // Assert switch occured
            _mockSwitcher.Received(1).Switch(
                Arg.Any<IProducer<int, long>>(),
                Arg.Any<Utils.NewDiscoveryConfigEventArgs>());
        }

        [Test, Timeout(2000)]
        public void SwitchingProducer_ProduceAfterInitTransactionsAndSwitch_ThrowsTransactionSwitchedException()
        {
            _mockSwitcher.Switch(Arg.Any<IProducer<int, long>>(), Arg.Any<Utils.NewDiscoveryConfigEventArgs>())
                .Returns(_innerNextProducer);

            Producer.InitTransactions(TimeSpan.Zero);

            // raise sync fake switch event
            _mockSwitcher.OnSwitchNeeded +=
                Raise.Event<EventHandler<Utils.NewDiscoveryConfigEventArgs>>(
                    this,
                    new Utils.NewDiscoveryConfigEventArgs(
                        null,
                        new DiscoveryResult()));

            // Assert
            Assert.Throws<TransactionSwitchedException>(() => Producer.Produce(string.Empty, Message));
            //  Didn't call the next producer
            _innerNextProducer.DidNotReceive().Produce(Arg.Any<string>(), Arg.Any<Message<int, long>>());
            // switch occured
            _mockSwitcher.Received(1)
                .Switch(Arg.Any<IProducer<int, long>>(), Arg.Any<Utils.NewDiscoveryConfigEventArgs>());
        }

        #endregion

        #region ProduceTopicPartition

        [Test, Timeout(2000)]
        public void SwitchingProducer_ProduceTopicPartition_DidNotCallSwitch()
        {
            Producer.Produce(new TopicPartition(Topic, new Partition()), Message);

            // Assert switch occured
            _mockSwitcher.DidNotReceive().Switch(
                Arg.Any<IProducer<int, long>>(),
                Arg.Any<Utils.NewDiscoveryConfigEventArgs>());
        }

        [Test, Timeout(2000)]
        public void SwitchingProducer_InitTransactionsAndProduceTopicPartition_DidNotCallSwitch()
        {
            Producer.InitTransactions(TimeSpan.Zero);
            Producer.Produce(TopicPartition, Message);

            // Assert switch occured
            _mockSwitcher.DidNotReceive().Switch(
                Arg.Any<IProducer<int, long>>(),
                Arg.Any<Utils.NewDiscoveryConfigEventArgs>());
        }

        [Test, Timeout(2000)]
        public void SwitchingProducer_ProduceTopicPartitionAfterSwitch_CalledSwitch()
        {
            _mockSwitcher.Switch(Arg.Any<IProducer<int, long>>(), Arg.Any<Utils.NewDiscoveryConfigEventArgs>())
                .Returns(_innerNextProducer);

            _mockSwitcher.OnSwitchNeeded +=
                Raise.Event<EventHandler<Utils.NewDiscoveryConfigEventArgs>>(
                    this,
                    new Utils.NewDiscoveryConfigEventArgs(
                        null,
                        new DiscoveryResult()));

            // Should not throw TransactionSwitchedException as didn't InitTransaction on previous cluster
            Producer.Produce(new TopicPartition(Topic, new Partition()), Message);

            // Assert switch occured
            _mockSwitcher.Received(1).Switch(
                Arg.Any<IProducer<int, long>>(),
                Arg.Any<Utils.NewDiscoveryConfigEventArgs>());
        }

        [Test, Timeout(2000)]
        public void SwitchingProducer_ProduceTopicAfterInitTransactionsAndSwitch_ThrowsTransactionSwitchedException()
        {
            _mockSwitcher.Switch(Arg.Any<IProducer<int, long>>(), Arg.Any<Utils.NewDiscoveryConfigEventArgs>())
                .Returns(_innerNextProducer);

            _mockSwitcher.OnSwitchNeeded +=
                Raise.Event<EventHandler<Utils.NewDiscoveryConfigEventArgs>>(
                    this,
                    new Utils.NewDiscoveryConfigEventArgs(
                        null,
                        new DiscoveryResult()));

            Producer.InitTransactions(TimeSpan.Zero);

            // Assert
            // throw TransactionSwitchedException
            Assert.Throws<TransactionSwitchedException>(() =>
                Producer.Produce(new TopicPartition(Topic, new Partition()), Message));
            // Didn't call the next producer
            _innerNextProducer.DidNotReceive().Produce(Arg.Any<TopicPartition>(), Arg.Any<Message<int, long>>());
            // Assert switch occured
            _mockSwitcher.Received(1)
                .Switch(Arg.Any<IProducer<int, long>>(), Arg.Any<Utils.NewDiscoveryConfigEventArgs>());
        }

        #endregion

        #region ProduceAsync

        [Test, Timeout(2000)]
        public void SwitchingProducer_ProduceAsync_DidNotCallSwitch()
        {
            Producer.ProduceAsync(Topic, Message).Wait();

            // Assert switch occured
            _mockSwitcher.DidNotReceive().Switch(
                Arg.Any<IProducer<int, long>>(),
                Arg.Any<Utils.NewDiscoveryConfigEventArgs>());
        }

        [Test, Timeout(2000)]
        public void SwitchingProducer_ProduceAsyncAfterSwitch_CalledSwitch()
        {
            _mockSwitcher.Switch(Arg.Any<IProducer<int, long>>(), Arg.Any<Utils.NewDiscoveryConfigEventArgs>())
                .Returns(_innerNextProducer);

            _mockSwitcher.OnSwitchNeeded +=
                Raise.Event<EventHandler<Utils.NewDiscoveryConfigEventArgs>>(
                    this,
                    new Utils.NewDiscoveryConfigEventArgs(
                        null,
                        new DiscoveryResult()));

            // Should not throw TransactionSwitchedException as didn't InitTransaction on previous cluster
            Producer.ProduceAsync(Topic, Message).Wait();

            // Assert switch occured
            _mockSwitcher.Received(1).Switch(
                Arg.Any<IProducer<int, long>>(),
                Arg.Any<Utils.NewDiscoveryConfigEventArgs>());
        }

        [Test, Timeout(2000)]
        public void SwitchingProducer_ProduceAsyncAfterInitTransactionsAndSwitch_ThrowsTransactionSwitchedException()
        {
            _mockSwitcher.Switch(Arg.Any<IProducer<int, long>>(), Arg.Any<Utils.NewDiscoveryConfigEventArgs>())
                .Returns(_innerNextProducer);

            Producer.InitTransactions(TimeSpan.Zero);

            // raise sync fake switch event
            _mockSwitcher.OnSwitchNeeded +=
                Raise.Event<EventHandler<Utils.NewDiscoveryConfigEventArgs>>(
                    this,
                    new Utils.NewDiscoveryConfigEventArgs(
                        null,
                        new DiscoveryResult()));

            // Assert
            // throw TransactionSwitchedException
            Assert.ThrowsAsync<TransactionSwitchedException>(() => Producer.ProduceAsync(Topic, Message));

            // Didn't call the next producer
            _innerNextProducer.DidNotReceive().ProduceAsync(Arg.Any<string>(), Arg.Any<Message<int, long>>());
            // switch occured
            _mockSwitcher.Received(1)
                .Switch(Arg.Any<IProducer<int, long>>(), Arg.Any<Utils.NewDiscoveryConfigEventArgs>());
        }

        #endregion

        #region ProduceAsyncTopicPartition

        [Test, Timeout(2000)]
        public void SwitchingProducer_ProduceAsyncTopicPartition_DidNotCallSwitch()
        {
            Producer.ProduceAsync(TopicPartition, Message).Wait();

            // Assert switch occured
            _mockSwitcher.DidNotReceive().Switch(
                Arg.Any<IProducer<int, long>>(),
                Arg.Any<Utils.NewDiscoveryConfigEventArgs>());
        }

        [Test, Timeout(2000)]
        public void SwitchingProducer_InitTransactionsAndProduceAsyncTopicPartition_DidNotCallSwitch()
        {
            Producer.InitTransactions(TimeSpan.Zero);
            Producer.ProduceAsync(TopicPartition, Message).Wait();

            // Assert switch occured
            _mockSwitcher.DidNotReceive().Switch(
                Arg.Any<IProducer<int, long>>(),
                Arg.Any<Utils.NewDiscoveryConfigEventArgs>());
        }

        [Test, Timeout(2000)]
        public void SwitchingProducer_ProduceAsyncTopicPartitionAfterSwitch_CalledSwitch()
        {
            _mockSwitcher.Switch(Arg.Any<IProducer<int, long>>(), Arg.Any<Utils.NewDiscoveryConfigEventArgs>())
                .Returns(_innerNextProducer);

            _mockSwitcher.OnSwitchNeeded +=
                Raise.Event<EventHandler<Utils.NewDiscoveryConfigEventArgs>>(
                    this,
                    new Utils.NewDiscoveryConfigEventArgs(
                        null,
                        new DiscoveryResult()));

            // Should not throw TransactionSwitchedException as didn't InitTransaction on previous cluster
            Producer.ProduceAsync(TopicPartition, Message).Wait();

            // Assert switch occured
            _mockSwitcher.Received(1).Switch(
                Arg.Any<IProducer<int, long>>(),
                Arg.Any<Utils.NewDiscoveryConfigEventArgs>());
        }

        [Test, Timeout(2000)]
        public void
            SwitchingProducer_ProduceAsyncTopicPartitionAfterInitTransactionsAndSwitch_ThrowsTransactionSwitchedException()
        {
            _mockSwitcher.Switch(Arg.Any<IProducer<int, long>>(), Arg.Any<Utils.NewDiscoveryConfigEventArgs>())
                .Returns(_innerNextProducer);

            Producer.InitTransactions(TimeSpan.Zero);

            // raise sync fake switch event
            _mockSwitcher.OnSwitchNeeded +=
                Raise.Event<EventHandler<Utils.NewDiscoveryConfigEventArgs>>(
                    this,
                    new Utils.NewDiscoveryConfigEventArgs(
                        null,
                        new DiscoveryResult()));

            // Assert
            Assert.ThrowsAsync(typeof(TransactionSwitchedException),
                () => Producer.ProduceAsync(TopicPartition, Message));
        }

        #endregion

        #region SendOffsetsToTransaction

        [Test, Timeout(2000)]
        public void SwitchingProducer_SendOffsetsToTransaction_DidNotCallSwitch()
        {
            IEnumerable<TopicPartitionOffset> topicPartitionOffsets = Array.Empty<TopicPartitionOffset>();
            var timeout = TimeSpan.FromMinutes(1);

            Producer.SendOffsetsToTransaction(topicPartitionOffsets, null, timeout);

            // Assert switch occured
            _mockSwitcher.DidNotReceive().Switch(
                Arg.Any<IProducer<int, long>>(),
                Arg.Any<Utils.NewDiscoveryConfigEventArgs>());
        }

        [Test, Timeout(2000)]
        public void SwitchingProducer_InitTransactionsAndSendOffsetsToTransaction_DidNotCallSwitch()
        {
            IEnumerable<TopicPartitionOffset> topicPartitionOffsets = Array.Empty<TopicPartitionOffset>();
            var timeout = TimeSpan.FromMinutes(1);

            Producer.InitTransactions(timeout);
            Producer.SendOffsetsToTransaction(topicPartitionOffsets, null, timeout);

            // Assert switch occured
            _mockSwitcher.DidNotReceive().Switch(
                Arg.Any<IProducer<int, long>>(),
                Arg.Any<Utils.NewDiscoveryConfigEventArgs>());
        }

        [Test, Timeout(2000)]
        public void SwitchingProducer_SendOffsetsToTransactionAfterSwitch_CalledSwitch()
        {
            IEnumerable<TopicPartitionOffset> topicPartitionOffsets = Array.Empty<TopicPartitionOffset>();
            var timeout = TimeSpan.FromMinutes(1);

            _mockSwitcher.Switch(Arg.Any<IProducer<int, long>>(), Arg.Any<Utils.NewDiscoveryConfigEventArgs>())
                .Returns(_innerNextProducer);

            // raise switch event
            _mockSwitcher.OnSwitchNeeded +=
                Raise.Event<EventHandler<Utils.NewDiscoveryConfigEventArgs>>(
                    this,
                    new Utils.NewDiscoveryConfigEventArgs(
                        null,
                        new DiscoveryResult()));

            // Should not throw TransactionSwitchedException as didn't InitTransaction on previous cluster
            Producer.SendOffsetsToTransaction(topicPartitionOffsets, null, timeout);

            // Assert switch occured
            _mockSwitcher.Received(1).Switch(
                Arg.Any<IProducer<int, long>>(),
                Arg.Any<Utils.NewDiscoveryConfigEventArgs>());
        }

        [Test, Timeout(2000)]
        public void
            SwitchingProducer_SendOffsetsToTransactionAfterInitTransactionsAndSwitch_ThrowsTransactionSwitchedException()
        {
            IEnumerable<TopicPartitionOffset> topicPartitionOffsets = Array.Empty<TopicPartitionOffset>();
            var timeout = TimeSpan.FromMinutes(1);

            _mockSwitcher.Switch(Arg.Any<IProducer<int, long>>(), Arg.Any<Utils.NewDiscoveryConfigEventArgs>())
                .Returns(_innerNextProducer);

            // raise switch event
            _mockSwitcher.OnSwitchNeeded +=
                Raise.Event<EventHandler<Utils.NewDiscoveryConfigEventArgs>>(
                    this,
                    new Utils.NewDiscoveryConfigEventArgs(
                        null,
                        new DiscoveryResult()));

            Producer.InitTransactions(timeout);

            // Assert
            // throw TransactionSwitchedException
            Assert.Throws<TransactionSwitchedException>(() =>
                Producer.SendOffsetsToTransaction(topicPartitionOffsets, null, timeout));

            // Didn't call the next producer
            _innerNextProducer.DidNotReceive().SendOffsetsToTransaction(
                Arg.Any<IEnumerable<TopicPartitionOffset>>(), Arg.Any<IConsumerGroupMetadata>(), Arg.Any<TimeSpan>());
            // Assert switch occured
            _mockSwitcher.Received(1)
                .Switch(Arg.Any<IProducer<int, long>>(), Arg.Any<Utils.NewDiscoveryConfigEventArgs>());
        }

        #endregion

        #endregion

        #region NotSwitchingMethods

        #region BeginTransaction

        [Test, Timeout(2000)]
        public void SwitchingProducer_BeginTransaction_DidNotCallSwitch()
        {
            Producer.BeginTransaction();

            // Assert switch occured
            _mockSwitcher.DidNotReceive().Switch(
                Arg.Any<IProducer<int, long>>(),
                Arg.Any<Utils.NewDiscoveryConfigEventArgs>());
        }

        [Test, Timeout(2000)]
        public void SwitchingProducer_BeginTransactionAfterNewCluster_DidNotCalledSwitch()
        {
            _mockSwitcher.Switch(Arg.Any<IProducer<int, long>>(), Arg.Any<Utils.NewDiscoveryConfigEventArgs>())
                .Returns(_innerNextProducer);

            // raise switch event
            _mockSwitcher.OnSwitchNeeded += Raise.Event<EventHandler<Utils.NewDiscoveryConfigEventArgs>>(
                this,
                new Utils.NewDiscoveryConfigEventArgs(
                    null,
                    new DiscoveryResult()));

            Producer.BeginTransaction();

            // Assert Doesn't switch for BeginTransaction
            _mockSwitcher.DidNotReceive().Switch(
                Arg.Any<IProducer<int, long>>(),
                Arg.Any<Utils.NewDiscoveryConfigEventArgs>());
        }

        #endregion

        #region CommitTransaction

        [Test, Timeout(2000)]
        public void SwitchingProducer_CommitTransaction_DidNotCallSwitch()
        {
            var timeout = TimeSpan.FromMinutes(1);
            Producer.CommitTransaction(timeout);

            // Assert switch occured
            _mockSwitcher.DidNotReceive().Switch(
                Arg.Any<IProducer<int, long>>(),
                Arg.Any<Utils.NewDiscoveryConfigEventArgs>());
        }

        [Test, Timeout(2000)]
        public void SwitchingProducer_CommitTransactionAfterNewCluster_DidNotCalledSwitch()
        {
            var timeout = TimeSpan.FromMinutes(1);
            _mockSwitcher.Switch(Arg.Any<IProducer<int, long>>(), Arg.Any<Utils.NewDiscoveryConfigEventArgs>())
                .Returns(_innerNextProducer);

            // raise switch event
            _mockSwitcher.OnSwitchNeeded += Raise.Event<EventHandler<Utils.NewDiscoveryConfigEventArgs>>(
                this,
                new Utils.NewDiscoveryConfigEventArgs(
                    null,
                    new DiscoveryResult()));

            Producer.CommitTransaction(timeout);

            // Assert Doesn't switch for BeginTransaction
            _mockSwitcher.DidNotReceive().Switch(
                Arg.Any<IProducer<int, long>>(),
                Arg.Any<Utils.NewDiscoveryConfigEventArgs>());
        }

        #endregion

        #region AbortTransaction

        [Test, Timeout(2000)]
        public void SwitchingProducer_AbortTransaction_DidNotCallSwitch()
        {
            var timeout = TimeSpan.FromMinutes(1);
            Producer.AbortTransaction(timeout);

            // Assert switch occured
            _mockSwitcher.DidNotReceive().Switch(
                Arg.Any<IProducer<int, long>>(),
                Arg.Any<Utils.NewDiscoveryConfigEventArgs>());
        }

        [Test, Timeout(2000)]
        public void SwitchingProducer_AbortTransactionAfterNewCluster_DidNotCalledSwitch()
        {
            var timeout = TimeSpan.FromMinutes(1);
            _mockSwitcher.Switch(Arg.Any<IProducer<int, long>>(), Arg.Any<Utils.NewDiscoveryConfigEventArgs>())
                .Returns(_innerNextProducer);

            // raise switch event
            _mockSwitcher.OnSwitchNeeded += Raise.Event<EventHandler<Utils.NewDiscoveryConfigEventArgs>>(
                this,
                new Utils.NewDiscoveryConfigEventArgs(
                    null,
                    new DiscoveryResult()));

            Producer.AbortTransaction(timeout);

            // Assert Doesn't switch for BeginTransaction
            _mockSwitcher.DidNotReceive().Switch(
                Arg.Any<IProducer<int, long>>(),
                Arg.Any<Utils.NewDiscoveryConfigEventArgs>());
        }

        #endregion

        #region InitTransactions

        [Test, Timeout(2000)]
        public void SwitchingProducer_InitTransactions_DidNotCallSwitch()
        {
            var timeout = TimeSpan.FromMinutes(1);
            Producer.InitTransactions(timeout);

            // Assert switch occured
            _mockSwitcher.DidNotReceive().Switch(
                Arg.Any<IProducer<int, long>>(),
                Arg.Any<Utils.NewDiscoveryConfigEventArgs>());
        }

        [Test, Timeout(2000)]
        public void SwitchingProducer_InitTransactionsAfterNewCluster_DidNotCalledSwitch()
        {
            var timeout = TimeSpan.FromMinutes(1);
            _mockSwitcher.Switch(Arg.Any<IProducer<int, long>>(), Arg.Any<Utils.NewDiscoveryConfigEventArgs>())
                .Returns(_innerNextProducer);

            // raise switch event
            _mockSwitcher.OnSwitchNeeded += Raise.Event<EventHandler<Utils.NewDiscoveryConfigEventArgs>>(
                this,
                new Utils.NewDiscoveryConfigEventArgs(
                    null,
                    new DiscoveryResult()));

            Producer.InitTransactions(timeout);

            // Assert Doesn't switch for BeginTransaction
            _mockSwitcher.DidNotReceive().Switch(
                Arg.Any<IProducer<int, long>>(),
                Arg.Any<Utils.NewDiscoveryConfigEventArgs>());
        }

        #endregion

        #endregion

        [Test, Timeout(2000)]
        public async Task SwitchingProducer_ProduceAsyncTwiceAfterSwitch_CalledSwitchOnce()
        {
            _mockSwitcher.Switch(Arg.Any<IProducer<int, long>>(), Arg.Any<Utils.NewDiscoveryConfigEventArgs>())
                .Returns(_innerNextProducer);

            _mockSwitcher.OnSwitchNeeded +=
                Raise.Event<EventHandler<Utils.NewDiscoveryConfigEventArgs>>(
                    this,
                    new Utils.NewDiscoveryConfigEventArgs(
                        null,
                        new DiscoveryResult()));

            var task1 = Producer.ProduceAsync(Topic, Message);
            var task2 = Producer.ProduceAsync(Topic, Message);

            await Task.WhenAll(task1, task2);

            // Assert switch occured once
            _mockSwitcher.Received(1).Switch(
                Arg.Any<IProducer<int, long>>(),
                Arg.Any<Utils.NewDiscoveryConfigEventArgs>());
        }

        [Test]
        public void ProducerSwitcher_SwitchOccur_SwitcherSuccuess()
        {
            var mockNextProducerBuilder = Substitute.For<IProducerBuilder<int, int>>();
            var mockNextProducer = Substitute.For<IProducer<int, int>>();
            var mockFetcher = Substitute.For<IFetcher>();
            var mockFetcherFactory = Substitute.For<IFetcherFactory>();
            var mockDiscoveryRestService = Substitute.For<IDiscoveryRestService>();
            mockNextProducerBuilder.Rebuild(Arg.Any<ClientConfig>()).Returns(mockNextProducer);
            mockFetcherFactory.CreateFetcher(mockDiscoveryRestService).Returns(mockFetcher);
            //mockNextProducer.Produce(Arg.Any<string>(),Arg.Any<Message<int,int>>()).Returns(new);

            var switcher = new ProducerSwitcher<int, int>(mockNextProducerBuilder, ProducerConfig, mockFetcherFactory,
                mockDiscoveryRestService);
            var producer = new SwitchingProducer<int, int>(mockNextProducerBuilder,
                ProducerConfig, switcher);

            Task.Factory.StartNew(() =>
            {
                //Tell the substitute to raise the event with a sender and EventArgs:
                mockFetcher.OnNewFetchResult += Raise.Event<EventHandler<DiscoveryResult>>(
                    this, new DiscoveryResult(new Dictionary<string, string>()
                    {
                        { Constants.ConfigurationKeys.Axual.Cluster, "Cluster A" }
                    }));

                mockFetcher.OnNewFetchResult += Raise.Event<EventHandler<DiscoveryResult>>(
                    this, new DiscoveryResult(new Dictionary<string, string>()
                    {
                        { Constants.ConfigurationKeys.Axual.Cluster, "Cluster B" },
                        // Make timeout 0
                        { Constants.ConfigurationKeys.Switching.DistributorDistance, "0" },
                        { Constants.ConfigurationKeys.Switching.Ttl, "0" }
                    }));
            }).ContinueWith(t => { producer.Produce("topic", new Message<int, int>()); }).Wait();
        }
    }
}