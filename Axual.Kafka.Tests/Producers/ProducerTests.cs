//
// Copyright 2020 Axual B.V. 2020
//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

using System;
using System.Collections.Generic;
using System.Threading;
using Axual.Kafka.Proxy.Proxies;
using Confluent.Kafka;
using NSubstitute;
using NSubstitute.ReceivedExtensions;
using NUnit.Framework;

namespace Axual.Kafka.Tests.Producers
{
    [TestFixture]
    internal abstract class ProducerTests<TKey, TValue>
    {
        protected string Topic;
        protected Message<TKey, TValue> Message;
        protected TopicPartition TopicPartition;

        protected IProducer<TKey, TValue> Producer;
        protected IProducer<TKey, TValue> MockInnerProducer;
        protected IProducerBuilder<TKey, TValue> MockInnerProducerBuilder;
        protected Confluent.Kafka.ProducerConfig ProducerConfig;
        
        [SetUp]
        public virtual void SetUp()
        {
            Message = new Message<TKey, TValue>();
            MockInnerProducer = Substitute.For<IProducer<TKey, TValue>>();
            MockInnerProducerBuilder = Substitute.For<IProducerBuilder<TKey, TValue>>();
            MockInnerProducerBuilder.Build().Returns(MockInnerProducer);
            MockInnerProducerBuilder.Rebuild(Arg.Any<Confluent.Kafka.ProducerConfig>()).Returns(MockInnerProducer);
        }

        [OneTimeSetUp]
        public virtual void OneTimeSetUp()
        {
            Topic = "topic name";
            TopicPartition = new TopicPartition(Topic, new Partition(0));
            ProducerConfig = new Confluent.Kafka.ProducerConfig();
        }

        [OneTimeTearDown]
        public virtual void OneTimeTearDown()
        {
            // nothing
        }

        protected void Handler(DeliveryReport<TKey, TValue> obj)
        {
            throw new NotImplementedException();
        }

        [Test]
        public void Produce_AbortTransactionWithTimeout_CalledInnerProducerAbortTransaction()
        {
            var timeout = TimeSpan.MaxValue;
            Producer.AbortTransaction(timeout);
            MockInnerProducer.Received(1).AbortTransaction(Arg.Is(timeout));
        }
        
        [Test]
        public void Produce_AbortTransaction_CalledInnerProducerAbortTransaction()
        {
            var timeout = TimeSpan.MaxValue;
            Producer.AbortTransaction();
            MockInnerProducer.Received(1).AbortTransaction();
        }

        [Test]
        public void Produce_AddBrokers_CalledInnerProducerAddBrokers()
        {
            var newBrokersToAdd = "newBrokers";
            Producer.AddBrokers(newBrokersToAdd);
            MockInnerProducer.Received(1).AddBrokers(newBrokersToAdd);
        }

        [Test]
        public void Produce_AxualProduce_CalledInnerProducerProduceWithConvertedHandler()
        {
            Producer.Produce(Topic, Message, Handler);

            MockInnerProducer.Received(1).Produce(
                Arg.Any<string>(),
                Arg.Any<Message<TKey, TValue>>(),
                Arg.Any<Action<DeliveryReport<TKey, TValue>>>());
        }

        [Test]
        public void Produce_BeginTransaction_CalledInnerProducerBeginTransaction()
        {
            Producer.BeginTransaction();
            MockInnerProducer.Received(1).BeginTransaction();
        }

        [Test]
        public void Produce_CommitTransactionWithTimeout_CalledInnerProducerCommitTransaction()
        {
            var timeout = TimeSpan.MaxValue;
            Producer.CommitTransaction(timeout);
            MockInnerProducer.Received(1).CommitTransaction(Arg.Is(timeout));
        }
        
        [Test]
        public void Produce_CommitTransaction_CalledInnerProducerCommitTransaction()
        {
            var timeout = TimeSpan.MaxValue;
            Producer.CommitTransaction();
            MockInnerProducer.Received(1).CommitTransaction();
        }

        [Test]
        public void Produce_Dispose_CalledInnerProducerDispose()
        {
            Producer.Dispose();
            MockInnerProducer.Received(Quantity.Exactly(1)).Dispose();

            Assert.Pass();
        }

        [Test]
        public void Produce_Flush_CalledInnerProducerFlush()
        {
            var cancellationToken = new CancellationToken(true);
            Producer.Flush(cancellationToken);
            MockInnerProducer.Received(1).Flush(cancellationToken);
        }

        [Test]
        public void Produce_FlushTimeOut_CalledInnerProducerFlushTimeout()
        {
            var timeout = TimeSpan.FromSeconds(5);
            Producer.Flush(timeout);
            MockInnerProducer.Received(1).Flush(timeout);
        }

        [Test]
        public void Produce_FlushWithDefault_CalledInnerProducerFlushWithDefault()
        {
            Producer.Flush();
            MockInnerProducer.Received(1).Flush();
        }

        [Test]
        public void Produce_Handle_CalledInnerProducerHandle()
        {
            var handle = Producer.Handle;
            var innerProducerHandle = MockInnerProducer.Received(1).Handle;

            Assert.AreEqual(handle, innerProducerHandle);
        }

        [Test]
        public void Produce_InitTransactions_CalledInnerProducerInitTransactions()
        {
            var timeout = TimeSpan.MaxValue;
            Producer.InitTransactions(timeout);
            MockInnerProducer.Received(1).InitTransactions(Arg.Is(timeout));
        }

        [Test]
        public void Produce_Name_CalledInnerProducerName()
        {
            var accessName = Producer.Name;
            var innerProducerNameCalled = MockInnerProducer.Received(1).Name;

            Assert.AreEqual(accessName, innerProducerNameCalled);
        }

        [Test]
        public void Produce_Poll_CalledInnerProducerPoll()
        {
            var timeSpan = TimeSpan.MaxValue;
            Producer.Poll(timeSpan);
            MockInnerProducer.Received(1).Poll(Arg.Is(timeSpan));
        }

        [Test]
        public void Produce_ProduceAsync_CalledInnerProducerProduceAsync()
        {
            Producer.ProduceAsync(Topic, Message).Wait();

            MockInnerProducer.Received(1).ProduceAsync(
                Arg.Any<string>(),
                Arg.Any<Message<TKey, TValue>>());
        }

        [Test]
        public void Produce_ProduceAsyncWithCancellationToken_CalledInnerProducerProduceAsyncWithCancellationToken()
        {
            var cancellationToken = new CancellationToken();
            Producer.ProduceAsync(Topic, Message, cancellationToken).Wait(cancellationToken);

            MockInnerProducer.Received(1).ProduceAsync(
                Arg.Any<string>(),
                Arg.Any<Message<TKey, TValue>>(),
                Arg.Is(cancellationToken));
        }

        [Test]
        public void Produce_ProduceAsyncTopicPartition_CalledInnerProducerProduceAsyncWithTopicPartition()
        {
            Producer.ProduceAsync(TopicPartition, Message).Wait();
            MockInnerProducer.Received(1).ProduceAsync(
                Arg.Any<TopicPartition>(),
                Arg.Any<Message<TKey, TValue>>());
        }

        [Test]
        public void
            Produce_ProduceAsyncTopicPartitionAndCancellationToken_CalledInnerProducerProduceAsyncTopicPartitionAndCancellationToken()
        {
            var cancellationToken = new CancellationToken();
            Producer.ProduceAsync(TopicPartition, Message, cancellationToken).Wait();

            MockInnerProducer.Received(1).ProduceAsync(
                Arg.Any<TopicPartition>(),
                Arg.Any<Message<TKey, TValue>>(),
                Arg.Is(cancellationToken));
        }

        [Test]
        public void Produce_ProduceWithNoHandler_CalledInnerProducerProduceWithoutHandler()
        {
            Producer.Produce(Topic, Message);

            MockInnerProducer.Received(1).Produce(
                Arg.Any<string>(),
                Arg.Any<Message<TKey, TValue>>());
        }

        [Test]
        public void Produce_SendOffsetsToTransaction_CalledInnerSendOffsetsToTransaction()
        {
            var mockConsumer = Substitute.For<IConsumer<int, byte[]>>();

            var offsets = new List<TopicPartitionOffset> {new TopicPartitionOffset("", new Partition(), new Offset())};
            var groupMetadata = mockConsumer.ConsumerGroupMetadata;
            var timeout = TimeSpan.MaxValue;

            Producer.SendOffsetsToTransaction(offsets, mockConsumer.ConsumerGroupMetadata, timeout);
            MockInnerProducer.Received(1)
                .SendOffsetsToTransaction(Arg.Is(offsets), Arg.Is(groupMetadata), Arg.Is(timeout));
        }

        [Test]
        public void
            Produce_TopicPartitionAxualProduce_CalledInnerProducerTopicPartitionProduceWithConvertedHandler()
        {
            Producer.Produce(TopicPartition, Message, Handler);

            MockInnerProducer.Received(1).Produce(
                Arg.Any<TopicPartition>(),
                Arg.Any<Message<TKey, TValue>>(),
                Arg.Any<Action<DeliveryReport<TKey, TValue>>>());
        }

        [Test]
        public void Produce_ProduceAsyncTopicPartition_CalledInnerProducerTopicPartitionProduceAsync()
        {
            Producer.ProduceAsync(TopicPartition, Message).Wait();

            MockInnerProducer.Received(1).ProduceAsync(
                Arg.Any<TopicPartition>(),
                Arg.Any<Message<TKey, TValue>>());
        }

        [Test]
        public void Produce_TopicPartitionProduceWithNoHandler_CalledInnerProducerTopicPartitionProduceWithoutHandler()
        {
            Producer.Produce(TopicPartition, Message);

            MockInnerProducer.Received(1).Produce(
                Arg.Any<TopicPartition>(),
                Arg.Any<Message<TKey, TValue>>());
        }
    }
}