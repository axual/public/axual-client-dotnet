//
// Copyright 2020 Axual B.V. 2020
//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

using System;
using Axual.Kafka.Proxy.Proxies;
using Axual.Kafka.Proxy.Proxies.Resolving;
using Axual.Kafka.Proxy.Proxies.Resolving.Resolvers;
using Confluent.Kafka;
using NSubstitute;
using NUnit.Framework;

namespace Axual.Kafka.Tests.Producers
{
    [TestFixture]
    internal class ResolverProducerTests : ProducerTests<int, long>
    {
        private IResolver _topicResolver;
        private string _unResolveTopic;
        private string _resolveTopic;
        
        [SetUp]
        public override void SetUp()
        {
            base.SetUp();
            Producer = new ResolvingProducer<int, long>(MockInnerProducerBuilder, ProducerConfig, _topicResolver);
        }

        [OneTimeSetUp]
        public override void OneTimeSetUp()
        {
            base.OneTimeSetUp();
            _unResolveTopic = Topic;

            var resolverProducerConfig = new ResolvingProducerConfig(ProducerConfig)
            {
                Tenant = "tenant name",
                Environment = "environment name",
                ApplicationId = "1",
                Instance = "platform",
                TopicPattern = $"{{{Constants.ConfigurationKeys.Axual.Tenant}}}-" +
                               $"{{{Constants.ConfigurationKeys.Axual.Environment}}}-" +
                               $"{{{Constants.ConfigurationKeys.Resolver.TopicResolverKey}}}",
                TopicResolver = $"{Constants.JavaResolverPath}TopicPatternResolver"
            };
            ProducerConfig = resolverProducerConfig;

            _topicResolver = new TopicPatternResolver(resolverProducerConfig);
            _resolveTopic = $"{resolverProducerConfig.Tenant}-{resolverProducerConfig.Environment}-{_unResolveTopic}";
        }

        [Test]
        public void ResolverProduce_CallAxualProducerAxualProduce_TopicResolved()
        {
            Producer.Produce(Topic, Message, Handler);

            MockInnerProducer.Received(1).Produce(
                Arg.Is(_resolveTopic),
                Arg.Any<Message<int, long>>(),
                // check if the handler changed the TValue to byte[]
                Arg.Any<Action<DeliveryReport<int, long>>>());
        }

        [Test]
        public void ResolverProduce_CallAxualProducerProduceAsync_TopicResolved()
        {
            Producer.ProduceAsync(Topic, Message).Wait();

            MockInnerProducer.Received(1).ProduceAsync(
                Arg.Is(_resolveTopic),
                Arg.Any<Message<int, long>>());
        }

        [Test]
        public void ResolverProduce_CallAxualProducerProduceWithNoHandler_TopicResolved()
        {
            Producer.Produce(Topic, Message);

            MockInnerProducer.Received(1).Produce(
                Arg.Is(_resolveTopic),
                Arg.Any<Message<int, long>>());
        }

        [Test]
        public void ResolverProduce_CallAxualProducerTopicPartitionAxualProduce_TopicResolved()
        {
            Producer.Produce(TopicPartition, Message, Handler);

            MockInnerProducer.Received(1).Produce(
                Arg.Is<TopicPartition>(tp => tp.Topic.Equals(_resolveTopic)),
                Arg.Any<Message<int, long>>(),
                // check if the handler changed the TValue to byte[]
                Arg.Any<Action<DeliveryReport<int, long>>>());
        }

        [Test]
        public void ResolverProduce_CallAxualProducerTopicPartitionProduceAsync_TopicResolved()
        {
            Producer.ProduceAsync(TopicPartition, Message);

            MockInnerProducer.Received(1).ProduceAsync(
                Arg.Is<TopicPartition>(tp => tp.Topic.Equals(_resolveTopic)),
                Arg.Any<Message<int, long>>());
        }

        [Test]
        public void ResolverProduce_CallAxualProducerTopicPartitionProduceWithNoHandler_TopicResolved()
        {
            Producer.Produce(TopicPartition, Message);

            MockInnerProducer.Received(1).Produce(
                Arg.Is<TopicPartition>(tp => tp.Topic.Equals(_resolveTopic)),
                Arg.Any<Message<int, long>>());
        }
    }
}
