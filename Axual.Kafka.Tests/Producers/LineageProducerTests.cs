//
// Copyright 2020 Axual B.V. 2020
//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

using System.Linq;
using Axual.Kafka.Proxy;
using Axual.Kafka.Proxy.Proxies.Lineage;
using Axual.Kafka.Proxy.ValueHeader;
using NUnit.Framework;

namespace Axual.Kafka.Tests.Producers
{
    internal class LineageProducerTests : ProducerTests<int, long>
    {
        [SetUp]
        public override void SetUp()
        {
            base.SetUp();

            Producer = new LineageProducer<int, long>(MockInnerProducerBuilder, ProducerConfig);
        }

        [OneTimeSetUp]
        public override void OneTimeSetUp()
        {
            base.OneTimeSetUp();
        }

        [OneTimeTearDown]
        public override void OneTimeTearDown()
        {
            base.OneTimeTearDown();
        }


        [Test]
        public void Lineage_ProducerMessage_AddedConsumeLineageHeaders()
        {
            ProducerConfig = new LineageProducerConfig(ProducerConfig)
            {
                ApplicationId = "LineageConsumerTests",
                ApplicationVersion = "2",
                System = "system",
                Instance = "instance",
                Tenant = "tenant",
                Environment = "environment",
                Cluster = "cluster"
            };

            Producer.Produce(Topic, Message);

            var messageHeaders = Message.Headers;
            var expectedKeyHeaders =
                Enum<Constants.AxualMessage.EnumProducerHeadersKey>.GetDescriptions();
            var unknownAddedHeaders =
                messageHeaders.Select(h => h.Key).Except(expectedKeyHeaders);
            var missingHeaders =
                expectedKeyHeaders.Except(messageHeaders.Select(h => h.Key));

            foreach (var missingHeader in missingHeaders)
                Assert.Fail($"Header '{missingHeader}' is missing.");

            foreach (var unknownHeader in unknownAddedHeaders)
                Assert.Fail($"Header '{unknownHeader}' shouldn't been added.");
        }
    }
}