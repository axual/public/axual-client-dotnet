//
// Copyright 2020 Axual B.V. 2020
//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

using Axual.Kafka.Proxy.ValueHeader;
using NUnit.Framework;

namespace Axual.Kafka.Tests
{
    [TestFixture]
    internal class AxualValueHeadersTest
    {
        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            _tempGuid = new AxualMessageId();
            _notDistributedMessage = AxualCopyFlag.OriginalMessage;
            _distributedFirstLevelMessage = AxualCopyFlag.OriginalMessage.SetDistributedToLevel(1);
            _distributedSecondLevelMessage = AxualCopyFlag.OriginalMessage.SetDistributedToLevel(2);
            _distributedThirdLevelMessage = AxualCopyFlag.OriginalMessage.SetDistributedToLevel(3);
            _distributedFirstAndSecondLevelMessage =
                AxualCopyFlag.OriginalMessage.SetDistributedToLevel(2).SetDistributedToLevel(1);
        }

        private ValueHeader<int> _valueHeader;
        private AxualMessageId _tempGuid;
        private AxualCopyFlag _notDistributedMessage;
        private AxualCopyFlag _distributedFirstLevelMessage;
        private AxualCopyFlag _distributedSecondLevelMessage;
        private AxualCopyFlag _distributedThirdLevelMessage;
        private AxualCopyFlag _distributedFirstAndSecondLevelMessage;
        private readonly AxualTime _serializationTime = AxualTime.Now;


        [Test]
        public void Constants_ValueHeaderMessage_ConstantsValueDidNotChange()
        {
            // Test offsets
            Assert.AreEqual(0, Constants.AxualMessage.Offsets.MagicOffset);
            Assert.AreEqual(2, Constants.AxualMessage.Offsets.VersionNumberOffset);
            Assert.AreEqual(4, Constants.AxualMessage.Offsets.ExtendedHeaderSizeOffset);

            var version0 = Constants.AxualMessage.Versions.BaseVersion;
            var version1 = Constants.AxualMessage.Versions.AxualMessageVersions[1];
            var version2 = Constants.AxualMessage.Versions.AxualMessageVersions[2];

            // Test header size values
            Assert.AreEqual(0, version0.VersionNumber);
            Assert.AreEqual(8, version0.HeaderSize);
            Assert.AreEqual(1, version1.VersionNumber);
            Assert.AreEqual(9, version1.HeaderSize);
            Assert.AreEqual(2, version2.VersionNumber);
            Assert.AreEqual(33, version2.HeaderSize);

            // Test fixed contents
            Assert.AreEqual(0x0BEB, Constants.AxualMessage.AxualMagicUInt16Value);
        }

        [Test]
        public void IsDistributedToLevel_IndexOutOfLowerBound_False()
        {
            _valueHeader = new ValueHeader<int>(1, _tempGuid, _serializationTime, _notDistributedMessage);

            Assert.IsFalse(_valueHeader.AxualCopyFlag.IsDistributedToLevel(0));
        }

        [Test]
        public void IsDistributedToLevel_IndexOutOfUpperBound_False()
        {
            _valueHeader = new ValueHeader<int>(1, _tempGuid, _serializationTime, _notDistributedMessage);

            Assert.IsFalse(_valueHeader.AxualCopyFlag.IsDistributedToLevel(32));
        }

        [Test]
        public void IsDistributedToLevel_MessageDidNotDistributed_FirstLevelCopyFalse()
        {
            _valueHeader = new ValueHeader<int>(1, _tempGuid, _serializationTime, _notDistributedMessage);

            Assert.IsFalse(_valueHeader.AxualCopyFlag.IsDistributedToLevel(1));
        }

        [Test]
        public void IsDistributedToLevel_MessageDidNotDistributed_SecondLevelCopyFalse()
        {
            _valueHeader = new ValueHeader<int>(1, _tempGuid, _serializationTime, _notDistributedMessage);

            Assert.IsFalse(_valueHeader.AxualCopyFlag.IsDistributedToLevel(2));
        }

        [Test]
        public void IsDistributedToLevel_MessageDistributedToFirstAndSecondLevel_FirstLevelCopyTrue()
        {
            _valueHeader =
                new ValueHeader<int>(1, _tempGuid, _serializationTime, _distributedFirstAndSecondLevelMessage);

            Assert.IsTrue(_valueHeader.AxualCopyFlag.IsDistributedToLevel(1));
        }

        [Test]
        public void IsDistributedToLevel_MessageDistributedToFirstLevel_FirstLevelCopyTrue()
        {
            _valueHeader = new ValueHeader<int>(1, _tempGuid, _serializationTime, _distributedFirstLevelMessage);

            Assert.IsTrue(_valueHeader.AxualCopyFlag.IsDistributedToLevel(1));
        }

        [Test]
        public void IsDistributedToLevel_MessageDistributedToFirstLevel_SecondLevelCopyFalse()
        {
            _valueHeader = new ValueHeader<int>(1, _tempGuid, _serializationTime, _distributedFirstLevelMessage);

            Assert.IsFalse(_valueHeader.AxualCopyFlag.IsDistributedToLevel(2));
        }

        [Test]
        public void IsDistributedToLevel_MessageDistributedToSecondLevel_FirstLevelCopyFalse()
        {
            _valueHeader = new ValueHeader<int>(1, _tempGuid, _serializationTime, _distributedSecondLevelMessage);

            Assert.IsFalse(_valueHeader.AxualCopyFlag.IsDistributedToLevel(1));
        }

        [Test]
        public void IsDistributedToLevel_MessageDistributedToSecondLevel_SecondLevelCopyTrue()
        {
            _valueHeader = new ValueHeader<int>(1, _tempGuid, _serializationTime, _distributedSecondLevelMessage);

            Assert.IsTrue(_valueHeader.AxualCopyFlag.IsDistributedToLevel(2));
        }

        [Test]
        public void IsDistributedToLevel_MessageDistributedToThirdLevel_ThirdLevelCopyTrue()
        {
            _valueHeader = new ValueHeader<int>(1, _tempGuid, _serializationTime, _distributedThirdLevelMessage);

            Assert.IsTrue(_valueHeader.AxualCopyFlag.IsDistributedToLevel(3));
        }

        [Test]
        public void IsDistributedToLevel_OrderCopy_MessageDistributedToFirstAndSecondLevel_SecondLevelCopyTrue()
        {
            _valueHeader =
                new ValueHeader<int>(1, _tempGuid, _serializationTime, _distributedFirstAndSecondLevelMessage);

            Assert.IsTrue(_valueHeader.AxualCopyFlag.IsDistributedToLevel(2));
        }
    }
}