using Axual.Kafka.Proxy.Exceptions;
using Axual.Kafka.Proxy.HttpClient;
using Axual.SchemaRegistry.Serdes.Avro.SchemaRegistryClient;
using Axual.SchemaRegistry.Serdes.Avro.SchemaRegistryClient.Rest.SchemaRegistry;
using NSubstitute;
using NUnit.Framework;

namespace Axual.Kafka.Tests.SchemaRegistryClient
{
    public class AxualSchemaRegistryClientBuilderTests
    {
        [Test]
        public void AxualSchemaRegistryClientBuilder_VerifyWithoutAxualSchemaRegistryConfig_ThrowRequireConfigurationException()
        {
            // Arrange
            var axualSchemaRegistryClientBuilder =  new AxualSchemaRegistryClientBuilder();
            
            // Act
            var exception = Assert.Throws<RequireConfigurationException>(() =>  axualSchemaRegistryClientBuilder.Verify());

            // Assert
            Assert.That(exception.Message,
                Is.EqualTo(string.Format(
                    RequireConfigurationException.RequireConfigurationExceptionMessageFormat,
                    nameof(AxualSchemaRegistryConfig))));
        }
        
        [Test]
        public void AxualSchemaRegistryClientBuilder_VerifyWithoutRestService_UseDefault()
        {
            // Arrange
            var config = new AxualSchemaRegistryConfig
            {
                Url = "https://platform.local:25000"
            };
            var axualSchemaRegistryClientBuilder = new AxualSchemaRegistryClientBuilder()
                .SetAxualSchemaRegistryConfig(config);

            // Act
            axualSchemaRegistryClientBuilder.Build();

            // Assert
            Assert.Pass();
        }
        
        [Test]
        public void AxualSchemaRegistryClientBuilder_VerifyWithRestService_NotOverwrite()
        {
            // Arrange
            var config = new AxualSchemaRegistryConfig();
            var mockRestServiceBuilder = Substitute.For<ISchemaRegistryRestServiceBuilder>();
            var axualSchemaRegistryClientBuilder = new AxualSchemaRegistryClientBuilder()
                .SetRestService(mockRestServiceBuilder)
                .SetAxualSchemaRegistryConfig(config);

            // Act
            var schemaRegistryClient = axualSchemaRegistryClientBuilder.Build();

            // Assert
            mockRestServiceBuilder.Received(1).Build();
            // Don't overwrite 
            mockRestServiceBuilder.Received(0).SetHttpClient(Arg.Any<IHttpClientFactory>());
            mockRestServiceBuilder.Received(0).SetSchemaRegistryConfig(Arg.Is(config));
        }
    }
}