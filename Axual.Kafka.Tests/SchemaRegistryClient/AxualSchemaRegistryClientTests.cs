using System;
using System.Collections.Generic;
using Axual.SchemaRegistry.Serdes.Avro.SchemaRegistryClient;
using Axual.SchemaRegistry.Serdes.Avro.SchemaRegistryClient.Rest.SchemaRegistry;
using Confluent.SchemaRegistry;
using NSubstitute;
using NUnit.Framework;

namespace Axual.Kafka.Tests.SchemaRegistryClient
{
    public class AxualSchemaRegistryClientTests
    {
        private ISchemaRegistryService _mockSchemaRegistryService;
        private AxualSchemaRegistryConfig _axualSchemaRegistryConfig;
        private AxualSchemaRegistryClient _axualSchemaRegistryClient;
        private string _avroSchema;

        [SetUp]
        public void Setup()
        {
            _mockSchemaRegistryService = Substitute.For<ISchemaRegistryService>();
            _axualSchemaRegistryConfig = new AxualSchemaRegistryConfig();
            _axualSchemaRegistryClient =
                new AxualSchemaRegistryClient(_mockSchemaRegistryService, _axualSchemaRegistryConfig);
            _avroSchema =
                "{ \"namespace\": \"Axual.Kafka.Example\", \"type\": \"record\", \"name\": \"User\", \"fields\": [{\"name\": \"name\", \"type\": \"string\"}]}";
        }

        [Test]
        public void AxualSchemaRegistryClient_CallNotSupportedExceptionMethods_ThrowNotSupportedException()
        {
            // Arrange
            var schema = new Schema(string.Empty, new List<SchemaReference>(), SchemaType.Avro);
            var subject = "subject";

            _axualSchemaRegistryClient =
                new AxualSchemaRegistryClient(_mockSchemaRegistryService, _axualSchemaRegistryConfig);

            // Act + Assert
            Assert.Throws<NotSupportedException>(() =>
                _axualSchemaRegistryClient.LookupSchemaAsync(subject, schema, false));
            Assert.Throws<NotSupportedException>(() => _axualSchemaRegistryClient.RegisterSchemaAsync(subject, schema));
            Assert.Throws<NotSupportedException>(() =>
                _axualSchemaRegistryClient.RegisterSchemaAsync(subject, _avroSchema));
            Assert.Throws<NotSupportedException>(() => _axualSchemaRegistryClient.GetRegisteredSchemaAsync(subject, 1));
            Assert.Throws<NotSupportedException>(() => _axualSchemaRegistryClient.GetSchemaAsync(subject, 1));
            Assert.Throws<NotSupportedException>(() => _axualSchemaRegistryClient.GetAllSubjectsAsync());
            Assert.Throws<NotSupportedException>(() => _axualSchemaRegistryClient.GetLatestSchemaAsync(subject));
            Assert.Throws<NotSupportedException>(() => _axualSchemaRegistryClient.GetSubjectVersionsAsync(subject));
            Assert.Throws<NotSupportedException>(() => _axualSchemaRegistryClient.IsCompatibleAsync(subject, schema));
            Assert.Throws<NotSupportedException>(
                () => _axualSchemaRegistryClient.IsCompatibleAsync(subject, _avroSchema));
        }

        [Test]
        public void AxualSchemaRegistryClient_GetSchemaAsyncFirstTime_NoCacheCalledGetSchemaAsync()
        {
            // Arrange
            _axualSchemaRegistryClient =
                new AxualSchemaRegistryClient(_mockSchemaRegistryService, _axualSchemaRegistryConfig);

            // Act 
            _axualSchemaRegistryClient.GetSchemaAsync(1).Wait();

            // Assert
            _mockSchemaRegistryService.Received(1).GetSchemaAsync(1, Arg.Any<string>());
        }

        [Test]
        public void AxualSchemaRegistryClient_GetSchemaAsyncSecondTime_GetCachedResult()
        {
            _mockSchemaRegistryService.GetSchemaAsync(1, Arg.Any<string>())
                .Returns(new Schema(_avroSchema, SchemaType.Avro));

            // Act 
            // 1st Time will call ISchemaRegistryService.GetSchemaAsync and save result
            _axualSchemaRegistryClient.GetSchemaAsync(1).Wait();
            // 2nd should not called ISchemaRegistryService.GetSchemaAsync
            _axualSchemaRegistryClient.GetSchemaAsync(1).Wait();

            // Assert
            // Only called once to GetSchemaAsync, 2nd result should be cached
            _mockSchemaRegistryService.Received(1).GetSchemaAsync(1, Arg.Any<string>());
        }
        
        [Test]
        public void AxualSchemaRegistryService_Dispose_DisposeSchemaRegistryService()
        {
            // ARRANGE
            // ACT
            _axualSchemaRegistryClient.Dispose();

            // ASSERT
            _mockSchemaRegistryService.Received(1).Dispose();
        }
    }
}