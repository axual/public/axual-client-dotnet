using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using Axual.SchemaRegistry.Serdes.Avro.SchemaRegistryClient;
using Axual.SchemaRegistry.Serdes.Avro.SchemaRegistryClient.Rest.DataContracts;
using Axual.SchemaRegistry.Serdes.Avro.SchemaRegistryClient.Rest.RollingRestService;
using Axual.SchemaRegistry.Serdes.Avro.SchemaRegistryClient.Rest.SchemaRegistry;
using Confluent.SchemaRegistry;
using Newtonsoft.Json;
using NSubstitute;
using NUnit.Framework;

namespace Axual.Kafka.Tests.SchemaRegistryClient.Rest
{
    public class SchemaRegistryServiceTests
    {
        private List<Uri> _endPoints;
        private AxualSchemaRegistryConfig _axualSchemaRegistryConfig;
        private IRollingRequest _rollingRequest;
        private ISchemaRegistryService _schemaRegistryService;

        private const string SchemaString =
            "{\"type\":\"record\",\"name\":\"Application\",\"namespace\":\"io.axual.client.example.schema\",\"fields\":[{\"name\":\"name\",\"doc\":\"The name of the application\",\"type\":\"string\"},{\"name\":\"version\",\"doc\":\"(Optional) The application version\",\"default\":null,\"type\":[\"null\",\"string\"]},{\"name\":\"owner\",\"doc\":\"The owner of the application\",\"default\":null,\"type\":[\"null\",\"string\"]}]}";

        [OneTimeSetUp]
        public void Setup()
        {
            _endPoints = new List<Uri>
            {
                new("http://sr-host-1/"),
                new("http://sr-host-2/"),
                new("http://sr-host-3/")
            };
            _axualSchemaRegistryConfig = new AxualSchemaRegistryConfig();
            _rollingRequest = Substitute.For<IRollingRequest>();
            _schemaRegistryService = new SchemaRegistryService(_rollingRequest);
        }

        [Test]
        public void SchemaRegistryService_LookupSchemaAsync_CallExecuteRequestAsync()
        {
            // ARRANGE
            // ACT
            _schemaRegistryService.LookupSchemaAsync(
                "axual-local-example-applicationlogevents-key",
                new Schema(SchemaString, SchemaType.Avro), false);

            // ASSERT
            _rollingRequest.ExecuteRequestAsync(Arg.Any<Func<Uri, HttpRequestMessage>>());
        }

        [Test]
        public void SchemaRegistryService_GetSchemaAsync_CallExecuteRequestAsync()
        {
            // ARRANGE

            // ACT
            _schemaRegistryService.GetSchemaAsync(1);

            // ASSERT
            _rollingRequest.ExecuteRequestAsync(Arg.Any<Func<Uri, HttpRequestMessage>>());
        }

        [Test]
        public void SchemaRegistryService_GetSchemaAsync_ConvertedToSchemaObject()
        {
            // ARRANGE
            var objects = GetObjects(new SchemaString(SchemaString));
            var content = new StringContent(
                string.Join("\n", objects.Select(JsonConvert.SerializeObject)),
                Encoding.UTF8);

            _rollingRequest.ExecuteRequestAsync(Arg.Any<Func<Uri, HttpRequestMessage>>())
                .Returns(new HttpResponseMessage
                {
                    Content = content
                });

            // ACT
            var schema = _schemaRegistryService.GetSchemaAsync(1).Result;

            // ASSERT
            Assert.AreEqual(SchemaString, schema.SchemaString);
        }

        private static IEnumerable<object> GetObjects(params object[] jsonBody)
        {
            return jsonBody;
        }

        [Test]
        public void SchemaRegistryService_Dispose_DisposeRollingRequest()
        {
            // ARRANGE
            // ACT
            _schemaRegistryService.Dispose();

            // ASSERT
            _rollingRequest.Received(1).Dispose();
        }
    }
}