using System;
using Axual.Kafka.Proxy.Exceptions;
using Axual.Kafka.Proxy.HttpClient;
using Axual.SchemaRegistry.Serdes.Avro.SchemaRegistryClient;
using Axual.SchemaRegistry.Serdes.Avro.SchemaRegistryClient.Rest.SchemaRegistry;
using Confluent.SchemaRegistry;
using NSubstitute;
using NUnit.Framework;

namespace Axual.Kafka.Tests.SchemaRegistryClient.Rest
{
    public class SchemaRegistryRestServiceBuilderTests
    {
        [Test]
        public void SchemaRegistryRestServiceBuilder_VerifyWithoutHttpClientFactory_ThrowRequireConfigurationException()
        {
            // Arrange
            var schemaRegistryClientBuilder =  new SchemaRegistryRestServiceBuilder();
            
            // Act
            var exception = Assert.Throws<RequireConfigurationException>(() =>  schemaRegistryClientBuilder.Verify());

            // Assert
            Assert.That(exception.Message,
                Is.EqualTo(string.Format(
                    RequireConfigurationException.RequireConfigurationExceptionMessageFormat,
                    nameof(HttpClientFactory))));
        }
        
        [Test]
        public void SchemaRegistryRestServiceBuilder_VerifyWithoutAxualSchemaRegistryConfig_ThrowRequireConfigurationException()
        {
            // Arrange
            var schemaRegistryClientBuilder =  new SchemaRegistryRestServiceBuilder();
            var mockHttpClient = Substitute.For<IHttpClientFactory>();
            schemaRegistryClientBuilder.SetHttpClient(mockHttpClient);
            
            // Act
            var exception = Assert.Throws<RequireConfigurationException>(() =>  schemaRegistryClientBuilder.Verify());

            // Assert
            Assert.That(exception.Message,
                Is.EqualTo(string.Format(
                    RequireConfigurationException.RequireConfigurationExceptionMessageFormat,
                    nameof(SchemaRegistryConfig))));
        }
        
        [Test]
        public void SchemaRegistryRestServiceBuilder_VerifyWithoutSchemaRegistryUrl_ThrowRequireConfigurationException()
        {
            // Arrange
            var schemaRegistryClientBuilder =  new SchemaRegistryRestServiceBuilder();
            var mockHttpClient = Substitute.For<IHttpClientFactory>();
            schemaRegistryClientBuilder.SetHttpClient(mockHttpClient);
            schemaRegistryClientBuilder.SetSchemaRegistryConfig(new AxualSchemaRegistryConfig());
            
            // Act
            var exception = Assert.Throws<RequireConfigurationException>(() =>  schemaRegistryClientBuilder.Verify());

            // Assert
            Assert.That(exception.Message,
                Is.EqualTo(string.Format(
                    RequireConfigurationException.RequireConfigurationExceptionMessageFormat,
                    SchemaRegistryConfig.PropertyNames.SchemaRegistryUrl)));
        }

        [Test]
        public void SchemaRegistryRestServiceBuilder_VerifyListOfUrlsValid_Pass()
        {
            // Arrange
            var uriString = "http://axual-local-schema-registry-master/," +
                            "http://10.100.20.62," +
                            "http://axual-local-schema-registry-master:20000/," +
                            "https://10.100.20.62:20000," +
                            "http://platform.local:20000/," +
                            "https://platform.local:20000/," +
                            "https://platform.local:25000";
            
            var schemaRegistryClientBuilder =  new SchemaRegistryRestServiceBuilder();
            var mockHttpClient = Substitute.For<IHttpClientFactory>();
            schemaRegistryClientBuilder.SetHttpClient(mockHttpClient);
            schemaRegistryClientBuilder.SetSchemaRegistryConfig(new AxualSchemaRegistryConfig()
            {
                Url = uriString
            });
            
            // Act
            schemaRegistryClientBuilder.Verify();

            // Assert
            Assert.Pass();
        }
        
        [Test]
        public void SchemaRegistryRestServiceBuilder_VerifyInvalidSRUrl_ThrowRequireConfigurationException()
        {
            // Arrange
            var uriString = "NotValid";
            var schemaRegistryClientBuilder =  new SchemaRegistryRestServiceBuilder();
            var mockHttpClient = Substitute.For<IHttpClientFactory>();
            schemaRegistryClientBuilder.SetHttpClient(mockHttpClient);
            schemaRegistryClientBuilder.SetSchemaRegistryConfig(new AxualSchemaRegistryConfig()
            {
                Url = uriString
            });
            
            // Act
            var exception = Assert.Throws<ArgumentException>(() =>  schemaRegistryClientBuilder.Verify());

            // Assert
            Assert.AreEqual(exception.Message, $"The Url '{uriString}' is not valid Uri.");
        }
    }
}