using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Axual.SchemaRegistry.Serdes.Avro.SchemaRegistryClient.Rest.DataContracts;
using Axual.SchemaRegistry.Serdes.Avro.SchemaRegistryClient.Rest.RollingRestService;
using Confluent.SchemaRegistry;
using Moq;
using Moq.Protected;
using Newtonsoft.Json;
using NSubstitute;
using NUnit.Framework;

namespace Axual.Kafka.Tests.SchemaRegistryClient.Rest
{
    public class RollingRestServiceTests
    { 
        private List<Uri> _endPoints;
        
        [SetUp]
        public void Setup()
        {
            _endPoints = new List<Uri>
            {
                new("http://sr-host-1/"),
                new("http://sr-host-2/"),
                new("http://sr-host-3/")
            };
        }
        
        [Test]
        public void RollingRestService_ExecuteRequestAsyncFailedForFirstHost_CallSecondHostButNotThird()
        {
            // ARRANGE
            var handlerMock = CreateMockHttpMessageHandler(new List<Tuple<HttpStatusCode, HttpContent>>()
            {
                // failed for 1st host
                new(HttpStatusCode.InternalServerError, new StringContent(string.Empty)),
                // Get empty string for 2nd and 3rd host
                new(HttpStatusCode.OK, new StringContent(string.Empty)),
                new(HttpStatusCode.OK, new StringContent(string.Empty))
            });
            var httpClient = new HttpClient(handlerMock.Object);
            var rollingRestService = new RollingRequest(httpClient, _endPoints);

            // ACT
            rollingRestService.ExecuteRequestAsync(CreateRequest);

            // ASSERT
            var lastEndPoint = _endPoints.Last();
            
            // Called the first and second call
            foreach (var endpoint in _endPoints)
            {
                if (!endpoint.Equals(lastEndPoint))
                {
                    // query 1st and 2nd host
                    handlerMock.Protected().Verify(
                        "SendAsync",
                        Times.Exactly(1), // we expected a single external request
                        ItExpr.Is<HttpRequestMessage>(req =>
                            req.Method == HttpMethod.Get // we expected a GET request
                            && req.RequestUri.Host == endpoint.Host
                        ),
                        ItExpr.IsAny<CancellationToken>());
                }
                else
                {
                    // Doesn't query last host
                    handlerMock.Protected().Verify(
                        "SendAsync",
                        Times.Exactly(0),
                        ItExpr.Is<HttpRequestMessage>(req => req.RequestUri.Host == endpoint.Host
                        ),
                        ItExpr.IsAny<CancellationToken>()
                    );
                }
            }
        }

        [Test]
        public void RollingRestService_ExecuteRequestAsyncForAllHost_IterateOverAllEndPoint()
        {
            // ARRANGE
            var handlerMock = CreateMockHttpMessageHandler(new List<Tuple<HttpStatusCode, HttpContent>>()
            {
                // failed for all hosts
                new(HttpStatusCode.InternalServerError, new StringContent(string.Empty)),
                new(HttpStatusCode.InternalServerError, new StringContent(string.Empty)),
                new(HttpStatusCode.InternalServerError, new StringContent(string.Empty)),
            });
            var httpClient = new HttpClient(handlerMock.Object);
            var rollingRestService = new RollingRequest(httpClient, _endPoints);

            // ACT
            rollingRestService.ExecuteRequestAsync(CreateRequest);
            
            // ASSERT
            // Tried to get result from all hosts
            foreach (var srEndPoint in _endPoints)
            {
                handlerMock.Protected().Verify(
                    "SendAsync",
                    Times.Exactly(1), // we expected a single external request
                    ItExpr.Is<HttpRequestMessage>(req => req.RequestUri.Host == srEndPoint.Host
                    ),
                    ItExpr.IsAny<CancellationToken>());
            }
        }
        
        [Test]
        public void RollingRestService_ExecuteRequestAsyncNotFound_ThrowsSchemaRegistryException()
        {
            // ARRANGE
            const string errorMessage = "Error Message";
            const int errorCode = 10;
            var endpoint = new List<Uri>
            {
                new("http://sr-host-1/"),
                new("http://sr-host-2/")
            };
            
            var expectedError = new ErrorMessage(errorCode, errorMessage);
            var json = JsonConvert.SerializeObject(expectedError);
            
            var handlerMock = CreateMockHttpMessageHandler(new List<Tuple<HttpStatusCode, HttpContent>>()
            {
                // failed for all hosts
                new(HttpStatusCode.NotFound, new StringContent(json)),
                new(HttpStatusCode.NotFound, new StringContent(json))
            });
            
            var httpClient = new HttpClient(handlerMock.Object);
            var rollingRestService = new RollingRequest(httpClient, endpoint);

            // ACT + ASSERT
            Assert.ThrowsAsync(
                Is.TypeOf<SchemaRegistryException>()
                    .And.Message.EqualTo($"{errorMessage}; error code: {errorCode}") ,
                () => rollingRestService.ExecuteRequestAsync(CreateRequest));
        }
        
        [Test]
        public void RollingRestService_ExecuteRequestAsyncUnauthorized_ThrowHttpRequestUnauthorized()
        {
            // ARRANGE
            var handlerMock = CreateMockHttpMessageHandler(new List<Tuple<HttpStatusCode, HttpContent>>()
            {
                new(HttpStatusCode.Unauthorized, new StringContent(string.Empty)),
            });
            
            var httpClient = new HttpClient(handlerMock.Object);
            var rollingRestService = new RollingRequest(httpClient, _endPoints);

            // ACT + ASSERT
            Assert.ThrowsAsync(
                Is.TypeOf<HttpRequestException>()
                    .And.Message.EqualTo("Unauthorized") ,
                () => rollingRestService.ExecuteRequestAsync(CreateRequest));
            
            // query the first host
            handlerMock.Protected().Verify(
                "SendAsync",
                Times.Exactly(1), // we expected a single external request
                ItExpr.Is<HttpRequestMessage>(req => req.RequestUri.Host == _endPoints.First().Host
                ),
                ItExpr.IsAny<CancellationToken>());
            
            // didn't query the rest of the host
            foreach (var srEndPoint in _endPoints.Skip(1))
            {
                handlerMock.Protected().Verify(
                    "SendAsync",
                    Times.Exactly(0), // we expected a single external request
                    ItExpr.Is<HttpRequestMessage>(req => req.RequestUri.Host == srEndPoint.Host
                    ),
                    ItExpr.IsAny<CancellationToken>());
            }
        }
        
        [Test]
        public void RollingRestService_ExecuteRequestAsyncExceptions_ThrowsExceptionInTheRightOrder()
        {
            // ARRANGE
            var responses = new List<Tuple<HttpStatusCode, HttpContent>>
            {
                // failed for all hosts
                new(HttpStatusCode.InternalServerError, new StringContent(string.Empty)),
                new(HttpStatusCode.BadGateway, new StringContent(string.Empty)),
                new(HttpStatusCode.UseProxy, new StringContent(string.Empty))
            };
            
            var handlerMock = CreateMockHttpMessageHandler(responses);
            
            var httpClient = new HttpClient(handlerMock.Object);
            var rollingRestService = new RollingRequest(httpClient, _endPoints);

            // ACT + ASSERT
            var aggregateException = Assert.ThrowsAsync<AggregateException>(() => rollingRestService.ExecuteRequestAsync(CreateRequest));
            
            foreach (var (exception, httpStatusCode) in aggregateException.InnerExceptions.Zip(responses, (i1, i2) => (i1, i2.Item1)))
            {
                if(!exception.Message.Contains(httpStatusCode.ToString()))
                    Assert.Fail($"Expect exception {exception.Message} to contain {httpStatusCode}");
            }
        }
        
        [Test]
        public void RollingRestService_Dispose_DisposeHttpClient()
        {
            // ARRANGE
            var httpClient = Substitute.For<HttpClient>();
            var rollingRestService = new RollingRequest(httpClient, _endPoints);

            // ACT
            rollingRestService.Dispose();
            
            // ASSERT
            httpClient.Received(1).Dispose();
        }

        private static HttpRequestMessage CreateRequest(Uri uri)
        {
            return new HttpRequestMessage
            {
                RequestUri = uri,
                Content = new StringContent(string.Empty),
                Method = HttpMethod.Get
            };
        }
        
        private static Mock<HttpMessageHandler> CreateMockHttpMessageHandler(
            List<Tuple<HttpStatusCode, HttpContent>> returns)
        {
            var handlerMock = new Mock<HttpMessageHandler>(MockBehavior.Strict);
            var handlerPart = handlerMock
                .Protected()
                // Setup the PROTECTED method to mock
                .SetupSequence<Task<HttpResponseMessage>>(
                    "SendAsync",
                    ItExpr.IsAny<HttpRequestMessage>(),
                    ItExpr.IsAny<CancellationToken>()
                );

            foreach (var (httpStatusCode, httpContent) in returns)
            {
                handlerPart // prepare the expected response of the mocked http call
                    .ReturnsAsync(new HttpResponseMessage
                    {
                        StatusCode = httpStatusCode, // HttpStatusCode.Unauthorized,
                        Content = httpContent //new StringContent("[{'id':1,'value':'1'}]"),
                    });
            }

            handlerMock.Verify();
            return handlerMock;
        }
    }
}