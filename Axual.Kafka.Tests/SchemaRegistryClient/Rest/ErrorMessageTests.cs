using Axual.SchemaRegistry.Serdes.Avro.SchemaRegistryClient.Rest.DataContracts;
using NUnit.Framework;

namespace Axual.Kafka.Tests.SchemaRegistryClient.Rest
{
    public class ErrorMessageTests
    {
        [Test]
        public void ErrorMessage_Getter_RightErrorCodeAndMessage()
        {
            // ARRANGE
            const string expectedMessage = "error message";
            const int expectedErrorCode = 200;

            // ACT
            var errorMessage = new ErrorMessage(expectedErrorCode, expectedMessage);


            // ASSERT
            Assert.AreEqual(expectedErrorCode, errorMessage.ErrorCode);
            Assert.AreEqual(expectedMessage, errorMessage.Message);
        }

        [Test]
        public void ErrorMessage_ToString_RightErrorCodeAndMessage()
        {
            // ARRANGE
            const string expectedMessage = "error message";
            const int expectedErrorCode = 200;

            // ACT
            var errorMessage = new ErrorMessage(expectedErrorCode, expectedMessage);


            // ASSERT
            Assert.AreEqual($"{{error_code={expectedErrorCode}, message={expectedMessage}}}", errorMessage.ToString());
        }
    }
}