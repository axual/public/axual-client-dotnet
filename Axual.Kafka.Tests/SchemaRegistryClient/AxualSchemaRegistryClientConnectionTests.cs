using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Security.Cryptography.X509Certificates;
using Axual.Kafka.Proxy.HttpClient;
using Axual.SchemaRegistry.Serdes.Avro.SchemaRegistryClient;
using Axual.SchemaRegistry.Serdes.Avro.SchemaRegistryClient.Rest.SchemaRegistry;
using Confluent.Kafka;
using NUnit.Framework;
using WireMock.RequestBuilders;
using WireMock.ResponseBuilders;
using WireMock.Server;
using WireMock.Settings;

namespace Axual.Kafka.Tests.SchemaRegistryClient
{
    [TestFixtureSource(typeof(TestFixtures))]
    public class AxualSchemaRegistryClientConnectionTests
    {
        private const int SchemaId = 123;
        private const string EndpointPath = "/schemas/ids/123";

        private const string AvroSchemaString =
            "{ \"namespace\": \"Axual.Kafka.Example\", \"type\": \"record\", \"name\": \"User\", \"fields\": [{\"name\": \"name\", \"type\": \"string\"}]}";

        private const string SchemaResponseBody =
            "{ \"schema\": \"{ \\\"namespace\\\": \\\"Axual.Kafka.Example\\\", \\\"type\\\": \\\"record\\\", \\\"name\\\": \\\"User\\\", \\\"fields\\\": [{\\\"name\\\": \\\"name\\\", \\\"type\\\": \\\"string\\\"}]}\"}";

        private readonly WireMockServer _wireMock;
        private readonly AxualSchemaRegistryConfig _schemaRegistryConfig;
        private readonly bool _validCombination;

        public AxualSchemaRegistryClientConnectionTests(WireMockServerSettings serverSettings,
            Object schemaRegistryConfig, bool validCombination)
        {
            _wireMock = WireMockServer.Start(serverSettings ?? new WireMockServerSettings());
            _validCombination = validCombination;
            _wireMock.Given(
                    Request.Create()
                        .WithPath(s => s.StartsWith(EndpointPath))
                        .UsingGet())
                .RespondWith(
                    Response.Create()
                        .WithSuccess()
                        .WithHeader("Content-Type", "application/vnd.schemaregistry.v1+json")
                        .WithBody(SchemaResponseBody)
                );

            _schemaRegistryConfig = (AxualSchemaRegistryConfig)schemaRegistryConfig;
            _schemaRegistryConfig.Url = _wireMock.Url;
        }

        [OneTimeTearDown]
        public void TearDownFixture()
        {
            _wireMock.Stop();
        }

        // Test endpoints are OK, but if server/client settings don't match exceptions are thrown
        [Test]
        public void SchemaRegistryConnection_ValidEndpoint()
        {
            var restService = new SchemaRegistryRestServiceBuilder()
                .SetHttpClient(new HttpClientFactory())
                .SetSchemaRegistryConfig(_schemaRegistryConfig)
                .Build();
            var request = new HttpRequestMessage(HttpMethod.Get, _wireMock.Url + EndpointPath);
            var task = restService.GetSchemaAsync(SchemaId, "AVRO");

            if (_validCombination)
            {
                Assert.DoesNotThrow(() => task.Wait(TimeSpan.FromSeconds(10)));
                Assert.True(task.IsCompletedSuccessfully,
                    "Request with valid settings encountered exceptions" + task.Exception);
                var response = task.Result;
                Assert.AreEqual(AvroSchemaString, response?.SchemaString);
            }
            else
            {
                var exc = Assert.Throws<AggregateException>(() => task.Wait(TimeSpan.FromSeconds(10)));
                Assert.IsInstanceOf<HttpRequestException>(exc?.InnerException);
            }
        }

        private class TestFixtures : IEnumerable
        {
            private const String ServerCertificateFileName = "server-with-chain.p12";
            private const String ClientCaPemFileName = "ca-intermediate.pem";
            private const String WmCaStoreName = "WiremockTestingCustomCa";

            private readonly WireMockServerSettings _wmHttp = _mockServerSettings();
            private readonly WireMockServerSettings _wmHttps = _mockServerSettings(ServerCertificateFileName);

            private readonly AxualSchemaRegistryConfig _clDefault = new();

            private readonly AxualSchemaRegistryConfig _clCaLocation = new()
            {
                SslCaLocation = _getFilePath(ClientCaPemFileName),
            };

            private readonly AxualSchemaRegistryConfig _clCaStore = new()
            {
                SslCaCertificateStores = WmCaStoreName
            };
            
            private readonly AxualSchemaRegistryConfig _clCaLocationAndValidation = new()
            {
                SslCaLocation = _getFilePath(ClientCaPemFileName),
                EnableSslCertificateVerification = true,
                SslEndpointIdentificationAlgorithm = SslEndpointIdentificationAlgorithm.Https
            };

            private readonly AxualSchemaRegistryConfig _clCaLocationAndNoValidation = new()
            {
                SslCaLocation = _getFilePath(ClientCaPemFileName),
                EnableSslCertificateVerification = false,
                SslEndpointIdentificationAlgorithm = SslEndpointIdentificationAlgorithm.None
            };

            private readonly AxualSchemaRegistryConfig _clNoCaLocationAndNoValidation = new()
            {
                EnableSslCertificateVerification = false,
                SslEndpointIdentificationAlgorithm = SslEndpointIdentificationAlgorithm.None
            };

            private readonly AxualSchemaRegistryConfig _clNoCaLocationAndValidation = new()
            {
                EnableSslCertificateVerification = true,
                SslEndpointIdentificationAlgorithm = SslEndpointIdentificationAlgorithm.Https
            };

            static string _getFilePath(string file)
            {
                return UtilsHelper.GetFileFromResourcesPath("httpClient/" + file);
            }


            public TestFixtures()
            {
                // Prepare stores
                X509Certificate2Collection ca = new X509Certificate2Collection();
                ca.ImportFromPemFile(_getFilePath(ClientCaPemFileName));
                X509Store wmCaStore = new(WmCaStoreName, StoreLocation.CurrentUser);
                wmCaStore.Open(OpenFlags.ReadWrite);
                wmCaStore.RemoveRange(wmCaStore.Certificates);
                wmCaStore.AddRange(ca);
                wmCaStore.Close();


                // Plain HTTP Tests, show that CA settings have no impact on plain connections
                _testData.Add(_createTest("Accepted: Server Plain - Client Default", _wmHttp, _clDefault, true));
                _testData.Add(_createTest("Accepted: Server Plain - Client CA Store", _wmHttp, _clCaStore, true));
                _testData.Add(_createTest("Accepted: Server Plain - Client CA Location", _wmHttp, _clCaLocation, true));
                _testData.Add(_createTest(
                    "Accepted: Server Plain - Client CA Location - Verification Host - Verification Certificate",
                    _wmHttp, _clCaLocationAndValidation, true));
                _testData.Add(_createTest("Accepted: Server Plain - Client CA Location - Verification Off", _wmHttp,
                    _clCaLocationAndNoValidation, true));


                // TLS Tests
                _testData.Add(_createTest("Rejected: Server TLS - Client Default", _wmHttps, _clDefault, false));
                _testData.Add(_createTest("Accepted: Server TLS - Client CA Location", _wmHttps, _clCaLocation, true));
                _testData.Add(_createTest(
                    "Accepted: Server TLS - Client No CA - Verification off",
                    _wmHttps, _clNoCaLocationAndNoValidation, true));
                _testData.Add(_createTest(
                    "Rejected: Server TLS - Client No CA - Verification Host - Verification Certificate",
                    _wmHttps, _clNoCaLocationAndValidation, false));
                _testData.Add(_createTest("Accepted: Server TLS - Client CA Store", _wmHttps, _clCaStore, true));
            }

            static WireMockServerSettings _mockServerSettings(string fileName = null)
            {
                var settings = new WireMockServerSettings();
                if (fileName != null)
                {
                    settings.UseSSL = true;
                    settings.CertificateSettings = new WireMockCertificateSettings()
                    {
                        X509CertificateFilePath = _getFilePath(fileName),
                        X509CertificatePassword = "notsecret"
                    };
                }

                return settings;
            }

            // Contains the test data set
            private readonly List<TestFixtureData> _testData = new();

            public IEnumerator GetEnumerator()
            {
                return _testData.GetEnumerator();
            }

            static TestFixtureData _createTest(string name, WireMockServerSettings mockServerSettings,
                AxualSchemaRegistryConfig schemaRegistryConfig, bool expectSuccess)
            {
                TestFixtureData data = new TestFixtureData(mockServerSettings, schemaRegistryConfig, expectSuccess);
                data.TestName = name;

                return data;
            }
        }
    }
}