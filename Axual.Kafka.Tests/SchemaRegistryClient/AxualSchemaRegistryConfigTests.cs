using System;
using Axual.SchemaRegistry.Serdes.Avro.SchemaRegistryClient;
using Confluent.SchemaRegistry;
using NUnit.Framework;

namespace Axual.Kafka.Tests.SchemaRegistryClient
{
    public class AxualSchemaRegistryConfigTests
    {
        [Test]
        public void AxualSchemaRegistryConfig_MaxCachedSchemas_GetCorrectValue()
        {
            var axualSchemaRegistryConfig = new AxualSchemaRegistryConfig {MaxCachedSchemas = Int32.MaxValue};

            Assert.AreEqual(Int32.MaxValue, axualSchemaRegistryConfig.MaxCachedSchemas);
        }
        
        [Test]
        public void AxualSchemaRegistryConfig_RequestTimeoutMs_GetCorrectValue()
        {
            var axualSchemaRegistryConfig = new AxualSchemaRegistryConfig { RequestTimeoutMs = Int32.MaxValue};
            
            Assert.AreEqual(Int32.MaxValue, axualSchemaRegistryConfig.RequestTimeoutMs);
        }

        [Test]
        public void AxualSchemaRegistryConfig_SslCertificateLocation_GetCorrectValue()
        {
            var axualSchemaRegistryConfig = new AxualSchemaRegistryConfig
                {SslCertificateLocation = nameof(AxualSchemaRegistryConfig.AxualPropertyNames.SslCertificateLocation)};
            Assert.AreEqual(nameof(AxualSchemaRegistryConfig.AxualPropertyNames.SslCertificateLocation),
                axualSchemaRegistryConfig.SslCertificateLocation);
        }

        [Test]
        public void AxualSchemaRegistryConfig_SslCertificatePem_GetCorrectValue()
        {
            var axualSchemaRegistryConfig = new AxualSchemaRegistryConfig
                {SslCertificatePem = nameof(AxualSchemaRegistryConfig.AxualPropertyNames.SslCertificatePem)};
            Assert.AreEqual(nameof(AxualSchemaRegistryConfig.AxualPropertyNames.SslCertificatePem),
                axualSchemaRegistryConfig.SslCertificatePem);
        }
        
        [Test]
        public void AxualSchemaRegistryConfig_SslKeyLocation_GetCorrectValue()
        {
            var axualSchemaRegistryConfig = new AxualSchemaRegistryConfig
                {SslKeyLocation = nameof(AxualSchemaRegistryConfig.AxualPropertyNames.SslKeyLocation)};
            Assert.AreEqual(nameof(AxualSchemaRegistryConfig.AxualPropertyNames.SslKeyLocation),
                axualSchemaRegistryConfig.SslKeyLocation);
        }
        
        [Test]
        public void AxualSchemaRegistryConfig_SslKeyPassword_GetCorrectValue()
        {
            var axualSchemaRegistryConfig = new AxualSchemaRegistryConfig
                {SslKeyPassword = nameof(AxualSchemaRegistryConfig.AxualPropertyNames.SslKeyPassword)};
            Assert.AreEqual(nameof(AxualSchemaRegistryConfig.AxualPropertyNames.SslKeyPassword),
                axualSchemaRegistryConfig.SslKeyPassword);
        }
        
        [Test]
        public void AxualSchemaRegistryConfig_SslKeyPem_GetCorrectValue()
        {
            var axualSchemaRegistryConfig = new AxualSchemaRegistryConfig
                {SslKeyPem = nameof(AxualSchemaRegistryConfig.AxualPropertyNames.SslKeyPem)};
            Assert.AreEqual(nameof(AxualSchemaRegistryConfig.AxualPropertyNames.SslKeyPem),
                axualSchemaRegistryConfig.SslKeyPem);
        }

        [Test]
        public void AxualSchemaRegistryConfig_EnableSslCertificateVerification_GetCorrectValue()
        {
            var axualSchemaRegistryConfig = new AxualSchemaRegistryConfig
                {EnableSslCertificateVerification = false};
            Assert.AreEqual(false, axualSchemaRegistryConfig.EnableSslCertificateVerification);
        }
        
        [Test]
        public void AxualSchemaRegistryConfig_EnableSslCertificateVerification_GetDefault()
        {
            var axualSchemaRegistryConfig = new AxualSchemaRegistryConfig();
            Assert.AreEqual(true, axualSchemaRegistryConfig.EnableSslCertificateVerification);
        }

        [Test]
        public void AxualSchemaRegistryConfig_KeySubjectNameStrategy_GetCorrectValue()
        {
            var axualSchemaRegistryConfig = new AxualSchemaRegistryConfig
                {KeySubjectNameStrategy = SubjectNameStrategy.Topic};

            Assert.AreEqual(SubjectNameStrategy.Topic, axualSchemaRegistryConfig.KeySubjectNameStrategy);

            axualSchemaRegistryConfig.KeySubjectNameStrategy = SubjectNameStrategy.Record;
            Assert.AreEqual(SubjectNameStrategy.Record, axualSchemaRegistryConfig.KeySubjectNameStrategy);

            axualSchemaRegistryConfig.KeySubjectNameStrategy = SubjectNameStrategy.TopicRecord;
            Assert.AreEqual(SubjectNameStrategy.TopicRecord, axualSchemaRegistryConfig.KeySubjectNameStrategy);
        }
        
        [Test]
        public void AxualSchemaRegistryConfig_ValueSubjectNameStrategy_GetCorrectValue()
        {
            var axualSchemaRegistryConfig = new AxualSchemaRegistryConfig
                {ValueSubjectNameStrategy = SubjectNameStrategy.Topic};

            Assert.AreEqual(SubjectNameStrategy.Topic, axualSchemaRegistryConfig.KeySubjectNameStrategy);

            axualSchemaRegistryConfig.KeySubjectNameStrategy = SubjectNameStrategy.Record;
            Assert.AreEqual(SubjectNameStrategy.Record, axualSchemaRegistryConfig.KeySubjectNameStrategy);

            axualSchemaRegistryConfig.KeySubjectNameStrategy = SubjectNameStrategy.TopicRecord;
            Assert.AreEqual(SubjectNameStrategy.TopicRecord, axualSchemaRegistryConfig.KeySubjectNameStrategy);
        }
    }
}