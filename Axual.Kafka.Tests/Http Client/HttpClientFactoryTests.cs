using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography.X509Certificates;
using Axual.Kafka.Proxy.HttpClient;
using NUnit.Framework;
using WireMock.RequestBuilders;
using WireMock.ResponseBuilders;
using WireMock.Server;
using WireMock.Settings;

namespace Axual.Kafka.Tests.Http_Client
{
    [TestFixtureSource(typeof(TestFixtures))]
    internal class HttpClientFactoryTests
    {
        private const string EndpointPath = "/example";
        private const string OkResultBody = "Success!";

        private readonly WireMockServer _wireMock;
        private readonly HttpClientConfig _clientConfig;
        private readonly bool _validCombination;

        // TestFixture will inject the correct configurations for these tests
        public HttpClientFactoryTests(WireMockServerSettings serverSettings, HttpClientConfig clientConfig,
            bool validCombination)
        {
            _wireMock = WireMockServer.Start(serverSettings ?? new WireMockServerSettings());
            _clientConfig = clientConfig;
            _validCombination = validCombination;
            _wireMock.Given(
                    Request.Create()
                        .WithPath(EndpointPath)
                        .UsingGet())
                .RespondWith(
                    Response.Create()
                        .WithSuccess()
                        .WithHeader("Content-Type", "text/plain")
                        .WithBody(OkResultBody)
                );
        }

        [OneTimeTearDown]
        public void TearDownFixture()
        {
            _wireMock.Stop();
        }

        // Test endpoints are OK, but if server/client settings don't match exceptions are thrown
        [Test]
        public void HttpConnection_ValidEndpoint()
        {
            var client = new HttpClientFactory().CreateClient(_clientConfig);
            var request = new HttpRequestMessage(HttpMethod.Get, _wireMock.Url + EndpointPath);
            if (_validCombination)
            {
                var response = client.Send(request);
                Assert.NotNull(response);
                Assert.AreEqual(HttpStatusCode.OK, response.StatusCode);
                Assert.AreEqual(OkResultBody, response.Content.ReadAsStringAsync().Result);
            }
            else
            {
                Assert.Throws(typeof(HttpRequestException),
                    () => client.Send(request, HttpCompletionOption.ResponseContentRead));
            }
        }

        // No matter what the combination of the server/client settings, an invalid endpoint should throw an Exception
        [Test]
        public void HttpConnection_InvalidEndpoint()
        {
            var client = new HttpClientFactory().CreateClient(_clientConfig);
            var request = new HttpRequestMessage(HttpMethod.Get, "http://unreachable:1/" + EndpointPath);
            Assert.Throws(typeof(HttpRequestException), () => client.Send(request));
        }

        private class TestFixtures : IEnumerable
        {
            private const String WmCaStoreWithRootName = "WiremockWithRoot";
            private const String WmCaStoreWithIntermediateName = "WiremockWithIntermediate";

            private readonly WireMockServerSettings _wmHttpOnly = _mockServerSettings();

            private readonly WireMockServerSettings
                _wmHttpsUnchainedCert = _mockServerSettings("server-with-chain.p12");

            private readonly HttpClientConfig _clDefault = new();

            private readonly HttpClientConfig _clCaWithRootStore = new()
            {
                SslCaCertificateStores = WmCaStoreWithRootName
            };

            private readonly HttpClientConfig _clCaWithRootStoreAndNoValidation = new()
            {
                SslCaCertificateStores = WmCaStoreWithRootName,
                EnableSslCertificateVerification = false
            };

            private readonly HttpClientConfig _clCaWithIntermediateStore = new()
            {
                SslCaCertificateStores = WmCaStoreWithIntermediateName
            };

            private readonly HttpClientConfig _clCaWithIntermediateLocation = new()
            {
                SslCaLocation = _getFilePath("ca-intermediate.pem")
            };

            private readonly HttpClientConfig _clCaWithRootLocation = new()
            {
                SslCaLocation = _getFilePath("ca-root.pem")
            };


            // Contains the test data set
            private readonly List<TestFixtureData> _testData = new();

            public TestFixtures()
            {
                // Prepare stores
                X509Certificate2Collection caRoot = new X509Certificate2Collection();
                caRoot.ImportFromPemFile(_getFilePath("ca-root.pem"));
                X509Store wmCaStoreWithRoot = new(WmCaStoreWithRootName, StoreLocation.CurrentUser);
                wmCaStoreWithRoot.Open(OpenFlags.ReadWrite);
                wmCaStoreWithRoot.RemoveRange(wmCaStoreWithRoot.Certificates);
                wmCaStoreWithRoot.AddRange(caRoot);
                wmCaStoreWithRoot.Close();

                X509Certificate2Collection caInter = new X509Certificate2Collection();
                caInter.ImportFromPemFile(_getFilePath("ca-intermediate.pem"));
                X509Store wmCaStoreWithIntermediate = new(WmCaStoreWithIntermediateName, StoreLocation.CurrentUser);
                wmCaStoreWithIntermediate.Open(OpenFlags.ReadWrite);
                wmCaStoreWithIntermediate.RemoveRange(wmCaStoreWithIntermediate.Certificates);
                wmCaStoreWithIntermediate.AddRange(caInter);
                wmCaStoreWithIntermediate.Close();

                // Setup stores with the CA

                // HTTP Wiremock, SSL CA settings should have no impact
                _testData.Add(_createTest("HTTP Server with default client", _wmHttpOnly, _clDefault, true));
                _testData.Add(_createTest("HTTP Server with CA Root Store client", _wmHttpOnly, _clCaWithRootStore,
                    true));
                _testData.Add(_createTest("HTTP Server with CA Root Location client", _wmHttpOnly,
                    _clCaWithRootLocation,
                    true));
                _testData.Add(_createTest("HTTP Server with CA Intermediate Store client",
                    _wmHttpOnly, _clCaWithIntermediateStore, true));
                _testData.Add(_createTest("HTTP Server with CA Intermediate Location client", _wmHttpOnly,
                    _clCaWithIntermediateLocation, true));

                // // HTTPS Wiremock has no signing CAs included, all tests should pass, except default and the no intermediate stores/pem
                _testData.Add(_createTest("HTTPS Server without chain with default client", _wmHttpsUnchainedCert,
                    _clDefault, false));
                _testData.Add(_createTest("HTTPS Server without chain with CA Root Store client",
                    _wmHttpsUnchainedCert, _clCaWithRootStore, false));
                _testData.Add(_createTest("HTTPS Server without chain with CA Root Store client ignoring unknown CAs",
                    _wmHttpsUnchainedCert, _clCaWithRootStoreAndNoValidation, true));
                _testData.Add(_createTest("HTTPS Server without chain with CA Root Location client",
                    _wmHttpsUnchainedCert, _clCaWithRootLocation,
                    false));
                _testData.Add(_createTest("HTTPS Server without chain with CA Intermediate Store client",
                    _wmHttpsUnchainedCert, _clCaWithIntermediateStore, true));
                _testData.Add(_createTest("HTTPS Server without chain with CA Intermediate Location client",
                    _wmHttpsUnchainedCert, _clCaWithIntermediateLocation, true));
            }

            static string _getFilePath(string file)
            {
                return UtilsHelper.GetFileFromResourcesPath("httpClient/" + file);
            }

            static WireMockServerSettings _mockServerSettings(string fileName = null)
            {
                var settings = new WireMockServerSettings();
                if (fileName != null)
                {
                    settings.UseSSL = true;
                    settings.CertificateSettings = new WireMockCertificateSettings()
                    {
                        X509CertificateFilePath = _getFilePath(fileName),
                        X509CertificatePassword = "notsecret"
                    };
                }

                return settings;
            }


            public IEnumerator GetEnumerator()
            {
                return _testData.GetEnumerator();
            }

            static TestFixtureData _createTest(string name, WireMockServerSettings mockServerSettings,
                HttpClientConfig httpClientConfig, bool expectSuccess)
            {
                TestFixtureData data = new TestFixtureData(mockServerSettings, httpClientConfig, expectSuccess);
                data.TestName = name;

                return data;
            }
        }
    }
}