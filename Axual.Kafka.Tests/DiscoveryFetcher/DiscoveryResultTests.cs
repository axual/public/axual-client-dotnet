using System.Collections.Generic;
using System.Reflection;
using Axual.Kafka.Proxy.DiscoveryFetcher;
using Axual.Kafka.Proxy.Proxies;
using NUnit.Framework;

namespace Axual.Kafka.Tests.DiscoveryFetcher
{
    [TestFixture]
    internal class DiscoveryResultTests
    {
        [TestCase(Constants.ConfigurationKeys.Axual.Instance)]
        [TestCase(Constants.ConfigurationKeys.Axual.Cluster)]
        [TestCase(Constants.ConfigurationKeys.Switching.Ttl)]
        [TestCase(Constants.ConfigurationKeys.Kafka.BootstrapServers)]
        [TestCase(Constants.ConfigurationKeys.Kafka.SchemaRegistryUrl)]
        [TestCase(Constants.ConfigurationKeys.Switching.DistributorTimeout)]
        [TestCase(Constants.ConfigurationKeys.Switching.DistributorDistance)]
        [TestCase(Constants.ConfigurationKeys.Headers.EnableValueHeaders)]
        [TestCase(Constants.ConfigurationKeys.Resolver.GroupIdResolver)]
        [TestCase(Constants.ConfigurationKeys.Resolver.GroupIdPattern)]
        [TestCase(Constants.ConfigurationKeys.Resolver.TopicResolver)]
        [TestCase(Constants.ConfigurationKeys.Resolver.TopicPattern)]
        [TestCase(Constants.ConfigurationKeys.Resolver.TransactionalIdResolver)]
        [TestCase(Constants.ConfigurationKeys.Resolver.TransactionalIdPattern)]
        public void DiscoveryResult_SetKeyWithoutValue_GetNull(string key)
        {
            var emptyDiscoveryResult = new DiscoveryResult();
            Assert.AreEqual(null, emptyDiscoveryResult.Get<string>(key));
        }

        [TestCase(Constants.ConfigurationKeys.Axual.Instance, "InstanceName")]
        [TestCase(Constants.ConfigurationKeys.Axual.Cluster, "ClusterName")]
        [TestCase(Constants.ConfigurationKeys.Switching.Ttl, "10")]
        [TestCase(Constants.ConfigurationKeys.Kafka.BootstrapServers, "boot.strap.server")]
        [TestCase(Constants.ConfigurationKeys.Kafka.SchemaRegistryUrl, "schema.registry.url")]
        [TestCase(Constants.ConfigurationKeys.Switching.DistributorTimeout, "10000")]
        [TestCase(Constants.ConfigurationKeys.Switching.DistributorDistance, "1")]
        [TestCase(Constants.ConfigurationKeys.Headers.EnableValueHeaders, "true")]
        [TestCase(Constants.ConfigurationKeys.Resolver.GroupIdResolver,
            "io.axual.common.resolver.GroupPatternResolver")]
        [TestCase(Constants.ConfigurationKeys.Resolver.GroupIdPattern, "{tenant}-{instance}-{environment}-{group}")]
        [TestCase(Constants.ConfigurationKeys.Resolver.TopicResolver, "io.axual.common.resolver.TopicPatternResolver")]
        [TestCase(Constants.ConfigurationKeys.Resolver.TopicPattern, "{tenant}-{instance}-{environment}-{topic}")]
        [TestCase(Constants.ConfigurationKeys.Resolver.TransactionalIdResolver,
            "io.axual.common.resolver.TransactionalIdResolver")]
        [TestCase(Constants.ConfigurationKeys.Resolver.TransactionalIdPattern,
            "{tenant}-{instance}-{environment}-{app.id}{transactional.id}")]
        public void DiscoveryResult_SetKeyValuePair_GetSetValue(string key, string value)
        {
            var dictionary = new Dictionary<string, string>
            {
                {key, value}
            };

            var discoveryResult = new DiscoveryResult(dictionary);
            Assert.AreEqual(value, discoveryResult.Get<string>(key));
        }

        [Test]
        public void DiscoveryResult_EmptyDiscoveryResult_GetNull()
        {
            var emptyDiscoveryResult = new DiscoveryResult();

            foreach (var property in typeof(DiscoveryResult).GetRuntimeProperties())
                try
                {
                    property.GetValue(emptyDiscoveryResult);
                }
                catch
                {
                    Assert.Fail($"Failed to get value from {property.Name} property");
                }
        }
    }
}