using System;
using System.Threading;
using System.Threading.Tasks;
using Axual.Kafka.Proxy.DiscoveryFetcher;
using NSubstitute;
using NUnit.Framework;

namespace Axual.Kafka.Tests.DiscoveryFetcher;

[TestFixture]
public class ClientRegistryTests
{
    private const string ApplicationId1 = "example.client.id.1";
    private const string ApplicationId2 = "example.client.id.2";

    private IFetcher _mockFetcher;
    private Func<IFetcher> _fetcherFactory;
    private Func<CancellationTokenSource> _mockTokenSourceFactory;

    [SetUp]
    public void SetUp()
    {
        _mockFetcher = Substitute.For<IFetcher>();
        _fetcherFactory = () => _mockFetcher;
        _mockTokenSourceFactory = Substitute.For<Func<CancellationTokenSource>>();
    }

    [Test]
    public void RegisterFirstClient()
    {
        _mockFetcher.IsRunning.Returns(true);
        _mockFetcher.CurrentDiscoveryResult.Returns((DiscoveryResult)null);
        const int expectedRegistrations = 1;
        EventHandler<DiscoveryResult> handler = Substitute.For<EventHandler<DiscoveryResult>>();
        var registry = new ClientRegistry(_mockTokenSourceFactory);
        var registrationsFound =
            registry.RegisterClientApplication(ApplicationId1, _fetcherFactory, handler);

        Assert.AreEqual(expectedRegistrations, registrationsFound);
        _mockFetcher.Received().OnNewFetchResult += handler;
    }

    [Test]
    public void RegisterSecondClient()
    {
        _mockFetcher.IsRunning.Returns(true);
        _mockFetcher.CurrentDiscoveryResult.Returns((DiscoveryResult)null);
        const int expectedRegistrations = 2;
        EventHandler<DiscoveryResult> handler1 = Substitute.For<EventHandler<DiscoveryResult>>();
        EventHandler<DiscoveryResult> handler2 = Substitute.For<EventHandler<DiscoveryResult>>();
        var registry = new ClientRegistry(_mockTokenSourceFactory);
        registry.RegisterClientApplication(ApplicationId1, _fetcherFactory, handler1);
        var registrationsFound =
            registry.RegisterClientApplication(ApplicationId1, _fetcherFactory, handler2);
        ClientRegistry.Registry.RegisterClientApplication(ApplicationId1, _fetcherFactory, handler1);

        Assert.AreEqual(expectedRegistrations, registrationsFound);
        _mockFetcher.Received().OnNewFetchResult += handler1;
        _mockFetcher.Received().OnNewFetchResult += handler2;
    }

    [Test]
    public void RegisterFirstClientWithStart()
    {
        var mockTokenSource = Substitute.For<CancellationTokenSource>();
        _mockTokenSourceFactory.Invoke().Returns(mockTokenSource);
        
        _mockFetcher.IsRunning.Returns(false, true);
        const int expectedRegistrations = 1;
        EventHandler<DiscoveryResult> handler = Substitute.For<EventHandler<DiscoveryResult>>();
        var registry = new ClientRegistry(_mockTokenSourceFactory);
        var registrationsFound =
            registry.RegisterClientApplication(ApplicationId1, _fetcherFactory, handler);

        Assert.AreEqual(expectedRegistrations, registrationsFound);
        _mockFetcher.Received().OnNewFetchResult += handler;
        _mockTokenSourceFactory.Received(1).Invoke();
        _mockFetcher.StartAsync(Arg.Any<CancellationToken>()).Returns(Task.CompletedTask);
        _mockFetcher.Received(1).StartAsync(Arg.Any<CancellationToken>());
    }

    [Test]
    public void RegisterSecondClientWithDiscoveryResultCallback()
    {
        var discoveryResult = new DiscoveryResult();
        _mockFetcher.IsRunning.Returns(true);
        _mockFetcher.CurrentDiscoveryResult.Returns(null, discoveryResult);
        const int expectedRegistrations = 2;
        EventHandler<DiscoveryResult> handler1 = Substitute.For<EventHandler<DiscoveryResult>>();
        EventHandler<DiscoveryResult> handler2 = Substitute.For<EventHandler<DiscoveryResult>>();
        var registry = new ClientRegistry(_mockTokenSourceFactory);
        registry.RegisterClientApplication(ApplicationId1, _fetcherFactory, handler1);
        var registrationsFound =
            registry.RegisterClientApplication(ApplicationId1, _fetcherFactory, handler2);

        Assert.AreEqual(expectedRegistrations, registrationsFound);
        _mockFetcher.Received().OnNewFetchResult += handler1;
        _mockFetcher.Received().OnNewFetchResult += handler2;
        _mockFetcher.StartAsync(Arg.Any<CancellationToken>()).Returns(Task.CompletedTask);
        _mockFetcher.Received(0).StartAsync(Arg.Any<CancellationToken>());

        handler1.Received(0).Invoke(Arg.Any<object>(), Arg.Any<DiscoveryResult>());
        handler2.Received(1).Invoke(Arg.Any<object>(), Arg.Is(discoveryResult));
    }

    [Test]
    public void UnregisterFinalClient()
    {
        var mockTokenSource = Substitute.For<CancellationTokenSource>();
        _mockTokenSourceFactory.Invoke().Returns(mockTokenSource);
        
        const int expectedRegistrationsAfterRegister = 1;
        const int expectedRegistrationsAfterRemove = 0;
        EventHandler<DiscoveryResult> handler1 = Substitute.For<EventHandler<DiscoveryResult>>();
        _mockFetcher.IsRunning.Returns(false, true);
        _mockFetcher.CurrentDiscoveryResult.Returns((DiscoveryResult)null);
        var registry = new ClientRegistry(_mockTokenSourceFactory);
        var afterRegister = registry.RegisterClientApplication(ApplicationId1, _fetcherFactory, handler1);
        
        Assert.AreEqual(expectedRegistrationsAfterRegister,afterRegister);
        _mockFetcher.Received().OnNewFetchResult += handler1;
        _mockTokenSourceFactory.Received(1).Invoke();
        
        var afterRemove = registry.UnregisterClientRegistration(ApplicationId1, handler1);
        _mockFetcher.Received().OnNewFetchResult -= handler1;
        mockTokenSource.Received(1).Cancel();
        Assert.AreEqual(expectedRegistrationsAfterRemove,afterRemove);
    }
    
    [Test]
    public void UnregisterFinalClientMultipleTimes()
    {
        const int expectedRegistrationsAfterRegister = 1;
        const int expectedRegistrationsAfterRemove = 0;
        const int expectedUnknownRegistrationCount = -1;
        EventHandler<DiscoveryResult> handler1 = Substitute.For<EventHandler<DiscoveryResult>>();
        _mockFetcher.IsRunning.Returns(true);
        _mockFetcher.CurrentDiscoveryResult.Returns((DiscoveryResult)null);
        var registry = new ClientRegistry(_mockTokenSourceFactory);
        var afterRegister = registry.RegisterClientApplication(ApplicationId1, _fetcherFactory, handler1);
        
        Assert.AreEqual(expectedRegistrationsAfterRegister,afterRegister);
        _mockFetcher.Received().OnNewFetchResult += handler1;
        
        var afterRemove = registry.UnregisterClientRegistration(ApplicationId1, handler1);
        _mockFetcher.Received().OnNewFetchResult -= handler1;
        Assert.AreEqual(expectedRegistrationsAfterRemove,afterRemove);
        
        var afterRemoveTooMany = registry.UnregisterClientRegistration(ApplicationId1, handler1);
        Assert.AreEqual(expectedUnknownRegistrationCount,afterRemoveTooMany);
    }

    
    [Test]
    public void UnregisterNonRegisteredClient()
    {
        const int expectedUnknownRegistrationCount = -1;
        EventHandler<DiscoveryResult> handler1 = Substitute.For<EventHandler<DiscoveryResult>>();
        _mockFetcher.IsRunning.Returns(true);
        _mockFetcher.CurrentDiscoveryResult.Returns((DiscoveryResult)null);
        var registry = new ClientRegistry(_mockTokenSourceFactory);
        
        var afterRemove = registry.UnregisterClientRegistration(ApplicationId1, handler1);
        _mockFetcher.Received(0).OnNewFetchResult -= handler1;
        Assert.AreEqual(expectedUnknownRegistrationCount,afterRemove);
    }

    [Test]
    public void DisposeWithMultipleClients()
    {
        var mockTokenSource1 = Substitute.For<CancellationTokenSource>();
        var mockTokenSource2 = Substitute.For<CancellationTokenSource>();
        _mockTokenSourceFactory.Invoke().Returns(mockTokenSource1,mockTokenSource2);

        _mockFetcher.IsRunning.Returns(false,true);
        _mockFetcher.CurrentDiscoveryResult.Returns((DiscoveryResult)null);

        var mockFetcher2 = Substitute.For<IFetcher>();
        mockFetcher2.IsRunning.Returns(false,true);
        mockFetcher2.CurrentDiscoveryResult.Returns((DiscoveryResult)null);

        var fetcherFactory2 = () => mockFetcher2;

        const int expectedRegistrationCounts = 1;
        EventHandler<DiscoveryResult> handler1 = Substitute.For<EventHandler<DiscoveryResult>>();
        EventHandler<DiscoveryResult> handler2 = Substitute.For<EventHandler<DiscoveryResult>>();
        using (var registry = new ClientRegistry(_mockTokenSourceFactory))
        {
            var afterRegistration1 = registry.RegisterClientApplication(ApplicationId1, _fetcherFactory, handler1);
            var afterRegistration2 = registry.RegisterClientApplication(ApplicationId2, fetcherFactory2, handler2);
            Assert.AreEqual(expectedRegistrationCounts, afterRegistration1);
            Assert.AreEqual(expectedRegistrationCounts, afterRegistration2);
        }
        // Make sure that registration happened properly and start fetcher was called
        _mockTokenSourceFactory.Received(2).Invoke();
        
        _mockFetcher.Received(1).StartAsync(Arg.Any<CancellationToken>());
        _mockFetcher.Received(1).OnNewFetchResult += handler1;
        _mockFetcher.Received(0).OnNewFetchResult += handler2;

        mockFetcher2.Received(1).StartAsync(Arg.Any<CancellationToken>());
        mockFetcher2.Received(0).OnNewFetchResult += handler1;
        mockFetcher2.Received(1).OnNewFetchResult += handler2;
        
        // Make sure that registrations where canceled during dispose
        mockTokenSource1.Received(1).Cancel();
        mockTokenSource2.Received(1).Cancel();
    }

}