using System;
using System.Threading;
using Axual.Kafka.Proxy.DiscoveryFetcher;
using NSubstitute;
using NSubstitute.ExceptionExtensions;
using NUnit.Framework;

namespace Axual.Kafka.Tests.DiscoveryFetcher
{
    [TestFixture]
    internal class DiscoveryFetcherTests
    {
        private IDiscoveryRestService _mockDiscoveryRestService;
        private IFetcher _discoveryFetcher;

        [SetUp]
        public void SetUp()
        {
            _mockDiscoveryRestService = Substitute.For<IDiscoveryRestService>();
            _mockDiscoveryRestService.GetLatestDiscoveryResult(Arg.Any<string>(), Arg.Any<CancellationToken>())
                .Returns(new DiscoveryResult());
            _discoveryFetcher = new Proxy.DiscoveryFetcher.DiscoveryFetcher(_mockDiscoveryRestService);
        }

        [Test]
        public void DiscoveryFetcher_StartTwice_ThrowInvalidOperationException()
        {
            // Act
            // 1st call
            _discoveryFetcher.StartAsync(new CancellationToken());
            // Assert - 2nd calls throws `InvalidOperationException`
            Assert.Throws<InvalidOperationException>(_discoveryFetcher.StartAsync(new CancellationToken())
                .RunSynchronously);
        }

        [Test]
        public void DiscoveryFetcher_StartStopStart_ManageToStartAgain()
        {
            _discoveryFetcher.StartAsync(new CancellationToken());
            _discoveryFetcher.StopAsync(new CancellationToken()).Wait();
            _discoveryFetcher.StartAsync(new CancellationToken());
        }
        
        [Test]
        public void DiscoveryFetcher_StopWithoutStart_Ignore()
        {
            _discoveryFetcher.StopAsync(new CancellationToken()).Wait();
        }

        [Test]
        [Timeout(2000)]
        public void DiscoveryFetcher_FirstFetchCall_CallingWithoutClusterParameter()
        {
            var isCalled = new AutoResetEvent(false);
            var ctx = new CancellationToken();

            _discoveryFetcher.OnNewFetchResult += (_, _) =>
            {
                // Assert - called DA without Cluster parameter
                _mockDiscoveryRestService.Received().GetLatestDiscoveryResult(null, Arg.Any<CancellationToken>());
                isCalled.Set();
            };

            // Act
            _discoveryFetcher.StartAsync(ctx);
            isCalled.WaitOne();
            Assert.Pass();
        }

        [Test]
        [Timeout(2000)]
        public void DiscoveryFetcher_FetchCallThrowException_OnErrorRaise()
        {
            var isCalled = new AutoResetEvent(false);
            var ctx = new CancellationTokenSource();
            // Arrange
            var exception = new Exception("DiscoveryFetcher_FirstFetchCallThrowException_ThrowException");
            Exception callbackException = null;
            _mockDiscoveryRestService.GetLatestDiscoveryResult(null, Arg.Any<CancellationToken>()).Throws(exception);

            _discoveryFetcher.OnError += (_, exception1) =>
            {
                callbackException = exception1;
                isCalled.Set();
            };

            // Act + Assert
            _discoveryFetcher.StartAsync(ctx.Token);
            isCalled.WaitOne();
            Assert.IsTrue(callbackException.Message.Contains(exception.Message));
        }

        [Test]
        [Timeout(2000)]
        public void DiscoveryFetcher_ThrowExceptionOnce_GetResult()
        {
            var isExceptionCalled = new AutoResetEvent(false);
            var isFetchCalled = new AutoResetEvent(false);
            // Arrange
            _mockDiscoveryRestService.GetLatestDiscoveryResult(null, Arg.Any<CancellationToken>())
                .Throws(new Exception());

            _discoveryFetcher.OnError += (_, _) =>
            {
                _mockDiscoveryRestService.GetLatestDiscoveryResult(null, Arg.Any<CancellationToken>())
                    .Returns(new DiscoveryResult());
                isExceptionCalled.Set();
            };

            _discoveryFetcher.OnNewFetchResult += (_, _) => { isFetchCalled.Set(); };

            // Act 
            _discoveryFetcher.StartAsync(new CancellationToken());
            isExceptionCalled.WaitOne();
            isFetchCalled.WaitOne();

            // Assert - Managed to get DiscoveryResult from Server after Exception
            Assert.Pass();
        }
    }
}