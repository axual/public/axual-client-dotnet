//
// Copyright 2020 Axual B.V. 2020
//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Axual.Kafka.Proxy;
using Axual.Kafka.Proxy.DiscoveryFetcher;
using Axual.Kafka.Proxy.Exceptions;
using Axual.Kafka.Proxy.HttpClient;
using Axual.Kafka.Proxy.Proxies.Axual;
using Moq;
using Moq.Protected;
using Newtonsoft.Json;
using NUnit.Framework;
using IHttpClientFactory = Axual.Kafka.Proxy.HttpClient.IHttpClientFactory;

namespace Axual.Kafka.Tests.DiscoveryFetcher
{
    internal class DiscoveryRestServiceTests
    {
        private const string DiscoveryApiEndPoint = "localhost";
        private const string LastCluster = "ClusterA";
        private const string Tenant = "tenantName";
        private const string Environment = "environmentName";
        private const string Instance = "instanceName";
        private const string System = "systemA";
        private const string ApplicationId = "DiscoveryFetcherTests";
        private const string Cluster = "ClusterB";
        private const string BootStrap = "bootstrap.public.v3.axual.io:29293";
        private const string SchemaRegistry = "bootstrap.public.v3.axual.io:29293";
        private const string RestProxyUrl = "http://proxy.v3.axual.io:8080";
        private const string DistributorTimeout = "60000";
        private const string DistributorDistance = "1";
        private const string Ttl = "60001";
        private const string EnableValueHeaders = "true";
        private const string GroupIdResolver = "io.axual.common.resolver.GroupPatternResolver";
        private const string GroupIdPattern = "{tenant}-{environment}-{group}";
        private const string TopicResolver = "io.axual.common.resolver.TopicPatternResolver";
        private const string TopicPattern = "{tenant}-{environment}-{topic}";
        private const string AclPrincipalBuilder = "acl.principal.builder";
        private const string TransactionalIdPatternResolver = "io.axual.common.resolver.TransactionalIdPatternResolver";
        private const string TransactionalIdPattern = "{tenant}-{environment}-{app.id}-{transactional.id}";

        private AxualProducerConfig _axualConfig;

        private readonly IDictionary<string, string> _discoveryKeyValue = new Dictionary<string, string>
        {
            { "tenant", Tenant },
            { "environment", Environment },
            { "instance", Instance },
            { "system", System },
            { "cluster", Cluster },
            { "bootstrap.servers", BootStrap },
            { "schema.registry.url", SchemaRegistry },
            { "rest.proxy.url", RestProxyUrl },
            { "distributor.timeout", DistributorTimeout },
            { "distributor.distance", DistributorDistance },
            { "ttl", Ttl },
            { "enable.value.headers", EnableValueHeaders },
            { "group.id.resolver", GroupIdResolver },
            { "group.id.pattern", GroupIdPattern },
            { "topic.resolver", TopicResolver },
            { "topic.pattern", TopicPattern },
            { "acl.principal.builder", AclPrincipalBuilder },
            { "transactional.id.resolver", TransactionalIdPatternResolver },
            { "transactional.id.pattern", TransactionalIdPattern }
        };

        private string _discoveryResponseOk;
        private IDiscoveryRestService _discoveryRestService;
        private HttpClient _httpClient;
        private Mock<IHttpClientFactory> _mockHttpClientFactory;

        [SetUp]
        public void SetUp()
        {
            _discoveryResponseOk = JsonConvert.SerializeObject(_discoveryKeyValue, Formatting.Indented);
            _axualConfig = new AxualProducerConfig
            {
                EndPoint = new UriBuilder("http", DiscoveryApiEndPoint).Uri,
                ApplicationId = ApplicationId,
                ApplicationVersion = "2.0"
            };
        }

        [Test]
        public void GetLatestDiscoveryResult_MissingEndPoint_ThrowsArgumentException()
        {
            // ARRANGE
            _axualConfig.EndPoint = null;
            _mockHttpClientFactory = new Mock<IHttpClientFactory>();

            // ACT + ASSERT
            Assert.Throws<RequireConfigurationException>(() =>
                new DiscoveryRestService(_mockHttpClientFactory.Object, _axualConfig.ToDictionary()));
        }

        [Test]
        public async Task GetLatestDiscoveryResult_ValidResponse_GetAllFields()
        {
            // ARRANGE
            var lastCluster = "ClusterA";
            var handlerMock = CreateMockHttpMessageHandler(HttpStatusCode.OK, _discoveryResponseOk);

            // use real http client with mocked handler here
            _httpClient = new HttpClient(handlerMock.Object);

            _mockHttpClientFactory = new Mock<IHttpClientFactory>();
            _mockHttpClientFactory.Setup(_ => _.CreateClient(
                It.IsAny<HttpClientConfig>()
            )).Returns(_httpClient);

            _discoveryRestService =
                new DiscoveryRestService(_mockHttpClientFactory.Object, _axualConfig.ToDictionary());


            // ACT
            var latestDiscoveryResult =
                await _discoveryRestService.GetLatestDiscoveryResult(lastCluster, new CancellationToken());

            // ASSERT

            foreach (var key in _discoveryKeyValue.Keys)
            {
                var discoveryValue = latestDiscoveryResult.Get<string>(key);
                if (discoveryValue is null) Assert.Fail($"key '{key}' is missing in DiscoveryResult");

                var expectedValue = _discoveryKeyValue[key];

                if (discoveryValue != expectedValue)
                    Assert.Fail($"Value of '{key}' is {discoveryValue} from DiscoveryResult expected {expectedValue}");
            }

            // // also check the 'http' call was like we expected it
            // handlerMock.Protected().Verify(
            //     "SendAsync",
            //     Times.Exactly(1), // we expected a single external request
            //     ItExpr.Is<HttpRequestMessage>(req =>
            //             req.Method == HttpMethod.Get // we expected a GET request
            //             && req.RequestUri == _expectedUrl // to this uri
            //     ),
            //     ItExpr.IsAny<CancellationToken>()
            // );
        }

        [Test]
        public void GetLatestDiscoveryResult_ValidResponse_LastClusterParameterSent()
        {
            // ARRANGE
            var expectedUrl = new UriBuilder(DiscoveryApiEndPoint + "/v2")
            {
                Query = $"?applicationId={ApplicationId}&lastCluster={LastCluster}"
            }.Uri;

            var handlerMock = CreateMockHttpMessageHandler(HttpStatusCode.OK, _discoveryResponseOk);

            // use real http client with mocked handler here
            _httpClient = new HttpClient(handlerMock.Object);

            _mockHttpClientFactory = new Mock<IHttpClientFactory>();
            _mockHttpClientFactory.Setup(_ => _.CreateClient(
                It.IsAny<HttpClientConfig>()
            )).Returns(_httpClient);

            _discoveryRestService =
                new DiscoveryRestService(_mockHttpClientFactory.Object, _axualConfig.ToDictionary());


            // ACT
            var latestDiscoveryResult = _discoveryRestService
                .GetLatestDiscoveryResult(LastCluster, new CancellationToken()).Result;

            // ASSERT
            // also check the 'http' call was like we expected it
            handlerMock.Protected().Verify(
                "SendAsync",
                Times.Exactly(1), // we expected a single external request
                ItExpr.Is<HttpRequestMessage>(req =>
                        req.Method == HttpMethod.Get // we expected a GET request
                        && req.RequestUri == expectedUrl // to this uri
                ),
                ItExpr.IsAny<CancellationToken>()
            );
        }

        [Test]
        public void GetLatestDiscoveryResult_InternalServerError_ThrowHttpRequestException()
        {
            // ARRANGE
            var handlerMock = CreateMockHttpMessageHandler(HttpStatusCode.InternalServerError, string.Empty);

            // use real http client with mocked handler here
            _httpClient = new HttpClient(handlerMock.Object);

            _mockHttpClientFactory = new Mock<IHttpClientFactory>();
            _mockHttpClientFactory.Setup(_ => _.CreateClient(
                It.IsAny<HttpClientConfig>()
            )).Returns(_httpClient);

            _discoveryRestService =
                new DiscoveryRestService(_mockHttpClientFactory.Object, _axualConfig.ToDictionary());


            // ACT + Assert
            Assert.ThrowsAsync<HttpRequestException>(async () =>
                await _discoveryRestService.GetLatestDiscoveryResult(LastCluster, new CancellationToken()));
        }

        [Test]
        public void GetLatestDiscoveryResult_ServerNotFound_ThrowHttpRequestException()
        {
            // ARRANGE
            var handlerMock = CreateMockHttpMessageHandler(HttpStatusCode.NotFound, string.Empty);

            // use real http client with mocked handler here
            _httpClient = new HttpClient(handlerMock.Object);

            _mockHttpClientFactory = new Mock<IHttpClientFactory>();
            _mockHttpClientFactory.Setup(_ => _.CreateClient(
                It.IsAny<HttpClientConfig>()
            )).Returns(_httpClient);

            _discoveryRestService =
                new DiscoveryRestService(_mockHttpClientFactory.Object, _axualConfig.ToDictionary());


            // ACT + ASSERT
            var exception =
                Assert.ThrowsAsync<HttpRequestException>(async () =>
                    await _discoveryRestService.GetLatestDiscoveryResult(LastCluster, new CancellationToken()));

            Assert.AreEqual(
                $"Couldn't reach Discovery API, URL: 'http://{DiscoveryApiEndPoint}/v2?applicationId={ApplicationId}&lastCluster={LastCluster}'",
                exception.Message);
        }

        [Test]
        public void GetLatestDiscoveryResult_InvalidJson_ThrowHttpRequestException()
        {
            // ARRANGE
            var handlerMock = CreateMockHttpMessageHandler(HttpStatusCode.OK, "{ NOT JSON JSON");

            // use real http client with mocked handler here
            _httpClient = new HttpClient(handlerMock.Object);

            _mockHttpClientFactory = new Mock<IHttpClientFactory>();
            _mockHttpClientFactory.Setup(_ => _.CreateClient(
                It.IsAny<HttpClientConfig>()
            )).Returns(_httpClient);

            // ACT
            _discoveryRestService =
                new DiscoveryRestService(_mockHttpClientFactory.Object, _axualConfig.ToDictionary());

            // ASSERT
            var exp = Assert.ThrowsAsync<JsonReaderException>(async () =>
                await _discoveryRestService.GetLatestDiscoveryResult(LastCluster, new CancellationToken()));
        }

        [Test]
        public void GetLatestDiscoveryResult_ServerNoContent_EmptyDiscoveryResult()
        {
            // ARRANGE
            var handlerMock = CreateMockHttpMessageHandler(HttpStatusCode.NoContent, string.Empty);

            // use real http client with mocked handler here
            _httpClient = new HttpClient(handlerMock.Object);

            _mockHttpClientFactory = new Mock<IHttpClientFactory>();
            _mockHttpClientFactory.Setup(_ => _.CreateClient(
                It.IsAny<HttpClientConfig>()
            )).Returns(_httpClient);

            _discoveryRestService =
                new DiscoveryRestService(_mockHttpClientFactory.Object, _axualConfig.ToDictionary());

            // ACT
            var latestDiscoveryResult = _discoveryRestService
                .GetLatestDiscoveryResult(LastCluster, new CancellationToken()).Result;

            // ASSERT
            // Discovery should return an empty result, not fields
            Assert.IsTrue(!latestDiscoveryResult.GetKeys().Any());
        }

        private static Mock<HttpMessageHandler> CreateMockHttpMessageHandler(HttpStatusCode httpStatusCode,
            string stringContent)
        {
            var handlerMock = new Mock<HttpMessageHandler>(MockBehavior.Strict);
            handlerMock
                .Protected()
                // Setup the PROTECTED method to mock
                .Setup<Task<HttpResponseMessage>>(
                    "SendAsync",
                    ItExpr.IsAny<HttpRequestMessage>(),
                    ItExpr.IsAny<CancellationToken>()
                )
                // prepare the expected response of the mocked http call
                .ReturnsAsync(new HttpResponseMessage
                {
                    StatusCode = httpStatusCode,
                    Content = new StringContent(stringContent)
                })
                .Verifiable();
            return handlerMock;
        }

        [TestCase("http://localhost")]
        [TestCase("http://localhost/")]
        [TestCase("http://localhost/v2")]
        [TestCase("http://localhost/V2")]
        [TestCase("http://localhost/v2/")]
        [TestCase("http://localhost/V2/")]
        [TestCase("http://localhost:80")]
        [TestCase("http://localhost:8080")]
        [TestCase("http://localhost:443")]
        [TestCase("http://localhost:80/")]
        [TestCase("http://localhost:8080/")]
        [TestCase("http://localhost:443/")]
        [TestCase("https://localhost")]
        [TestCase("https://localhost/")]
        [TestCase("https://localhost:80")]
        [TestCase("https://localhost:8080")]
        [TestCase("https://localhost:443")]
        [TestCase("https://localhost:80/")]
        [TestCase("https://localhost:8080/")]
        [TestCase("https://localhost:443/")]
        [TestCase("http://127.0.0.1")]
        [TestCase("http://127.0.0.1/")]
        [TestCase("http://127.0.0.1:80")]
        [TestCase("http://127.0.0.1:8080")]
        [TestCase("http://127.0.0.1:443")]
        [TestCase("http://127.0.0.1:80/")]
        [TestCase("http://127.0.0.1:8080/")]
        [TestCase("http://127.0.0.1:443/")]
        [TestCase("http://some.url.with.dots")]
        [TestCase("http://some.url.with.dots:80")]
        [TestCase("http://some.url.with.dots:8080")]
        [TestCase("http://some.url.with.dots:443")]
        [TestCase("http://some.url.with.dots")]
        [TestCase("http://some.url.with.dots/")]
        [TestCase("https://some.url.with.dots")]
        [TestCase("https://some.url.with.dots/")]
        [TestCase("http://some.url.with.dots:80")]
        [TestCase("http://some.url.with.dots:443")]
        [TestCase("http://some.url.with.dots:80/")]
        [TestCase("http://some.url.with.dots:443/")]
        [TestCase("https://discovery.streaming-dta.company_name.com:28001")]
        [TestCase("https://discovery.streaming-pt.company_name.com:28001")]
        [TestCase("https://discovery.streaming.company_name.com:28001")]
        [TestCase("https://discoveryapi-companyprod.cloud.axual.io")]
        [TestCase("https://discoveryapi-companyota.cloud.axual.io")]
        public void GetLatestDiscoveryResult_CheckDifferentEndPoints_CallV2EndPoint(string discoveryEndPoint)
        {
            // ARRANGE
            _axualConfig.EndPoint = new Uri(discoveryEndPoint);

            if (!discoveryEndPoint.EndsWith("v2", StringComparison.CurrentCultureIgnoreCase) &&
                !discoveryEndPoint.EndsWith("v2/", StringComparison.CurrentCultureIgnoreCase))
            {
                discoveryEndPoint += discoveryEndPoint.EndsWith('/') ? "v2" : "/v2";
            }

            var expectedUrl = new UriBuilder(discoveryEndPoint)
            {
                Query = $"?applicationId={ApplicationId}&lastCluster={LastCluster}"
            }.Uri;

            var handlerMock = CreateMockHttpMessageHandler(HttpStatusCode.OK, _discoveryResponseOk);

            // use real http client with mocked handler here
            _httpClient = new HttpClient(handlerMock.Object);

            _mockHttpClientFactory = new Mock<IHttpClientFactory>();
            _mockHttpClientFactory.Setup(_ => _.CreateClient(
                It.IsAny<HttpClientConfig>()
            )).Returns(_httpClient);

            _discoveryRestService =
                new DiscoveryRestService(_mockHttpClientFactory.Object, _axualConfig.ToDictionary());


            // ACT
            var latestDiscoveryResult = _discoveryRestService
                .GetLatestDiscoveryResult(LastCluster, new CancellationToken()).Result;

            // ASSERT
            // also check the 'http' call was like we expected it
            handlerMock.Protected().Verify(
                "SendAsync",
                Times.Exactly(1), // we expected a single external request
                ItExpr.Is<HttpRequestMessage>(req =>
                        req.Method == HttpMethod.Get // we expected a GET request
                        && req.RequestUri == expectedUrl // to this uri
                ),
                ItExpr.IsAny<CancellationToken>()
            );
        }
    }
}