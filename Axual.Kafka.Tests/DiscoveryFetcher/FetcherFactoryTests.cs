using Axual.Kafka.Proxy.DiscoveryFetcher;
using NSubstitute;
using NUnit.Framework;

namespace Axual.Kafka.Tests.DiscoveryFetcher
{
    [TestFixture]
    public class FetcherFactoryTests
    {
        [Test]
        public void FetcherFactoryTest_StartStopStart_ManageToResume()
        {
            var fetcherFactory = new FetcherFactory();
            var fetcher = fetcherFactory.CreateFetcher(Substitute.For<IDiscoveryRestService>());

            Assert.IsTrue(typeof(Proxy.DiscoveryFetcher.DiscoveryFetcher) == fetcher.GetType());
        }
    }
}