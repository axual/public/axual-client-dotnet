//
// Copyright 2020 Axual B.V. 2020
//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Axual.Kafka.Proxy;
using Axual.Kafka.Proxy.DiscoveryFetcher;
using Axual.Kafka.Proxy.Proxies;
using Axual.Kafka.Proxy.Proxies.Switching;
using Axual.Kafka.Proxy.Proxies.Switching.Proxy;
using Confluent.Kafka;
using NSubstitute;
using NUnit.Framework;

namespace Axual.Kafka.Tests.Consumers
{
    internal class SwitchingConsumerTests : ConsumerTests<long, int>
    {
        [SetUp]
        public override void SetUp()
        {
            base.SetUp();

            _mockSwitcher = Substitute.For<ISwitcher>();
            _nextSwitchedConsumer = Substitute.For<IConsumer<long, int>>();
            MockConsumer = new SwitchingConsumer<long, int>(MockNextConsumerBuilder, ConsumerConfig, _mockSwitcher);
            _mockSwitcher.OnFirstDiscoveryResult += Raise.Event<EventHandler<DiscoveryResult>>(
                this, new DiscoveryResult());
        }

        [OneTimeSetUp]
        public override void OneTimeSetUp()
        {
            base.OneTimeSetUp();
            ConsumerConfig = new SwitchingConsumerConfig(ConsumerConfig)
            {
                ApplicationId = "SwitchingConsumerTests",
                EndPoint = new UriBuilder("http", "localhost").Uri
            };
        }

        [OneTimeTearDown]
        public override void OneTimeTearDown()
        {
            base.OneTimeTearDown();
        }

        private ISwitcher _mockSwitcher;
        private IConsumer<long, int> _nextSwitchedConsumer;

        [Test]
        public void SwitchingConsumer_GetAxualConsumerAssignmentAfterAssignment_GetNextConsumerAssignment()
        {
            var testTopic = "test";
            var topicPartition = new TopicPartition(testTopic, new Partition());

            MockConsumer.Assign(topicPartition);
            var accessAssignment = MockConsumer.Assignment;
            // shouldn't get list from the kafkaProxy should get the cached list
            var nextConsumerAssignment = MockNextConsumer.Received(0).Assignment;

            Assert.AreSame(topicPartition, accessAssignment.First());
            Assert.AreEqual(1, accessAssignment.Count);
        }

        [Test]
        public void SwitchingConsumer_GetAxualConsumerSubscriptionAfterSubscribe_GetNextConsumerSubscription()
        {
            var testTopic = "test";

            MockConsumer.Subscribe(testTopic);
            var accessSubscription = MockConsumer.Subscription;
            // shouldn't get list from the kafkaProxy should get the cached list
            var nextConsumerSubscription = MockNextConsumer.Received(0).Subscription;

            Assert.AreEqual(testTopic, accessSubscription.First());
            Assert.AreEqual(1, accessSubscription.Count);
        }

        [Test]
        public override void Consumer_Assignment_GetNextConsumerAssignment()
        {
            var accessAssignment = MockConsumer.Assignment;
            // shouldn't get list from the kafkaProxy should get the cached list
            var NextConsumerAssignment = MockNextConsumer.Received(0).Assignment;

            Assert.IsTrue(accessAssignment.Count == 0);
            Assert.IsTrue(NextConsumerAssignment == null || NextConsumerAssignment.Count == 0);
        }

        [Test]
        public override void Consumer_Subscription_GetNextConsumerSubscription()
        {
            var subscription = MockConsumer.Subscription;
            // shouldn't get list from the kafkaProxy should get the cached list
            var nextConsumerSubscription = MockNextConsumer.Received(0).Subscription;

            Assert.IsTrue(subscription.Count == 0);
            Assert.IsTrue(nextConsumerSubscription == null || nextConsumerSubscription.Count == 0);
        }

        [Test]
        public override void Consumer_Consume_CalledNextConsumerConsume()
        {
            MockConsumer.Consume();
            MockNextConsumer.Received(1).Consume(Arg.Any<TimeSpan>());
        }

        [Test]
        public override void Consumer_ConsumeWithCancellationTokenConsumeResultWithNullMessage_ReturnNull()
        {
            var cancellationToken = new CancellationToken();

            MockConsumer.Consume(cancellationToken);
            Assert.IsNull(MockNextConsumer.Received(1).Consume(Arg.Any<TimeSpan>()));
        }

        [Test]
        public override void Consumer_ConsumeCancellationToken_CalledNextConsumerConsume()
        {
            var cancellationToken = new CancellationToken();

            MockConsumer.Consume(cancellationToken);
            MockNextConsumer.Received(1).Consume(Arg.Any<TimeSpan>());
        }

        [Test]
        public override void Consumer_ConsumeWithCancellationTokenConsumeResultIsNull_ReturnNull()
        {
            var cancellationToken = new CancellationToken();

            MockConsumer.Consume(cancellationToken);
            Assert.IsNull(MockNextConsumer.Received(1).Consume(Arg.Any<TimeSpan>()));
        }

        [Test]
        public override void Consumer_StoreOffset_CalledNextConsumerStoreOffsetWithConsumeResult()
        {
            MockConsumer.StoreOffset(ConsumeResult);
            // Check that we call 'void StoreOffset(TopicPartitionOffset offset)' 
            MockNextConsumer.Received(1).StoreOffset(ConsumeResult);
        }

        [Test]
        public override void Consumer_CommitWithConsumeResult_CalledNextConsumerCommitWithConsumeResult()
        {
            MockConsumer.Commit(ConsumeResult);
            MockNextConsumer.Received(1).Commit(ConsumeResult);
        }

        [Test]
        public override void Consumer_CommitWithNullMessage_ThrowInvalidOperationException()
        {
            var consumeResult = new ConsumeResult<long, int>();
            MockConsumer.Commit(consumeResult);
            MockNextConsumer.Received(1).Commit(consumeResult);
        }

        [Test]
        public void SwitchingConsumer_SwitchOccur_CalledSwitch()
        {
            _mockSwitcher.Switch(Arg.Any<IConsumer<long, int>>(), Arg.Any<Utils.NewDiscoveryConfigEventArgs>())
                .Returns(_nextSwitchedConsumer);

            _mockSwitcher.OnSwitchNeeded +=
                Raise.Event<EventHandler<Utils.NewDiscoveryConfigEventArgs>>(
                    this,
                    new Utils.NewDiscoveryConfigEventArgs(
                        null,
                        new DiscoveryResult()));

            MockConsumer.Consume(TimeSpan.FromMilliseconds(100));

            // Assert switched occured
            _mockSwitcher.Received(1).Switch(
                Arg.Any<IConsumer<long, int>>(),
                Arg.Any<Utils.NewDiscoveryConfigEventArgs>());
        }

        [Test]
        public void ConsumerSwitcher_SwitchOccur_SwitcherSuccuess()
        {
            var mockNextConsumerBuilder = Substitute.For<IConsumerBuilder<int, int>>();
            var mockNextConsumer = Substitute.For<IConsumer<int, int>>();
            var mockFetcher = Substitute.For<IFetcher>();
            var mockFetcherFactory = Substitute.For<IFetcherFactory>();
            var mockDiscoveryRestService = Substitute.For<IDiscoveryRestService>();
            mockNextConsumerBuilder.Rebuild(Arg.Any<Confluent.Kafka.ConsumerConfig>()).Returns(mockNextConsumer);
            mockFetcherFactory.CreateFetcher(mockDiscoveryRestService).Returns(mockFetcher);
            mockNextConsumer.Consume(Arg.Any<TimeSpan>()).Returns(new ConsumeResult<int, int>());

            var switcher = new ConsumerSwitcher<int, int>(mockNextConsumerBuilder, ConsumerConfig, mockFetcherFactory,
                mockDiscoveryRestService);
            var consumer = new SwitchingConsumer<int, int>(mockNextConsumerBuilder,
                ConsumerConfig, switcher);

            Task.Factory.StartNew(() =>
            {
                //Tell the substitute to raise the event with a sender and EventArgs:
                mockFetcher.OnNewFetchResult += Raise.Event<EventHandler<DiscoveryResult>>(
                    this, new DiscoveryResult(new Dictionary<string, string>()
                    {
                        { Constants.ConfigurationKeys.Axual.Cluster, "Cluster A" }
                    }));

                mockFetcher.OnNewFetchResult += Raise.Event<EventHandler<DiscoveryResult>>(
                    this, new DiscoveryResult(new Dictionary<string, string>()
                    {
                        { Constants.ConfigurationKeys.Axual.Cluster, "Cluster B" },
                        // Make timeout 0
                        { Constants.ConfigurationKeys.Switching.DistributorDistance, "0" },
                        { Constants.ConfigurationKeys.Switching.Ttl, "0" }
                    }));
            }).ContinueWith(t => { consumer.Consume(); }).Wait();
        }
    }
}