//
// Copyright 2020 Axual B.V. 2020
//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

using System;
using System.Collections.Generic;
using System.Threading;
using Axual.Kafka.Proxy.Proxies;
using Confluent.Kafka;
using NSubstitute;
using NUnit.Framework;

namespace Axual.Kafka.Tests.Consumers
{
    [TestFixture]
    internal abstract class ConsumerTests<TKey, TValue>
    {
        [SetUp]
        public virtual void SetUp()
        {
            TopicPartition = new TopicPartition(Topic, new Partition(1));
            TopicPartitionOffset = new TopicPartitionOffset(TopicPartition, new Offset(1));

            Message = new Message<TKey, TValue>
            {
                Headers = new Headers(),
                Key = default,
                Value = default,
                Timestamp = new Timestamp()
            };

            ConsumeResult = new ConsumeResult<TKey, TValue>
            {
                Message = Message,
                Topic = Topic
            };


            MockNextConsumer = Substitute.For<IConsumer<TKey, TValue>>();
            MockNextConsumerBuilder = Substitute.For<IConsumerBuilder<TKey, TValue>>();

            MockNextConsumer.Consume().Returns(ConsumeResult);
            MockNextConsumer.Consume(Arg.Any<CancellationToken>()).Returns(ConsumeResult);
            MockNextConsumer.Consume(Arg.Any<int>()).Returns(ConsumeResult);
            MockNextConsumer.Consume(Arg.Any<TimeSpan>()).Returns(ConsumeResult);

            MockNextConsumerBuilder.Build().Returns(MockNextConsumer);
            MockNextConsumerBuilder.Rebuild(Arg.Any<Confluent.Kafka.ConsumerConfig>()).Returns(MockNextConsumer);
        }

        [OneTimeSetUp]
        public virtual void OneTimeSetUp()
        {
            Topic = "topic";
            ConsumerConfig = new Confluent.Kafka.ConsumerConfig();
        }

        [OneTimeTearDown]
        public virtual void OneTimeTearDown()
        {
            // nothing
        }

        protected string Topic;
        protected Message<TKey, TValue> Message;
        protected TopicPartition TopicPartition;
        protected TopicPartitionOffset TopicPartitionOffset;
        protected ConsumeResult<TKey, TValue> ConsumeResult;

        protected IConsumer<TKey, TValue> MockConsumer;
        protected IConsumer<TKey, TValue> MockNextConsumer;
        protected IConsumerBuilder<TKey, TValue> MockNextConsumerBuilder;
        protected Confluent.Kafka.ConsumerConfig ConsumerConfig;

        [Test]
        public void Consumer_AddBrokers_CalledNextConsumerAddBrokers()
        {
            var newBrokersToAdd = "newBrokers";
            MockConsumer.AddBrokers(newBrokersToAdd);
            MockNextConsumer.Received(1).AddBrokers(newBrokersToAdd);
        }

        [Test]
        public virtual void Consumer_Assign_CalledNextConsumerAssign()
        {
            var lstTopicPartition = new List<TopicPartition> { TopicPartition };
            var lstTopicPartitionOffset = new List<TopicPartitionOffset> { TopicPartitionOffset };

            MockNextConsumer.Subscription.Returns(new List<string>());

            MockConsumer.Assign(TopicPartition);
            MockNextConsumer.Received(1).Assign(TopicPartition);


            MockConsumer.Assign(lstTopicPartition);
            MockNextConsumer.Received(1).Assign(lstTopicPartition);


            MockConsumer.Assign(TopicPartitionOffset);
            MockNextConsumer.Received(1).Assign(TopicPartitionOffset);


            MockConsumer.Assign(lstTopicPartitionOffset);
            MockNextConsumer.Received(1).Assign(lstTopicPartitionOffset);
        }

        [Test]
        public virtual void Consumer_Assignment_GetNextConsumerAssignment()
        {
            var assignment = MockConsumer.Assignment;
            var nextConsumerAssignment = MockNextConsumer.Received(1).Assignment;

            Assert.AreEqual(assignment, nextConsumerAssignment);
        }

        [Test]
        public void Consumer_Close_CalledNextConsumerClose()
        {
            MockNextConsumer.Assignment.Returns(new List<TopicPartition>());
            MockNextConsumer.Subscription.Returns(new List<string>());

            MockNextConsumerBuilder = Substitute.For<IConsumerBuilder<TKey, TValue>>();

            MockConsumer.Close();
            MockNextConsumer.Received(1).Close();
        }

        [Test]
        public virtual void Consumer_Commit_CalledNextConsumerCommit()
        {
            MockConsumer.Commit();
            MockNextConsumer.Received(1).Commit();
        }

        [Test]
        public virtual void Consumer_Committed_CalledNextConsumerCommitted()
        {
            var timeSpan = new TimeSpan(TimeSpan.MaxValue.Ticks);
            var lstTopicPartition = new List<TopicPartition> { TopicPartition };

            MockConsumer.Committed(lstTopicPartition, timeSpan);
            MockNextConsumer.Received(1).Committed(lstTopicPartition, timeSpan);
        }

        [Test]
        public virtual void Consumer_CommittedWithTimeSpan_CalledNextConsumerCommittedWithTimeSpan()
        {
            var timeSpan = new TimeSpan();
            MockConsumer.Committed(timeSpan);
            MockNextConsumer.Received(1).Committed(timeSpan);
        }

        [Test]
        public virtual void Consumer_CommitWithConsumeResult_CalledNextConsumerCommitWithConsumeResult()
        {
            MockConsumer.Commit(ConsumeResult);

            // Calling the IEnumerable<TopicPartitionOffset>> instead of ConsumerResult
            MockNextConsumer.Received(1).Commit(Arg.Any<IEnumerable<TopicPartitionOffset>>());
        }

        [Test]
        public virtual void Consumer_CommitWithNullMessage_ThrowInvalidOperationException()
        {
            Assert.Throws<InvalidOperationException>(() => MockConsumer.Commit(new ConsumeResult<TKey, TValue>()));
        }


        [Test]
        public virtual void Consumer_CommitWithTopicPartitionOffset_CalledNextConsumerCommitWithTopicPartitionOffset()
        {
            var lstTopicPartitionOffset = new List<TopicPartitionOffset> { TopicPartitionOffset };

            MockConsumer.Commit(lstTopicPartitionOffset);
            MockNextConsumer.Received(1).Commit(lstTopicPartitionOffset);
        }

        [Test]
        public virtual void Consumer_Consume_CalledNextConsumerConsume()
        {
            MockConsumer.Consume();
            MockNextConsumer.Received(1).Consume();
        }

        [Test]
        public virtual void Consumer_ConsumeCancellationToken_CalledNextConsumerConsume()
        {
            var cancellationToken = new CancellationToken();

            MockConsumer.Consume(cancellationToken);
            MockNextConsumer.Received(1).Consume(cancellationToken);
        }

        [Test]
        public virtual void Consumer_ConsumeMillisecondsTimeout_CalledNextConsumerMillisecondsTimeout()
        {
            var millisecondsTimeout = TimeSpan.FromSeconds(1).Milliseconds;
            MockConsumer.Consume(millisecondsTimeout);
            MockNextConsumer.Received(1).Consume(millisecondsTimeout);
        }

        [Test]
        public virtual void Consumer_ConsumeWithCancellationTokenConsumeResultIsNull_ReturnNull()
        {
            var cancellationToken = new CancellationToken();

            MockConsumer.Consume(cancellationToken);
            Assert.IsNull(MockNextConsumer.Received(1).Consume(cancellationToken));
        }

        [Test]
        public virtual void Consumer_ConsumeWithTimeoutConsumeResultWithNullMessage_ReturnNull()
        {
            MockConsumer.Consume(TimeSpan.MaxValue);
            Assert.IsNull(MockNextConsumer.Received(1).Consume(TimeSpan.MaxValue));
        }

        [Test]
        public virtual void Consumer_ConsumeWithCancellationTokenConsumeResultWithNullMessage_ReturnNull()
        {
            var cancellationToken = new CancellationToken(true);

            MockConsumer.Consume(cancellationToken);
            Assert.IsNull(MockNextConsumer.Received(1).Consume(cancellationToken));
        }

        [Test]
        public virtual void Consumer_ConsumerGroupMetadata_GetNextConsumerConsumerGroupMetadata()
        {
            var consumerGroupMetadata = MockConsumer.ConsumerGroupMetadata;
            var res = MockNextConsumer.Received(1).ConsumerGroupMetadata;
            Assert.AreSame(consumerGroupMetadata, res);
        }

        [Test]
        public virtual void Consumer_ConsumeWithTimeSpan_CalledNextConsumerConsumeWithTimeSpan()
        {
            var timeSpan = new TimeSpan();

            MockConsumer.Consume(timeSpan);
            MockNextConsumer.Received(1).Consume(timeSpan);
        }

        [Test]
        public void Consumer_Dispose_CalledNextConsumerDispose()
        {
            MockConsumer.Dispose();
            MockNextConsumer.Received(1).Dispose();
        }

        [Test]
        public virtual void Consumer_GetWatermarkOffsets_CalledNextConsumerGetWatermarkOffsets()
        {
            MockConsumer.GetWatermarkOffsets(TopicPartition);
            MockNextConsumer.Received(1).GetWatermarkOffsets(TopicPartition);
        }

        [Test]
        public void Consumer_Handle_GetNextConsumerHandle()
        {
            var accessHandle = MockConsumer.Handle;
            var nextConsumerHandle = MockNextConsumer.Received(1).Handle;

            Assert.AreSame(accessHandle, nextConsumerHandle);
        }

        [Test]
        public void Consumer_MemberId_GetNextConsumerMemberId()
        {
            var accessMemberId = MockConsumer.MemberId;
            var nextConsumerMemberId = MockNextConsumer.Received(1).MemberId;

            Assert.AreSame(accessMemberId, nextConsumerMemberId);
        }

        [Test]
        public void Consumer_Name_GetNextConsumerName()
        {
            var consumerName = MockConsumer.Name;
            var nextConsumerName = MockNextConsumer.Received(1).Name;

            Assert.AreEqual(consumerName, nextConsumerName);
        }

        [Test]
        public virtual void Consumer_OffsetsForTimes_CalledNextConsumerOffsetsForTimes()
        {
            var lstTopicPartitionTimestamp = new List<TopicPartitionTimestamp>
                { new TopicPartitionTimestamp(TopicPartition, new Timestamp(DateTime.Now)) };

            MockConsumer.OffsetsForTimes(lstTopicPartitionTimestamp, TimeSpan.MaxValue);
            MockNextConsumer.Received(1).OffsetsForTimes(lstTopicPartitionTimestamp, TimeSpan.MaxValue);
        }

        [Test]
        public virtual void Consumer_Pause_CalledNextConsumerPause()
        {
            MockNextConsumerBuilder = Substitute.For<IConsumerBuilder<TKey, TValue>>();

            var lstTopicPartition = new List<TopicPartition> { TopicPartition };
            MockConsumer.Pause(lstTopicPartition);
            MockNextConsumer.Received(1).Pause(lstTopicPartition);
        }

        [Test]
        public virtual void Consumer_Position_CalledNextConsumerPosition()
        {
            MockConsumer.Position(TopicPartition);
            MockNextConsumer.Received(1).Position(TopicPartition);
        }

        [Test]
        public virtual void
            Consumer_QueryWatermarkOffsets_CalledNextConsumerQueryWatermarkOffsets()
        {
            MockConsumer.QueryWatermarkOffsets(TopicPartition, TimeSpan.MaxValue);
            MockNextConsumer.Received(1).QueryWatermarkOffsets(TopicPartition, TimeSpan.MaxValue);
        }

        [Test]
        public virtual void Consumer_Resume_CalledNextConsumerResume()
        {
            var lstTopicPartition = new List<TopicPartition> { TopicPartition };

            MockConsumer.Resume(lstTopicPartition);
            MockNextConsumer.Received(1).Resume(lstTopicPartition);
        }

        [Test]
        public virtual void Consumer_Seek_CalledNextConsumerSeek()
        {
            MockConsumer.Seek(TopicPartitionOffset);
            MockNextConsumer.Received(1).Seek(TopicPartitionOffset);
        }

        [Test]
        public virtual void Consumer_StoreOffset_CalledNextConsumerStoreOffsetWithTopicPartitionOffset()
        {
            MockConsumer.StoreOffset(TopicPartitionOffset);
            MockNextConsumer.Received(1).StoreOffset(TopicPartitionOffset);
        }

        [Test]
        public virtual void Consumer_StoreOffset_CalledNextConsumerStoreOffsetWithConsumeResult()
        {
            MockConsumer.StoreOffset(ConsumeResult);
            // Check that we call 'void StoreOffset(TopicPartitionOffset offset)' 
            MockNextConsumer.Received(1).StoreOffset(Arg.Any<TopicPartitionOffset>());
        }

        [Test]
        public virtual void Consumer_Subscribe_CalledNextConsumerSubscribe()
        {
            var topic = "Some String";
            var topics = new List<string> { topic };

            MockNextConsumer.Assignment.Returns(new List<TopicPartition>());

            MockConsumer.Subscribe(topic);
            MockNextConsumer.Received(1).Subscribe(topic);


            MockConsumer.Subscribe(topics);
            MockNextConsumer.Received(1).Subscribe(topics);
        }

        [Test]
        public virtual void Consumer_Subscription_GetNextConsumerSubscription()
        {
            var consumerSubscription = MockConsumer.Subscription;
            var nextConsumerSubscription = MockNextConsumer.Received(1).Subscription;

            Assert.IsNull(consumerSubscription);
            Assert.IsNull(nextConsumerSubscription);
        }

        [Test]
        public void Consumer_Unassign_CalledNextConsumerUnassign()
        {
            MockConsumer.Unassign();
            MockNextConsumer.Received(1).Unassign();
        }

        [Test]
        public void Consumer_Unsubscribe_CalledNextConsumerUnsubscribe()
        {
            MockConsumer.Unsubscribe();
            MockNextConsumer.Received(1).Unsubscribe();
        }

        [Test]
        public virtual void
            Consumer_IncrementalAssignTopicPartitionOffset_CalledNextConsumerIncrementalAssignTopicPartitionOffset()
        {
            IEnumerable<TopicPartitionOffset> topicPartitionOffsets = new List<TopicPartitionOffset>();
            MockConsumer.IncrementalAssign(topicPartitionOffsets);
            MockNextConsumer.Received(1).IncrementalAssign(topicPartitionOffsets);
        }

        [Test]
        public virtual void Consumer_IncrementalAssignTopicPartition_CalledNextConsumerIncrementalAssignTopicPartition()
        {
            IEnumerable<TopicPartition> topicPartitions = new List<TopicPartition>();
            MockConsumer.IncrementalAssign(topicPartitions);
            MockNextConsumer.Received(1).IncrementalAssign(topicPartitions);
        }

        [Test]
        public virtual void Consumer_IncrementalUnassign_CalledNextConsumerIncrementalUnassign()
        {
            IEnumerable<TopicPartition> topicPartitions = new List<TopicPartition>();
            MockConsumer.IncrementalUnassign(topicPartitions);
            MockNextConsumer.Received(1).IncrementalUnassign(topicPartitions);
        }
    }
}