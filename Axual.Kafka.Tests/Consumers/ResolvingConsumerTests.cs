//
// Copyright 2020 Axual B.V. 2020
//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Axual.Kafka.Proxy.Proxies;
using Axual.Kafka.Proxy.Proxies.Resolving;
using Axual.Kafka.Proxy.Proxies.Resolving.Resolvers;
using Confluent.Kafka;
using NSubstitute;
using NUnit.Framework;
using static Axual.Kafka.Proxy.Proxies.Constants.ConfigurationKeys.Resolver;

namespace Axual.Kafka.Tests.Consumers
{
    internal class ResolvingConsumerTests : ConsumerTests<int, long>
    {
        private string _resolveTopic;
        private string _unResolveTopic;
        private IResolver _topicResolver;
        
        [SetUp]
        public override void SetUp()
        {
            base.SetUp();

            _resolveTopic = _topicResolver.Resolve(_unResolveTopic);
            MockConsumer = new ResolvingConsumer<int, long>(MockNextConsumerBuilder, ConsumerConfig, _topicResolver);

            // Change the the return of the consume result to the resolved topic
            ConsumeResult.Topic = _resolveTopic;
        }

        [OneTimeSetUp]
        public override void OneTimeSetUp()
        {
            base.OneTimeSetUp();

            _unResolveTopic = Topic;

            ConsumerConfig = new ResolvingConsumerConfig(ConsumerConfig)
            {
                ApplicationId = "1",
                Tenant = "axual",
                Environment = "dev",
                Instance = "platform",
                GroupId = "ResolvingConsumerTests",
                TopicResolver = $"{Constants.JavaResolverPath}TopicPatternResolver",
                TopicPattern = $"{{{Constants.ConfigurationKeys.Axual.Tenant}}}-" +
                               $"{{{Constants.ConfigurationKeys.Axual.Environment}}}-" +
                               $"{{{TopicResolverKey}}}",
                GroupIdResolver = $"{Constants.JavaResolverPath}GroupPatternResolver",
                GroupIdPattern = $"{{{Constants.ConfigurationKeys.Axual.Tenant}}}-" +
                               $"{{{Constants.ConfigurationKeys.Axual.Environment}}}-" +
                               $"{{{GroupIdResolverKey}}}"
            };

            _topicResolver = new TopicPatternResolver(ConsumerConfig as ResolvingConsumerConfig);
        }

        private bool IsResolveTopic(string topic)
        {
            return topic.Equals(_resolveTopic);
        }

        private bool IsResolveTopic(TopicPartition tp)
        {
            return IsResolveTopic(tp.Topic);
        }

        private bool IsResolveTopic(TopicPartitionOffset tpo)
        {
            return IsResolveTopic(tpo.Topic);
        }

        private bool IsResolveTopic(TopicPartitionTimestamp tpt)
        {
            return IsResolveTopic(tpt.Topic);
        }

        [Test]
        public override void Consumer_Assign_CalledNextConsumerAssign()
        {
            var lstTopicPartition = new List<TopicPartition> {TopicPartition};
            var lstTopicPartitionOffset = new List<TopicPartitionOffset> {TopicPartitionOffset};

            MockConsumer.Assign(TopicPartition);
            MockNextConsumer.Received(1).Assign(Arg.Is<TopicPartition>(tp => IsResolveTopic(tp)));

            MockConsumer.Assign(lstTopicPartition);
            MockNextConsumer.Received(1).Assign(Arg.Is<IEnumerable<TopicPartition>>(tp => tp.All(IsResolveTopic)));


            MockConsumer.Assign(TopicPartitionOffset);
            MockNextConsumer.Received(1).Assign(Arg.Is<TopicPartitionOffset>(tp => IsResolveTopic(tp)));


            MockConsumer.Assign(lstTopicPartitionOffset);
            MockNextConsumer.Received(1)
                .Assign(Arg.Is<IEnumerable<TopicPartitionOffset>>(tp => tp.All(IsResolveTopic)));
        }
        
        [Test]
        public override void Consumer_IncrementalAssignTopicPartition_CalledNextConsumerIncrementalAssignTopicPartition()
        {
            var lstTopicPartition = new List<TopicPartition> {TopicPartition};
            
            MockConsumer.IncrementalAssign(lstTopicPartition);
            
            MockNextConsumer.Received(1).IncrementalAssign(Arg.Is<IEnumerable<TopicPartition>>(tp => tp.All(IsResolveTopic)));
        }
        
        [Test]
        public override void Consumer_IncrementalAssignTopicPartitionOffset_CalledNextConsumerIncrementalAssignTopicPartitionOffset()
        {
            var lstTopicPartitionOffset = new List<TopicPartitionOffset> {TopicPartitionOffset};
            
            MockConsumer.IncrementalAssign(lstTopicPartitionOffset);
            
            MockNextConsumer.Received(1)
                .IncrementalAssign(Arg.Is<IEnumerable<TopicPartitionOffset>>(tp => tp.All(IsResolveTopic)));
        }
        
        [Test]
        public override void Consumer_IncrementalUnassign_CalledNextConsumerIncrementalUnassign()
        {
            var lstTopicPartition = new List<TopicPartition> {TopicPartition};
            
            MockConsumer.IncrementalUnassign(lstTopicPartition);
            
            MockNextConsumer.Received(1).IncrementalUnassign(Arg.Is<IEnumerable<TopicPartition>>(tp => tp.All(IsResolveTopic)));
        }

        [Test]
        public override void Consumer_Committed_CalledNextConsumerCommitted()
        {
            var timeSpan = new TimeSpan(TimeSpan.MaxValue.Ticks);
            var lstTopicPartition = new List<TopicPartition> {TopicPartition};

            MockConsumer.Committed(lstTopicPartition, timeSpan);
            MockNextConsumer.Received(1)
                .Committed(Arg.Is<IEnumerable<TopicPartition>>(tp => tp.All(IsResolveTopic)), timeSpan);
        }

        [Test]
        public override void Consumer_CommitWithConsumeResult_CalledNextConsumerCommitWithConsumeResult()
        {
            MockConsumer.Commit(new ConsumeResult<int, long>
            {
                Message = Message,
                Topic = _unResolveTopic,
                TopicPartitionOffset = new TopicPartitionOffset(new TopicPartition(_unResolveTopic, new Partition(1)),
                    new Offset(0))
            });

            // Calling the IEnumerable<TopicPartitionOffset>> instead of ConsumerResult
            MockNextConsumer.Received(1)
                .Commit(Arg.Is<IEnumerable<TopicPartitionOffset>>(tp => tp.All(IsResolveTopic)));
        }

        [Test]
        public override void Consumer_CommitWithTopicPartitionOffset_CalledNextConsumerCommitWithTopicPartitionOffset()
        {
            var lstTopicPartitionOffset = new List<TopicPartitionOffset> {TopicPartitionOffset};

            MockConsumer.Commit(lstTopicPartitionOffset);
            MockNextConsumer.Received(1)
                .Commit(Arg.Is<IEnumerable<TopicPartitionOffset>>(tp => tp.All(IsResolveTopic)));
        }

        [Test]
        public override void Consumer_GetWatermarkOffsets_CalledNextConsumerGetWatermarkOffsets()
        {
            MockConsumer.GetWatermarkOffsets(TopicPartition);
            MockNextConsumer.Received(1).GetWatermarkOffsets(Arg.Is<TopicPartition>(tp => IsResolveTopic(tp)));
        }

        [Test]
        public override void Consumer_OffsetsForTimes_CalledNextConsumerOffsetsForTimes()
        {
            var lstTopicPartitionTimestamp = new List<TopicPartitionTimestamp>
                {new(TopicPartition, new Timestamp(DateTime.Now))};

            MockConsumer.OffsetsForTimes(lstTopicPartitionTimestamp, TimeSpan.MaxValue);
            MockNextConsumer.Received(1).OffsetsForTimes(
                Arg.Is<IEnumerable<TopicPartitionTimestamp>>(tpts => tpts.All(IsResolveTopic)), TimeSpan.MaxValue);
        }

        [Test]
        public override void Consumer_Pause_CalledNextConsumerPause()
        {
            var lstTopicPartition = new List<TopicPartition> {TopicPartition};
            MockConsumer.Pause(lstTopicPartition);
            MockNextConsumer.Received(1).Pause(Arg.Is<IEnumerable<TopicPartition>>(tp => tp.All(IsResolveTopic)));
        }

        [Test]
        public override void Consumer_Position_CalledNextConsumerPosition()
        {
            MockConsumer.Position(TopicPartition);
            MockNextConsumer.Received(1).Position(Arg.Is<TopicPartition>(tp => IsResolveTopic(tp)));
        }

        [Test]
        public override void Consumer_QueryWatermarkOffsets_CalledNextConsumerQueryWatermarkOffsets()
        {
            MockConsumer.QueryWatermarkOffsets(TopicPartition, TimeSpan.MaxValue);
            MockNextConsumer.Received(1)
                .QueryWatermarkOffsets(Arg.Is<TopicPartition>(tp => IsResolveTopic(tp)), TimeSpan.MaxValue);
        }

        [Test]
        public override void Consumer_Resume_CalledNextConsumerResume()
        {
            var lstTopicPartition = new List<TopicPartition> {TopicPartition};

            MockConsumer.Resume(lstTopicPartition);
            MockNextConsumer.Received(1).Resume(Arg.Is<IEnumerable<TopicPartition>>(tp => tp.All(IsResolveTopic)));
        }

        [Test]
        public override void Consumer_Seek_CalledNextConsumerSeek()
        {
            MockConsumer.Seek(TopicPartitionOffset);
            MockNextConsumer.Received(1).Seek(Arg.Is<TopicPartitionOffset>(tp => IsResolveTopic(tp)));
        }

        [Test]
        public override void Consumer_Consume_CalledNextConsumerConsume()
        {
            MockConsumer.Consume();
            MockNextConsumer.Received(1).Consume();
        }

        [Test]
        public override void Consumer_ConsumeCancellationToken_CalledNextConsumerConsume()
        {
            var cancellationToken = new CancellationToken();

            MockConsumer.Consume(cancellationToken);
            MockNextConsumer.Received(1).Consume(cancellationToken);
        }

        [Test]
        public override void Consumer_StoreOffset_CalledNextConsumerStoreOffsetWithTopicPartitionOffset()
        {
            MockConsumer.StoreOffset(TopicPartitionOffset);
            MockNextConsumer.Received(1).StoreOffset(Arg.Is<TopicPartitionOffset>(tp => IsResolveTopic(tp)));
        }

        [Test]
        public override void Consumer_StoreOffset_CalledNextConsumerStoreOffsetWithConsumeResult()
        {
            MockConsumer.StoreOffset(new ConsumeResult<int, long>
            {
                Message = Message,
                Topic = _unResolveTopic,
                TopicPartitionOffset = new TopicPartitionOffset(new TopicPartition(_unResolveTopic, new Partition(1)),
                    new Offset(0))
            });

            // Check that we call 'void StoreOffset(TopicPartitionOffset offset)' 
            MockNextConsumer.Received(1).StoreOffset(Arg.Is<TopicPartitionOffset>(tp => IsResolveTopic(tp)));
        }


        [Test]
        public override void Consumer_Subscribe_CalledNextConsumerSubscribe()
        {
            var topic = _unResolveTopic;
            var topics = new List<string> {_unResolveTopic};

            MockConsumer.Subscribe(topic);
            MockNextConsumer.Received(1).Subscribe(_resolveTopic);


            MockConsumer.Subscribe(topics);
            MockNextConsumer.Received(1)
                .Subscribe(Arg.Is<IEnumerable<string>>(sentTopics => sentTopics.All(IsResolveTopic)));
        }
    }
}