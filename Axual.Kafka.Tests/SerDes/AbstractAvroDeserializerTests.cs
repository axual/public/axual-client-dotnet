using System;
using System.Collections.Generic;
using Axual.Kafka.Proxy.Exceptions;
using Axual.Kafka.Proxy.Proxies;
using Axual.SchemaRegistry.Serdes.Avro.Deserializers;
using Confluent.Kafka;
using NUnit.Framework;

namespace Axual.Kafka.Tests.SerDes
{
    public class AbstractAvroDeserializerTests<T>
    {
        protected AbstractAvroDeserializer<T> AbstractAvroDeserializer;

        [SetUp]
        public virtual void SetUp()
        {
        }

        [Test]
        public void AbstractAvroDeserializer_DeserializeWithoutConfigure_ThrowsInvalidOperationException()
        {
            Assert.Throws<InvalidOperationException>(() =>
                AbstractAvroDeserializer.DeserializeAsync(ReadOnlyMemory<byte>.Empty, false,
                    new SerializationContext()));
        }

        [Test]
        public void AbstractAvroDeserializer_ConfigureWithEmptyConfiguration_ThrowsRequireConfigurationException()
        {
            Assert.Throws<RequireConfigurationException>(() => AbstractAvroDeserializer.Configure(new ClientConfig()));
        }

        [Test]
        public void AbstractAvroDeserializer_DeserializeWithSRUrl_NoException()
        {
            AbstractAvroDeserializer.Configure(new ClientConfig(new Dictionary<string, string>
            {
                {Constants.ConfigurationKeys.Kafka.SchemaRegistryUrl, new Uri("https://localhost").ToString()}
            }));

            AbstractAvroDeserializer.DeserializeAsync(ReadOnlyMemory<byte>.Empty, false,
                new SerializationContext());
        }
    }
}