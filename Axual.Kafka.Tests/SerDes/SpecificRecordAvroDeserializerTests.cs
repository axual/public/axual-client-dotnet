using Avro.Specific;
using Axual.SchemaRegistry.Serdes.Avro.Deserializers;
using NUnit.Framework;

namespace Axual.Kafka.Tests.SerDes
{
    public class SpecificRecordAvroDeserializerTests: AbstractAvroDeserializerTests<ISpecificRecord>
    {
        [SetUp]
        public override void SetUp()
        {
            AbstractAvroDeserializer = new SpecificAvroDeserializer<ISpecificRecord>();
        }
    }
}