using Avro.Generic;
using Axual.SchemaRegistry.Serdes.Avro.Deserializers;
using NUnit.Framework;

namespace Axual.Kafka.Tests.SerDes
{
    public class GenericAvroDeserializerTests: AbstractAvroDeserializerTests<GenericRecord>
    {
        [SetUp]
        public override void SetUp()
        {
            AbstractAvroDeserializer = new GenericAvroDeserializer<GenericRecord>();
        }
    }
}