//
// Copyright 2020 Axual B.V. 2020
//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

using System;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;
using Axual.Kafka.Proxy;
using Axual.Kafka.Proxy.SerDes;
using Axual.Kafka.Proxy.ValueHeader;
using Confluent.Kafka;
using NSubstitute;
using NUnit.Framework;
using static Axual.Kafka.Proxy.ValueHeader.Constants.AxualMessage.EnumProducerHeadersKey;

[assembly: InternalsVisibleTo("DynamicProxyGenAssembly2")]

namespace Axual.Kafka.Tests.SerDes
{
    [TestFixture]
    public class ValueHeaderDeserializerTests
    {
        [SetUp]
        public void SetUp()
        {
            // reset headers
            _serializationContextKey = new SerializationContext(MessageComponentType.Key, string.Empty, new Headers());
            _serializationContextValue =
                new SerializationContext(MessageComponentType.Value, string.Empty, new Headers());
            _serializeValue = Serializers.Int32.Serialize(ExpectedNumber, _serializationContextValue);
            _serializeValueAsValueHeader = _valueHeaderSerializer.Serialize(ExpectedNumber, _serializationContextValue);
        }

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            _valueHeaderDeserializer = new ValueHeaderDeserializer<int>(Deserializers.Int32);
            _valueHeaderSerializer = new ValueHeaderSerializer<int>(Serializers.Int32);
        }

        [OneTimeTearDown]
        public void OneTimeTearDown()
        {
            // nothing
        }

        private static readonly int ExpectedNumber = 0x9ABCDEF;
        private byte[] _serializeValueAsValueHeader;
        private byte[] _serializeValue;
        private IDeserializer<int> _valueHeaderDeserializer;
        private ISerializer<int> _valueHeaderSerializer;
        private SerializationContext _serializationContextKey;
        private SerializationContext _serializationContextValue;

        [Test]
        public void ValueHeaderDeserialize_DeserializeKeyAfterSerializeWithoutHeaders_KeysAreEqual()
        {
            Assert.Throws<SerializationException>(() => _valueHeaderDeserializer.Deserialize(
                _serializeValueAsValueHeader, _serializeValueAsValueHeader == null,
                _serializationContextKey));
        }

        [Test]
        public void ValueHeaderDeserialize_DeserializeValueAfterSerializeValueWithoutHeaders_ValuesAreEqual()
        {
            var value = _valueHeaderDeserializer.Deserialize(_serializeValueAsValueHeader,
                _serializeValueAsValueHeader == null,
                _serializationContextValue);

            Assert.AreEqual(ExpectedNumber, value);
        }

        [Test]
        public void ValueHeaderDeserialize_DeserializeValueHeaderKeyAfterSerialize_ThrowSerializationException()
        {
            Assert.Throws<SerializationException>(() => _valueHeaderDeserializer.Deserialize(
                _serializeValueAsValueHeader, _serializeValueAsValueHeader == null,
                _serializationContextKey));
        }

        [Test]
        public void ValueHeaderDeserialize_MessageSmallerThanMinSizeOfMessage_ThrowInvalidDataException()
        {
            Assert.Throws<InvalidDataException>(() =>
            {
                var messageWithWrongMagicBytes = new ReadOnlySpan<byte>(new byte[] {0x0B, 0xEB, 0x00, 0x00});
                _valueHeaderDeserializer.Deserialize(messageWithWrongMagicBytes, false, _serializationContextValue);
            });
        }

        [Test]
        public void ValueHeaderDeserialize_NullValue_ThrowArgumentNullException()
        {
            Assert.Throws<ArgumentNullException>(() =>
            {
                _valueHeaderDeserializer.Deserialize(null, true, _serializationContextValue);
            });
        }

        [Test]
        public void ValueHeaderDeserialize_ValueByteMessage_DeserializeValuesCorrect()
        {
            var deserializer = new ValueHeaderDeserializer<byte[]>(Deserializers.ByteArray);
            var data = new[] {(byte) 1};
            var rawData =
                Serializers.ByteArray.Serialize(data, _serializationContextValue);
            var value =
                deserializer.Deserialize(rawData, false, _serializationContextValue);

            Assert.AreEqual(data, value);
        }

        [Test]
        public void ValueHeaderDeserialize_ValueHeaderMessageVersion0_DeserializeValuesCorrect()
        {
            var rawData = UtilsHelper.CreateSerializeValueHeader(Constants.AxualMessage.AxualMagicUInt16Value,
                distributionLevelFlag: 0,
                messageId: AxualMessageId.Empty,
                payload: _serializeValue,
                versionNumber: Constants.AxualMessage.Versions.BaseVersion.VersionNumber);
            var value = _valueHeaderDeserializer.Deserialize(rawData, false, _serializationContextValue);

            // Check Value and Kafka Headers properties
            Assert.AreEqual(ExpectedNumber, value);
            Assert.AreEqual(AxualMessageId.Empty,
                AxualMessageId.Deserialize(
                    _serializationContextValue.Headers.Single(header => header.Key.Equals(MessageId.GetDescription()))
                        .GetValueBytes()));
            Assert.AreEqual(AxualCopyFlag.OriginalMessage,
                AxualCopyFlag.Deserialize(
                    _serializationContextValue.Headers.Single(header => header.Key.Equals(CopyFlags.GetDescription()))
                        .GetValueBytes()));
        }

        [Test]
        public void ValueHeaderDeserialize_ValueHeaderMessageVersion1_DeserializeValuesCorrect()
        {
            var expectedAxualMessageId = AxualMessageId.Empty;
            var expectedDistributionLevelFlag = AxualCopyFlag.OriginalMessage;

            var rawData = UtilsHelper.CreateSerializeValueHeader(
                Constants.AxualMessage.AxualMagicUInt16Value,
                distributionLevelFlag: 0,
                messageId: AxualMessageId.Empty,
                payload: _serializeValue,
                versionNumber: Constants.AxualMessage.Versions.Version1Number);
            var value = _valueHeaderDeserializer.Deserialize(rawData, false, _serializationContextValue);

            // Check Value and Kafka Headers properties
            Assert.AreEqual(ExpectedNumber, value);
            Assert.AreEqual(expectedAxualMessageId,
                AxualMessageId.Deserialize(
                    _serializationContextValue.Headers.Single(header => header.Key.Equals(MessageId.GetDescription()))
                        .GetValueBytes()));
            Assert.AreEqual(expectedDistributionLevelFlag,
                AxualCopyFlag.Deserialize(
                    _serializationContextValue.Headers.Single(header => header.Key.Equals(CopyFlags.GetDescription()))
                        .GetValueBytes()));
        }


        [Test]
        public void ValueHeaderDeserialize_ValueHeaderMessageVersion2_DeserializeValuesCorrect()
        {
            byte[] messageIdInBytes =
            {
                0x01, 0x02, 0x03, 0x04,
                0x05, 0x06, 0x07, 0x08,
                0x09, 0x0A, 0x0B, 0x0C,
                0x0D, 0x0E, 0x0F, 0x10
            };

            var expectedMessageId = AxualMessageId.Deserialize(messageIdInBytes);
            var expectedDistributionLevelFlag = AxualCopyFlag.OriginalMessage;

            var rawData = UtilsHelper.CreateSerializeValueHeader(
                Constants.AxualMessage.AxualMagicUInt16Value,
                distributionLevelFlag: 0,
                messageId: expectedMessageId,
                payload: _serializeValue,
                versionNumber: Constants.AxualMessage.Versions.Version2Number);
            var value = _valueHeaderDeserializer.Deserialize(rawData, false, _serializationContextValue);

            // Check valueHeader properties
            Assert.AreEqual(ExpectedNumber, value);
            Assert.AreEqual(expectedMessageId,
                AxualMessageId.Deserialize(
                    _serializationContextValue.Headers.Single(header => header.Key.Equals(MessageId.GetDescription()))
                        .GetValueBytes()));
            Assert.AreEqual(expectedDistributionLevelFlag,
                AxualCopyFlag.Deserialize(
                    _serializationContextValue.Headers.Single(header => header.Key.Equals(CopyFlags.GetDescription()))
                        .GetValueBytes()));
        }

        [Test]
        public void ValueHeaderDeserialize_ValueHeaderMessageVersion2WithWrongExtendedSize_DeserializeValuesCorrect()
        {
            var wrongHeaderSize = 500;
            var rawData = UtilsHelper.CreateSerializeValueHeader(
                Constants.AxualMessage.AxualMagicUInt16Value,
                distributionLevelFlag: 0,
                messageId: AxualMessageId.Empty,
                payload: _serializeValueAsValueHeader,
                versionNumber: Constants.AxualMessage.Versions.Version2Number, extendedHeader: wrongHeaderSize);

            var exception = Assert.Throws<InvalidDataException>(() =>
                _valueHeaderDeserializer.Deserialize(rawData, false, _serializationContextValue));

            Assert.AreEqual($"Encountered data of length {rawData.Length}. " +
                            "Which is even smaller than the expected header size: " +
                            $"{wrongHeaderSize + Constants.AxualMessage.Versions.BaseVersion.HeaderSize}.",
                exception.Message);
        }

        [Test]
        public void ValueHeaderDeserialize_ValueInt64Message_DeserializeValuesCorrect()
        {
            var deserializer = new ValueHeaderDeserializer<long>(Deserializers.Int64);
            var rawData =
                Serializers.Int64.Serialize(long.MaxValue, _serializationContextValue);
            var value =
                deserializer.Deserialize(rawData, false, _serializationContextValue);

            Assert.AreEqual(long.MaxValue, value);
        }

        [Test]
        public void ValueHeaderDeserialize_Configure_Deserializer_CallInnerDeserializeConfigure()
        {
            // Arrange
            var config = new ClientConfig();
            var innerDeserializer = Substitute.For<DummySerDes.IConfigurableDeserializer<string>>();
            IConfigurable valueHeaderDeserializer = new ValueHeaderDeserializer<string>(innerDeserializer);

            // Act
            valueHeaderDeserializer.Configure(config);

            // Assert called inner Deserializer.Configure();
            innerDeserializer.Received().Configure(Arg.Is<ClientConfig>(c => c == config));
        }

        [Test]
        public void ValueHeaderDeserialize_AsyncDeserializer_CallInnerDeserializeConfigure()
        {
            // Arrange
            var config = new ClientConfig();
            var innerAsyncDeserializer = Substitute.For<DummySerDes.IConfigurableAsyncDeserializer<string>>();
            IConfigurable valueHeaderDeserializer = new ValueHeaderDeserializer<string>(innerAsyncDeserializer);

            // Act
            valueHeaderDeserializer.Configure(config);

            // Assert called inner Deserializer.Configure();
            innerAsyncDeserializer.Received().Configure(Arg.Is<ClientConfig>(c => c == config));
        }
    }
}
