//
// Copyright 2020 Axual B.V. 2020
//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

using System;
using System.Threading.Tasks;
using Axual.Kafka.Proxy.SerDes;
using Confluent.Kafka;

namespace Axual.Kafka.Tests.SerDes
{
    internal static class DummySerDes
    {
        internal class DummySerializer<T> : ISerializer<T>
        {
            public byte[] Serialize(T data, SerializationContext context)
            {
                throw new NotImplementedException();
            }
        }
        
        internal class DummyAsyncSerializer<T> : IAsyncSerializer<T>
        {
            public Task<byte[]> SerializeAsync(T data, SerializationContext context)
            {
                throw new NotImplementedException();
            }
        }

        internal class DummyDeserializer<T>: IDeserializer<T> 
        {
            public T Deserialize(ReadOnlySpan<byte> data, bool isNull, SerializationContext context)
            {
                throw new NotImplementedException();
            }
        }
        
        internal class DummyAsyncDeserializer<T> : IAsyncDeserializer<T>
        {
            public Task<T> DeserializeAsync(ReadOnlyMemory<byte> data, bool isNull, SerializationContext context)
            {
                throw new NotImplementedException();
            }
        }
        
        internal interface IConfigurableDeserializer<T> : IConfigurable, IDeserializer<T>
        {
        }

        internal interface IConfigurableAsyncDeserializer<T> : IConfigurable, IAsyncDeserializer<T>
        {
        }
        
        internal interface IConfigurableSerializer<T> : IConfigurable, ISerializer<T>
        {
        }

        internal interface IConfigurableAsyncSerializer<T> : IConfigurable, IAsyncSerializer<T>
        {
        }
    }
}