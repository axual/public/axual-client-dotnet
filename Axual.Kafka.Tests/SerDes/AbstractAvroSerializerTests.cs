using Axual.Kafka.Proxy.Exceptions;
using Axual.SchemaRegistry.Serdes.Avro.Serializers;
using Confluent.Kafka;
using NUnit.Framework;

namespace Axual.Kafka.Tests.SerDes
{
    public class AbstractAvroSerializerTests<T>
    {
        protected AbstractAvroSerializer<T> AbstractAvroSerializer;

        [SetUp]
        public virtual void SetUp()
        {
        }

        [Test]
        public virtual void AbstractAvroSerializer_SerializeWithoutConfigure_ThrowsInvalidOperationException()
        {
        }

        [Test]
        public void AbstractAvroSerializer_ConfigureWithEmptyConfiguration_ThrowsRequireConfigurationException()
        {
            Assert.Throws<RequireConfigurationException>(() => AbstractAvroSerializer.Configure(new ClientConfig()));
        }

        [Test]
        public virtual void AbstractAvroSerializer_SerializeWithSRUrl_NoException()
        {
        }
    }
}