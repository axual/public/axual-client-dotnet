//
// Copyright 2020 Axual B.V. 2020
//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

using Avro.Generic;
using Avro.Specific;
using Axual.Kafka.Proxy.Proxies.Axual;
using Axual.SchemaRegistry.Serdes.Avro.Deserializers;
using Axual.SchemaRegistry.Serdes.Avro.Serializers;
using NUnit.Framework;

namespace Axual.Kafka.Tests.SerDes
{
    [TestFixture]
    internal class AvroSerDesTests
    {
        [SetUp]
        public void SetUp()
        {
            _axualConsumerConfig = new AxualConsumerConfig
            {
                ApplicationId = "appid",
                SchemaRegistryUrl = "https://sr-url.io"
            };

            _axualProducerConfig = new AxualProducerConfig
            {
                ApplicationId = "appid",
                SchemaRegistryUrl = "https://sr-url.io"
            };
        }

        private AxualConsumerConfig _axualConsumerConfig;
        private AxualProducerConfig _axualProducerConfig;


        [Test]
        public void GenericAvroDeserializer_Configure_NoException()
        {
            var genericAvroDeserializer = new GenericAvroDeserializer<GenericRecord>();
            genericAvroDeserializer.Configure(_axualConsumerConfig);

            Assert.Pass();
        }

        [Test]
        public void GenericAvroSerializer_Configure_NoException()
        {
            var genericAvroSerializer = new GenericAvroSerializer<GenericRecord>();
            genericAvroSerializer.Configure(_axualProducerConfig);

            Assert.Pass();
        }

        [Test]
        public void SpecificAvroDeserializer_Configure_NoException()
        {
            var specificAvroDeserializer = new SpecificAvroDeserializer<ISpecificRecord>();
            specificAvroDeserializer.Configure(_axualConsumerConfig);

            Assert.Pass();
        }

        [Test]
        public void SpecificAvroSerializer_Configure_NoException()
        {
            var specificAvroSerializer = new SpecificAvroSerializer<ISpecificRecord>();
            specificAvroSerializer.Configure(_axualProducerConfig);

            Assert.Pass();
        }
    }
}