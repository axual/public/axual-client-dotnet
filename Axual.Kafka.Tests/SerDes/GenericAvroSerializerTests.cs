using System;
using System.Collections.Generic;
using System.IO;
using Avro;
using Avro.Generic;
using Axual.Kafka.Proxy.Proxies;
using Axual.SchemaRegistry.Serdes.Avro.Serializers;
using Confluent.Kafka;
using NUnit.Framework;

namespace Axual.Kafka.Tests.SerDes
{
    public class GenericAvroSerializerTests : AbstractAvroSerializerTests<GenericRecord>
    {
        private RecordSchema _applicationLogEventSchema;

        private readonly string _path =
            Path.Combine(Directory.GetParent(Environment.CurrentDirectory).Parent.Parent.FullName, "resources", "avro",
                "io.axual.client.example.schema.ApplicationLogEvent.dotnet.avsc");

        [SetUp]
        public override void SetUp()
        {
            _applicationLogEventSchema = (RecordSchema) Schema.Parse(File.ReadAllText(_path));
            AbstractAvroSerializer = new GenericAvroSerializer<GenericRecord>();
        }

        [Test]
        public override void AbstractAvroSerializer_SerializeWithoutConfigure_ThrowsInvalidOperationException()
        {
            Assert.Throws<InvalidOperationException>(() =>
                AbstractAvroSerializer.SerializeAsync(new GenericRecord(_applicationLogEventSchema),
                    new SerializationContext()));
        }

        [Test]
        public override void AbstractAvroSerializer_SerializeWithSRUrl_NoException()
        {
            AbstractAvroSerializer.Configure(new ClientConfig(new Dictionary<string, string>
            {
                {Constants.ConfigurationKeys.Kafka.SchemaRegistryUrl, new Uri("https://localhost").ToString()}
            }));

            AbstractAvroSerializer.SerializeAsync(new GenericRecord(_applicationLogEventSchema),
                new SerializationContext());
        }
    }
}