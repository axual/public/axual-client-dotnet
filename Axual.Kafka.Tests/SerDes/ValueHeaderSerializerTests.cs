//
// Copyright 2020 Axual B.V. 2020
//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading;
using Axual.Kafka.Proxy.SerDes;
using Axual.Kafka.Proxy.ValueHeader;
using Confluent.Kafka;
using NSubstitute;
using NUnit.Framework;
using static Axual.Kafka.Proxy.Utils;

namespace Axual.Kafka.Tests.SerDes
{
    [TestFixture]
    public class ValueHeaderSerializerTests
    {
        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            _number = int.MaxValue;
            _valueMessageSerializer = new ValueHeaderSerializer<int>(Serializers.Int32);
            _serializationContextKey = new SerializationContext(MessageComponentType.Key, string.Empty);
            _serializationContextValue = new SerializationContext(MessageComponentType.Value, string.Empty);

            _axualMagicBytesBigEndian =
                BitsUtils.BigEndian.GetValueBytesBigEndian(Constants.AxualMessage.AxualMagicUInt16Value);
            _version2BigEndian =
                BitsUtils.BigEndian.GetValueBytesBigEndian(Constants.AxualMessage.Versions.Version2Number);
            _extendedHeaderLengthBigEndian =
                BitsUtils.BigEndian.GetValueBytesBigEndian(Constants.AxualMessage.Versions
                    .Version2ExtendedHeaderSize);
            _numberBigEndian = BitsUtils.BigEndian.GetValueBytesBigEndian(_number);
        }

        private int _number;
        private ValueHeaderSerializer<int> _valueMessageSerializer;
        private SerializationContext _serializationContextKey;
        private SerializationContext _serializationContextValue;

        private static byte[] _axualMagicBytesBigEndian;
        private static byte[] _version2BigEndian;
        private static byte[] _extendedHeaderLengthBigEndian;
        private static byte[] _numberBigEndian;

        private static readonly byte[] _messageIdBigEndian =
        {
            0x01, 0x02, 0x03, 0x04,
            0x05, 0x06, 0x07, 0x08,
            0x09, 0x0A, 0x0B, 0x0C,
            0x0D, 0x0E, 0x0F, 0x10
        };

        private readonly AxualMessageId _axualMessageId = AxualMessageId.Deserialize(_messageIdBigEndian);
        private readonly AxualTime _serializationTime = AxualTime.Now;

        private IEnumerable<byte> GetMagicBytes(IEnumerable<byte> messageRawData)
        {
            return TakeFrom(messageRawData, Constants.AxualMessage.Offsets.MagicOffset, sizeof(ushort));
        }

        private IEnumerable<byte> GetVersionBytes(IEnumerable<byte> messageRawData)
        {
            return TakeFrom(messageRawData, Constants.AxualMessage.Offsets.VersionNumberOffset, sizeof(ushort));
        }

        private IEnumerable<byte> GetExtendedHeaderSizeBytes(IEnumerable<byte> messageRawData)
        {
            return TakeFrom(messageRawData, Constants.AxualMessage.Offsets.ExtendedHeaderSizeOffset, sizeof(int));
        }

        private IEnumerable<byte> GetMessageFlagsBytes(IEnumerable<byte> messageRawData)
        {
            return TakeFrom(messageRawData, Constants.AxualMessage.Offsets.MessageFlagsOffset, sizeof(byte));
        }

        private IEnumerable<byte> GetMessageIdBytes(IEnumerable<byte> messageRawData)
        {
            return TakeFrom(messageRawData, Constants.AxualMessage.Offsets.MessageIdOffset, 16);
        }

        private IEnumerable<byte> GetSerializationTimeBytes(IEnumerable<byte> messageRawData)
        {
            return TakeFrom(messageRawData, Constants.AxualMessage.Offsets.SerializationTimeOffset, sizeof(long));
        }

        private IEnumerable<byte> GetPayloadBytes(IEnumerable<byte> messageRawData, int payloadOffset)
        {
            return messageRawData.Skip(payloadOffset);
        }

        private static IEnumerable<T> TakeFrom<T>(IEnumerable<T> source, int startIndex, int count)
        {
            return source.Skip(startIndex).Take(count);
        }

        [Test]
        public void Serialize_ValueHeaderSerializer_SerializeDataOnly()
        {
            var serializeValue = _valueMessageSerializer.Serialize(_number, _serializationContextKey);

            Assert.AreEqual(sizeof(int), serializeValue.Length);
            Assert.AreEqual(_number, BitsUtils.BigEndian.ToInt32FromBigEndian(serializeValue, 0));
        }


        [Test]
        public void Serialize_ValueHeaderSerializerMessage_SerializeMessageConstruct()
        {
            var serializeValue = _valueMessageSerializer.Serialize(_number,
                _serializationContextValue);

            Assert.AreEqual(_axualMagicBytesBigEndian, GetMagicBytes(serializeValue),
                "Axual magic byte are not correct");
            Assert.AreEqual(_version2BigEndian, GetVersionBytes(serializeValue), "Version bytes are not correct");
            Assert.AreEqual(new byte[] {0b00000000}, GetMessageFlagsBytes(serializeValue),
                "Message flag bytes are not correct");

            // Message Id bytes are not GUID reversible
            new Guid(GetMessageIdBytes(serializeValue).ToArray());

            var extendedHeaderSizeBytes = GetExtendedHeaderSizeBytes(serializeValue);
            Assert.AreEqual(_extendedHeaderLengthBigEndian, extendedHeaderSizeBytes,
                "Extended Header Size bytes are not correct");

            var payloadOffset =
                BitsUtils.BigEndian.ToInt32FromBigEndian(_extendedHeaderLengthBigEndian.ToArray(), 0) +
                Constants.AxualMessage.Versions.BaseVersion.HeaderSize;
            Assert.AreEqual(_numberBigEndian, GetPayloadBytes(serializeValue, payloadOffset),
                "TVal value bytes are not correct");
        }

        [Test]
        public void Serialize_ValueHeaderSerializerMessage_SerializeTimestampValueCorrect()
        {
            Thread.CurrentThread.Join(1);
            var serializeValue = _valueMessageSerializer.Serialize(_number,
                _serializationContextValue).ToArray();
            Thread.CurrentThread.Join(1);
            Thread.Sleep(1000);
            var timeAfterSerialize = AxualTime.Now;
            var serializationTime = AxualTime.Deserialize(serializeValue
                .Skip(Constants.AxualMessage.Offsets.SerializationTimeOffset).Take(AxualTime.Size).ToArray());

            Assert.IsTrue(serializationTime < timeAfterSerialize,
                $"Serialize time should be before: {timeAfterSerialize}, but was should be before: {timeAfterSerialize}.");
        }

        [Test]
        public void Serialize_ValueHeaderWrongMessageComponentType_ThrowsSerializationException()
        {
            var delegateSerializer = new TestDelegate(() =>
                _valueMessageSerializer.Serialize(_number, new SerializationContext()));

            Assert.Throws<SerializationException>(delegateSerializer,
                $"Unexpected MessageComponentType type:{string.Empty}");
        }
        
        [Test]
        public void ValueHeaderDeserialize_Configure_Deserializer_CallInnerDeserializeConfigure()
        {
            // Arrange
            var config = new ClientConfig();
            var innerSerializer = Substitute.For<IConfigurableSerializer<string>>();
            IConfigurable valueHeaderDeserializer = new ValueHeaderSerializer<string>(innerSerializer);

            // Act
            valueHeaderDeserializer.Configure(config);

            // Assert called inner Serializer.Configure();
            innerSerializer.Received().Configure(Arg.Is<ClientConfig>(c => c == config));
        }

        [Test]
        public void ValueHeaderDeserialize_AsyncDeserializer_CallInnerDeserializeConfigure()
        {
            // Arrange
            var config = new ClientConfig();
            var innerAsyncSerializer = Substitute.For<IConfigurableAsyncSerializer<string>>();
            IConfigurable valueHeaderDeserializer = new ValueHeaderSerializer<string>(innerAsyncSerializer);

            // Act
            valueHeaderDeserializer.Configure(config);

            // Assert called inner Serializer.Configure();
            innerAsyncSerializer.Received().Configure(Arg.Is<ClientConfig>(c => c == config));
        }

        internal interface IConfigurableSerializer<T> : IConfigurable, ISerializer<T>
        {
        }

        internal interface IConfigurableAsyncSerializer<T> : IConfigurable, IAsyncSerializer<T>
        {
        }
    }
}