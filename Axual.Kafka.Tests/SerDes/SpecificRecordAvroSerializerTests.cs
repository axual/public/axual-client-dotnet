using System;
using System.Collections.Generic;
using Avro;
using Avro.Specific;
using Axual.Kafka.Proxy.Proxies;
using Axual.SchemaRegistry.Serdes.Avro.Serializers;
using Confluent.Kafka;
using NUnit.Framework;

namespace Axual.Kafka.Tests.SerDes
{
    public class SpecificRecordAvroSerializerTests: AbstractAvroSerializerTests<ISpecificRecord>
    {
        [SetUp]
        public override void SetUp()
        {
            AbstractAvroSerializer = new SpecificAvroSerializer<ISpecificRecord>();
        }
        
        [Test]
        public override void AbstractAvroSerializer_SerializeWithoutConfigure_ThrowsInvalidOperationException()
        {
            Assert.Throws<InvalidOperationException>(() =>
                AbstractAvroSerializer.SerializeAsync(new DummySpecificRecord(), new SerializationContext()));
        }
        
        [Test]
        public override void AbstractAvroSerializer_SerializeWithSRUrl_NoException()
        {
            AbstractAvroSerializer.Configure(new ClientConfig(new Dictionary<string, string>
            {
                {Constants.ConfigurationKeys.Kafka.SchemaRegistryUrl, new Uri("https://localhost").ToString()}
            }));

            AbstractAvroSerializer.SerializeAsync(new DummySpecificRecord(), new SerializationContext());
        }
        
        private class DummySpecificRecord : ISpecificRecord
        {
            public object Get(int fieldPos)
            {
                throw new NotImplementedException();
            }

            public void Put(int fieldPos, object fieldValue)
            {
                throw new NotImplementedException();
            }

            public Schema Schema { get; }
        }
    }
}