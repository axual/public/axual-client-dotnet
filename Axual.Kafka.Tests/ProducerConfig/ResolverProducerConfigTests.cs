using Axual.Kafka.Proxy.Proxies.Resolving;
using Confluent.Kafka;
using NUnit.Framework;

namespace Axual.Kafka.Tests.ProducerConfig
{
    [TestFixture]
    public class ResolverProducerConfigTests
    {
        [Test]
        public void ResolverProducerConfig_NoResolverOrPatternAreSet_ReturnNull()
        {
            var resolvingProducerConfig = new ResolvingProducerConfig(new ClientConfig());

            Assert.AreEqual(null, resolvingProducerConfig.TopicPattern);
            Assert.AreEqual(null, resolvingProducerConfig.TopicResolver);
            Assert.AreEqual(null, resolvingProducerConfig.TransactionalIdPattern);
            Assert.AreEqual(null, resolvingProducerConfig.TransactionalIdResolver);
        }
    }
}