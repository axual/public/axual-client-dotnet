using System;
using System.IO;
using Axual.Kafka.Proxy.Proxies.Axual;
using NUnit.Framework;

namespace Axual.Kafka.Tests.ProducerConfig
{
    [TestFixture]
    public class AxualProducerConfigTests
    {
        [Test]
        public virtual void AxualProducerConfig_EndPointSetAndGet_GetCorrectValue()
        {
            var axualProducerConfig = new AxualProducerConfig(new Confluent.Kafka.ProducerConfig())
                {EndPoint = new Uri("https://localhost/")};
            Assert.AreEqual(new Uri("https://localhost/"), axualProducerConfig.EndPoint);
        }

        [Test]
        public virtual void AxualProducerConfig_SchemaRegistryUrlSetAndGet_GetCorrectValue()
        {
            var axualProducerConfig = new AxualProducerConfig(new Confluent.Kafka.ProducerConfig())
                {SchemaRegistryUrl = "https://schema.registry.url/"};
            Assert.AreEqual("https://schema.registry.url/", axualProducerConfig.SchemaRegistryUrl);
        }

        [Test]
        public virtual void AxualProducerConfig_ApplicationIdSetAndGet_GetCorrectValue()
        {
            var guid = new Guid();
            var axualProducerConfig = new AxualProducerConfig(new Confluent.Kafka.ProducerConfig())
                {ApplicationId = guid.ToString()};
            Assert.AreEqual(guid.ToString(), axualProducerConfig.ApplicationId);
        }

        [Test]
        public virtual void AxualProducerConfig_ApplicationVersionSetAndGet_GetCorrectValue()
        {
            var axualProducerConfig = new AxualProducerConfig(new Confluent.Kafka.ProducerConfig())
                {ApplicationVersion = "test 1.0"};
            Assert.AreEqual("test 1.0", axualProducerConfig.ApplicationVersion);
        }

        [Test]
        public virtual void AxualProducerConfig_TenantSetAndGet_GetCorrectValue()
        {
            var axualProducerConfig = new AxualProducerConfig(new Confluent.Kafka.ProducerConfig())
                {Tenant = "tenantName"};
            Assert.AreEqual("tenantName", axualProducerConfig.Tenant);
        }

        [Test]
        public virtual void AxualProducerConfig_EnvironmentSetAndGet_GetCorrectValue()
        {
            var axualProducerConfig = new AxualProducerConfig(new Confluent.Kafka.ProducerConfig())
                {Environment = "environmentName"};
            Assert.AreEqual("environmentName", axualProducerConfig.Environment);
        }
        
        [Test]
        public virtual void AxualProducerConfig_PathCaLocation_GetCorrectValue()
        {
            string cerPath = UtilsHelper.GetFileFromResourcesPath("certificate.cer");
            var axualProducerConfig = new AxualProducerConfig(new Confluent.Kafka.ProducerConfig())
                {SslCaLocation = cerPath};
            Assert.AreEqual(cerPath, axualProducerConfig.SslCaLocation);
        }
        
        [Test]
        public virtual void AxualProducerConfig_PemFormatCaLocation_TempFileCreatedInTempFolder()
        {
            string pemFormat = File.ReadAllText(UtilsHelper.GetFileFromResourcesPath("certificate.cer"));
            var axualProducerConfig = new AxualProducerConfig(new Confluent.Kafka.ProducerConfig())
                {SslCaLocation = pemFormat};
            Assert.IsTrue(axualProducerConfig.SslCaLocation.StartsWith(Path.GetTempPath()));
        }
    }
}