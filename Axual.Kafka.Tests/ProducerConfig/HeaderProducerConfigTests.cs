using Axual.Kafka.Proxy.Proxies.Headers;
using Confluent.Kafka;
using NUnit.Framework;

namespace Axual.Kafka.Tests.ProducerConfig
{
    [TestFixture]
    public class HeaderProducerConfigTests
    {
        [Test]
        public void HeaderConsumerConfig_EnableValueHeaderSetAndGet_GetCorrectValue()
        {
            var headerProducerConfig = new HeaderProducerConfig(new ClientConfig()){EnableValueHeaders = true};
            Assert.AreEqual(true, headerProducerConfig.EnableValueHeaders);

            headerProducerConfig.EnableValueHeaders = false;
            Assert.AreEqual(false, headerProducerConfig.EnableValueHeaders);
        }
    }
}