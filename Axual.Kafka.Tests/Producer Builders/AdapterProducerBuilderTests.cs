using Axual.Kafka.Proxy.Proxies.Base;
using Axual.Kafka.Tests.SerDes;
using Confluent.Kafka;
using NSubstitute;
using NUnit.Framework;

namespace Axual.Kafka.Tests.Producer_Builders
{
    internal class AdapterProducerBuilderTests : ProducerBuildersTests<string, string>
    {
        [SetUp]
        public override void SetUp()
        {
            base.SetUp();
            
            ProducerBuilder = new AdapterProducerBuilder<string, string>(ProducerConfig)
            {
                NextProducerBuilder = MockNextProducerBuilder
            };
        }
        
        [Test]
        public void AdapterConsumerBuilder_BuildKeyValueSerializer_NoException()
        {
            var keySerializer = Substitute.For<ISerializer<string>>();
            var valueSerializer = Substitute.For<ISerializer<string>>();
            
            new AdapterProducerBuilder<string, string>(ProducerConfig)
                .SetKeySerializer(keySerializer)
                .SetValueSerializer(valueSerializer)
                .Build();
        }
        
        [Test]
        public void AdapterConsumerBuilder_BuildKeyValueAsyncSerializer_NoException()
        {
            var keySerializer = Substitute.For<IAsyncSerializer<string>>();
            var valueSerializer = Substitute.For<IAsyncSerializer<string>>();
            
            new AdapterProducerBuilder<string, string>(ProducerConfig)
                .SetKeySerializer(keySerializer)
                .SetValueSerializer(valueSerializer)
                .Build();
        }
        
        [Test]
        public void AdapterConsumerBuilder_BuildAsyncKeyValueIConfigurableSerializer_NoException()
        {
            var keySerializer = Substitute.For<DummySerDes.IConfigurableSerializer<string>>();
            var valueSerializer = Substitute.For<DummySerDes.IConfigurableSerializer<string>>();

            new AdapterProducerBuilder<string, string>(ProducerConfig)
                .SetKeySerializer(keySerializer)
                .SetValueSerializer(valueSerializer)
                .Build();
            
            // Assert
            keySerializer.Received(1).Configure(ProducerConfig);
            valueSerializer.Received(1).Configure(ProducerConfig);
        }
        
        [Test]
        public void AdapterConsumerBuilder_BuildAsyncKeyValueIConfigurableAsyncSerializer_NoException()
        {
            var keySerializer = Substitute.For<DummySerDes.IConfigurableAsyncSerializer<string>>();
            var valueSerializer = Substitute.For<DummySerDes.IConfigurableAsyncSerializer<string>>();

            new AdapterProducerBuilder<string, string>(ProducerConfig)
                .SetKeySerializer(keySerializer)
                .SetValueSerializer(valueSerializer)
                .Build();
            
            // Assert
            keySerializer.Received(1).Configure(ProducerConfig);
            valueSerializer.Received(1).Configure(ProducerConfig);
        }
    }
}