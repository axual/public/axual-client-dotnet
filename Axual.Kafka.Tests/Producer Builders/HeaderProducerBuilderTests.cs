//
// Copyright 2020 Axual B.V. 2020
//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

using Axual.Kafka.Proxy.Proxies.Headers;
using Axual.Kafka.Proxy.SerDes;
using Axual.Kafka.Tests.SerDes;
using Confluent.Kafka;
using NSubstitute;
using NUnit.Framework;

namespace Axual.Kafka.Tests.Producer_Builders
{
    internal class HeaderProducerBuilderTests : ProducerBuildersTests<string, string>
    {
        [SetUp]
        public override void SetUp()
        {
            base.SetUp();
            ProducerBuilder = new HeaderProducerBuilder<string, string>(ProducerConfig, MockNextProducerBuilder)
                .SetKeySerializer(Serializers.Utf8)
                .SetValueSerializer(Serializers.Utf8);
        }

        [Test]
        public void HeaderProducerBuilder_IsEnableValueHeadersIsSet_SetValueHeaderSerializer()
        {
            var headerProducerConfig = new HeaderProducerConfig(ProducerConfig) {EnableValueHeaders = true};
            ProducerBuilder = new HeaderProducerBuilder<string, string>(headerProducerConfig, MockNextProducerBuilder)
                .SetKeySerializer(new DummySerDes.DummySerializer<string>())
                .SetValueSerializer(new DummySerDes.DummySerializer<string>());

            ProducerBuilder.Build();

            // Assert wrapped DummySerializer with ValueHeaderSerializer
            MockNextProducerBuilder.Received(1).SetValueSerializer(
                Arg.Is<ISerializer<string>>(configurableSerializer =>
                    configurableSerializer.GetType() == typeof(ValueHeaderSerializer<string>)));
        }

        [Test]
        public void HeaderProducerBuilder_IsEnableValueHeadersIsNotSet_NotSetValueHeaderSerializer()
        {
            var headerProducerConfig = new HeaderProducerConfig(ProducerConfig) {EnableValueHeaders = false};
            ProducerBuilder = new HeaderProducerBuilder<string, string>(headerProducerConfig, MockNextProducerBuilder)
                .SetKeySerializer(new DummySerDes.DummySerializer<string>())
                .SetValueSerializer(new DummySerDes.DummySerializer<string>());

            ProducerBuilder.Build();
  
            // Assert kept DummyAsyncDeserializer as ValueDeserializer
            MockNextProducerBuilder.Received(1).SetValueSerializer(
                Arg.Is<ISerializer<string>>(configurableSerializer =>
                    configurableSerializer.GetType() == typeof(DummySerDes.DummySerializer<string>)));
        }
        
        [Test]
        public void HeaderProducerBuilder_IsEnableValueHeadersIsSetWithAsyncValueDeserializer_SetValueHeaderSerializer()
        {
            var headerProducerConfig = new HeaderProducerConfig(ProducerConfig) {EnableValueHeaders = true};
            ProducerBuilder = new HeaderProducerBuilder<string, string>(headerProducerConfig, MockNextProducerBuilder)
                .SetKeySerializer(new DummySerDes.DummyAsyncSerializer<string>())
                .SetValueSerializer(new DummySerDes.DummyAsyncSerializer<string>());

            ProducerBuilder.Build();

            // Assert wrapped DummyAsyncSerializer with ValueHeaderSerializer (ISerializer)
            MockNextProducerBuilder.Received(1).SetValueSerializer(
                Arg.Is<ISerializer<string>>(configurableSerializer =>
                    configurableSerializer.GetType() == typeof(ValueHeaderSerializer<string>)));
        }

        [Test]
        public void HeaderProducerBuilder_IsEnableValueHeadersIsNotSetWithAsyncValueDeserializer_NotSetValueHeaderSerializer()
        {
            var headerProducerConfig = new HeaderProducerConfig(ProducerConfig) {EnableValueHeaders = false};
            ProducerBuilder = new HeaderProducerBuilder<string, string>(headerProducerConfig, MockNextProducerBuilder)
                .SetKeySerializer(new DummySerDes.DummyAsyncSerializer<string>())
                .SetValueSerializer(new DummySerDes.DummyAsyncSerializer<string>());

            ProducerBuilder.Build();
  
            // Assert kept DummyAsyncDeserializer as ValueDeserializer
            MockNextProducerBuilder.Received(1).SetValueSerializer(
                Arg.Is<IAsyncSerializer<string>>(configurableSerializer =>
                    configurableSerializer.GetType() == typeof(DummySerDes.DummyAsyncSerializer<string>)));
        }
    }
}