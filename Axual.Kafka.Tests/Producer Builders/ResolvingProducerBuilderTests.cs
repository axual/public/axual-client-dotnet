//
// Copyright 2020 Axual B.V. 2020
//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

using Axual.Kafka.Proxy.Exceptions;
using Axual.Kafka.Proxy.Proxies;
using Axual.Kafka.Proxy.Proxies.Resolving;
using Confluent.Kafka;
using NUnit.Framework;

namespace Axual.Kafka.Tests.Producer_Builders
{
    internal class ResolvingProducerBuilderTests : ProducerBuildersTests<string, string>
    {
        private const string ApplicationId = "ResolvingProducerBuilderTests";
        private const string Environment = "dev";
        private const string Tenant = "local";
        private const string Instance = "axual";
        private const string TransactionalId = "transactionalA";
        
        [SetUp]
        public override void SetUp()
        {
            base.SetUp();

            ProducerConfig = new ResolvingProducerConfig(ProducerConfig)
            {
                ApplicationId = ApplicationId,
                Environment = Environment,
                Tenant = Tenant,
                Instance = Instance,
                TransactionalId = "TransactionalId",
                TopicResolver = $"{Constants.JavaResolverPath}TopicPatternResolver",
                TopicPattern = $"{{{Constants.ConfigurationKeys.Axual.Tenant}}}-" +
                               $"{{{Constants.ConfigurationKeys.Axual.Environment}}}-" +
                               $"{{{Constants.ConfigurationKeys.Resolver.TopicResolverKey}}}",
                TransactionalIdResolver = $"{Constants.JavaResolverPath}TransactionalIdPatternResolver",
                TransactionalIdPattern = $"{{{Constants.ConfigurationKeys.Axual.Tenant}}}-" +
                                         $"{{{Constants.ConfigurationKeys.Axual.Environment}}}-" +
                                         $"{{{Constants.ConfigurationKeys.Axual.ApplicationId}}}-" +
                                         $"{{{Constants.ConfigurationKeys.Kafka.TransactionalIdKey}}}"
            };

            ProducerBuilder = new ResolvingProducerBuilder<string, string>(ProducerConfig, MockNextProducerBuilder)
                .SetKeySerializer(Serializers.Utf8)
                .SetValueSerializer(Serializers.Utf8);
        }

        [Test]
        public void ResolvingProducerBuilder_ValidateMissingEnvironment_ThrowRequireConfigurationException()
        {
            ProducerConfig = new ResolvingProducerConfig(ProducerConfig)
            {
                Environment = null
            };
            ProducerBuilder = new ResolvingProducerBuilder<string, string>(ProducerConfig, MockNextProducerBuilder);

            var exception = Assert.Throws<RequireConfigurationException>(() => ProducerBuilder.Validate());
            Assert.That(exception.Message,
                Is.EqualTo(string.Format(
                    RequireConfigurationException.RequireConfigurationExceptionMessageFormat,
                    Constants.ConfigurationKeys.Axual.Environment)));
        }

        [Test]
        public void ResolvingProducerBuilder_ValidateMissingInstance_ThrowRequireConfigurationException()
        {
            ProducerConfig = new ResolvingProducerConfig(ProducerConfig)
            {
                Instance = null
            };
            ProducerBuilder = new ResolvingProducerBuilder<string, string>(ProducerConfig, MockNextProducerBuilder);

            var exception = Assert.Throws<RequireConfigurationException>(() => ProducerBuilder.Validate());
            Assert.That(exception.Message,
                Is.EqualTo(string.Format(
                    RequireConfigurationException.RequireConfigurationExceptionMessageFormat,
                    Constants.ConfigurationKeys.Axual.Instance)));
        }

        [Test]
        public void ResolvingProducerBuilder_ValidateMissingTenant_ThrowRequireConfigurationException()
        {
            ProducerConfig = new ResolvingProducerConfig(ProducerConfig)
            {
                Tenant = null
            };
            ProducerBuilder = new ResolvingProducerBuilder<string, string>(ProducerConfig, MockNextProducerBuilder);

            var exception = Assert.Throws<RequireConfigurationException>(() => ProducerBuilder.Validate());
            Assert.That(exception.Message,
                Is.EqualTo(string.Format(
                    RequireConfigurationException.RequireConfigurationExceptionMessageFormat,
                    Constants.ConfigurationKeys.Axual.Tenant)));
        }
        
        [Test]
        public void ResolvingProducerBuilder_ValidateMissingTopicPattern_ThrowRequireConfigurationException()
        {
            ProducerConfig = new ResolvingProducerConfig(ProducerConfig)
            {
                TopicPattern = null
            };
            ProducerBuilder = new ResolvingProducerBuilder<string, string>(ProducerConfig, MockNextProducerBuilder);

            var exception = Assert.Throws<RequireConfigurationException>(() => ProducerBuilder.Validate());
            Assert.That(exception.Message,
                Is.EqualTo(string.Format(
                    RequireConfigurationException.RequireConfigurationExceptionMessageFormat,
                    Constants.ConfigurationKeys.Resolver.TopicPattern)));
        }
        
        [Test]
        public void ResolvingProducerBuilder_ValidateMissingTopicResolver_ThrowRequireConfigurationException()
        {
            ProducerConfig = new ResolvingProducerConfig(ProducerConfig)
            {
                TopicResolver = null
            };
            ProducerBuilder = new ResolvingProducerBuilder<string, string>(ProducerConfig, MockNextProducerBuilder);

            var exception = Assert.Throws<RequireConfigurationException>(() => ProducerBuilder.Validate());
            Assert.That(exception.Message,
                Is.EqualTo(string.Format(
                    RequireConfigurationException.RequireConfigurationExceptionMessageFormat,
                    Constants.ConfigurationKeys.Resolver.TopicResolver)));
        }

        [Test]
        public void ResolvingProducerBuilder_BuildTransactionalIdSet_ResolvingTransactionalId()
        {
            ProducerConfig.TransactionalId = TransactionalId;

            ProducerBuilder = new ResolvingProducerBuilder<string, string>(ProducerConfig, MockNextProducerBuilder);
            ProducerBuilder.Build();

            Assert.AreEqual($"{Tenant}-{Environment}-{ApplicationId}-{TransactionalId}",
                ProducerConfig.TransactionalId);
        }
        
        [Test]
        public void ResolvingProducerBuilder_Build_ThrowRequireConfigurationExceptionForTransactionalIdPattern()
        {
            var resolvingProducerConfig = new ResolvingProducerConfig(ProducerConfig) {TransactionalIdPattern = null};
            ProducerBuilder =
                new ResolvingProducerBuilder<string, string>(resolvingProducerConfig, MockNextProducerBuilder);

            var exception = Assert.Throws<RequireConfigurationException>(() => ProducerBuilder.Validate());
            Assert.That(exception.Message,
                Is.EqualTo(string.Format(
                    RequireConfigurationException.RequireConfigurationExceptionMessageFormat,
                    Constants.ConfigurationKeys.Resolver.TransactionalIdPattern)));
        }

        [Test]
        public void
            ResolvingProducerBuilder_BuildTransactionalIdSet_ThrowRequireConfigurationExceptionForTransactionalIdPattern()
        {
            var resolvingProducerConfig = new ResolvingProducerConfig(ProducerConfig) {TransactionalIdPattern = null};
            ProducerBuilder =
                new ResolvingProducerBuilder<string, string>(resolvingProducerConfig, MockNextProducerBuilder);

            var exception = Assert.Throws<RequireConfigurationException>(() => ProducerBuilder.Validate());
            Assert.That(exception.Message,
                Is.EqualTo(string.Format(
                    RequireConfigurationException.RequireConfigurationExceptionMessageFormat,
                    Constants.ConfigurationKeys.Resolver.TransactionalIdPattern)));
        }

        [Test]
        public void
            ResolvingProducerBuilder_BuildTransactionalIdSet_ThrowRequireConfigurationExceptionForTransactionalIdResolver()
        {
            var resolvingProducerConfig = new ResolvingProducerConfig(ProducerConfig) {TransactionalIdResolver = null};
            ProducerBuilder =
                new ResolvingProducerBuilder<string, string>(resolvingProducerConfig, MockNextProducerBuilder);

            var exception = Assert.Throws<RequireConfigurationException>(() => ProducerBuilder.Validate());
            Assert.That(exception.Message,
                Is.EqualTo(string.Format(
                    RequireConfigurationException.RequireConfigurationExceptionMessageFormat,
                    Constants.ConfigurationKeys.Resolver.TransactionalIdResolver)));
        }
    }
}
