//
// Copyright 2020 Axual B.V. 2020
//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

using System;
using Axual.Kafka.Proxy.Exceptions;
using Axual.Kafka.Proxy.Proxies;
using Axual.Kafka.Proxy.Proxies.Axual;
using Confluent.Kafka;
using NUnit.Framework;

namespace Axual.Kafka.Tests.Producer_Builders
{
    [TestFixture]
    internal class AxualProducerBuilderTests : ProducerBuildersTests<string, string>
    {
        private const string ApplicationId = "AxualProducerBuilderTests";
        private static readonly Uri EndPoint = new UriBuilder("http", "localhost").Uri;
        
        [SetUp]
        public override void SetUp()
        {
            base.SetUp();

            ProducerConfig = new AxualProducerConfig(ProducerConfig)
            {
                ApplicationId = ApplicationId,
                EndPoint = EndPoint
            };

            ProducerBuilder = new AxualProducerBuilder<string, string>(ProducerConfig)
                {
                    NextProducerBuilder = MockNextProducerBuilder
                }
                .SetKeySerializer(Serializers.Utf8)
                .SetValueSerializer(Serializers.Utf8);
        }

        [Test]
        public void BaseProducerBuilder_ValidateMissingApplicationId_ThrowRequireConfigurationException()
        {
            var axualProducerConfig = new AxualProducerConfig(ProducerConfig);
            var temp = axualProducerConfig.ApplicationId;
            axualProducerConfig.ApplicationId = null;

            var exception = Assert.Throws<RequireConfigurationException>(() => ProducerBuilder.Validate());

            Assert.That(exception.Message,
                Is.EqualTo(string.Format(
                    RequireConfigurationException.RequireConfigurationExceptionMessageFormat,
                    Constants.ConfigurationKeys.Axual.ApplicationId)));

            // Set back the BootstrapServers
            axualProducerConfig.ApplicationId = temp;
        }
    }
}