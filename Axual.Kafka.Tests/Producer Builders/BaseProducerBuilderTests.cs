//
// Copyright 2020 Axual B.V. 2020
//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

using Axual.Kafka.Proxy.Exceptions;
using Axual.Kafka.Proxy.Proxies;
using Axual.Kafka.Proxy.Proxies.Base;
using Confluent.Kafka;
using NUnit.Framework;

namespace Axual.Kafka.Tests.Producer_Builders
{
    internal class BaseProducerBuilderTests : ProducerBuildersTests<string, string>
    {
        
        [SetUp]
        public override void SetUp()
        {
            base.SetUp();
            ProducerConfig.BootstrapServers = "BootstrapServers";
            ProducerBuilder = new BaseProducerBuilder<string, string>(ProducerConfig, MockNextProducerBuilder)
                .SetKeySerializer(Serializers.Utf8)
                .SetValueSerializer(Serializers.Utf8);
        }

        [Test]
        public void BaseProducerBuilder_ValidateMissingBootstrapServers_ThrowRequireConfigurationException()
        {
            var producerBuilder = new BaseProducerBuilder<string, string>(
                    new Confluent.Kafka.ProducerConfig(ProducerConfig) {BootstrapServers = null},
                    MockNextProducerBuilder)
                .SetKeySerializer(Serializers.Utf8)
                .SetValueSerializer(Serializers.Utf8);

            var exception = Assert.Throws<RequireConfigurationException>(() => producerBuilder.Validate());

            Assert.That(exception.Message,
                Is.EqualTo(string.Format(
                    RequireConfigurationException.RequireConfigurationExceptionMessageFormat,
                    Constants.ConfigurationKeys.Kafka.BootstrapServers)));
        }

        [Test]
        public void BaseProducerBuilders_PartitionAssignmentStrategyNotDefined_ChangeToMurmur2Random()
        {
            Assert.AreEqual(Partitioner.Murmur2Random, ProducerBuilder.ProducerConfig.Partitioner);
        }
        
        [Test]
        public void BaseConsumerBuilder_ValidateMissingValueDeserializer_ThrowRequireConfigurationException()
        {
            ProducerBuilder = new BaseProducerBuilder<string, string>(ProducerConfig, MockNextProducerBuilder)
                .SetKeySerializer(Serializers.Utf8);

            var exception = Assert.Throws<RequireConfigurationException>(() => ProducerBuilder.Validate());
                
            Assert.That(exception.Message,
                Is.EqualTo(string.Format(
                    RequireConfigurationException.RequireConfigurationExceptionMessageFormat,
                    "Value Serializer / Async Value Serializer")));
        }
        
        [Test]
        public void BaseConsumerBuilder_ValidateMissingKeyDeserializer_ThrowRequireConfigurationException()
        {
            ProducerBuilder = new BaseProducerBuilder<string, string>(ProducerConfig, MockNextProducerBuilder)
                .SetValueSerializer(Serializers.Utf8);

            var exception = Assert.Throws<RequireConfigurationException>(() => ProducerBuilder.Validate());
                
            Assert.That(exception.Message,
                Is.EqualTo(string.Format(
                    RequireConfigurationException.RequireConfigurationExceptionMessageFormat,
                    "Key Serializer / Async Key Serializer")));
        }
    }
}