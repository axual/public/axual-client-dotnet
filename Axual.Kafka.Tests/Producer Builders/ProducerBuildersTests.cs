//
// Copyright 2020 Axual B.V. 2020
//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

using System;
using Axual.Kafka.Proxy.Proxies;
using Confluent.Kafka;
using NSubstitute;
using NUnit.Framework;

namespace Axual.Kafka.Tests.Producer_Builders
{
    [TestFixture]
    internal abstract class ProducerBuildersTests<TKey, TValue>
    {
        protected Confluent.Kafka.ProducerConfig ProducerConfig;
        protected IProducer<TKey, TValue> MockNextProducer;
        protected IProducerBuilder<TKey, TValue> ProducerBuilder;
        protected IProducerBuilder<TKey, TValue> MockNextProducerBuilder;
        
        [SetUp]
        public virtual void SetUp()
        {
            ProducerConfig = new Confluent.Kafka.ProducerConfig();
            MockNextProducer = Substitute.For<IProducer<TKey, TValue>>();
            MockNextProducerBuilder = Substitute.For<IProducerBuilder<TKey, TValue>>();
            MockNextProducerBuilder.Build().Returns(MockNextProducer);
            MockNextProducerBuilder.Rebuild(Arg.Any<Confluent.Kafka.ProducerConfig>()).Returns(MockNextProducer);
        }

        private void ErrorHandler(IProducer<TKey, TValue> producer, Error arg2)
        {
            throw new NotImplementedException();
        }

        private void LogHandler(IProducer<TKey, TValue> producer, LogMessage arg2)
        {
            throw new NotImplementedException();
        }

        private void StatisticsHandler(IProducer<TKey, TValue> producer, string arg2)
        {
            throw new NotImplementedException();
        }

        [Test]
        public virtual void ProducerBuilder_Build_NoException()
        {
            ProducerBuilder.Build();

            Assert.Pass();
        }

        [Test]
        public virtual void ProducerBuilder_Rebuild_RebuildConsumerBuilder()
        {
            ProducerBuilder.Rebuild(ProducerConfig);

            Assert.Pass();
        }

        [Test]
        public virtual void ProducerBuilder_RebuildAfterConfigurationChange_ConfigurationChanged()
        {
            var newConfig = new Confluent.Kafka.ProducerConfig(ProducerConfig)
            {
                ClientId = "TEST"
            };
            var newConsumer = ProducerBuilder.Rebuild(newConfig);

            Assert.AreEqual("TEST", ProducerBuilder.ProducerConfig.ClientId);
        }

        [Test]
        public void ProducerBuilder_SetErrorHandler_NoException()
        {
            ProducerBuilder.SetErrorHandler(ErrorHandler)
                .Build();

            Assert.Pass();
        }

        [Test]
        public void ProducerBuilder_SetLogHandler_NoException()
        {
            ProducerBuilder.SetLogHandler(LogHandler)
                .Build();

            Assert.Pass();
        }

        [Test]
        public void ProducerBuilder_SetStatisticsHandler_NoException()
        {
            ProducerBuilder.SetStatisticsHandler(StatisticsHandler)
                .Build();

            Assert.Pass();
        }

        [Test]
        public virtual void ProducerBuilder_Validate_NoException()
        {
            ProducerBuilder.Validate();
        }
    }
}