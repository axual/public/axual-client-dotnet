//
// Copyright 2020 Axual B.V. 2020
//
// Licensed to the Apache Software Foundation (ASF) under one
// or more contributor license agreements.  See the NOTICE file
// distributed with this work for additional information
// regarding copyright ownership.  The ASF licenses this file
// to you under the Apache License, Version 2.0 (the
// "License"); you may not use this file except in compliance
// with the License.  You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing,
// software distributed under the License is distributed on an
// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, either express or implied.  See the License for the
// specific language governing permissions and limitations
// under the License.

using System.Collections.Generic;
using System.IO;
using Axual.Kafka.Proxy.ValueHeader;
using BitConverter;

namespace Axual.Kafka.Tests
{
    internal static class UtilsHelper
    {
        private static readonly byte[] DefaultMagicBytesBigEndian = {0x0B, 0xEB};
        private static readonly byte[] DefaultVersionBigEndian = {0x00, 0x02};
        private static readonly byte[] DefaultExtendedHeaderBigEndian = {0x00, 0x00, 0x00, 0x19};
        private static readonly byte[] DefaultMessageFlagBigEndian = {0x00};

        private static readonly byte[] DefaultMessageIdBigEndian =
        {
            0x01, 0x02, 0x03, 0x04,
            0x05, 0x06, 0x07, 0x08,
            0x09, 0x0A, 0x0B, 0x0C,
            0x0D, 0x0E, 0x0F, 0x10
        };

        private static readonly byte[] DefaultTimestampBigEndian =
        {
            0x01, 0x02, 0x03, 0x04,
            0x05, 0x06, 0x07, 0x08
        };

        private static readonly byte[] DefaultPayload = { };

        internal static byte[] CreateSerializeValueHeader(ushort? magicByte,
            AxualMessageId messageId, byte distributionLevelFlag, AxualTime? axualTimeStamp = null,
            byte[] payload = null,
            ushort? versionNumber = Constants.AxualMessage.Versions.Version2Number,
            int? extendedHeader = null)
        {
            var version = Constants.AxualMessage.Versions.AxualMessageVersions[(int) versionNumber];
            var endianBitConverter = EndianBitConverter.BigEndian;
            var magicByteInBytes =
                endianBitConverter.GetBytes(magicByte ?? Constants.AxualMessage.AxualMagicUInt16Value);
            var versionInBytes = endianBitConverter.GetBytes(version.VersionNumber);
            var extendedHeaderInBytes = endianBitConverter.GetBytes(
                extendedHeader ?? Constants.AxualMessage.Versions.AxualMessageVersions[(int) versionNumber]
                    .ExtendedHeaderSize);
            var messageIdInBytes = messageId.Serialize();
            var timestampInBytes = axualTimeStamp == null ? AxualTime.Now.Serialize() : axualTimeStamp.Serialize();


            var serializeValue = new List<byte>();
            serializeValue.AddRange(magicByteInBytes); // (0)    Magic Byte
            serializeValue.AddRange(versionInBytes); // (2)    Version (short - 2 Bytes)
            serializeValue.AddRange(extendedHeaderInBytes); // (4)    Extended header length (int - 4 Bytes)
            if (version.VersionNumber >= Constants.AxualMessage.Versions.Version1Number)
            {
                serializeValue.Add(distributionLevelFlag); // (8)    Message flag bit (Byte)
                if (version.VersionNumber >= Constants.AxualMessage.Versions.Version2Number)
                {
                    serializeValue.AddRange(messageIdInBytes); // (9)    Message id (16 bytes)
                    serializeValue.AddRange(timestampInBytes); // (25)   Serialization timestamp (long - 8 Bytes)
                }
            }

            serializeValue.AddRange(payload ?? DefaultPayload); // (33) Payload

            return serializeValue.ToArray();
        }

        internal static string GetFileFromResourcesPath(string file)
        {
            return Path.Combine(new DirectoryInfo(Directory.GetCurrentDirectory()).Parent.Parent.Parent.FullName,
                "resources", file);
        }
    }
}