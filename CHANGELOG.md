# [master]
## Enhancements

# [1.5.1]
## Enhancements
- [Issue #37](https://gitlab.com/axual/public/axual-client-dotnet/-/issues/37) - Create explicit libraries for DotNet 6

# [1.5.0]
## Enhancements
- [Issue #33](https://gitlab.com/axual/public/axual-client-dotnet/-/issues/33) - Downgrade Confluent Client to 1.7.0 to allow PKCS12 files with RSA
- [Issue #30](https://gitlab.com/axual/public/axual-client-dotnet/-/issues/30) - Producer/Consumer leaves resources open on Close/Dispose causing locks on quick recreate
- [Issue #31](https://gitlab.com/axual/public/axual-client-dotnet/-/issues/31) - Bring TLS configuration options of Schema Registry Client in sync with Discovery Service
- Update Build Pipeline to accomodate release candidate builds

# [1.4.3]
## Fixes
- [Issue #27](https://gitlab.com/axual/public/axual-client-dotnet/-/issues/27) - Discovery Service client should load version header from version resource
- [Issue #28](https://gitlab.com/axual/public/axual-client-dotnet/-/issues/28) - Setting `EnableSslCertificateVerification` ignored for HTTP Connections

# [1.4.2]
## Fixes
- [Issue #26](https://gitlab.com/axual/public/axual-client-dotnet/-/issues/26) - Client fails with certificate error on Windows when using PEM files
- [Issue #22](https://gitlab.com/axual/public/axual-client-dotnet/-/issues/22) - Client fails with certificate error on Windows when using PEM strings

# 1.4.1
## Fixes
- [Issue #20](https://gitlab.com/axual/public/axual-client-dotnet/-/issues/20) - Consumed message lost upon using CancellationToken
- Updated Apache.Avro to v1.11.0 (report from vulnerability) 

# [1.4.0]
## Enhancements
- [Issue #16](https://gitlab.com/axual/public/axual-client-dotnet/-/issues/16) - Add Schema Registry PEM support for client Authentication
- [Issue #15](https://gitlab.com/axual/public/axual-client-dotnet/-/issues/15) - Update dependencies libraries - `Confluent.Kafka`, `Confluent.SchemaRegistry` to [1.7.0](https://github.com/confluentinc/confluent-kafka-dotnet/releases/tag/v1.7.0)
## Fixes
- [Issue #17](https://gitlab.com/axual/public/axual-client-dotnet/-/issues/17) - Client authentication fails when using PEM file location
- [Issue #11](https://gitlab.com/axual/public/axual-client-dotnet/-/issues/11) - Prevent .NET client from crashing in case of unexpected response from Discovery-API

# [1.3.2]
## Fixes
- [Issue #12](https://gitlab.com/axual/public/axual-client-dotnet/-/issues/12) - Value deserializer may not be specified more than once. when `EnableValueHeaders` is enabled

# [1.3.1]
## Fixes
- [Issue #10](https://gitlab.com/axual/public/axual-client-dotnet/-/issues/10) - Retrying produce results in re-resolving topic names
- [Issue #8](https://gitlab.com/axual/public/axual-client-dotnet/-/issues/8) - Topic resolver failure when using Produce with callback

# [1.3.0]
## Enhancements
- Added support for Kafka transactions
## Fixes
- [Issue #9](https://gitlab.com/axual/public/axual-client-dotnet/-/issues/9) - ValueDeserializer may not be specified more than once

[master]: https://gitlab.com/axual/public/axual-client-dotnet/-/compare/1.5.1...master
[1.5.1]: https://gitlab.com/axual/public/axual-client-dotnet/-/compare/1.5.0...1.5.1
[1.5.0]: https://gitlab.com/axual/public/axual-client-dotnet/-/compare/1.4.3...1.5.0
[1.4.3]: https://gitlab.com/axual/public/axual-client-dotnet/-/compare/1.4.2...1.4.3
[1.4.2]: https://gitlab.com/axual/public/axual-client-dotnet/-/compare/1.4.1...1.4.2
[1.4.1]: https://gitlab.com/axual/public/axual-client-dotnet/-/compare/1.4.0...1.4.1
[1.4.0]: https://gitlab.com/axual/public/axual-client-dotnet/-/compare/1.3.2...1.4.0
[1.3.2]: https://gitlab.com/axual/public/axual-client-dotnet/-/compare/1.3.1...1.3.2
[1.3.1]: https://gitlab.com/axual/public/axual-client-dotnet/-/compare/1.3.0...1.3.1
[1.3.0]: https://gitlab.com/axual/public/axual-client-dotnet/-/compare/1.2.0...1.3.0
