.NET Avro Schema Parser
=====================================================
## Intro
The tool provides a .NET Avro Schema version to upload to the Axual Self-Service

## Requirement
- .NET Standard 2.0 - https://dotnet.microsoft.com/platform/dotnet-standard

```console
brew cast install dotnet-sdk
```
## How to Build

```console
dotnet build AvroSchemaParser.csproj 
```

## How to Run
Run the following command

```console
dotnet AvroSchemaParser.dll [avsc FILE PATH] > [OUTPUT PATH].avsc
```
For example:
```console
dotnet AvroSchemaParser.dll io.axual.client.example.schema.ApplicationLogEvent.avsc > io.axual.client.example.schema.ApplicationLogEvent.dotnet.avsc
```

<details>
  <summary>Source File: io.axual.client.example.schema.ApplicationLogEvent.avsc </summary>

```json
{
  "type": "record",
  "name": "ApplicationLogEvent",
  "namespace": "io.axual.client.example.schema",
  "doc": "Generic application log event",
  "fields": [
    {
      "name": "timestamp",
      "type": "long",
      "doc": "Timestamp of the event"
    },
    {
      "name": "source",
      "type": {
        "type": "record",
        "name": "Application",
        "doc": "Identification of an application",
        "fields": [
          {
            "name": "name",
            "type": "string",
            "doc": "The name of the application"
          },
          {
            "name": "version",
            "type": [
              "null",
              "string"
            ],
            "doc": "(Optional) The application version",
            "default": null
          },
          {
            "name": "owner",
            "type": [
              "null",
              "string"
            ],
            "doc": "The owner of the application",
            "default": null
          }
        ]
      },
      "doc": "The application that sent the event"
    },
    {
      "name": "context",
      "type": {
        "type": "map",
        "values": "string"
      },
      "doc": "The application context, contains application-specific key-value pairs"
    },
    {
      "name": "level",
      "type": {
        "type": "enum",
        "name": "ApplicationLogLevel",
        "doc": "The level of the log message",
        "symbols": [
          "DEBUG",
          "INFO",
          "WARN",
          "ERROR",
          "FATAL"
        ]
      },
      "doc": "The log level, being either DEBUG, INFO, WARN or ERROR"
    },
    {
      "name": "message",
      "type": "string",
      "doc": "The log message"
    }
  ]
}
```
</details>

<details>
  <summary>Output File: io.axual.client.example.schema.ApplicationLogEvent.dotnet.avsc</summary>

```json
{
  "type": "record",
  "name": "ApplicationLogEvent",
  "namespace": "io.axual.client.example.schema",
  "fields": [
    {
      "name": "timestamp",
      "doc": "Timestamp of the event",
      "type": "long"
    },
    {
      "name": "source",
      "doc": "The application that sent the event",
      "type": {
        "type": "record",
        "name": "Application",
        "namespace": "io.axual.client.example.schema",
        "fields": [
          {
            "name": "name",
            "doc": "The name of the application",
            "type": "string"
          },
          {
            "name": "version",
            "doc": "(Optional) The application version",
            "default": null,
            "type": [
              "null",
              "string"
            ]
          },
          {
            "name": "owner",
            "doc": "The owner of the application",
            "default": null,
            "type": [
              "null",
              "string"
            ]
          }
        ]
      }
    },
    {
      "name": "context",
      "doc": "The application context, contains application-specific key-value pairs",
      "type": {
        "type": "map",
        "values": "string"
      }
    },
    {
      "name": "level",
      "doc": "The log level, being either DEBUG, INFO, WARN or ERROR",
      "type": {
        "type": "enum",
        "name": "ApplicationLogLevel",
        "namespace": "io.axual.client.example.schema",
        "symbols": [
          "DEBUG",
          "INFO",
          "WARN",
          "ERROR",
          "FATAL"
        ]
      }
    },
    {
      "name": "message",
      "doc": "The log message",
      "type": "string"
    }
  ]
}
```
</details>
