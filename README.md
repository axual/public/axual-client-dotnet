Axual's .NET Client for Apache Kafka
=====================================================

[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=axual-client-dotnet&metric=coverage&token=67aba7f77276b4262c927a5ee899ed2536076b7d)](https://sonarcloud.io/dashboard?id=axual-client-dotnet)
[![Maintainability Rating](https://sonarcloud.io/api/project_badges/measure?project=axual-client-dotnet&metric=sqale_rating&token=67aba7f77276b4262c927a5ee899ed2536076b7d)](https://sonarcloud.io/dashboard?id=axual-client-dotnet)
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=axual-client-dotnet&metric=alert_status&token=67aba7f77276b4262c927a5ee899ed2536076b7d)](https://sonarcloud.io/dashboard?id=axual-client-dotnet)
[![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)

**Axual.Kafka** is Axual's .NET client for [Apache Kafka](http://kafka.apache.org/) and the [Axual Platform](https://docs.cloud.axual.io/). The client is using the [Confluent's .NET Client](https://github.com/confluentinc/confluent-kafka-dotnet).

## EOL notice
Please note that Axual Client will reach EOL by the end of Q3 2024.
Read the full blog post here: https://axual.com/update-on-support-of-axual-client-libraries

## UML Diagram

```plantuml
!include <cloudinsight/kafka>

'
' copied inline here from the plantuml/ folder.
'

frame "Producer app" as producer {
	component "Axual client" as prodclient
}
frame "Consumer app" as consumer {
	component "Axual client" as consclient
}

frame "Axual" as axual {

	component "Discovery API" as disco
	component "Schema registry" as registry
	queue "<$kafka>" as kafka1
	queue "<$kafka>" as kafka2

	disco -[hidden]-> registry
	registry -[hidden]-> kafka1
	kafka1 --> kafka2
	kafka2 --> kafka1

}

prodclient <-> disco
prodclient <-> registry
prodclient <-> kafka1
registry <-> consclient
disco <-> consclient
consclient <-> kafka2
```

## Prerequisites

* .NET Standard 1.3 / 2.0 - https://dotnet.microsoft.com/platform/dotnet-standard
* .Net Core 5.0+ must be used for client ssl support. (For Axual Discovery and Service Registry)
* Your favorite IDE

## Install

To install `Axual.Kafka.Proxy` from your IDE, search for `Axual.Kafka.Proxy` in the NuGet Package Manager, or run the following command in the Package Manager Console.

```
Install-Package Axual.Kafka.Proxy -Version 1.5.0
```

To add a reference to a dotnet core project, execute the following at the command line:

```
dotnet add package -v 1.5.0 Axual.Kafka.Proxy
```

**Note:** 
`Axual.Proxy.Kafka` depends on `Confluent.Kafka` and `librdkafka.redist` package which provides a number of different builds of `librdkafka` that are compatible with [common platforms](https://github.com/edenhill/librdkafka/wiki/librdkafka.redist-NuGet-package-runtime-libraries). 

If you are on one of these platforms this will all work seamlessly (and you don't need to explicitly reference `librdkafka.redist`). If you are on a different platform, you may need to [build librdkafka](https://github.com/edenhill/librdkafka#building) manually (or acquire it via other means) and load it using the [Library.Load](https://docs.confluent.io/current/clients/confluent-kafka-dotnet/api/Confluent.Kafka.Library.html#Confluent_Kafka_Library_Load_System_String_) method.

## Tests
### Unit Tests
The `Axual.Kafka.Tests` project contains all the unit tests for solution. To execute, run the following code

```
cd Axual.Kafka.Tests
dotnet test
```

## Project Overview
The solution is divided into projects based functionality in order to be included separately depending
on the use case.

The projects are as follows:

 * [`Axual.Kafka.Proxy`](Axual.Kafka.Proxy) <br/>
  .NET Kafka proxy which enables interaction with the Axual platform
  
 * [`Axual.Kafka.Tests`](Axual.Kafka.Tests) <br/>
  Unit test project for Axual.Kafka.Proxy

 * [`AvroSchemaParser`](AvroSchemaParser) <br/>
 Tool to run your .avsc file before uploading the file in [Self-Service](https://docs.cloud.axual.io/axual/2020.3/self-service/schema-management.html#upload-schema)


 * [`Axual.SchemaRegistry.Serdes.Avro`](Axual.SchemaRegistry.Serdes.Avro) <br/>
 Provides a serializer and deserializer for working with Avro serialized data.

## Usage
Documentation on how to use the client can be found in the 
[Axual Documentation](https://docs.axual.io/dotnet-client/1.5/index.html).

## Examples
Simple use cases using the client code can be found in the 
[Axual Client .NET Examples](https://gitlab.com/axual-public/axual-client-dotnet-examples).

## Contributing 

Axual is interested in building the community; we would welcome any thoughts or [patches](https://gitlab.com/axual/public/axual-client-dotnet/-/issues).
You can reach us [here](https://axual.com/contact/).

See [CONTRIBUTING.MD](CONTRIBUTING.md).

## License
Axual .NET Client is licensed under the [Apache License, Version 2.0](https://www.apache.org/licenses/LICENSE-2.0.txt).
