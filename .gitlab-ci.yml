image: $AXUAL_REGISTRY_URL/axual/build-ci-dotnet_6:1.2.0

stages:
  - validate
  - build
  - test
  - quality
  - pack
  - push

cache:
  key: $CI_PROJECT_ID
  paths:
    - .nuget_packages
  policy: pull-push

default:
  tags:
    - aws
  before_script:
    - export PATH="${PATH}:${HOME}/.dotnet/tools"
    - export NUGET_PACKAGES="${PWD}/.nuget_packages"
    - dotnet nuget update source axual -s $NUGET_BUILD_URL -u $NUGET_REPO_USER -p $NUGET_REPO_PASS --store-password-in-clear-text
    - |
      if [[ -z "${CI_COMMIT_TAG}" ]]; then
        export VERSION="$(cat 'Axual.Kafka.Proxy/Resources/axual_client_version')"
        export VERSION_SUFFIX_ARGUMENT="/p:VersionSuffix=${VERSION}-${CI_COMMIT_REF_SLUG}"
        export PACKAGE_VERSION="${VERSION}-${CI_COMMIT_REF_SLUG}"
      else
        if [[ "${CI_COMMIT_TAG}" =~ -RC[0-9]+$ ]]; then
          export VERSION="$(echo "${CI_COMMIT_TAG}" | sed -E 's/-RC[0-9]+//')"
          export RC="RC$(echo "${CI_COMMIT_TAG}" | sed -E 's/.*-RC//')"
          export VERSION_SUFFIX_ARGUMENT="/p:VersionSuffix=${VERSION}-${RC}"
          export PACKAGE_VERSION="${VERSION}-${RC}"
        else
          export VERSION="${CI_COMMIT_TAG}"
          export VERSION_SUFFIX_ARGUMENT=
          export PACKAGE_VERSION="${CI_COMMIT_TAG}"
        fi
      fi
      rm -rf 'Axual.Kafka.Proxy/Resources/axual_client_version'
      echo "${PACKAGE_VERSION}" > 'Axual.Kafka.Proxy/Resources/axual_client_version'
      echo "Releasing version $(cat Axual.Kafka.Proxy/Resources/axual_client_version)"

.base_feature:
  except:
    - tags
    - ^master.*
    - ^release.*

.base_releasable:
  only:
    - tags
    - ^master.*
    - ^release.*

.base_build_project:
  stage: build
  script:
    - dotnet build -c ${CONFIGURATION} "/p:Version=${VERSION}" "${VERSION_SUFFIX_ARGUMENT}" "/p:PackageVersion=${PACKAGE_VERSION}" Axual.Kafka.sln

.base_test_project:
  stage: test
  script:
    - dotnet test -c ${CONFIGURATION} --logger "trx;LogFileName=TestResults.trx" /p:CollectCoverage=true /p:CoverletOutputFormat="opencover" /p:CoverletOutput=../TestResults/coverage /p:MergeWith=../TestResults/coverage Axual.Kafka.sln

.base_scan_project:
  stage: quality
  variables:
    PROJECT_NAME: "Axual Client Dotnet"
  script:
    - dotnet sonarscanner begin /k:axual-client-dotnet "/n:${PROJECT_NAME}" /o:axual "/d:sonar.host.url=${SONAR_HOST_URL}" "/d:sonar.login=${SONAR_TOKEN}" "/v:${PACKAGE_VERSION}"  /d:sonar.cs.opencover.reportsPaths=TestResults/coverage.opencover.xml
    - dotnet build
    - dotnet sonarscanner end /d:sonar.login=${SONAR_TOKEN}

.base_pack_project:
  stage: pack
  script:
    - dotnet pack -c ${CONFIGURATION} "/p:Version=${VERSION}" "${VERSION_SUFFIX_ARGUMENT}" "/p:PackageVersion=${PACKAGE_VERSION}" Axual.Kafka.sln
  artifacts:
    expire_in: 4 hours
    paths:
      - ./${CONFIGURATION}/*.nupkg

.base_push_project:
  stage: push
  variables:
    NUGET_TARGET_REPO: ""
    NUGET_TARGET_API_KEY: ""
  script:
    - |
      for PKG in ${CONFIGURATION}/*.nupkg; do
        dotnet nuget push "${PKG}" -k $NUGET_TARGET_API_KEY -s $NUGET_TARGET_REPO
      done

## Start of Job Definition

build_project_feature:
  extends:
    - .base_feature
    - .base_build_project
  variables:
    CONFIGURATION: Debug
  artifacts:
    expire_in: 4 hours
    paths:
      - ./Debug
      - ./*/bin
      - ./*/obj

build_project_releasable:
  extends:
    - .base_releasable
    - .base_build_project
  variables:
    CONFIGURATION: Release
  artifacts:
    expire_in: 4 hours
    paths:
      - ./Release
      - ./*/bin
      - ./*/obj

test_project_feature:
  extends:
    - .base_feature
    - .base_test_project
  variables:
    CONFIGURATION: Debug
  allow_failure: true
  artifacts:
    expire_in: 4 hours
    paths:
      - ./Debug
      - ./*/bin
      - ./*/obj
      - ./*/TestResults
      - ./TestResults

test_project_releaseable:
  extends:
    - .base_releasable
    - .base_test_project
  variables:
    CONFIGURATION: Release
  allow_failure: false
  artifacts:
    expire_in: 4 hours
    paths:
      - ./Release
      - ./*/bin
      - ./*/obj
      - ./*/TestResults
      - ./TestResults

scan_project_feature:
  extends:
    - .base_feature
    - .base_scan_project
  allow_failure: true

scan_project_releasable:
  extends:
    - .base_releasable
    - .base_scan_project
  allow_failure: false

pack_project_feature:
  extends:
    - .base_feature
    - .base_pack_project
  variables:
    CONFIGURATION: Debug
  allow_failure: true
  artifacts:
    expire_in: 4 hours
    paths:
      - ./Debug

pack_project_releaseable:
  extends:
    - .base_releasable
    - .base_pack_project
  variables:
    CONFIGURATION: Release
  allow_failure: false
  artifacts:
    expire_in: 4 hours
    paths:
      - ./Release

push_project_feature:
  extends:
    - .base_push_project
  except:
    - tags
  variables:
    CONFIGURATION: Debug
    NUGET_TARGET_REPO: "${NUGET_SNAPSHOTS_URL}"
    NUGET_TARGET_API_KEY: "${NUGET_API_KEY}"
  artifacts:
    expire_in: 2 days
    paths:
      - ./Debug/*.nupkg

push_project_releasable:
  extends:
    - .base_push_project
  only:
    - tags
  variables:
    CONFIGURATION: Release
    NUGET_TARGET_REPO: "${NUGET_PUBLIC_RELEASES_URL}"
    NUGET_TARGET_API_KEY: "${NUGET_PUBLIC_API_KEY}"
  artifacts:
    expire_in: 2 days
    paths:
      - ./Release/*.nupkg
